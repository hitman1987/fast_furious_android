package com.hotstock.app.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.hotstock.R;
import com.hotstock.app.AppController;
import com.hotstock.app.adapter.WatchlistConsensusAdapter;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.Consensus;
import com.hotstock.app.utils.Enums;
import com.hotstock.app.utils.IStockModel;
import com.hotstock.app.utils.PreferenceUtils;
import com.hotstock.app.views.MainActivity;

public class WatchlistConsensusFragment extends Fragment{
	private View rootView;
	private ProgressDialog pDialog;
	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	
	private ListView listConsensus;
	private ArrayList<Consensus> mDataConsensus = new ArrayList<Consensus>();
	private WatchlistConsensusAdapter mAdapterConsensus;
	
	private static final int GET_CONSENSUS_SUCCESS = 0;
	private static final int GET_CONSENSUS_FAIL = 1;
	
	private final int limitConsensus = 20;
	private int offsetConsensus = 0;
	private boolean isLoading = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_watchlist_consensus, container, false);
		initView(rootView);
		return rootView;
	}
	
	private void initView(View v){		
		
		if(listConsensus == null){
			listConsensus = (ListView) v.findViewById(R.id.fragment_watchlist_consensus_list);
			listConsensus.setOnScrollListener(new OnScrollListener() {
				
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					
				}
				
				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,
						int visibleItemCount, int totalItemCount) {
					// TODO Auto-generated method stub
					if (totalItemCount == 0 || visibleItemCount == 0) {
						return;
					}
					if (totalItemCount == firstVisibleItem + visibleItemCount && !isLoading) {
						// 排他制御フラグをON
						isLoading = true;
						loadDataConsensus(true);
					}
				}
			});
			listConsensus.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					PreferenceUtils.writeString(AppController.getAppContext(),Enums.PREF_CURRENT_PRICE_CODE, mDataConsensus.get(position).getCode());
					MainActivity.constant.displayView(Enums.STOCKSVIEW);
				}
				
			});
		}
		
		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		loadDataConsensus(false);
	}
	
	private void loadDataConsensus(final boolean isLoadMore) {
		showProgressDialog();
		isLoading = true;
		if (!isLoadMore) {
			offsetConsensus = 0;
		} else {
			offsetConsensus += limitConsensus;
		}
		
		restService.getConsensus(IStockModel.getInstance().getAccessToken(),String.valueOf(limitConsensus), String.valueOf(offsetConsensus), new IStockResponseListener<ArrayList<Consensus>>() {
			
			@Override
			public void onResponse(ArrayList<Consensus> data) {
				hideProgressDialog();
				isLoading = false;
				if (!isLoadMore) {
					mDataConsensus.clear();
				}
				if(data.size()> 0){
					for (Consensus item : data) {
						mDataConsensus.add(item);
					}
				}else{
					isLoading = true;
				}
			
				mHandler.sendEmptyMessage(GET_CONSENSUS_SUCCESS);
			}
			
			@Override
			public void onError(Exception ex) {
				hideProgressDialog();
				ex.printStackTrace();
				isLoading = false;
			}
		});
	}
	
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}
	
	private Handler mHandler =new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case GET_CONSENSUS_SUCCESS:
				hideProgressDialog();
				if(mAdapterConsensus == null){
					mAdapterConsensus = new WatchlistConsensusAdapter(AppController.getAppContext(),mDataConsensus);
					listConsensus.setAdapter(mAdapterConsensus);
				}else{
					mAdapterConsensus.notifyDataSetChanged();
				}								
				break;
				
			case GET_CONSENSUS_FAIL:
				
				break;

			default:
				break;
			}
			return false;
		}
	});
}

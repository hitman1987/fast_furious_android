package com.hotstock.app.model;

public class OverviewTestDto {
	
	public OverviewTestDto(int type, String time, String description) {
		this.type = type;
		this.time = time;
		this.description = description;
	}

	public int type;
	public String time;
	public String description;
}

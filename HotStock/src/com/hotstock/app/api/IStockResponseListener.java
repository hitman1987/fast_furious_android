package com.hotstock.app.api;

public interface IStockResponseListener<T> {
	public void onResponse(T data);
	public void onError(Exception ex);
}

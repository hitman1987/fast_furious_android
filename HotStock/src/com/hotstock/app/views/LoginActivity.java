package com.hotstock.app.views;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hotstock.R;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.DataReceiver;
import com.hotstock.app.entities.StockList;
import com.hotstock.app.notification.MessageReceivingService;
import com.hotstock.app.pushnotification.PushNotificationUtil;
import com.hotstock.app.utils.Enums;
import com.hotstock.app.utils.IStockModel;
import com.hotstock.app.utils.PreferenceUtils;
import com.hotstock.app.utils.StringUtils;
import com.hotstock.app.utils.Utils;

public class LoginActivity extends Activity implements OnClickListener {

	private IStockRestfulService restService = new IStockRestfulServiceImpl();

	private EditText edtUserName;
	private EditText edtPassword;
	private Button btnLogin;
	private Button btnRegister;
	private ImageView imgCheckRemember;
	private ImageView imgUnCheckRemember;
	private ImageView imgFlagVN;
	private ImageView imgFlagEN;
	private Button btnForgot;

	private Context mContext;
	private Intent intentLogin;
	private ProgressDialog pDialog;
	private String listStocks;
	private String device_ID;
	private String userName;
	private String errorMessage = null;

	private static final int LOGIN_SUCCESS = 0;
	private static final int LOGIN_FAIL = 1;
	private static final int LOAD_DATA_STOCK_SUCCESS = 2;
	private static final int LOAD_DATA_STOCK_FAIL = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		initView();
		PushNotificationUtil.getInstance(this).register(this);
		startService(new Intent(this, MessageReceivingService.class));
	}

	private void initView() {
		mContext = this;
		
		errorMessage = mContext.getResources().getString(R.string.activity_login_str_fail);
		
		if (Enums.sharedPreferences == null) {
			Enums.sharedPreferences = Utils.getSharedPreferences(mContext, Context.MODE_PRIVATE);
		}
		
		if (edtUserName == null) {
			edtUserName = (EditText) findViewById(R.id.activity_login_et_username);
		}

		if (edtPassword == null) {
			edtPassword = (EditText) findViewById(R.id.activity_login_et_password);
		}

		if (imgUnCheckRemember == null) {
			imgUnCheckRemember = (ImageView) findViewById(R.id.activity_login_img_unCheck_remember_password);
		}

		if (imgCheckRemember == null) {
			imgCheckRemember = (ImageView) findViewById(R.id.activity_login_img_check_remember_password);
		}

		if (imgFlagVN == null) {
			imgFlagVN = (ImageView) findViewById(R.id.activity_login_img_language_vn);
		}

		if (imgFlagEN == null) {
			imgFlagEN = (ImageView) findViewById(R.id.activity_login_img_language_eng);
		}

		if (btnLogin == null) {
			btnLogin = (Button) findViewById(R.id.activity_login_btn_login);
		}

		if (btnForgot == null) {
			btnForgot = (Button) findViewById(R.id.activity_login_btn_forgot_user_pass);
		}

		if (btnRegister == null) {
			btnRegister = (Button) findViewById(R.id.activity_login_btn_register_new_account);
		}

		userName = Utils.loadPreferences(Enums.sharedPreferences, Enums.PREF_USERNAME);
		if (userName != null && userName.trim().length() > 0) {
			edtUserName.setText(userName);
			imgCheckRemember.setVisibility(View.VISIBLE);
			imgUnCheckRemember.setVisibility(View.VISIBLE);
		}

		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		loadData();
		listener();
	}

	private void loadData() {
		showProgressDialog();
//		if (!checkDataStock()) {
			restService.getListStock(mContext, new IStockResponseListener<ArrayList<StockList>>() {

				@Override
				public void onResponse(ArrayList<StockList> data) {
					Utils.mDataStockList.addAll(data);
					mHandler.sendEmptyMessage(LOAD_DATA_STOCK_SUCCESS);
				}

				@Override
				public void onError(Exception ex) {
					ex.printStackTrace();
					mHandler.sendEmptyMessage(LOAD_DATA_STOCK_FAIL);
				}
			});
//		} else {
//			Utils.mDataStockList.addAll(convertListStock(listStocks));
//			hideProgressDialog();
//		}
	}

	private ArrayList<StockList> convertListStock(String response) {
		ArrayList<StockList> stockTrades = new ArrayList<StockList>();
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(response);

			PreferenceUtils.writeString(mContext, Enums.PREF_LIST_STOCK, response);
			JSONArray jsonArray = getJSONArrayObject(jsonObject);
			for (int i = 0; i < jsonArray.length(); i++) {
				StockList stock;
				try {
					stock = new Gson().fromJson(jsonArray.get(i).toString(), StockList.class);
					stockTrades.add(stock);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return stockTrades;
	}

	private JSONArray getJSONArrayObject(JSONObject jsonObject) {
		JSONArray jsonArray = null;
		try {
			jsonArray = jsonObject.getJSONArray("data");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonArray;
	}

	private boolean checkDataStock() {
		listStocks = PreferenceUtils.getString(getApplicationContext(), Enums.PREF_LIST_STOCK, null);
		if (listStocks != null) {
			return true;
		} else {
			return false;
		}
	}

	private void listener() {
		imgCheckRemember.setOnClickListener(this);
		imgUnCheckRemember.setOnClickListener(this);
		imgFlagVN.setOnClickListener(this);
		imgFlagEN.setOnClickListener(this);
		btnLogin.setOnClickListener(this);
		btnForgot.setOnClickListener(this);
		btnRegister.setOnClickListener(this);
	}

	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}

	private void actLogin(String userName, String passWord, String deviceID, String idPushNotification) {
		showProgressDialog();
		restService.login(userName, passWord, deviceID, idPushNotification, new IStockResponseListener<String>() {

			@Override
			public void onResponse(String data) {
				hideProgressDialog();
				try {
					JSONObject jsonObject = new JSONObject(data);
					DataReceiver dataReceiver = new Gson().fromJson((new JSONObject(jsonObject.getString("meta"))).toString(), DataReceiver.class);
						if(dataReceiver.code.equals(Enums.CODE_RETURN_SUCCESS)){
						JSONObject jsonObject2 = new JSONObject(jsonObject.getString("data"));
						String token = jsonObject2.optString("token", null);
							if(token != null && token.length() > 0){
								JSONObject jsonObject3 = new JSONObject(jsonObject2.getString("user"));
								IStockModel.getInstance().setPassWord(jsonObject3.optString("password", StringUtils.EMPTY_STR));
								IStockModel.getInstance().setFullName(jsonObject3.optString("fullname", StringUtils.EMPTY_STR));
								IStockModel.getInstance().setPhoneNumber(jsonObject3.optString("phone", StringUtils.EMPTY_STR));
								IStockModel.getInstance().setEmail(jsonObject3.optString("email", StringUtils.EMPTY_STR));
								IStockModel.getInstance().setUserName(jsonObject3.optString("username", StringUtils.EMPTY_STR));
								IStockModel.getInstance().setAccessToken(token);
								mHandler.sendEmptyMessage(LOGIN_SUCCESS);
							}else{
								hideProgressDialog();
								mHandler.sendEmptyMessage(LOGIN_FAIL);
							}
						}else{
							hideProgressDialog();
							errorMessage = dataReceiver.error;
							mHandler.sendEmptyMessage(LOGIN_FAIL);
						}
					}
				catch (JSONException e) {
					hideProgressDialog();
					mHandler.sendEmptyMessage(LOGIN_FAIL);
					e.printStackTrace();
				}	
			}

			@Override
			public void onError(Exception ex) {
				mHandler.sendEmptyMessage(LOGIN_FAIL);
				hideProgressDialog();
				ex.printStackTrace();
			}
		});
	}

	private Handler mHandler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case LOGIN_SUCCESS:
				if (imgCheckRemember.getVisibility() == View.VISIBLE) {
					Utils.saveStringPreferences(Enums.PREF_USERNAME, edtUserName.getText().toString().trim(), Enums.sharedPreferences);
				}
				intentLogin = new Intent(mContext, MainActivity.class);
				startActivity(intentLogin);
				finish();
				break;

			case LOGIN_FAIL:
				Toast.makeText(mContext, "Login failed , please try again.", Toast.LENGTH_SHORT).show();
				break;

			case LOAD_DATA_STOCK_SUCCESS:
				hideProgressDialog();
				break;

			case LOAD_DATA_STOCK_FAIL:
				hideProgressDialog();
				break;

			default:
				break;
			}
			return false;
		}
	});

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.activity_login_btn_login:
			final String username = edtUserName.getText().toString();
			final String password = edtPassword.getText().toString();
			String deviceID = Utils.loadPreferences(Enums.sharedPreferences, Enums.PREF_DEVICE_ID);
			String idNotification = Utils.loadPreferences(Enums.sharedPreferences, Enums.PREF_NOTIFICATION);

			if (username.isEmpty()) {
				Toast.makeText(mContext, "Please enter your username", Toast.LENGTH_SHORT).show();
				return;
			}

			if (password.isEmpty()) {
				Toast.makeText(mContext, "Please enter your password", Toast.LENGTH_SHORT).show();
				return;
			}

			if (deviceID == null) {
				deviceID = Utils.getUniquePsuedoID();
				Utils.saveStringPreferences(Enums.PREF_DEVICE_ID, deviceID, Enums.sharedPreferences);
			}

			if (idNotification == null) {
				idNotification = Utils.loadPreferences(Enums.sharedPreferences, Enums.PREF_NOTIFICATION);
			}
			
//			IStockModel.getInstance().setAccessToken("cd7681043146e2e4061cc3418f5cb4de");
			actLogin(username, password, deviceID, idNotification);
//			mHandler.sendEmptyMessage(LOGIN_SUCCESS);
			break;

		case R.id.activity_login_btn_forgot_user_pass:

			break;

		case R.id.activity_login_img_unCheck_remember_password:
			imgCheckRemember.setVisibility(View.VISIBLE);
			imgUnCheckRemember.setVisibility(View.GONE);
			break;

		case R.id.activity_login_img_check_remember_password:
			imgCheckRemember.setVisibility(View.GONE);
			imgUnCheckRemember.setVisibility(View.VISIBLE);
			break;

		case R.id.activity_login_img_language_eng:
			imgFlagEN.setVisibility(View.GONE);
			imgFlagVN.setVisibility(View.VISIBLE);
			break;

		case R.id.activity_login_img_language_vn:
			imgFlagVN.setVisibility(View.GONE);
			imgFlagEN.setVisibility(View.VISIBLE);
			break;

		case R.id.activity_login_btn_register_new_account:
			intentLogin = new Intent(mContext, ActivationActivity.class);
			startActivity(intentLogin);
			break;

		default:
			break;
		}
	}
}

package com.hotstock.app.entities;

public class Portfolio {
	private int id;
	private String code;
	private double numberofshares;
	private double market_price;
	private double purchase_price;
	private double percentage_change;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public double getNumberofshares() {
		return numberofshares;
	}
	public void setNumberofshares(double numberofshares) {
		this.numberofshares = numberofshares;
	}
	public double getMarket_price() {
		return market_price;
	}
	public void setMarket_price(double market_price) {
		this.market_price = market_price;
	}
	public double getPurchase_price() {
		return purchase_price;
	}
	public void setPurchase_price(double purchase_price) {
		this.purchase_price = purchase_price;
	}
	public double getPercentage_change() {
		return percentage_change;
	}
	public void setPercentage_change(double percentage_change) {
		this.percentage_change = percentage_change;
	}
}

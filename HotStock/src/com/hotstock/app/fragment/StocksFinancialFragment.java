package com.hotstock.app.fragment;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hotstock.R;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.StockFinancialItems;
import com.hotstock.app.utils.Enums;
import com.hotstock.app.utils.Utils;

public class StocksFinancialFragment extends Fragment {

	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	
	//layout Income
	private LinearLayout layoutIncomeNetSale;
	private LinearLayout layoutIncomeGrossProfit;
	private LinearLayout layoutIncomeTotalExpenses;
	private LinearLayout layoutIncomeProfitBeforeTax;
	private LinearLayout layoutIncomeNetProfitAfterTax;
	private LinearLayout layoutIncomeDirectInsurance;
	private LinearLayout layoutIncomeRevenuesInsuranceActivities;
	private LinearLayout layoutIncomeGrossProfitInsuranceActivities;
	private LinearLayout layoutIncomeFinancialIncome;
	private LinearLayout layoutIncomeTotalRevenues;
	private LinearLayout layoutIncomeOperatingExpenses;
	private LinearLayout layoutIncomeAdministrativeExpenses;
	
	//layoutAssets
	private LinearLayout layoutAssetsCurrentAssets;
	private LinearLayout layoutAssetsLongTermAssets;
	private LinearLayout layoutAssetsCurrentLiabilities;
	private LinearLayout layoutAssetsLongTermDebts;
	private LinearLayout layoutAssetsOwnerEquity;
	private LinearLayout layoutAssetsTotalAssets;
	private LinearLayout layoutAssetsCashQuivalents;
	private LinearLayout layoutShortTermInvestment;
	private LinearLayout layoutAssetsShortTermReceivables;
	private LinearLayout layoutAssetsProvisionDoubtfulDebts;
	private LinearLayout layoutAssetsFixedAssets;
	private LinearLayout layoutAssetsUnderwritingReserves;
	private LinearLayout layoutAssetsProvisionDiminution;
	private LinearLayout layoutAssetsLongTermInvestments;
	private LinearLayout layoutAssetsLongTermLiabilities;

	private Button btnQuarter;
	private Button btnYear;
	private TextView tv1;
	private TextView tv2;
	private TextView tv3;
	private TextView tv4;
	private TextView tvNSL1;
	private TextView tvNSL2;
	private TextView tvNSL3;
	private TextView tvNSL4;
	private TextView tvPBT1;
	private TextView tvPBT2;
	private TextView tvPBT3;
	private TextView tvPBT4;
	private TextView tvGOP1;
	private TextView tvGOP2;
	private TextView tvGOP3;
	private TextView tvGOP4;
	private TextView tvTOTAL_EXPENSE1;
	private TextView tvTOTAL_EXPENSE2;
	private TextView tvTOTAL_EXPENSE3;
	private TextView tvTOTAL_EXPENSE4;
	private TextView tvNIAT1;
	private TextView tvNIAT2;
	private TextView tvNIAT3;
	private TextView tvNIAT4;
	private TextView tvPRINS1;
	private TextView tvPRINS2;
	private TextView tvPRINS3;
	private TextView tvPRINS4;
	private TextView tvNETREVINS1;
	private TextView tvNETREVINS2;
	private TextView tvNETREVINS3;
	private TextView tvNETREVINS4;
	private TextView tvGOPINS1;
	private TextView tvGOPINS2;
	private TextView tvGOPINS3;
	private TextView tvGOPINS4;
	private TextView tvFNINC1;
	private TextView tvFNINC2;
	private TextView tvFNINC3;
	private TextView tvFNINC4;
	private TextView tvTOTREV1;
	private TextView tvTOTREV2;
	private TextView tvTOTREV3;
	private TextView tvTOTREV4;
	private TextView tvOPREXP1;
	private TextView tvOPREXP2;
	private TextView tvOPREXP3;
	private TextView tvOPREXP4;
	private TextView tvGENADMEX1;
	private TextView tvGENADMEX2;
	private TextView tvGENADMEX3;
	private TextView tvGENADMEX4;
	private TextView tvCA1;
	private TextView tvCA2;
	private TextView tvCA3;
	private TextView tvCA4;
	private TextView tvLT_ASSET1;
	private TextView tvLT_ASSET2;
	private TextView tvLT_ASSET3;
	private TextView tvLT_ASSET4;
	private TextView tvCURLIB1;
	private TextView tvCURLIB2;
	private TextView tvCURLIB3;
	private TextView tvCURLIB4;
	private TextView tvLT_DEBT1;
	private TextView tvLT_DEBT2;
	private TextView tvLT_DEBT3;
	private TextView tvLT_DEBT4;
	private TextView tvOWNEQ1;
	private TextView tvOWNEQ2;
	private TextView tvOWNEQ3;
	private TextView tvOWNEQ4;
	private TextView tvTA1;
	private TextView tvTA2;
	private TextView tvTA3;
	private TextView tvTA4;
	private TextView tvCACE1;
	private TextView tvCACE2;
	private TextView tvCACE3;
	private TextView tvCACE4;
	private TextView tvSHINV1;
	private TextView tvSHINV2;
	private TextView tvSHINV3;
	private TextView tvSHINV4;
	private TextView tvSHREC1;
	private TextView tvSHREC2;
	private TextView tvSHREC3;
	private TextView tvSHREC4;
	private TextView tvPRDEBT1;
	private TextView tvPRDEBT2;
	private TextView tvPRDEBT3;
	private TextView tvPRDEBT4;
	private TextView tvFXASST1;
	private TextView tvFXASST2;
	private TextView tvFXASST3;
	private TextView tvFXASST4;
	private TextView tvUNRESV1;
	private TextView tvUNRESV2;
	private TextView tvUNRESV3;
	private TextView tvUNRESV4;
	private TextView tvPRDIMINV1;
	private TextView tvPRDIMINV2;
	private TextView tvPRDIMINV3;
	private TextView tvPRDIMINV4;
	private TextView tvLONGINV1;
	private TextView tvLONGINV2;
	private TextView tvLONGINV3;
	private TextView tvLONGINV4;
	private TextView tvLIABILITY1;
	private TextView tvLIABILITY2;
	private TextView tvLIABILITY3;
	private TextView tvLIABILITY4;

	private String stockCode;
	private ProgressDialog pDialog;
	
	private static final int SHOW_FINANCIAL_QUARTER_FAIL = 0;

	public StocksFinancialFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_stocks_financial, container, false);
		stockCode = getArguments().getString(Enums.PREF_STOCK_CODE, null);
		if (stockCode == null) {
			stockCode = "VNM";
		}
		initView(rootView);
		loadData(stockCode);

		return rootView;
	}

	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}

	private void initView(View view) {

		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);

		//---------------layout Income-----------------------------
		layoutIncomeNetSale = (LinearLayout) view.findViewById(R.id.fragment_stocks_financial_layout_net_sell);
		layoutIncomeGrossProfit = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_gross_profit);
		layoutIncomeTotalExpenses = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_total_expenses);
		layoutIncomeProfitBeforeTax = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_profit_before_tax);
		layoutIncomeNetProfitAfterTax = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_profit_after_tax);
		layoutIncomeDirectInsurance = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_direct_insurance);
		layoutIncomeRevenuesInsuranceActivities = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_net_revenues_insurance_activities);
		layoutIncomeGrossProfitInsuranceActivities = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_gross_profit_insurance_activities);
		layoutIncomeFinancialIncome = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_financial_income);
		layoutIncomeTotalRevenues = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_total_revenues);
		layoutIncomeOperatingExpenses = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_operating_expenses);
		layoutIncomeAdministrativeExpenses = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_general_and_administrative_expenses);
		
		//---------------layout Assets-----------------------------		
		
		layoutAssetsCurrentAssets = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_current_assets);
		layoutAssetsLongTermAssets = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_long_term_assets);
		layoutAssetsCurrentLiabilities = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_current_liabilities);
		layoutAssetsLongTermDebts = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_long_term_debts);
		layoutAssetsOwnerEquity = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_owners_equity);
		layoutAssetsTotalAssets = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_total_assets);
		layoutAssetsCashQuivalents = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_cash_quivalents);
		layoutShortTermInvestment = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_short_term_investment);
		layoutAssetsShortTermReceivables = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_short_term_receivables);
		layoutAssetsProvisionDoubtfulDebts = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_provision_doubtful_debts);
		layoutAssetsFixedAssets = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_fixed_assets);
		layoutAssetsUnderwritingReserves = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_underwriting_reserves);
		layoutAssetsProvisionDiminution = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_provision_diminution);
		layoutAssetsLongTermInvestments = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_long_term_investments);
		layoutAssetsLongTermLiabilities = (LinearLayout) view.findViewById(R.id.fragment_stock_financial_layout_liabilities);
		
		btnQuarter = (Button) view.findViewById(R.id.fragment_stock_financial_btn_quarter);
		btnYear = (Button) view.findViewById(R.id.fragment_stock_financial_btn_year);
		tv1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_1);
		tv2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_2);
		tv3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_3);
		tv4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_4);
		tvNSL1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_nsl_1);
		tvNSL2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_nsl_2);
		tvNSL3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_nsl_3);
		tvNSL4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_nsl_4);
		tvPBT1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_pbt_1);
		tvPBT2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_pbt_2);
		tvPBT3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_pbt_3);
		tvPBT4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_pbt_4);
		tvGOP1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_gop_1);
		tvGOP2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_gop_2);
		tvGOP3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_gop_3);
		tvGOP4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_gop_4);
		tvTOTAL_EXPENSE1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_total_expenses_1);
		tvTOTAL_EXPENSE2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_total_expenses_2);
		tvTOTAL_EXPENSE3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_total_expenses_3);
		tvTOTAL_EXPENSE4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_total_expenses_4);
		tvNIAT1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_niat_1);
		tvNIAT2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_niat_2);
		tvNIAT3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_niat_3);
		tvNIAT4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_niat_4);
		tvPRINS1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRINS_1);
		tvPRINS2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRINS_2);
		tvPRINS3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRINS_3);
		tvPRINS4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRINS_4);
		tvNETREVINS1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_NETREVINS_1);
		tvNETREVINS2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_NETREVINS_2);
		tvNETREVINS3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_NETREVINS_3);
		tvNETREVINS4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_NETREVINS_4);
		tvGOPINS1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_GOPINS_1);
		tvGOPINS2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_GOPINS_2);
		tvGOPINS3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_GOPINS_3);
		tvGOPINS4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_GOPINS_4);
		tvFNINC1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_FNINC_1);
		tvFNINC2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_FNINC_2);
		tvFNINC3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_FNINC_3);
		tvFNINC4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_FNINC_4);
		tvTOTREV1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_TOTREV_1);
		tvTOTREV2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_TOTREV_2);
		tvTOTREV3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_TOTREV_3);
		tvTOTREV4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_TOTREV_4);
		tvOPREXP1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_OPREXP_1);
		tvOPREXP2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_OPREXP_2);
		tvOPREXP3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_OPREXP_3);
		tvOPREXP4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_OPREXP_4);
		tvGENADMEX1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_GENADMEX_1);
		tvGENADMEX2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_GENADMEX_2);
		tvGENADMEX3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_GENADMEX_3);
		tvGENADMEX4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_GENADMEX_4);
		tvCA1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_ca_1);
		tvCA2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_ca_2);
		tvCA3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_ca_3);
		tvCA4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_ca_4);
		tvLT_ASSET1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LT_ASSET_1);
		tvLT_ASSET2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LT_ASSET_2);
		tvLT_ASSET3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LT_ASSET_3);
		tvLT_ASSET4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LT_ASSET_4);
		tvCURLIB1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_CURLIB_1);
		tvCURLIB2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_CURLIB_2);
		tvCURLIB3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_CURLIB_3);
		tvCURLIB4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_CURLIB_4);
		tvLT_DEBT1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LT_DEBT_1);
		tvLT_DEBT2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LT_DEBT_2);
		tvLT_DEBT3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LT_DEBT_3);
		tvLT_DEBT4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LT_DEBT_4);
		tvOWNEQ1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_OWNEQ_1);
		tvOWNEQ2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_OWNEQ_2);
		tvOWNEQ3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_OWNEQ_3);
		tvOWNEQ4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_OWNEQ_4);
		tvTA1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_ta_1);
		tvTA2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_ta_2);
		tvTA3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_ta_3);
		tvTA4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_ta_4);
		tvCACE1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_CACE_1);
		tvCACE2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_CACE_2);
		tvCACE3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_CACE_3);
		tvCACE4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_CACE_4);
		tvSHINV1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_SHINV_1);
		tvSHINV2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_SHINV_2);
		tvSHINV3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_SHINV_3);
		tvSHINV4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_SHINV_4);
		tvSHREC1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_SHREC_1);
		tvSHREC2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_SHREC_2);
		tvSHREC3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_SHREC_3);
		tvSHREC4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_SHREC_4);
		tvPRDEBT1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDEBT_1);
		tvPRDEBT2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDEBT_2);
		tvPRDEBT3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDEBT_3);
		tvPRDEBT4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDEBT_4);
		tvFXASST1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_FXASST_1);
		tvFXASST2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_FXASST_2);
		tvFXASST3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_FXASST_3);
		tvFXASST4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_FXASST_4);
		tvUNRESV1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_UNRESV_1);
		tvUNRESV2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_UNRESV_2);
		tvUNRESV3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_UNRESV_3);
		tvUNRESV4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_UNRESV_4);
		tvPRDIMINV1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDIMINV_1);
		tvPRDIMINV2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDIMINV_2);
		tvPRDIMINV3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDIMINV_3);
		tvPRDIMINV4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDIMINV_4);
		tvPRDEBT1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDEBT_1);
		tvPRDEBT2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDEBT_2);
		tvPRDEBT3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDEBT_3);
		tvPRDEBT4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_PRDEBT_4);
		tvLONGINV1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LONGINV_1);
		tvLONGINV2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LONGINV_2);
		tvLONGINV3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LONGINV_3);
		tvLONGINV4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LONGINV_4);
		tvLIABILITY1 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LIABILITY_1);
		tvLIABILITY2 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LIABILITY_2);
		tvLIABILITY3 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LIABILITY_3);
		tvLIABILITY4 = (TextView) view.findViewById(R.id.fragment_stock_financial_tv_LIABILITY_4);

		onTabPress(0);

		OnStockFinancialFragmentClickListener onClickListener = new OnStockFinancialFragmentClickListener();
		btnQuarter.setOnClickListener(onClickListener);
		btnYear.setOnClickListener(onClickListener);
	}

	private void loadData(String stockCode) {
	}

	private void onTabPress(int index) {
		// Initialize default
		btnQuarter.setTextColor(getActivity().getResources().getColor(R.color.text_color_grey));
		btnYear.setTextColor(getActivity().getResources().getColor(R.color.text_color_grey));

		// Set tab depend on index
		switch (index) {
		case 0:
			showProgressDialog();
			btnQuarter.setTextColor(getActivity().getResources().getColor(R.color.text_color_yellow));

			// Get API for Quarter info
			Calendar c = Calendar.getInstance();
			final int currYear = c.get(Calendar.YEAR);
			ArrayList<Integer> years = new ArrayList<Integer>();
			years.add(currYear - 1);
			years.add(currYear);
			JSONArray jsArray = new JSONArray(years);
			restService.getStockFinancialQuarter(stockCode, jsArray, new IStockResponseListener<String>() {

				@Override
				public void onResponse(String data) {
					System.out.println("data = " + data);
					if (data != null && data.length() > 0) {
						try {
							ArrayList<StockFinancialItems> financials = new ArrayList<StockFinancialItems>();
							JSONObject jsonObject = new JSONObject(data);
							JSONObject jsonCurrYearObject  = null;
							try {
								jsonCurrYearObject = jsonObject.getJSONObject(String.valueOf(currYear));
							} catch (Exception e) {
								//mHandler.sendEmptyMessage(SHOW_FINANCIAL_QUARTER_FAIL);
								e.printStackTrace();
							}
							
							if(jsonCurrYearObject != null){
								if (jsonCurrYearObject.has("Q4")) {
									JSONObject jsonCurrYearQ4Object = jsonCurrYearObject.getJSONObject("Q4");
									StockFinancialItems holder = new StockFinancialItems();
									holder = new Gson().fromJson(jsonCurrYearQ4Object.toString(), StockFinancialItems.class);								
									holder.title = "Q4/" + currYear;								
									financials.add(holder);
								}
								if (financials.size() <= 3) {
									if (jsonCurrYearObject.has("Q3")) {															
										JSONObject jsonCurrYearQ3Object = jsonCurrYearObject.getJSONObject("Q3");
										StockFinancialItems holder = new StockFinancialItems();
										holder = new Gson().fromJson(jsonCurrYearQ3Object.toString(), StockFinancialItems.class);
										holder.title = "Q3/" + currYear;
										financials.add(holder);
									}
								}
								if (financials.size() <= 3) {
									if (jsonCurrYearObject.has("Q2")) {
										JSONObject jsonCurrYearQ2Object = jsonCurrYearObject.getJSONObject("Q2");
										StockFinancialItems holder = new StockFinancialItems();
										holder = new Gson().fromJson(jsonCurrYearQ2Object.toString(), StockFinancialItems.class);
										holder.title = "Q2/" + currYear;
										financials.add(holder);
									}
								}
								if (financials.size() <= 3) {
									if (jsonCurrYearObject.has("Q1")) {
										JSONObject jsonCurrYearQ1Object = jsonCurrYearObject.getJSONObject("Q1");
										StockFinancialItems holder = new StockFinancialItems();
										holder = new Gson().fromJson(jsonCurrYearQ1Object.toString(), StockFinancialItems.class);
										holder.title = "Q1/" + currYear;
										financials.add(holder);
									}
								}
							}
								
								JSONObject jsonPrevYearObject  = null;
								try {
									 jsonPrevYearObject = jsonObject.getJSONObject(String.valueOf(currYear - 1));
								} catch (Exception e) {
									e.printStackTrace();
								}
								if(jsonPrevYearObject != null){
									if (financials.size() <= 3) {
										if (jsonPrevYearObject.has("Q4")) {
											JSONObject jsonPrevYearQ4Object = jsonPrevYearObject.getJSONObject("Q4");
											StockFinancialItems holder = new StockFinancialItems();
											holder = new Gson().fromJson(jsonPrevYearQ4Object.toString(), StockFinancialItems.class);
											holder.title = "Q4/" + (currYear - 1);
											financials.add(holder);
										}
									}
									if (financials.size() <= 3) {
										if (jsonPrevYearObject.has("Q3")) {
											JSONObject jsonPrevYearQ3Object = jsonPrevYearObject.getJSONObject("Q3");
											StockFinancialItems holder = new StockFinancialItems();
											holder = new Gson().fromJson(jsonPrevYearQ3Object.toString(), StockFinancialItems.class);
											holder.title = "Q3/" + (currYear - 1);
											financials.add(holder);
										}
									}
									if (financials.size() <= 3) {
										if (jsonPrevYearObject.has("Q2")) {
											JSONObject jsonPrevYearQ2Object = jsonPrevYearObject.getJSONObject("Q2");
											StockFinancialItems holder = new StockFinancialItems();
											holder = new Gson().fromJson(jsonPrevYearQ2Object.toString(), StockFinancialItems.class);
											holder.title = "Q2/" + (currYear - 1);
											financials.add(holder);
										}
									}
									if (financials.size() <= 3) {
										if (jsonPrevYearObject.has("Q1")) {
											JSONObject jsonPrevYearQ1Object = jsonPrevYearObject.getJSONObject("Q1");
											StockFinancialItems holder = new StockFinancialItems();
											holder = new Gson().fromJson(jsonPrevYearQ1Object.toString(), StockFinancialItems.class);
											holder.title = "Q1/" + (currYear - 1);
											financials.add(holder);
										}
									}
								}
								
								StockFinancialItems holder = new StockFinancialItems();
								if(financials != null && financials.size() > 0){
									holder = financials.get(0);
									if (holder != null) {
										tv1.setText(holder.title);
										if (holder.NIAT != null) {
											tvNIAT1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NIAT)));
										}
										if (holder.PRINS != null) {
											tvPRINS1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRINS)));
										}
										if (holder.NETREVINS != null) {
											tvNETREVINS1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NETREVINS)));
										}
										if (holder.GOPINS != null) {
											tvGOPINS1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GOPINS)));
										}
										if (holder.FNINC != null) {
											tvFNINC1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.FNINC)));
										}
										if (holder.TOTREV != null) {
											tvTOTREV1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTREV)));
										}
										if (holder.OPREXP != null) {
											tvOPREXP1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.OPREXP)));
										}
										if (holder.GENADMEX != null) {
											tvGENADMEX1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GENADMEX)));
										}
										if (holder.NSL != null) {
											tvNSL1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NSL)));
										}
										if (holder.PBT != null) {
											tvPBT1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PBT)));
										}
										if (holder.GOP != null) {
											tvGOP1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GOP)));
										}
										if (holder.TOTAL_EXPENSE != null) {
											tvTOTAL_EXPENSE1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTAL_EXPENSE)));
										}
										if (holder.CURRENT_ASSET != null) {
											tvCA1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CURRENT_ASSET)));
										}
										if (holder.LT_ASSET != null) {
											tvLT_ASSET1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LT_ASSET)));
										}
										if (holder.CURLIB != null) {
											tvCURLIB1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CURLIB)));
										}
										if (holder.LT_DEBT != null) {
											tvLT_DEBT1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LT_DEBT)));
										}
										if (holder.OWNEQ != null) {
											tvOWNEQ1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.OWNEQ)));
										}
										if (holder.TOTAL_ASSET != null) {
											tvTA1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTAL_ASSET)));
										}
										if (holder.CACE != null) {
											tvCACE1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CACE)));
										}
										if (holder.SHINV != null) {
											tvSHINV1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.SHINV)));
										}
										if (holder.SHREC != null) {
											tvSHREC1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.SHREC)));
										}
										if (holder.PRDEBT != null) {
											tvPRDEBT1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRDEBT)));
										}
										if (holder.FXASST != null) {
											tvFXASST1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.FXASST)));
										}
										if (holder.UNRESV != null) {
											tvUNRESV1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.UNRESV)));
										}
										if (holder.PRDIMINV != null) {
											tvPRDIMINV1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRDIMINV)));
										}
										if (holder.LONGINV != null) {
											tvLONGINV1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LONGINV)));
										}
										if (holder.LIABILITY != null) {
											tvLIABILITY1.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LIABILITY)));
										}
									}
								}
								if(financials.size() > 1){
									holder = financials.get(1);
									if (holder != null) {
										tv2.setText(holder.title);
										if (holder.NIAT != null) {
											tvNIAT2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NIAT)));
										}
										if (holder.PRINS != null) {
											tvPRINS2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRINS)));
										}
										if (holder.NETREVINS != null) {
											tvNETREVINS2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NETREVINS)));
										}
										if (holder.GOPINS != null) {
											tvGOPINS2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GOPINS)));
										}
										if (holder.FNINC != null) {
											tvFNINC2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.FNINC)));
										}
										if (holder.TOTREV != null) {
											tvTOTREV2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTREV)));
										}
										if (holder.OPREXP != null) {
											tvOPREXP2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.OPREXP)));
										}
										if (holder.GENADMEX != null) {
											tvGENADMEX2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GENADMEX)));
										}
										if (holder.NSL != null) {
											tvNSL2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NSL)));
										}
										if (holder.PBT != null) {
											tvPBT2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PBT)));
										}
										if (holder.GOP != null) {
											tvGOP2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GOP)));
										}
										if (holder.TOTAL_EXPENSE != null) {
											tvTOTAL_EXPENSE2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTAL_EXPENSE)));
										}
										if (holder.CURRENT_ASSET != null) {
											tvCA2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CURRENT_ASSET)));
										}
										if (holder.LT_ASSET != null) {
											tvLT_ASSET2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LT_ASSET)));
										}
										if (holder.CURLIB != null) {
											tvCURLIB2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CURLIB)));
										}
										if (holder.LT_DEBT != null) {
											tvLT_DEBT2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LT_DEBT)));
										}
										if (holder.OWNEQ != null) {
											tvOWNEQ2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.OWNEQ)));
										}
										if (holder.TOTAL_ASSET != null) {
											tvTA2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTAL_ASSET)));
										}
										if (holder.CACE != null) {
											tvCACE2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CACE)));
										}
										if (holder.SHINV != null) {
											tvSHINV2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.SHINV)));
										}
										if (holder.SHREC != null) {
											tvSHREC2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.SHREC)));
										}
										if (holder.PRDEBT != null) {
											tvPRDEBT2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRDEBT)));
										}
										if (holder.FXASST != null) {
											tvFXASST2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.FXASST)));
										}
										if (holder.UNRESV != null) {
											tvUNRESV2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.UNRESV)));
										}
										if (holder.PRDIMINV != null) {
											tvPRDIMINV2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRDIMINV)));
										}
										if (holder.LONGINV != null) {
											tvLONGINV2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LONGINV)));
										}
										if (holder.LIABILITY != null) {
											tvLIABILITY2.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LIABILITY)));
										}
									}
								}
								if(financials.size() >2){
									holder = financials.get(2);
									if (holder != null) {
										tv3.setText(holder.title);
										if (holder.NIAT != null) {
											tvNIAT3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NIAT)));
										}
										if (holder.PRINS != null) {
											tvPRINS3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRINS)));
										}
										if (holder.NETREVINS != null) {
											tvNETREVINS3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NETREVINS)));
										}
										if (holder.GOPINS != null) {
											tvGOPINS3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GOPINS)));
										}
										if (holder.FNINC != null) {
											tvFNINC3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.FNINC)));
										}
										if (holder.TOTREV != null) {
											tvTOTREV3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTREV)));
										}
										if (holder.OPREXP != null) {
											tvOPREXP3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.OPREXP)));
										}
										if (holder.GENADMEX != null) {
											tvGENADMEX3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GENADMEX)));
										}
										if (holder.NSL != null) {
											tvNSL3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NSL)));
										}
										if (holder.PBT != null) {
											tvPBT3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PBT)));
										}
										if (holder.GOP != null) {
											tvGOP3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GOP)));
										}
										if (holder.TOTAL_EXPENSE != null) {
											tvTOTAL_EXPENSE3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTAL_EXPENSE)));
										}
										if (holder.CURRENT_ASSET != null) {
											tvCA3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CURRENT_ASSET)));
										}
										if (holder.LT_ASSET != null) {
											tvLT_ASSET3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LT_ASSET)));
										}
										if (holder.CURLIB != null) {
											tvCURLIB3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CURLIB)));
										}
										if (holder.LT_DEBT != null) {
											tvLT_DEBT3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LT_DEBT)));
										}
										if (holder.OWNEQ != null) {
											tvOWNEQ3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.OWNEQ)));
										}
										if (holder.TOTAL_ASSET != null) {
											tvTA3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTAL_ASSET)));
										}
										if (holder.CACE != null) {
											tvCACE3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CACE)));
										}
										if (holder.SHINV != null) {
											tvSHINV3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.SHINV)));
										}
										if (holder.SHREC != null) {
											tvSHREC3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.SHREC)));
										}
										if (holder.PRDEBT != null) {
											tvPRDEBT3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRDEBT)));
										}
										if (holder.FXASST != null) {
											tvFXASST3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.FXASST)));
										}
										if (holder.UNRESV != null) {
											tvUNRESV3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.UNRESV)));
										}
										if (holder.PRDIMINV != null) {
											tvPRDIMINV3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRDIMINV)));
										}
										if (holder.LONGINV != null) {
											tvLONGINV3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LONGINV)));
										}
										if (holder.LIABILITY != null) {
											tvLIABILITY3.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LIABILITY)));
										}
									}
								}
								if(financials.size() >3){
									holder = financials.get(3);
									if (holder != null) {
										tv4.setText(holder.title);
										if (holder.NIAT != null) {
											tvNIAT4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NIAT)));
										}
										if (holder.PRINS != null) {
											tvPRINS4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRINS)));
										}
										if (holder.NETREVINS != null) {
											tvNETREVINS4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NETREVINS)));
										}
										if (holder.GOPINS != null) {
											tvGOPINS4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GOPINS)));
										}
										if (holder.FNINC != null) {
											tvFNINC4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.FNINC)));
										}
										if (holder.TOTREV != null) {
											tvTOTREV4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTREV)));
										}
										if (holder.OPREXP != null) {
											tvOPREXP4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.OPREXP)));
										}
										if (holder.GENADMEX != null) {
											tvGENADMEX4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GENADMEX)));
										}
										if (holder.NSL != null) {
											tvNSL4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.NSL)));
										}
										if (holder.PBT != null) {
											tvPBT4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PBT)));
										}
										if (holder.GOP != null) {
											tvGOP4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.GOP)));
										}
										if (holder.TOTAL_EXPENSE != null) {
											tvTOTAL_EXPENSE4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTAL_EXPENSE)));
										}
										if (holder.CURRENT_ASSET != null) {
											tvCA4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CURRENT_ASSET)));
										}
										if (holder.LT_ASSET != null) {
											tvLT_ASSET4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LT_ASSET)));
										}
										if (holder.CURLIB != null) {
											tvCURLIB4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CURLIB)));
										}
										if (holder.LT_DEBT != null) {
											tvLT_DEBT4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LT_DEBT)));
										}
										if (holder.OWNEQ != null) {
											tvOWNEQ4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.OWNEQ)));
										}
										if (holder.TOTAL_ASSET != null) {
											tvTA4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.TOTAL_ASSET)));
										}
										if (holder.CACE != null) {
											tvCACE4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.CACE)));
										}
										if (holder.SHINV != null) {
											tvSHINV4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.SHINV)));
										}
										if (holder.SHREC != null) {
											tvSHREC4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.SHREC)));
										}
										if (holder.PRDEBT != null) {
											tvPRDEBT4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRDEBT)));
										}
										if (holder.FXASST != null) {
											tvFXASST4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.FXASST)));
										}
										if (holder.UNRESV != null) {
											tvUNRESV4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.UNRESV)));
										}
										if (holder.PRDIMINV != null) {
											tvPRDIMINV4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.PRDIMINV)));
										}
										if (holder.LONGINV != null) {
											tvLONGINV4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LONGINV)));
										}
										if (holder.LIABILITY != null) {
											tvLIABILITY4.setText(Utils.formatAsStockVolume(Double.valueOf(holder.LIABILITY)));
										}
									}			
								}
							
							hideEmptyData();
							hideProgressDialog();
						} catch (JSONException e) {
							e.printStackTrace();
							hideProgressDialog();
						}
					}
				}

				@Override
				public void onError(Exception ex) {
					ex.printStackTrace();
					hideProgressDialog();
				}
			});
			break;

		case 1:
			showProgressDialog();
			btnYear.setTextColor(getActivity().getResources().getColor(R.color.text_color_yellow));

			Calendar c2 = Calendar.getInstance();
			final int currYear2 = c2.get(Calendar.YEAR);
			ArrayList<Integer> years2 = new ArrayList<Integer>();
			years2.add(currYear2 - 4);
			years2.add(currYear2 - 3);
			years2.add(currYear2 - 2);
			years2.add(currYear2 - 1);
			years2.add(currYear2);
			JSONArray jsArray2 = new JSONArray(years2);
			restService.getStockFinancialYear(stockCode, jsArray2, new IStockResponseListener<String>() {

				@Override
				public void onResponse(String data) {
					System.out.println("data Stock Year = " + data);
					if (data != null && data.length() > 0) {
						try {
							ArrayList<StockFinancialItems> financials = new ArrayList<StockFinancialItems>();
							JSONObject jsonObject = new JSONObject(data);
							try {
								JSONObject jsonCurrYearObject = jsonObject.getJSONObject(String.valueOf(currYear2));
								StockFinancialItems holder = new StockFinancialItems();
								holder = new Gson().fromJson(jsonCurrYearObject.toString(), StockFinancialItems.class);
								holder.title = currYear2 + "";
								financials.add(holder);
							} catch (JSONException e) {
								e.printStackTrace();
								hideProgressDialog();
							}

							if (financials.size() <= 3) {
								try {
									JSONObject jsonCurrYearObject1 = jsonObject.getJSONObject(String.valueOf(currYear2 - 1));
									StockFinancialItems holder1 = new StockFinancialItems();
									holder1 = new Gson().fromJson(jsonCurrYearObject1.toString(), StockFinancialItems.class);
									holder1.title = (currYear2 - 1) + "";
									financials.add(holder1);
								} catch (JSONException e) {
									e.printStackTrace();
									hideProgressDialog();
								}
							}
							
							if (financials.size() <= 3) {
								try {
									JSONObject jsonCurrYearObject2 = jsonObject.getJSONObject(String.valueOf(currYear2 - 2));
									StockFinancialItems holder2 = new StockFinancialItems();
									holder2 = new Gson().fromJson(jsonCurrYearObject2.toString(), StockFinancialItems.class);
									holder2.title = (currYear2 - 2) + "";
									financials.add(holder2);
								} catch (JSONException e) {
									// TODO: handle exception
								}
							}
							
							if (financials.size() <= 3) {
								try {
									JSONObject jsonCurrYearObject3 = jsonObject.getJSONObject(String.valueOf(currYear2 - 3));
									StockFinancialItems holder3 = new StockFinancialItems();
									holder3 = new Gson().fromJson(jsonCurrYearObject3.toString(), StockFinancialItems.class);
									holder3.title = (currYear2 - 3) + "";
									financials.add(holder3);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							if (financials.size() <= 3) {
								try {
									JSONObject jsonCurrYearObject4 = jsonObject.getJSONObject(String.valueOf(currYear2 - 4));
									StockFinancialItems holder4 = new StockFinancialItems();
									holder4 = new Gson().fromJson(jsonCurrYearObject4.toString(), StockFinancialItems.class);
									holder4.title = (currYear2 - 4) + "";
									financials.add(holder4);
								} catch (JSONException e) {
									e.printStackTrace();
									hideProgressDialog();
								}
							}

							StockFinancialItems holderResult = new StockFinancialItems();
							if(financials.size() > 0){
								holderResult = financials.get(0);
								if (holderResult != null) {
									tv1.setText(holderResult.title);
									if (holderResult.NIAT != null) {
										tvNIAT1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NIAT)));
									}
									if (holderResult.PRINS != null) {
										tvPRINS1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRINS)));
									}
									if (holderResult.NETREVINS != null) {
										tvNETREVINS1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NETREVINS)));
									}
									if (holderResult.GOPINS != null) {
										tvGOPINS1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GOPINS)));
									}
									if (holderResult.FNINC != null) {
										tvFNINC1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.FNINC)));
									}
									if (holderResult.TOTREV != null) {
										tvTOTREV1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTREV)));
									}
									if (holderResult.OPREXP != null) {
										tvOPREXP1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.OPREXP)));
									}
									if (holderResult.GENADMEX != null) {
										tvGENADMEX1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GENADMEX)));
									}
									if (holderResult.NSL != null) {
										tvNSL1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NSL)));
									}
									if (holderResult.PBT != null) {
										tvPBT1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PBT)));
									}
									if (holderResult.GOP != null) {
										tvGOP1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GOP)));
									}
									if (holderResult.TOTAL_EXPENSE != null) {
										tvTOTAL_EXPENSE1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTAL_EXPENSE)));
									}
									if (holderResult.CURRENT_ASSET != null) {
										tvCA1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CURRENT_ASSET)));
									}
									if (holderResult.LT_ASSET != null) {
										tvLT_ASSET1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LT_ASSET)));
									}
									if (holderResult.CURLIB != null) {
										tvCURLIB1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CURLIB)));
									}
									if (holderResult.LT_DEBT != null) {
										tvLT_DEBT1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LT_DEBT)));
									}
									if (holderResult.OWNEQ != null) {
										tvOWNEQ1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.OWNEQ)));
									}
									if (holderResult.TOTAL_ASSET != null) {
										tvTA1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTAL_ASSET)));
									}
									if (holderResult.CACE != null) {
										tvCACE1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CACE)));
									}
									if (holderResult.SHINV != null) {
										tvSHINV1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.SHINV)));
									}
									if (holderResult.SHREC != null) {
										tvSHREC1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.SHREC)));
									}
									if (holderResult.PRDEBT != null) {
										tvPRDEBT1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRDEBT)));
									}
									if (holderResult.FXASST != null) {
										tvFXASST1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.FXASST)));
									}
									if (holderResult.UNRESV != null) {
										tvUNRESV1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.UNRESV)));
									}
									if (holderResult.PRDIMINV != null) {
										tvPRDIMINV1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRDIMINV)));
									}
									if (holderResult.LONGINV != null) {
										tvLONGINV1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LONGINV)));
									}
									if (holderResult.LIABILITY != null) {
										tvLIABILITY1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LIABILITY)));
									}
								}
							}
							if(financials.size() > 1){
								holderResult = financials.get(1);
								if (holderResult != null) {
									tv2.setText(holderResult.title);
									if (holderResult.NIAT != null) {
										tvNIAT2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NIAT)));
									}
									if (holderResult.PRINS != null) {
										tvPRINS2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRINS)));
									}
									if (holderResult.NETREVINS != null) {
										tvNETREVINS2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NETREVINS)));
									}
									if (holderResult.GOPINS != null) {
										tvGOPINS2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GOPINS)));
									}
									if (holderResult.FNINC != null) {
										tvFNINC2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.FNINC)));
									}
									if (holderResult.TOTREV != null) {
										tvTOTREV2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTREV)));
									}
									if (holderResult.OPREXP != null) {
										tvOPREXP2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.OPREXP)));
									}
									if (holderResult.GENADMEX != null) {
										tvGENADMEX2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GENADMEX)));
									}
									if (holderResult.NSL != null) {
										tvNSL2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NSL)));
									}
									if (holderResult.PBT != null) {
										tvPBT2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PBT)));
									}
									if (holderResult.GOP != null) {
										tvGOP2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GOP)));
									}
									if (holderResult.TOTAL_EXPENSE != null) {
										tvTOTAL_EXPENSE2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTAL_EXPENSE)));
									}
									if (holderResult.CURRENT_ASSET != null) {
										tvCA2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CURRENT_ASSET)));
									}
									if (holderResult.LT_ASSET != null) {
										tvLT_ASSET2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LT_ASSET)));
									}
									if (holderResult.CURLIB != null) {
										tvCURLIB2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CURLIB)));
									}
									if (holderResult.LT_DEBT != null) {
										tvLT_DEBT2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LT_DEBT)));
									}
									if (holderResult.OWNEQ != null) {
										tvOWNEQ2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.OWNEQ)));
									}
									if (holderResult.TOTAL_ASSET != null) {
										tvTA2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTAL_ASSET)));
									}
									if (holderResult.CACE != null) {
										tvCACE2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CACE)));
									}
									if (holderResult.SHINV != null) {
										tvSHINV2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.SHINV)));
									}
									if (holderResult.SHREC != null) {
										tvSHREC2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.SHREC)));
									}
									if (holderResult.PRDEBT != null) {
										tvPRDEBT2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRDEBT)));
									}
									if (holderResult.FXASST != null) {
										tvFXASST2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.FXASST)));
									}
									if (holderResult.UNRESV != null) {
										tvUNRESV2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.UNRESV)));
									}
									if (holderResult.PRDIMINV != null) {
										tvPRDIMINV2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRDIMINV)));
									}
									if (holderResult.LONGINV != null) {
										tvLONGINV2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LONGINV)));
									}
									if (holderResult.LIABILITY != null) {
										tvLIABILITY2.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LIABILITY)));
									}
								}
							}
							if(financials.size() > 2){
							holderResult = financials.get(2);
								if (holderResult != null) {
									tv3.setText(holderResult.title);
									if (holderResult.NIAT != null) {
										tvNIAT3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NIAT)));
									}
									if (holderResult.PRINS != null) {
										tvPRINS3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRINS)));
									}
									if (holderResult.NETREVINS != null) {
										tvNETREVINS3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NETREVINS)));
									}
									if (holderResult.GOPINS != null) {
										tvGOPINS3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GOPINS)));
									}
									if (holderResult.FNINC != null) {
										tvFNINC3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.FNINC)));
									}
									if (holderResult.TOTREV != null) {
										tvTOTREV3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTREV)));
									}
									if (holderResult.OPREXP != null) {
										tvOPREXP3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.OPREXP)));
									}
									if (holderResult.GENADMEX != null) {
										tvGENADMEX1.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GENADMEX)));
									}
									if (holderResult.NSL != null) {
										tvNSL3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NSL)));
									}
									if (holderResult.PBT != null) {
										tvPBT3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PBT)));
									}
									if (holderResult.GOP != null) {
										tvGOP3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GOP)));
									}
									if (holderResult.TOTAL_EXPENSE != null) {
										tvTOTAL_EXPENSE3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTAL_EXPENSE)));
									}
									if (holderResult.CURRENT_ASSET != null) {
										tvCA3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CURRENT_ASSET)));
									}
									if (holderResult.LT_ASSET != null) {
										tvLT_ASSET3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LT_ASSET)));
									}
									if (holderResult.CURLIB != null) {
										tvCURLIB3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CURLIB)));
									}
									if (holderResult.LT_DEBT != null) {
										tvLT_DEBT3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LT_DEBT)));
									}
									if (holderResult.OWNEQ != null) {
										tvOWNEQ3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.OWNEQ)));
									}
									if (holderResult.TOTAL_ASSET != null) {
										tvTA3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTAL_ASSET)));
									}
									if (holderResult.CACE != null) {
										tvCACE3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CACE)));
									}
									if (holderResult.SHINV != null) {
										tvSHINV3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.SHINV)));
									}
									if (holderResult.SHREC != null) {
										tvSHREC3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.SHREC)));
									}
									if (holderResult.PRDEBT != null) {
										tvPRDEBT3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRDEBT)));
									}
									if (holderResult.FXASST != null) {
										tvFXASST3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.FXASST)));
									}
									if (holderResult.UNRESV != null) {
										tvUNRESV3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.UNRESV)));
									}
									if (holderResult.PRDIMINV != null) {
										tvPRDIMINV3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRDIMINV)));
									}
									if (holderResult.LONGINV != null) {
										tvLONGINV3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LONGINV)));
									}
									if (holderResult.LIABILITY != null) {
										tvLIABILITY3.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LIABILITY)));
									}
								}
							}
							if(financials.size() > 3){							
								holderResult = financials.get(3);
								if (holderResult != null) {
									tv4.setText(holderResult.title);
									if (holderResult.NIAT != null) {
										tvNIAT4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NIAT)));
									}
									if (holderResult.PRINS != null) {
										tvPRINS4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRINS)));
									}
									if (holderResult.NETREVINS != null) {
										tvNETREVINS4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NETREVINS)));
									}
									if (holderResult.GOPINS != null) {
										tvGOPINS4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GOPINS)));
									}
									if (holderResult.FNINC != null) {
										tvFNINC4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.FNINC)));
									}
									if (holderResult.TOTREV != null) {
										tvTOTREV4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTREV)));
									}
									if (holderResult.OPREXP != null) {
										tvOPREXP4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.OPREXP)));
									}
									if (holderResult.GENADMEX != null) {
										tvGENADMEX4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GENADMEX)));
									}
									if (holderResult.NSL != null) {
										tvNSL4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.NSL)));
									}
									if (holderResult.PBT != null) {
										tvPBT4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PBT)));
									}
									if (holderResult.GOP != null) {
										tvGOP4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.GOP)));
									}
									if (holderResult.TOTAL_EXPENSE != null) {
										tvTOTAL_EXPENSE4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTAL_EXPENSE)));
									}
									if (holderResult.CURRENT_ASSET != null) {
										tvCA4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CURRENT_ASSET)));
									}
									if (holderResult.LT_ASSET != null) {
										tvLT_ASSET4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LT_ASSET)));
									}
									if (holderResult.CURLIB != null) {
										tvCURLIB4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CURLIB)));
									}
									if (holderResult.LT_DEBT != null) {
										tvLT_DEBT4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LT_DEBT)));
									}
									if (holderResult.OWNEQ != null) {
										tvOWNEQ4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.OWNEQ)));
									}
									if (holderResult.TOTAL_ASSET != null) {
										tvTA4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.TOTAL_ASSET)));
									}
									if (holderResult.CACE != null) {
										tvCACE4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.CACE)));
									}
									if (holderResult.SHINV != null) {
										tvSHINV4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.SHINV)));
									}
									if (holderResult.SHREC != null) {
										tvSHREC4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.SHREC)));
									}
									if (holderResult.PRDEBT != null) {
										tvPRDEBT4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRDEBT)));
									}
									if (holderResult.FXASST != null) {
										tvFXASST4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.FXASST)));
									}
									if (holderResult.UNRESV != null) {
										tvUNRESV4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.UNRESV)));
									}
									if (holderResult.PRDIMINV != null) {
										tvPRDIMINV4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.PRDIMINV)));
									}
									if (holderResult.LONGINV != null) {
										tvLONGINV4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LONGINV)));
									}
									if (holderResult.LIABILITY != null) {
										tvLIABILITY4.setText(Utils.formatAsStockVolume(Double.valueOf(holderResult.LIABILITY)));
									}
								}
							}
							
							hideEmptyData();
							
							
							hideProgressDialog();
						} catch (JSONException e) {
							e.printStackTrace();
							hideProgressDialog();
						}
					}
				}

				@Override
				public void onError(Exception ex) {
					ex.printStackTrace();
					hideProgressDialog();
				}
			});
			break;
		}
	}
	
	private void hideEmptyData(){
		// layout Income
		
		if(tvNSL1.getText().toString().trim().length() == 0 && tvNSL2.getText().toString().trim().length() == 0 && 
				tvNSL3.getText().toString().trim().length() == 0 && tvNSL4.getText().toString().trim().length() == 0){
			layoutIncomeNetSale.setVisibility(View.GONE);
		}else{
			layoutIncomeNetSale.setVisibility(View.VISIBLE);
		}
		
		if(tvGOP1.getText().toString().trim().length() == 0 && tvGOP2.getText().toString().trim().length() == 0 && 
				tvGOP3.getText().toString().trim().length() == 0 && tvGOP4.getText().toString().trim().length() == 0){
			layoutIncomeGrossProfit.setVisibility(View.GONE);
		}else{
			layoutIncomeGrossProfit.setVisibility(View.VISIBLE);
		}
		
		if(tvTOTAL_EXPENSE1.getText().toString().trim().length() == 0 && tvTOTAL_EXPENSE2.getText().toString().trim().length() == 0 && 
				tvTOTAL_EXPENSE3.getText().toString().trim().length() == 0 && tvTOTAL_EXPENSE4.getText().toString().trim().length() == 0){
			layoutIncomeTotalExpenses.setVisibility(View.GONE);
		}else{
			layoutIncomeTotalExpenses.setVisibility(View.VISIBLE);
		}
		
		if(tvTOTAL_EXPENSE1.getText().toString().trim().length() == 0 && tvTOTAL_EXPENSE2.getText().toString().trim().length() == 0 && 
				tvTOTAL_EXPENSE3.getText().toString().trim().length() == 0 && tvTOTAL_EXPENSE4.getText().toString().trim().length() == 0){
			layoutIncomeProfitBeforeTax.setVisibility(View.GONE);
		}else{
			layoutIncomeProfitBeforeTax.setVisibility(View.VISIBLE);
		}
		
		if(tvPBT1.getText().toString().trim().length() == 0 && tvPBT2.getText().toString().trim().length() == 0 && 
				tvPBT3.getText().toString().trim().length() == 0 && tvPBT4.getText().toString().trim().length() == 0){
			layoutIncomeNetProfitAfterTax.setVisibility(View.GONE);
		}else{
			layoutIncomeNetProfitAfterTax.setVisibility(View.VISIBLE);
		}
		
		if(tvPRINS1.getText().toString().trim().length() == 0 && tvPRINS1.getText().toString().trim().length() == 0 && 
				tvPRINS1.getText().toString().trim().length() == 0 && tvPRINS1.getText().toString().trim().length() == 0){
			layoutIncomeDirectInsurance.setVisibility(View.GONE);
		}else{
			layoutIncomeDirectInsurance.setVisibility(View.VISIBLE);
		}
		
		if(tvNETREVINS1.getText().toString().trim().length() == 0 && tvNETREVINS2.getText().toString().trim().length() == 0 && 
				tvNETREVINS3.getText().toString().trim().length() == 0 && tvNETREVINS4.getText().toString().trim().length() == 0){
			layoutIncomeRevenuesInsuranceActivities.setVisibility(View.GONE);
		}else{
			layoutIncomeRevenuesInsuranceActivities.setVisibility(View.VISIBLE);
		}
		
		if(tvGOPINS1.getText().toString().trim().length() == 0 && tvGOPINS2.getText().toString().trim().length() == 0 && 
				tvGOPINS3.getText().toString().trim().length() == 0 && tvGOPINS4.getText().toString().trim().length() == 0){
			layoutIncomeGrossProfitInsuranceActivities.setVisibility(View.GONE);
		}else{
			layoutIncomeGrossProfitInsuranceActivities.setVisibility(View.VISIBLE);
		}
		
		if(tvFNINC1.getText().toString().trim().length() == 0 && tvFNINC2.getText().toString().trim().length() == 0 && 
				tvFNINC3.getText().toString().trim().length() == 0 && tvFNINC4.getText().toString().trim().length() == 0){
			layoutIncomeFinancialIncome.setVisibility(View.GONE);
		}else{
			layoutIncomeFinancialIncome.setVisibility(View.VISIBLE);
		}
		
		if(tvTOTREV1.getText().toString().trim().length() == 0 && tvTOTREV2.getText().toString().trim().length() == 0 && 
				tvTOTREV3.getText().toString().trim().length() == 0 && tvTOTREV4.getText().toString().trim().length() == 0){
			layoutIncomeTotalRevenues.setVisibility(View.GONE);
		}else{
			layoutIncomeTotalRevenues.setVisibility(View.VISIBLE);
		}
		
		if(tvOPREXP1.getText().toString().trim().length() == 0 && tvOPREXP2.getText().toString().trim().length() == 0 && 
				tvOPREXP3.getText().toString().trim().length() == 0 && tvOPREXP4.getText().toString().trim().length() == 0){
			layoutIncomeOperatingExpenses.setVisibility(View.GONE);
		}else{
			layoutIncomeOperatingExpenses.setVisibility(View.VISIBLE);
		}
		
		if(tvGENADMEX1.getText().toString().trim().length() == 0 && tvGENADMEX2.getText().toString().trim().length() == 0 && 
				tvGENADMEX3.getText().toString().trim().length() == 0 && tvGENADMEX4.getText().toString().trim().length() == 0){
			layoutIncomeAdministrativeExpenses.setVisibility(View.GONE);
		}else{
			layoutIncomeAdministrativeExpenses.setVisibility(View.VISIBLE);
		}
		
		
		// layout Assets
		if(tvCA1.getText().toString().trim().length() == 0 && tvCA2.getText().toString().trim().length() == 0 && 
				tvCA3.getText().toString().trim().length() == 0 && tvCA4.getText().toString().trim().length() == 0){
			layoutAssetsCurrentAssets.setVisibility(View.GONE);
		}else{
			layoutAssetsCurrentAssets.setVisibility(View.VISIBLE);
		}
		
		if(tvLT_ASSET1.getText().toString().trim().length() == 0 && tvLT_ASSET2.getText().toString().trim().length() == 0 && 
				tvLT_ASSET3.getText().toString().trim().length() == 0 && tvLT_ASSET4.getText().toString().trim().length() == 0){
			layoutAssetsLongTermAssets.setVisibility(View.GONE);
		}else{
			layoutAssetsLongTermAssets.setVisibility(View.VISIBLE);
		}
		
		if(tvCURLIB1.getText().toString().trim().length() == 0 && tvCURLIB2.getText().toString().trim().length() == 0 && 
				tvCURLIB3.getText().toString().trim().length() == 0 && tvCURLIB4.getText().toString().trim().length() == 0){
			layoutAssetsCurrentLiabilities.setVisibility(View.GONE);
		}else{
			layoutAssetsCurrentLiabilities.setVisibility(View.VISIBLE);
		}
		
		if(tvLT_DEBT1.getText().toString().trim().length() == 0 && tvLT_DEBT2.getText().toString().trim().length() == 0 && 
				tvLT_DEBT3.getText().toString().trim().length() == 0 && tvLT_DEBT4.getText().toString().trim().length() == 0){
			layoutAssetsLongTermDebts.setVisibility(View.GONE);
		}else{
			layoutAssetsLongTermDebts.setVisibility(View.VISIBLE);
		}
		
		if(tvOWNEQ1.getText().toString().trim().length() == 0 && tvOWNEQ2.getText().toString().trim().length() == 0 && 
				tvOWNEQ3.getText().toString().trim().length() == 0 && tvOWNEQ4.getText().toString().trim().length() == 0){
			layoutAssetsOwnerEquity.setVisibility(View.GONE);
		}else{
			layoutAssetsOwnerEquity.setVisibility(View.VISIBLE);
		}
		
		if(tvTA1.getText().toString().trim().length() == 0 && tvTA2.getText().toString().trim().length() == 0 && 
				tvTA3.getText().toString().trim().length() == 0 && tvTA4.getText().toString().trim().length() == 0){
			layoutAssetsTotalAssets.setVisibility(View.GONE);
		}else{
			layoutAssetsTotalAssets.setVisibility(View.VISIBLE);
		}
		
		if(tvCACE1.getText().toString().trim().length() == 0 && tvCACE2.getText().toString().trim().length() == 0 && 
				tvCACE3.getText().toString().trim().length() == 0 && tvCACE4.getText().toString().trim().length() == 0){
			layoutAssetsCashQuivalents.setVisibility(View.GONE);
		}else{
			layoutAssetsCashQuivalents.setVisibility(View.VISIBLE);
		}
		
		if(tvSHINV1.getText().toString().trim().length() == 0 && tvSHINV2.getText().toString().trim().length() == 0 && 
				tvSHINV3.getText().toString().trim().length() == 0 && tvSHINV4.getText().toString().trim().length() == 0){
			layoutShortTermInvestment.setVisibility(View.GONE);
		}else{
			layoutShortTermInvestment.setVisibility(View.VISIBLE);
		}
		
		if(tvSHREC1.getText().toString().trim().length() == 0 && tvSHREC2.getText().toString().trim().length() == 0 && 
				tvSHREC3.getText().toString().trim().length() == 0 && tvSHREC4.getText().toString().trim().length() == 0){
			layoutAssetsShortTermReceivables.setVisibility(View.GONE);
		}else{
			layoutAssetsShortTermReceivables.setVisibility(View.VISIBLE);
		}
		
		if(tvPRDEBT1.getText().toString().trim().length() == 0 && tvPRDEBT2.getText().toString().trim().length() == 0 && 
				tvPRDEBT3.getText().toString().trim().length() == 0 && tvPRDEBT4.getText().toString().trim().length() == 0){
			layoutAssetsProvisionDoubtfulDebts.setVisibility(View.GONE);
		}else{
			layoutAssetsProvisionDoubtfulDebts.setVisibility(View.VISIBLE);
		}
		
		if(tvFXASST1.getText().toString().trim().length() == 0 && tvFXASST2.getText().toString().trim().length() == 0 && 
				tvFXASST3.getText().toString().trim().length() == 0 && tvFXASST4.getText().toString().trim().length() == 0){
			layoutAssetsFixedAssets.setVisibility(View.GONE);
		}else{
			layoutAssetsFixedAssets.setVisibility(View.VISIBLE);
		}
		
		if(tvUNRESV1.getText().toString().trim().length() == 0 && tvUNRESV2.getText().toString().trim().length() == 0 && 
				tvUNRESV3.getText().toString().trim().length() == 0 && tvUNRESV4.getText().toString().trim().length() == 0){
			layoutAssetsUnderwritingReserves.setVisibility(View.GONE);
		}else{
			layoutAssetsUnderwritingReserves.setVisibility(View.VISIBLE);
		}
		
		if(tvPRDIMINV1.getText().toString().trim().length() == 0 && tvPRDIMINV2.getText().toString().trim().length() == 0 && 
				tvPRDIMINV3.getText().toString().trim().length() == 0 && tvPRDIMINV4.getText().toString().trim().length() == 0){
			layoutAssetsProvisionDiminution.setVisibility(View.GONE);
		}else{
			layoutAssetsProvisionDiminution.setVisibility(View.VISIBLE);
		}
		
		if(tvLONGINV1.getText().toString().trim().length() == 0 && tvLONGINV2.getText().toString().trim().length() == 0 && 
				tvLONGINV3.getText().toString().trim().length() == 0 && tvLONGINV4.getText().toString().trim().length() == 0){
			layoutAssetsLongTermInvestments.setVisibility(View.GONE);
		}else{
			layoutAssetsLongTermInvestments.setVisibility(View.VISIBLE);
		}
		
		if(tvLIABILITY1.getText().toString().trim().length() == 0 && tvLIABILITY2.getText().toString().trim().length() == 0 && 
				tvLIABILITY3.getText().toString().trim().length() == 0 && tvLIABILITY4.getText().toString().trim().length() == 0){
			layoutAssetsLongTermLiabilities.setVisibility(View.GONE);
		}else{
			layoutAssetsLongTermLiabilities.setVisibility(View.VISIBLE);
		}
	}
	
	private Handler mHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case SHOW_FINANCIAL_QUARTER_FAIL:
			tv1.setText("");
			tv2.setText("");
			tv3.setText("");
			tv4.setText("");
			layoutIncomeNetSale.setVisibility(View.GONE);
		
			layoutIncomeGrossProfit.setVisibility(View.GONE);
	
			layoutIncomeTotalExpenses.setVisibility(View.GONE);
		
			layoutIncomeProfitBeforeTax.setVisibility(View.GONE);
		
			layoutIncomeNetProfitAfterTax.setVisibility(View.GONE);
		
			layoutIncomeDirectInsurance.setVisibility(View.GONE);
		
			layoutIncomeRevenuesInsuranceActivities.setVisibility(View.GONE);
	
			layoutIncomeGrossProfitInsuranceActivities.setVisibility(View.GONE);
	
			layoutIncomeFinancialIncome.setVisibility(View.GONE);
	
			layoutIncomeTotalRevenues.setVisibility(View.GONE);
		
			layoutIncomeOperatingExpenses.setVisibility(View.GONE);
	
			layoutIncomeAdministrativeExpenses.setVisibility(View.GONE);
		
			layoutAssetsCurrentAssets.setVisibility(View.GONE);
	
			layoutAssetsLongTermAssets.setVisibility(View.GONE);
	
			layoutAssetsCurrentLiabilities.setVisibility(View.GONE);
	
			layoutAssetsLongTermDebts.setVisibility(View.GONE);
		
			layoutAssetsOwnerEquity.setVisibility(View.GONE);
	
			layoutAssetsTotalAssets.setVisibility(View.GONE);
	
			layoutAssetsCashQuivalents.setVisibility(View.GONE);
	
			layoutShortTermInvestment.setVisibility(View.GONE);
		
			layoutAssetsShortTermReceivables.setVisibility(View.GONE);
		
			layoutAssetsProvisionDoubtfulDebts.setVisibility(View.GONE);
	
			layoutAssetsFixedAssets.setVisibility(View.GONE);
		
			layoutAssetsUnderwritingReserves.setVisibility(View.GONE);
		
			layoutAssetsProvisionDiminution.setVisibility(View.GONE);
	
			layoutAssetsLongTermInvestments.setVisibility(View.GONE);
		
			layoutAssetsLongTermLiabilities.setVisibility(View.GONE);
			break;

			default:
				break;
			}
			return false;
		}
	});
	
	private void clearTextView(){
		tv1.setText("");
		tv2.setText("");
		tv3.setText("");
		tv4.setText("");
		tvNSL1.setText("");
		tvNSL2.setText("");
		tvNSL3.setText("");
		tvNSL4.setText("");
		tvPBT1.setText("");
		tvPBT2.setText("");
		tvPBT3.setText("");
		tvPBT4.setText("");
		tvGOP1.setText("");
		tvGOP2.setText("");
		tvGOP3.setText("");
		tvGOP4.setText("");
		tvTOTAL_EXPENSE1.setText("");
		tvTOTAL_EXPENSE2.setText("");
		tvTOTAL_EXPENSE3.setText("");
		tvTOTAL_EXPENSE4.setText("");
		tvNIAT1.setText("");
		tvNIAT2.setText("");
		tvNIAT3.setText("");
		tvNIAT4.setText("");
		tvPRINS1.setText("");
		tvPRINS2.setText("");
		tvPRINS3.setText("");
		tvPRINS4.setText("");
		tvNETREVINS1.setText("");
		tvNETREVINS2.setText("");
		tvNETREVINS3.setText("");
		tvNETREVINS4.setText("");
		tvGOPINS1.setText("");
		tvGOPINS2.setText("");
		tvGOPINS3.setText("");
		tvGOPINS4.setText("");
		tvFNINC1.setText("");
		tvFNINC2.setText("");
		tvFNINC3.setText("");
		tvFNINC4.setText("");
		tvTOTREV1.setText("");
		tvTOTREV2.setText("");
		tvTOTREV3.setText("");
		tvTOTREV4.setText("");
		tvOPREXP1.setText("");
		tvOPREXP2.setText("");
		tvOPREXP3.setText("");
		tvOPREXP4.setText("");
		tvGENADMEX1.setText("");
		tvGENADMEX2.setText("");
		tvGENADMEX3.setText("");
		tvGENADMEX4.setText("");
		tvCA1.setText("");
		tvCA2.setText("");
		tvCA3.setText("");
		tvCA4.setText("");
		tvLT_ASSET1.setText("");
		tvLT_ASSET2.setText("");
		tvLT_ASSET3.setText("");
		tvLT_ASSET4.setText("");
		tvCURLIB1.setText("");
		tvCURLIB2.setText("");
		tvCURLIB3.setText("");
		tvCURLIB4.setText("");
		tvLT_DEBT1.setText("");
		tvLT_DEBT2.setText("");
		tvLT_DEBT3.setText("");
		tvLT_DEBT4.setText("");
		tvOWNEQ1.setText("");
		tvOWNEQ2.setText("");
		tvOWNEQ3.setText("");
		tvOWNEQ4.setText("");
		tvTA1.setText("");
		tvTA2.setText("");
		tvTA3.setText("");
		tvTA4.setText("");
		tvCACE1.setText("");
		tvCACE2.setText("");
		tvCACE3.setText("");
		tvCACE4.setText("");
		tvSHINV1.setText("");
		tvSHINV2.setText("");
		tvSHINV3.setText("");
		tvSHINV4.setText("");
		tvSHREC1.setText("");
		tvSHREC2.setText("");
		tvSHREC3.setText("");
		tvSHREC4.setText("");
		tvPRDEBT1.setText("");
		tvPRDEBT2.setText("");
		tvPRDEBT3.setText("");
		tvPRDEBT4.setText("");
		tvFXASST1.setText("");
		tvFXASST2.setText("");
		tvFXASST3.setText("");
		tvFXASST4.setText("");
		tvUNRESV1.setText("");
		tvUNRESV2.setText("");
		tvUNRESV3.setText("");
		tvUNRESV4.setText("");
		tvPRDIMINV1.setText("");
		tvPRDIMINV2.setText("");
		tvPRDIMINV3.setText("");
		tvPRDIMINV4.setText("");
		tvPRDEBT1.setText("");
		tvPRDEBT2.setText("");
		tvPRDEBT3.setText("");
		tvPRDEBT4.setText("");
		tvLONGINV1.setText("");
		tvLONGINV2.setText("");
		tvLONGINV3.setText("");
		tvLONGINV4.setText("");
		tvLIABILITY1.setText("");
		tvLIABILITY2.setText("");
		tvLIABILITY3.setText("");
		tvLIABILITY4.setText("");
	}

	private class OnStockFinancialFragmentClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.fragment_stock_financial_btn_quarter:
				clearTextView();
				onTabPress(0);
				break;

			case R.id.fragment_stock_financial_btn_year:
				clearTextView();
				onTabPress(1);
				break;
			}
		}
	}
}

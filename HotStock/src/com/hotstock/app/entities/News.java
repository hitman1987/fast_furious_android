package com.hotstock.app.entities;

import java.util.ArrayList;


public class News {
	public int id;
	public String title;
    public String short_content;
    public String content;
    public String published_time;    
    
    public ArrayList<Tags> tags;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getShort_content() {
		return short_content;
	}
	public void setShort_content(String short_content) {
		this.short_content = short_content;
	}	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPublished_time() {
		return published_time;
	}
	public void setPublished_time(String published_time) {
		this.published_time = published_time;
	}
	public ArrayList<Tags> getTags() {
		return tags;
	}
	public void setTags(ArrayList<Tags> tags) {
		this.tags = tags;
	}
	
	
}

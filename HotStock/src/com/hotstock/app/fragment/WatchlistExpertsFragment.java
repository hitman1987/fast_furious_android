package com.hotstock.app.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.hotstock.R;
import com.hotstock.app.AppController;
import com.hotstock.app.adapter.WatchlistFundamentalAdapter;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.WatchList;
import com.hotstock.app.views.WatchListNoteActivity;

public class WatchlistExpertsFragment extends Fragment{
	
	private View rootView;
	
	private ListView listFundamental;
	private ListView listTranding;
	private Intent intentWatchList;
	private ArrayList<WatchList> mDataFundamental = new ArrayList<WatchList>();
	private WatchlistFundamentalAdapter mAdapterFundamental;
	private ArrayList<WatchList> mDataTranding = new ArrayList<WatchList>();
	private WatchlistFundamentalAdapter mAdapterTranding;
	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	
	private static final int GET_FUNDAMENTAL_SUCCESS = 0;
	private static final int GET_FUNDAMENTAL_FAIL = 1;
	private static final int GET_TRENDING_SUCCESS = 2;
	private static final int GET_TRENDING_FAIL = 3;
	
	private static final String TAG = "Watchlist Experts Fundamental Fragment";
	private ProgressDialog pDialog;
	
	private final int limitFundamental = 20;
	private int offsetFundamental = 0;
	private boolean isLoadingFundamental = false;
	
	private final int limitTranding = 20;
	private int offsetTranding = 0;
	private boolean isLoadingTranding = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_watchlist_experts, container, false);
	    initView(rootView);
	    return rootView;
	}
	
	private void initView(View v){
		if(listFundamental == null){
			listFundamental = (ListView) v.findViewById(R.id.fragment_watchlist_expert_list_fundatental);
			
			listFundamental.setOnScrollListener(new OnScrollListener() {
				
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					
				}
				
				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,
						int visibleItemCount, int totalItemCount) {
					// TODO Auto-generated method stub
					if (totalItemCount == 0 || visibleItemCount == 0) {
						return;
					}
					if (totalItemCount == firstVisibleItem + visibleItemCount && !isLoadingFundamental) {
						// 排他制御フラグをON
						isLoadingFundamental = true;
						loadDataFundamental(true);
					}
				}
			});
			
			listFundamental.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					intentWatchList = new Intent(AppController.getAppContext(), WatchListNoteActivity.class);
					intentWatchList.putExtra("id", mDataFundamental.get(position).getId());
					intentWatchList.putExtra("statusEdit", false);
					startActivity(intentWatchList);
				}
			});
		}
		
		if(listTranding == null){
			listTranding = (ListView) v.findViewById(R.id.fragment_watchlist_expert_list_tranding);
			
			listTranding.setOnScrollListener(new OnScrollListener() {
				
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					
				}
				
				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,
						int visibleItemCount, int totalItemCount) {
					// TODO Auto-generated method stub
					if (totalItemCount == 0 || visibleItemCount == 0) {
						return;
					}
					if (totalItemCount == firstVisibleItem + visibleItemCount && !isLoadingTranding) {
						// 排他制御フラグをON
						isLoadingTranding = true;
						loadDataTranding(true);
					}
				}
			});
			
			listTranding.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					intentWatchList = new Intent(AppController.getAppContext(), WatchListNoteActivity.class);
					intentWatchList.putExtra("id", mDataTranding.get(position).getId());
					intentWatchList.putExtra("statusEdit", false);
					startActivity(intentWatchList);
				}
			});
		}
		
		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		
		//loadData();
		loadDataFundamental(false);
		loadDataTranding(false);
	}
	
	private void loadDataFundamental(final boolean isLoadMore) {
		showProgressDialog();
		isLoadingFundamental = true;
		if (!isLoadMore) {
			offsetFundamental = 0;
		} else {
			offsetFundamental += limitFundamental;
		}
		
		restService.getWatchlistFundamental(limitFundamental, offsetFundamental, new IStockResponseListener<ArrayList<WatchList>>() {
			
			@Override
			public void onResponse(ArrayList<WatchList> data) {
				hideProgressDialog();
				isLoadingFundamental = false;
				if (!isLoadMore) {
					mDataFundamental.clear();
				}
				if(data.size()> 0){
					for (WatchList item : data) {
						mDataFundamental.add(item);
					}
				}else{
					isLoadingFundamental = true;
				}
				
				//mDataNews = data;
				mHandler.sendEmptyMessage(GET_FUNDAMENTAL_SUCCESS);
			}
			
			@Override
			public void onError(Exception ex) {
				hideProgressDialog();
				ex.printStackTrace();
				isLoadingFundamental = false;
				mHandler.sendEmptyMessage(GET_FUNDAMENTAL_FAIL);
			}
		});
	}
	
	private void loadDataTranding(final boolean isLoadMore) {
		showProgressDialog();
		isLoadingTranding = true;
		if (!isLoadMore) {
			offsetTranding = 0;
		} else {
			offsetTranding += limitTranding;
		}
		
		restService.getWatchlistTranding(limitTranding, offsetTranding, new IStockResponseListener<ArrayList<WatchList>>() {
			
			@Override
			public void onResponse(ArrayList<WatchList> data) {
				hideProgressDialog();
				isLoadingTranding = false;
				if (!isLoadMore) {
					mDataTranding.clear();
				}
				if(data.size()> 0){
					for (WatchList item : data) {
						mDataTranding.add(item);
					}
				}else{
					isLoadingTranding = true;
				}
				
				
				//mDataNews = data;
				mHandler.sendEmptyMessage(GET_TRENDING_SUCCESS);
			}
			
			@Override
			public void onError(Exception ex) {
				hideProgressDialog();
				ex.printStackTrace();
				isLoadingTranding = false;
				mHandler.sendEmptyMessage(GET_TRENDING_FAIL);
			}
		});
	}
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}
	
	private Handler mHandler =new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case GET_FUNDAMENTAL_SUCCESS:
				hideProgressDialog();
				if(mAdapterFundamental == null){
					mAdapterFundamental = new WatchlistFundamentalAdapter(AppController.getAppContext(),mDataFundamental);
					listFundamental.setAdapter(mAdapterFundamental);
				}else{
					mAdapterFundamental.notifyDataSetChanged();
				}								
				break;
			case GET_FUNDAMENTAL_FAIL:
				
				break;
			case GET_TRENDING_SUCCESS:
				hideProgressDialog();
				if(mAdapterTranding == null){
					mAdapterTranding = new WatchlistFundamentalAdapter(AppController.getAppContext(),mDataTranding);
					listTranding.setAdapter(mAdapterTranding);
				}else{
					mAdapterTranding.notifyDataSetChanged();
				}								
				break;
			case GET_TRENDING_FAIL:
	
				break;

			default:
				break;
			}
			return false;
		}
	});
}

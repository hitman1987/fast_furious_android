package com.hotstock.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.Consults;

public class StockConsensusAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private List<Consults> mData;
	private Context mContext;

	public StockConsensusAdapter(Context c, List<Consults> consults) {
		this.mContext = c;
		this.mData = consults;
		mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		if (mData == null || mData.size() <= 0) {
			return 0;
		}
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(com.hotstock.R.layout.stock_consensus_item, null);
			viewHolder.tvBuy = (TextView) convertView.findViewById(R.id.consensus_tv_buy);
			viewHolder.tvSell = (TextView) convertView.findViewById(R.id.consensus_tv_sell);
			viewHolder.tvHold = (TextView) convertView.findViewById(R.id.consensus_tv_hold);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		Consults data = mData.get(position);
		viewHolder.tvSell.setText(data.getSell() + "");
		viewHolder.tvBuy.setText(data.getBuy() + "");
		viewHolder.tvHold.setText(data.getHold() + "");

		return convertView;
	}

	private class ViewHolder {
		private TextView tvBuy;
		private TextView tvSell;
		private TextView tvHold;
	}

}

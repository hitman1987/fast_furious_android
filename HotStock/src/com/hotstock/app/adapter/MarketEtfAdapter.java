package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.ETF;
import com.hotstock.app.utils.Utils;

public class MarketEtfAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private ArrayList<ETF> mData = new ArrayList<ETF>();
	private Context mContext;

	public MarketEtfAdapter(Context c, ArrayList<ETF> d) {
		this.mContext = c;
		this.mData = d;
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(
					R.layout.adapter_etf_view, null);
			viewHolder.layoutMain = (ViewGroup) convertView
					.findViewById(R.id.items_etf_layout);
			viewHolder.txtFundName = (TextView) convertView
					.findViewById(R.id.item_etf_fund_name);
			viewHolder.txtNav = (TextView) convertView
					.findViewById(R.id.item_etf_nav);
			viewHolder.txtLastPrice = (TextView) convertView
					.findViewById(R.id.item_etf_last_price);
			viewHolder.txtSLCCQ = (TextView) convertView
					.findViewById(R.id.item_etf_slccq);
			viewHolder.txtSLCCQChange = (TextView) convertView
					.findViewById(R.id.item_etf_slccq_change);
			viewHolder.txtDisPrenium = (TextView) convertView
					.findViewById(R.id.item_etf_prenium);
			viewHolder.txtDiscount = (TextView) convertView
					.findViewById(R.id.item_etf_discout);
			viewHolder.txtUpdatedDate = (TextView) convertView
					.findViewById(R.id.item_etf_updated_date);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		ETF currentItem = mData.get(position);

		
		int colorChange;
		
		if(String.valueOf(currentItem.getPremium()).equals("0.00")){
			colorChange = R.color.kPriceRefColor;
		}else{
			if(String.valueOf(currentItem.getPremium()).substring(0, 1).equals("-")){
				colorChange = R.color.kPriceDowncColor;
			}else{
				colorChange = R.color.kPriceUpColor;
			}
		}
		viewHolder.txtFundName.setText(currentItem.getFundname());
		viewHolder.txtNav.setText(Utils.formatAsETFSLCCQ(currentItem.getNav()));
		viewHolder.txtLastPrice.setText(Utils.formatETFPrice(currentItem.getLast_price()));
		viewHolder.txtSLCCQ.setText(Utils.formatAsETFSLCCQ(currentItem.getSlccq()));
		viewHolder.txtSLCCQChange.setText(Utils.formatAsETFSLCCQ(currentItem.getSlccq_change()));
		viewHolder.txtDisPrenium.setText(String.valueOf(currentItem.getDiscount()));
		viewHolder.txtDisPrenium.setTextColor(mContext.getResources().getColor(getColorDiscout(currentItem.getDiscount())));
		viewHolder.txtDiscount.setText((String.valueOf(currentItem.getPremium()) + "%"));
		viewHolder.txtDiscount.setTextColor(mContext.getResources().getColor(colorChange));
		viewHolder.txtUpdatedDate.setText(Utils.convertStringtoETFDate(mData.get(position).getUpdated_time()));

		if (position % 2 == 0) {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_1));
		} else {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_2));
		}

		return convertView;
	}
	private class ViewHolder {
		private ViewGroup layoutMain;
		private TextView txtFundName;
		private TextView txtNav;
		private TextView txtLastPrice;
		private TextView txtSLCCQ;
		private TextView txtSLCCQChange;
		private TextView txtDisPrenium;
		private TextView txtDiscount;
		private TextView txtUpdatedDate;
	}
	
	private int getColorDiscout(int discount){
		int color;
		if(discount == 0){
			color = R.color.kPriceRefColor;
		}else{
			if(discount < 0){
				color = R.color.kPriceDowncColor;
			}else{
				color = R.color.kPriceUpColor;
			}
		}
		return color;
	}

}

package com.hotstock.app.entities;

public class StockFinancialItems {
	public String title;
	public String NIAT;
	public String PRINS;
	public String NETREVINS;
	public String GOPINS;
	public String FNINC;
	public String TOTREV;
	public String OPREXP;
	public String GENADMEX;
	public String NSL;
	public String PBT;
	public String GOP;
	public String TOTAL_EXPENSE;
	public String CURRENT_ASSET;
	public String LT_ASSET;
	public String CURLIB;
	public String LT_DEBT;
	public String OWNEQ;
	public String TOTAL_ASSET;
	public String CACE;
	public String SHINV;
	public String SHREC;
	public String PRDEBT;
	public String FXASST;
	public String UNRESV;
	public String PRDIMINV;
	public String LONGINV;
	public String LIABILITY;
}
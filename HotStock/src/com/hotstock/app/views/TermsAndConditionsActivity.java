package com.hotstock.app.views;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import com.hotstock.R;


public class TermsAndConditionsActivity extends Activity implements
		OnClickListener {

	private ImageButton imgBack;
	private WebView mWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.terms_and_condition);
		initView();
	}

	private void initView() {
		if (mWebView == null) {
			mWebView = (WebView) findViewById(R.id.webView);
			mWebView.loadUrl("file:///android_asset/khuyen_cao.html");
			mWebView.getSettings().setJavaScriptEnabled(true);
			mWebView.getSettings().setSaveFormData(true);
			mWebView.getSettings().setBuiltInZoomControls(true);
			mWebView.setWebViewClient(new MyWebViewClient());
		}

		if (imgBack == null) {
			imgBack = (ImageButton) findViewById(R.id.terms_and_codition_img_back);
			imgBack.setOnClickListener(this);
		}
	}

	private class MyWebViewClient extends WebViewClient {

		@Override
		// show the web page in webview but not in web browser
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.terms_and_codition_img_back:
			finish();
			break;

		default:
			break;
		}
	}
}

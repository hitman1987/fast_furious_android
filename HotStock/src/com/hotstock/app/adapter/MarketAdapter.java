package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.Markets;
import com.hotstock.app.utils.AutofitHelper;
import com.hotstock.app.utils.Utils;

public class MarketAdapter extends BaseAdapter {

	private ArrayList<Markets> mData = new ArrayList<Markets>();
	private LayoutInflater mLayoutInflater;
	private Context mContext;

	public MarketAdapter(Context c,ArrayList<Markets> d) {
		this.mContext = c;
		this.mData = d;
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(
					com.hotstock.R.layout.market_items, null);
			viewHolder.layoutMain = (LinearLayout) convertView
					.findViewById(R.id.market_items_layout_main);
			viewHolder.txtMatketName = (TextView) convertView.findViewById(R.id.market_items_txt_marketName);
			viewHolder.txtMarketIndex = (TextView) convertView.findViewById(R.id.market_items_txt_index);
			viewHolder.txtMarketChange = (TextView) convertView.findViewById(R.id.market_items_txt_change);
			viewHolder.txtMarketValue = (TextView) convertView.findViewById(R.id.market_items_txt_value);
			AutofitHelper.create(viewHolder.txtMarketValue);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		int textColor = Utils.getStockColor(Utils.getPriceTrend(mData.get(position).getIndices().get(0).getPercentage_change()));
		viewHolder.txtMatketName.setText(mData.get(position).getName());
		viewHolder.txtMarketIndex.setText(Utils.formatMarketIndex(mData.get(position).getIndices().get(0).getIndex()));
		viewHolder.txtMarketIndex.setTextColor(mContext.getResources().getColor(textColor));
		viewHolder.txtMarketChange.setText(String.valueOf(mData.get(position).getIndices().get(0).getPercentage_change())  + "%");
		viewHolder.txtMarketChange.setTextColor(mContext.getResources().getColor(textColor));
		String value = mData.get(position).getIndices().get(0).getValue().trim();
		if(value != null && value.length() > 6){
			value = value.substring(0, value.length() - 6);
			viewHolder.txtMarketValue.setText(Utils.formatAsStockVolume(Double.parseDouble(value)));
		}else{
			viewHolder.txtMarketValue.setText("0");
		}
		
		
		if (position % 2 == 0) {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources().getColor(R.color.market_list_item_1));
		} else {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources().getColor(R.color.market_list_item_2));
		}

		return convertView;
	}

	private class ViewHolder {
		private LinearLayout layoutMain;
		private TextView txtMatketName;
		private TextView txtMarketIndex;
		private TextView txtMarketChange;
		private TextView txtMarketValue;
	}
}

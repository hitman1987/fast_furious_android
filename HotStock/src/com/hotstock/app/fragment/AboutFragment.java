package com.hotstock.app.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hotstock.R;

public class AboutFragment extends Fragment {
	
	private TextView txtContent;
	
	public AboutFragment(){}	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
         txtContent = (TextView) rootView.findViewById(R.id.fragment_about_txt_content); 
        
        return rootView;
    }
}

package com.hotstock.app.views;

import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hotstock.R;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.News;

public class NewDetailsActivity extends Activity implements OnTouchListener{
	
	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	
	private Context mContext;
	private TextView txtTitle;
	private TextView txtDescription;
	private WebView webView;
	private WebView webViewLoadPDF;
	private ImageView imgBack;
	private ProgressDialog pDialog;
	
	private int idNews;
	private ArrayList<News> newDetails = new ArrayList<News>();
	
	private static final int LOAD_DATA_SUCCESS = 0;
	private static final int LOAD_DATA_FAILED = 1;
	private static final int CLICK_ON_URL = 3;
	private static final int CLICK_ON_WEBVIEW = 4;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_details);
		initView();
	}
	
	private void initView(){
		mContext = this;
		
		idNews = getIntent().getExtras().getInt("idNews", -1);
		
		if(webView == null){
			webView = (WebView) findViewById(R.id.webview);
			webView.setOnTouchListener(this);
			webView.setWebViewClient(new WebViewClient(){
				public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
					if (url.contains(".pdf") || url.contains(".doc") || url.contains(".ppt")|| url.contains(".xls")) {
						System.out.println("url = " + url);
						loadOfficeFile(url);
		                return getCssWebResourceResponseFromAsset();
		            } else {
		                return super.shouldInterceptRequest(view, url);
		            }
				};
				
				/**
		         * Return WebResourceResponse with CSS markup from an asset (e.g. "assets/style.css"). 
		         */
		        private WebResourceResponse getCssWebResourceResponseFromAsset() {
		        	
		            return null;
		        }

		        /**
		         * Return WebResourceResponse with CSS markup from a raw resource (e.g. "raw/style.css"). 
		         */
		        private WebResourceResponse getCssWebResourceResponseFromRawResource() {
		            return null;
		        }

		        private WebResourceResponse getUtf8EncodedCssWebResourceResponse(InputStream data) {
		            return new WebResourceResponse("text/css", "UTF-8", data);
		        }
			});
		}

		
		if(webViewLoadPDF == null){
			webViewLoadPDF = (WebView) findViewById(R.id.webview_load_pdf);
		}
		
		if(txtDescription == null){
			txtDescription = (TextView) findViewById(R.id.activity_new_details_txt_description);
		}
		
		if(txtTitle == null){
			txtTitle = (TextView) findViewById(R.id.activity_new_details_txt_title);
		}
		
		if(imgBack == null){
			imgBack = (ImageView) findViewById(R.id.activity_new_details_img_back);
			imgBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					finish();
				}
			});
		}	
		
		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		
		loadData();
	}
	
	private void loadOfficeFile(final String url){
		runOnUiThread(new Runnable() {
		    public void run(){   
		    	webView.setVisibility(View.GONE);
				webViewLoadPDF.setVisibility(View.VISIBLE);
				webViewLoadPDF.loadUrl("https://docs.google.com/gview?embedded=true&url="+ url);
		    }
		});		
	}
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}
	
	private void loadData(){
		showProgressDialog();
		restService.getNewDetails(idNews, new IStockResponseListener<ArrayList<News>>() {
			
			@Override
			public void onResponse(ArrayList<News> data) {
				hideProgressDialog();
				newDetails.addAll(data);
				mHandler.sendEmptyMessage(LOAD_DATA_SUCCESS);
			}
			
			@Override
			public void onError(Exception ex) {
				hideProgressDialog();
				ex.printStackTrace();
				mHandler.sendEmptyMessage(LOAD_DATA_FAILED);
			}
		});
	}
	
	private Handler mHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case LOAD_DATA_SUCCESS:
				webView.loadDataWithBaseURL(null, newDetails.get(0).getContent(), "text/html", "UTF-8", null);
				txtTitle.setText(newDetails.get(0).getTitle());
				if(newDetails.get(0).getContent() != null && newDetails.get(0).getContent().length()>0){
					txtDescription.setText(Html.fromHtml(newDetails.get(0).getContent()));
				}
				
				break;
				
			case LOAD_DATA_FAILED:
				Toast.makeText(mContext, "Load data failed!", Toast.LENGTH_SHORT).show();
				break;

			case CLICK_ON_URL:
			//	Toast.makeText(mContext, "FUCK YOU URL", Toast.LENGTH_SHORT).show();
				break;
				
			case CLICK_ON_WEBVIEW:
				//Toast.makeText(mContext, "WebView clicked", Toast.LENGTH_SHORT).show();
				break;
				
			default:
				break;
			}
			return false;
		}
	});

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v.getId() == R.id.webview && event.getAction() == MotionEvent.ACTION_DOWN){
	        mHandler.sendEmptyMessageDelayed(CLICK_ON_WEBVIEW, 500);
	    }
	    return false;
	}
	
	
}

package com.hotstock.app.views;

import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.Note;
import com.hotstock.app.entities.WatchListNote;
import com.hotstock.app.utils.Utils;

public class WatchListNoteActivity extends Activity implements OnClickListener, OnTouchListener{
	
	private ImageButton btnBack;
	private TextView txtStock;
	
	private TextView txtValue2;
	private TextView txtValue3;
	private TextView txtValue4;
	
	private TextView txtStockCode;
	private TextView txtStockStatus;
	private TextView txtStockCurrentPrice;
	private TextView txtStockTargetPrice;
	
	private boolean statusEdit;
	private WebView webView;
	private WebView webViewLoadPDF;
	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	private WatchListNote mWatchListNote = new WatchListNote();
	private Note mDataNote = new Note();
	
	private int id;
	private ProgressDialog pDialog;
	
	private static final int GET_NOTE_SUCCESS = 0;
	private static final int GET_NOTE_FAIL = 1;
	private static final int CLICK_ON_WEBVIEW = 2;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.watchlist_note);
		initView();
	}
	
	private void initView(){
		
		statusEdit = getIntent().getBooleanExtra("statusEdit", false);
		id = getIntent().getExtras().getInt("id", -1);
		
		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		
		if(btnBack == null){
			btnBack = (ImageButton) findViewById(R.id.watchlist_note_img_back);
		}
		
		if(txtStock == null){
			txtStock = (TextView) findViewById(R.id.watchlist_note_txt_stock);
		}
		
		///////////////////-----UI PORTFOLIO------/////////////////////////////
		if(txtValue2 == null){
			txtValue2 = (TextView) findViewById(R.id.watchlist_note_txt_value_2);
		}
		if(txtValue3 == null){
			txtValue3 = (TextView) findViewById(R.id.watchlist_note_txt_value_3);
		}
		if(txtValue4 == null){
			txtValue4 = (TextView) findViewById(R.id.watchlist_note_txt_value_4);
		}
		
		
		if(txtStockCode == null){
			txtStockCode = (TextView) findViewById(R.id.watchlist_note_txt_stock_code);
		}
		if(txtStockStatus == null){
			txtStockStatus = (TextView) findViewById(R.id.watchlist_note_txt_status);
		}
		if(txtStockCurrentPrice == null){
			txtStockCurrentPrice = (TextView) findViewById(R.id.watchlist_note_txt_current_price);
		}
		if(txtStockTargetPrice == null){
			txtStockTargetPrice = (TextView) findViewById(R.id.watchlist_note_txt_target_price);
		}
		
		if(webView == null){
			webView = (WebView) findViewById(R.id.watchlist_note_webview);
			webView.setOnTouchListener(this);
			webView.setWebViewClient(new WebViewClient(){
				public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
					if (url.contains(".pdf")) {
						System.out.println("url = " + url);
						loadPDF(url);
		                return getCssWebResourceResponseFromAsset();
		            } else {
		                return super.shouldInterceptRequest(view, url);
		            }
				};
				
				/**
		         * Return WebResourceResponse with CSS markup from an asset (e.g. "assets/style.css"). 
		         */
		        private WebResourceResponse getCssWebResourceResponseFromAsset() {
		        	
		            return null;
		        }

		        /**
		         * Return WebResourceResponse with CSS markup from a raw resource (e.g. "raw/style.css"). 
		         */
		        private WebResourceResponse getCssWebResourceResponseFromRawResource() {
		            return null;
		        }

		        private WebResourceResponse getUtf8EncodedCssWebResourceResponse(InputStream data) {
		            return new WebResourceResponse("text/css", "UTF-8", data);
		        }
			});
		}
		
		if(webViewLoadPDF == null){
			webViewLoadPDF = (WebView) findViewById(R.id.watchlist_note_webview_load_pdf);
		}
		
		loadData();	
		listener();
		
	}
	
	private void listener(){
		btnBack.setOnClickListener(this);
	}
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}
	
	private void loadData(){
		showProgressDialog();
		restService.getWatchlistNote(id, new IStockResponseListener<ArrayList<WatchListNote>>() {
			
			@Override
			public void onResponse(ArrayList<WatchListNote> data) {
				if(data!= null && data.size()>0){
					mWatchListNote = data.get(0);
					mHandler.sendEmptyMessage(GET_NOTE_SUCCESS);
				}			
			}
			
			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
				mHandler.sendEmptyMessage(GET_NOTE_FAIL);
			}
		});
	}
	
	private void loadPDF(final String url){
		runOnUiThread(new Runnable() {
		    public void run(){   
		    	webView.setVisibility(View.GONE);
				webViewLoadPDF.setVisibility(View.VISIBLE);
				webViewLoadPDF.loadUrl("https://docs.google.com/gview?embedded=true&url="+ url);
		    }
		});		
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.watchlist_note_img_back:
			finish();
			break;

		default:
			break;
		}
	}
	
	private Handler mHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case GET_NOTE_SUCCESS:
				hideProgressDialog();
				txtStock.setText(mWatchListNote.getCode());
				txtStockCode.setText(mWatchListNote.getCode());
				txtStockStatus.setText(mWatchListNote.getRate());
				txtStockStatus.setTextColor(getResources().getColor(Utils.colorStatus(mWatchListNote.getRate())));
				txtStockCurrentPrice.setText(Utils.formatAsStockVolume(mWatchListNote.getBasic_price()));
				txtStockTargetPrice.setText(Utils.formatAsStockVolume(mWatchListNote.getTargeted_price()));
				webView.loadDataWithBaseURL(null, mWatchListNote.getNote(), "text/html", "UTF-8", null);
				break;
				
			case GET_NOTE_FAIL:
				hideProgressDialog();
				break;


			default:
				break;
			}
			return false;
		}
	});


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v.getId() == R.id.webview && event.getAction() == MotionEvent.ACTION_DOWN){
	        mHandler.sendEmptyMessageDelayed(CLICK_ON_WEBVIEW, 500);
	    }
	    return false;
	}
}
package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.model.OverviewTestDto;

public class StockOverviewListAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private Context mContext;
	private ArrayList<OverviewTestDto> dtos;

	public StockOverviewListAdapter(Context c, ArrayList<OverviewTestDto> dtos) {
		this.mContext = c;
		this.dtos = dtos;
		mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return dtos.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(com.hotstock.R.layout.item_stock_overview, null);
			viewHolder.lnTitle = (LinearLayout) convertView.findViewById(R.id.item_stock_overview_ln_title);
			viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.item_stock_overview_tv_title);
			viewHolder.rlContent = (RelativeLayout) convertView.findViewById(R.id.item_stock_overview_rl_content);
			viewHolder.tvContent = (TextView) convertView.findViewById(R.id.item_stock_overview_tv_content);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		OverviewTestDto dto = dtos.get(position);

		if (dto.type == 0) {
			viewHolder.lnTitle.setVisibility(View.VISIBLE);
			viewHolder.rlContent.setVisibility(View.GONE);
			viewHolder.tvTitle.setText(dto.time);
		} else {
			viewHolder.lnTitle.setVisibility(View.GONE);
			viewHolder.rlContent.setVisibility(View.VISIBLE);
			viewHolder.tvContent.setText(dto.description);
		}

		return convertView;
	}

	private class ViewHolder {
		private LinearLayout lnTitle;
		private TextView tvTitle;
		private RelativeLayout rlContent;
		private TextView tvContent;
	}

}

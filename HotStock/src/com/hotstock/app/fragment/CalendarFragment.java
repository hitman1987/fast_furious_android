package com.hotstock.app.fragment;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.hotstock.R;
import com.hotstock.app.AppController;
import com.hotstock.app.adapter.EventAdapter;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.calendar.Day;
import com.hotstock.app.calendar.ExtendedCalendarView;
import com.hotstock.app.calendar.ExtendedCalendarView.OnDayClickListener;
import com.hotstock.app.entities.Event;
import com.hotstock.app.utils.IStockModel;

public class CalendarFragment extends Fragment{
	
	private View rootView;
	private ExtendedCalendarView calendarView;
	private String date;
	private ListView listviewEvent;
	private ImageButton btnBack;
	private EventAdapter eventAdapter;
	
	private ArrayList<Event> mDataEvents = new ArrayList<Event>();
	
	private ProgressDialog pDialog;
	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	
	private static final int SHOW_DATA_SUCCESS = 0;
	private static final int SHOW_DATA_FAIL = 1;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_calendar, container, false);
		initView(rootView);
		return rootView;
	}
	
	private void initView(View v){		
		if(calendarView == null){
			calendarView = (ExtendedCalendarView) v.findViewById(R.id.calendar);
			calendarView.setOnDayClickListener(new OnDayClickListener() {
				
				@Override
				public void onDayClicked(AdapterView<?> adapter, View view, int position,
						long id, Day day) {
					date = dateFormat(String.valueOf(day.getDay()),String.valueOf(day.getMonth()+1),String.valueOf(day.getYear()));
					loadData(IStockModel.getInstance().getAccessToken(), date);
				}
			});
		}
		
		if(listviewEvent == null){
			listviewEvent = (ListView) v.findViewById(R.id.fragment_calendar_listview);
		}
		
		if(btnBack == null){
			btnBack = (ImageButton) v.findViewById(R.id.fragment_img_back);
		}
		
		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_MONTH); 
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		
		loadData(IStockModel.getInstance().getAccessToken(), dateFormat(String.valueOf(day),String.valueOf(month+1),String.valueOf(year)));
	}
	
	private String dateFormat(String day,String month,String year){
		String date = null;
		date = year + "-" + month + "-" + day + " 00:00:00";
		return date;
	}
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}
	
	private void loadData(String token,String date){
		showProgressDialog();
		restService.getListEvent(token, date, new IStockResponseListener<ArrayList<Event>>() {
			
			@Override
			public void onResponse(ArrayList<Event> data) {
				if(data!= null){
					mDataEvents = data;
					mHandler.sendEmptyMessage(SHOW_DATA_SUCCESS);
				}else{
					mHandler.sendEmptyMessage(SHOW_DATA_FAIL);
				}
			}
			
			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
				mHandler.sendEmptyMessage(SHOW_DATA_FAIL);				
			}
		});
	}
	
	private Handler mHandler = new Handler(new Handler.Callback() {
			
			@Override
			public boolean handleMessage(Message msg) {
				switch (msg.what) {
				case SHOW_DATA_SUCCESS:
					hideProgressDialog();
					//if(eventAdapter == null){
						eventAdapter = new EventAdapter(AppController.getAppContext(),mDataEvents);
						listviewEvent.setAdapter(eventAdapter);
//					}else{
//						eventAdapter.notifyDataSetChanged();
//					}
					break;
				case SHOW_DATA_FAIL:
								
					break;
			
				default:
					break;
				}
				return false;
			}
		});
}

package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.MostActive;
import com.hotstock.app.utils.Utils;

public class MarketTradingMostActiveAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private ArrayList<MostActive> mData = new ArrayList<MostActive>();
	private Context mContext;

	public MarketTradingMostActiveAdapter(Context c, ArrayList<MostActive> d) {
		this.mContext = c;
		this.mData = d;
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(
					R.layout.adapter_most_active_view, null);
			viewHolder.layoutMain = (ViewGroup) convertView
					.findViewById(R.id.item_market_most_active);
			viewHolder.txtStock = (TextView) convertView
					.findViewById(R.id.market_most_active_stock);
			viewHolder.txtClosePrice = (TextView) convertView
					.findViewById(R.id.market_most_active_close_price);
			viewHolder.txtChange = (TextView) convertView
					.findViewById(R.id.market_most_active_chg);
			viewHolder.txtVolume = (TextView) convertView
					.findViewById(R.id.market_most_active_volume);
			viewHolder.txtStock.setTextColor(mContext.getResources().getColor(android.R.color.white));
			viewHolder.txtClosePrice.setTextColor(mContext.getResources().getColor(android.R.color.white));
			viewHolder.txtVolume.setTextColor(mContext.getResources().getColor(android.R.color.white));
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		MostActive currentItem = mData.get(position);
		
		int colorChange;
		
		if(currentItem.percentage_change.equals("0.00")){
			colorChange = R.color.kPriceRefColor;
		}else{
			if(currentItem.percentage_change.substring(0, 1).equals("-")){
				colorChange = R.color.kPriceDowncColor;
			}else{
				colorChange = R.color.kPriceUpColor;
			}
		}
		
		viewHolder.txtStock.setText(currentItem.code);
		viewHolder.txtClosePrice.setText(Utils.formatAsStockVolume(currentItem.close_price));
		viewHolder.txtChange.setText(currentItem.percentage_change);
		viewHolder.txtChange.setTextColor(mContext.getResources().getColor(colorChange));
		viewHolder.txtVolume.setText(Utils.formatAsStockVolume(currentItem.volume));

		if (position % 2 == 0) {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_1));
		} else {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_2));
		}

		return convertView;
	}
	private class ViewHolder {
		private ViewGroup layoutMain;
		private TextView txtStock;
		private TextView txtClosePrice;
		private TextView txtChange;
		private TextView txtVolume;
	}

}

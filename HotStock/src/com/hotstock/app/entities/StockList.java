package com.hotstock.app.entities;

import java.util.List;

public class StockList {
	public int id;
	public String code;
	public String name;
	public List<Stock> markets;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Stock> getMarkets() {
		return markets;
	}
	public void setMarkets(List<Stock> markets) {
		this.markets = markets;
	}
}

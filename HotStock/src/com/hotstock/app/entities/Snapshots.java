package com.hotstock.app.entities;

public class Snapshots {
	
	private int id;
	private String lowestin52weeks;
	private String highestin52weeks;
	private String updated_time;
	private long avaragevolumein10days;
	private String dividend_yield;
	private double market_capitalization;
	private long outstanding_shares;
	private long listed_shares;
	private long eps_4q;
	private String roa_trailing;
	private String roe_trailing;
	private double pe_trailing;
	private double pb;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLowestin52weeks() {
		return lowestin52weeks;
	}

	public void setLowestin52weeks(String lowestin52weeks) {
		this.lowestin52weeks = lowestin52weeks;
	}

	public String getHighestin52weeks() {
		return highestin52weeks;
	}

	public void setHighestin52weeks(String highestin52weeks) {
		this.highestin52weeks = highestin52weeks;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public double getMarket_capitalization() {
		return market_capitalization;
	}

	public void setMarket_capitalization(double market_capitalization) {
		this.market_capitalization = market_capitalization;
	}

	public double getPe_trailing() {
		return pe_trailing;
	}

	public void setPe_trailing(double pe_trailing) {
		this.pe_trailing = pe_trailing;
	}

	public double getPb() {
		return pb;
	}

	public void setPb(double pb) {
		this.pb = pb;
	}

	public String getDividend_yield() {
		return dividend_yield;
	}

	public void setDividend_yield(String dividend_yield) {
		this.dividend_yield = dividend_yield;
	}

	public long getOutstanding_shares() {
		return outstanding_shares;
	}

	public void setOutstanding_shares(long outstanding_shares) {
		this.outstanding_shares = outstanding_shares;
	}

	public long getListed_shares() {
		return listed_shares;
	}

	public void setListed_shares(long listed_shares) {
		this.listed_shares = listed_shares;
	}

	public long getEps_4q() {
		return eps_4q;
	}

	public void setEps_4q(long eps_4q) {
		this.eps_4q = eps_4q;
	}

	public String getRoa_trailing() {
		return roa_trailing;
	}

	public void setRoa_trailing(String roa_trailing) {
		this.roa_trailing = roa_trailing;
	}

	public String getRoe_trailing() {
		return roe_trailing;
	}

	public void setRoe_trailing(String roe_trailing) {
		this.roe_trailing = roe_trailing;
	}

	public long getAvaragevolumein10days() {
		return avaragevolumein10days;
	}

	public void setAvaragevolumein10days(long avaragevolumein10days) {
		this.avaragevolumein10days = avaragevolumein10days;
	}
}

package com.hotstock.app.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.hotstock.app.AppController;
import com.hotstock.app.entities.APIMeta;
import com.hotstock.app.entities.Consensus;
import com.hotstock.app.entities.ETF;
import com.hotstock.app.entities.Event;
import com.hotstock.app.entities.ForeignTrades;
import com.hotstock.app.entities.MainWatchList;
import com.hotstock.app.entities.MarketChart;
import com.hotstock.app.entities.Markets;
import com.hotstock.app.entities.MostActive;
import com.hotstock.app.entities.News;
import com.hotstock.app.entities.Note;
import com.hotstock.app.entities.Portfolio;
import com.hotstock.app.entities.StockLastDay;
import com.hotstock.app.entities.StockList;
import com.hotstock.app.entities.StockOverviews;
import com.hotstock.app.entities.StockSnapshots;
import com.hotstock.app.entities.StockStatistic;
import com.hotstock.app.entities.StockTrades;
import com.hotstock.app.entities.WatchList;
import com.hotstock.app.entities.WatchListNote;
import com.hotstock.app.utils.Enums;
import com.hotstock.app.utils.PreferenceUtils;
import com.hotstock.app.utils.QueryString;
import com.hotstock.app.utils.Utils;

public class IStockRestfulServiceImpl implements IStockRestfulService {
	
	private static final Logger logger = LoggerFactory.getLogger(IStockRestfulServiceImpl.class);
	
//	private static final String SVG_SERVER ="http://api0804.aws.sly.io/";
	private static final String SVG_SERVER ="http://api.istockvina.com/";
	private static final String SVG_URL_LOGIN_API ="user/login";
	private static final String SVG_URL_CREATE_API ="user/create";
	private static final String SVG_URL_GET_MARKET_LIST = "market/list";
	private static final String SVG_URL_GET_NEWS = "news/top";
	private static final String SVG_URL_GET_MARKET_CHART ="";
	private static final String SVG_URL_GET_STOCK ="stock/";
	private static final String SVG_URL_GET_LIST_STOCK = "stock/list";
	private static final String SVG_URL_GET_STOCK_FINANCIAL = "finance/";
	private static final String SVG_URL_GET_STOCK_NEWS="news/tag/";
	private static final String SVG_URL_GET_NEW_DETAIL="news/details/";
	private static final String SVG_URL_GET_STOCK_STATISTIC ="/consult";
	private static final String SVG_URL_GET_MARKET_ETF="ETF/list";
	private static final String SVG_URL_GET_STOCK_SNAPSHOTS ="/snapshot";
	private static final String SVG_URL_GET_STOCK_OVERVIEW ="/overview";
	private static final String SVG_URL_GET_WATCHLIST="watchlist/";
	private static final String SVG_URL_GET_WATCHLIST_TRANDING ="watchlist/trending";
	private static final String SVG_URL_GET_WATCHLIST_FUNDAMENTAL="watchlist/fundamental";
	private static final String SVG_URL_GET_WATCHLIST_NOTE="/detail";
	private static final String SVG_URL_GET_PORFOLIO = "portfolio";
	private static final String SVG_URL_ADD_STOCK_PORFOLIO = "portfolio/create";
	private static final String SVG_URL_UPDATE_STOCK_PORFOLIO = "portfolio/update";
	private static final String SVG_URL_DELETE_STOCK_PORFOLIO = "portfolio/delete";
	private static final String SVG_URL_GET_NEWS_MARKET = "news/market/";
	private static final String SVG_URL_GET_TRADING_SUMMARY = "tradingsummary/";
	private static final String SVG_URL_GET_TRADING_MOST_ACTIVE = "mostactives/";
	private static final String SVG_URL_GET_FOREIGN_TRADE = "/foreigntrades";
	private static final String SVG_URL_GET_MAIN_WATCHLIST ="mainscreen/watchlist";
	private static final String SVG_URL_GET_PORTFOLIO_NOTE="/note";
	private static final String SVG_URL_GET_STOCK_LASTDAY = "/lastday";
	private static final String SVG_URL_GET_LIST_EVENT = "event";
	private static final String SVG_URL_GET_CONSENSUS = "consensus";
	private static final String SVG_URL_GET_DAILYMARKET = "dailymarketindex/list";
	private static final String SVG_URL_UPDATE_PROFILE="user/update";
	
	private static String tag_login = "login";
	private static String tag_register = "register";
	private static String tag_get_market_list = "get market list";
	private static String tag_get_get_news = "get news";
	private static String tag_get_get_market_chart = "get market chart";
	private static String tag_get_get_stock = "get stock";
	private static String tag_get_get_stock_trades = "get stock trades";
	private static String tag_get_stock_list = "get stock list";
	private static String tag_get_stock_finacial_quarter = "get stock quarter";
	private static String tag_get_stock_finacial_year = "get stock year";
	private static String tag_get_stock_news = "get stock news";
	private static String tag_get_new_details = "get new details";
	private static String tag_get_stock_statistic = "get stock statistic ";
	private static String tag_get_market_etf ="get market etf";
	private static String tag_get_stock_snapshots ="get stock snapshots";
	private static String tag_get_stock_overview ="get stock overview";
	private static String tag_get_watchlist ="get watch list";
	private static String tag_get_watchlist_fundamental ="get watchlist fundamental";
	private static String tag_get_watchlist_tranding ="get watchlist tranding";
	private static String tag_get_watchlist_note ="get watchlist note() : ";
	private static String tag_get_portfolio ="get portfolio () : ";
	private static String tag_add_stock_portfolio ="add stock portfolio () : ";
	private static String tag_update_stock_portfolio ="update stock portfolio () : ";
	private static String tag_delete_stock_portfolio ="delete stock portfolio () : ";
	private static String tag_get_market_news ="get market news () :";
	private static String tag_get_trading_most_active_buy ="get trading most active buy () :";
	private static String tag_get_trading_most_active_sell ="get trading most active sell () :";
	private static String tag_get_trading_foreign_trades ="get trading foregin trades () :";
	private static String tag_get_mainscreen_watchlist ="get main screen watchlist () :";
	private static String tag_get_portfolio_note ="get portfolio note () :";
	private static String tag_get_stock_last_day ="get stock last day () :";
	private static String tag_get_list_event ="get list event () :";
	private static String tag_get_consensus ="get consensus () :";
	private static String tag_get_daily_market ="get daily market () :";
	private static String tag_update_profile="update profile () : ";
	
	private boolean getStatusResponse(JSONObject jsonObject){
		boolean status = false;
		APIMeta apiMeta;
		try {
			apiMeta = new Gson().fromJson(jsonObject.getJSONObject("meta").toString(), APIMeta.class);
			if(apiMeta.getCode() == 200 && apiMeta.getMessage().equals("OK")){
				status = true;
			}
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}		
		return status;
	}
	
	private JSONArray getJSONArrayObject (JSONObject jsonObject){
		JSONArray jsonArray = null;
		try {
			jsonArray = jsonObject.getJSONArray("data");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonArray;
	}
	
	private JSONObject getJSONObject(JSONObject jsonData){
		JSONObject jsonObject = null;
		try {
			jsonObject = jsonData.getJSONObject("data");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
	
	
	//-------------------------API Login User-------------------------------------//
	@Override
	public void login(String userName,String password,String device_id,String idPushNotification,
			final IStockResponseListener<String> responseListener) {
		QueryString query = new QueryString(SVG_SERVER, SVG_URL_LOGIN_API);
		query.add("username", userName);
		query.add("password", password);
		query.add("device_id", device_id);
		query.add("device_os_type", "android");
		query.add("device_notification_token", idPushNotification);//"87177be979d54be64e128f732b45e47af1b6e88f7c309912fab764af33431e15");
		query.add("verison_name", Utils.getAppVersionName(AppController.getAppContext()));
		query.add("group_code", Enums.GROUP_CODE);
		String url = query.toString();
		StringRequest strRequest = new StringRequest(Method.POST, url, 
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						logger.debug("Login (): " + response);
						if(response != null && response.length() > 0){
							
						}
						responseListener.onResponse(response);
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						logger.error("login exception", error);
						responseListener.onError(error);
					}
				}
		){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}
		};
		AppController.getInstance().addToRequestQueue(strRequest,
				tag_login);
	}

	//-------------------------API Register User-------------------------------------//
	@Override
	public void register(String username, String password, String fullName,String email,
			String deviceId, String referralCode,String idPushNotification,
			final IStockResponseListener<String> responseListener) {
		QueryString query = new QueryString(SVG_SERVER, SVG_URL_CREATE_API);
		query.add("username", username);
		query.add("password", password);
		query.add("fullname", fullName);
		query.add("email", email);
		query.add("device_id", deviceId);
		query.add("device_os_type", "android");
		query.add("referral_code", referralCode);
		query.add("device_notification_token",idPushNotification);
		query.add("group_code", Enums.GROUP_CODE);
		String url = query.toString();
		StringRequest strRequest = new StringRequest(Method.POST, url, 
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						if(response != null && response.length() > 0){
							logger.debug("Register (): " + response);
							
						}
						responseListener.onResponse(response);
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						logger.error("Register exception", error);
						responseListener.onError(error);
					}
				}
		){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}
		};
		AppController.getInstance().addToRequestQueue(strRequest,
				tag_register);
	}

	//-------------------------API  Markets-------------------------------------//
	@Override
	public void getMarketList(final IStockResponseListener<ArrayList<Markets>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER, SVG_URL_GET_MARKET_LIST);
		
		String url = query.toString();
		StringRequest strRequest = new StringRequest(Method.GET, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				logger.debug("get Market List (): " + response);
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(response);
					if(getStatusResponse(jsonObject)){
						ArrayList<Markets> markets = new ArrayList<Markets>();
						JSONArray jsonArray =getJSONArrayObject(jsonObject);
						for (int i = 0; i < jsonArray.length(); i++) {
							Markets market;
							try {
								market = new Gson().fromJson(jsonArray.get(i).toString(), Markets.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
								markets.add(market);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
						responseListener.onResponse(markets);
					}					
					
				} catch (JSONException e) {
					e.printStackTrace();
					responseListener.onError(e);
				}								
			}			
			
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				logger.debug("getEventDetails()  failed : " + error);
				responseListener.onError(error);
			}
		});
		
		AppController.getInstance().addToRequestQueue(strRequest, tag_get_market_list);
	}

	
	//-------------------------API MainScreen's News-------------------------------------//
	@Override
	public void getNewsList(String limit, String offset,final IStockResponseListener<ArrayList<News>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_NEWS);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug("get News List (): " + response);
						if(getStatusResponse(response)){
							ArrayList<News> arrayNews = new ArrayList<News>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								News news;
								try {
									news = new Gson().fromJson(jsonArray.get(i).toString(), News.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
									arrayNews.add(news);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayNews);							
						}					
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_get_news,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_get_news);
	}

	
	//-------------------------API Market's Chart-------------------------------------//
	@Override
	public void getMarketChart(String marketName,String fromDate, String toDate,
			final IStockResponseListener<ArrayList<Markets>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_NEWS);
		String url = SVG_SERVER +"market/" + marketName + "/indices";
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("from_date", fromDate);
			jsonObject.put("to_date", toDate);			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug("get Market Chart (): " + response);
						if(getStatusResponse(response)){
							ArrayList<Markets> arrayMarkets = new ArrayList<Markets>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								Markets market;
								try {
									market = new Gson().fromJson(jsonArray.get(i).toString(), Markets.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
									arrayMarkets.add(market);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayMarkets);							
						}					
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_get_market_chart,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}
		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_get_market_chart);
	}

	@Override
	public void getStock(String stockName,
			final IStockResponseListener<ArrayList<StockTrades>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER, SVG_URL_GET_STOCK);
		String url = SVG_SERVER + SVG_URL_GET_STOCK + stockName + "/latest";
		StringRequest strRequest = new StringRequest(Method.GET, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				logger.debug("get Market List (): " + response);
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(response);
					if(getStatusResponse(jsonObject)){
						ArrayList<StockTrades> stockTrades = new ArrayList<StockTrades>();
						JSONArray jsonArray =getJSONArrayObject(jsonObject);
						for (int i = 0; i < jsonArray.length(); i++) {
							StockTrades stockTrade;
							try {
								stockTrade = new Gson().fromJson(jsonArray.get(i).toString(), StockTrades.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
								stockTrades.add(stockTrade);
							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
						responseListener.onResponse(stockTrades);
					}					
					
				} catch (JSONException e) {
					e.printStackTrace();
				}								
			}			
			
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				logger.debug("getEventDetails()  failed : " + error);
				responseListener.onError(error);
			}
		});
		
		AppController.getInstance().addToRequestQueue(strRequest, tag_get_get_stock);
	}

	@Override
	public void getStockTransaction(String stockName, String fromDate,
			String toDate, final IStockResponseListener<ArrayList<StockTrades>> responseListener) {
		String url = SVG_SERVER +"stock/" + stockName + "/trades";
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("from_date", fromDate);
			jsonObject.put("to_date", toDate);			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug("get Stock Trades (): " + response);
						if(getStatusResponse(response)){
							ArrayList<StockTrades> arrayStockTrades = new ArrayList<StockTrades>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								StockTrades trade;
								try {
									trade = new Gson().fromJson(jsonArray.get(i).toString(), StockTrades.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
									arrayStockTrades.add(trade);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayStockTrades);							
						}					
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_get_stock_trades,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}
		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_get_stock_trades);
	}

	@Override
	public void getListStock(final Context mContext, final IStockResponseListener<ArrayList<StockList>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER, SVG_URL_GET_LIST_STOCK);
		
		String url = query.toString();
		StringRequest strRequest = new StringRequest(Method.GET, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				logger.debug("get Stocks List (): " + response);
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(response);
					if(getStatusResponse(jsonObject)){
						PreferenceUtils.writeString(mContext,Enums.PREF_LIST_STOCK, response);
						ArrayList<StockList> stockTrades = new ArrayList<StockList>();
						JSONArray jsonArray =getJSONArrayObject(jsonObject);
						for (int i = 0; i < jsonArray.length(); i++) {
							StockList stock;
							try {
								stock = new Gson().fromJson(jsonArray.get(i).toString(), StockList.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
								stockTrades.add(stock);
							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
						responseListener.onResponse(stockTrades);
					}					
					
				} catch (JSONException e) {
					e.printStackTrace();
				}								
			}			
			
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				logger.debug("getStocks List()  failed : " + error);
				responseListener.onError(error);
			}
		});
		
		AppController.getInstance().addToRequestQueue(strRequest, tag_get_stock_list);

	}

	@Override
	public void getStockFinancialQuarter(String stockCode,JSONArray years,
			final IStockResponseListener<String> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_STOCK_FINANCIAL);
		String url = query.toString();
		url = SVG_SERVER + SVG_URL_GET_STOCK_FINANCIAL + stockCode + "/quarteronly";
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("years", years);		
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_get_stock_finacial_quarter + response);
						if(getStatusResponse(response)){							
							JSONObject jsonObject;
							jsonObject = getJSONObject(response);
							responseListener.onResponse(jsonObject.toString());						
						}						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_stock_finacial_quarter,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};		
		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_stock_finacial_quarter);
	}

	@Override
	public void getStockFinancialYear(String stockCode,JSONArray years,
			final IStockResponseListener<String> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_STOCK_FINANCIAL);
		String url = query.toString();
		url = SVG_SERVER + SVG_URL_GET_STOCK_FINANCIAL + stockCode + "/yearonly";
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("years", years);		
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_get_stock_finacial_year + response);
						if(getStatusResponse(response)){							
							JSONObject jsonObject;
							jsonObject = getJSONObject(response);
							responseListener.onResponse(jsonObject.toString());						
						}						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_stock_finacial_year,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};		
		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_stock_finacial_year);
	}

	///////////////////////// get Stock News ///////////////////////////////////////////////////
	@Override
	public void getStockNews(int limit,int offset,String stockCode,
			final IStockResponseListener<ArrayList<News>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_STOCK_NEWS);
		String url = SVG_SERVER + SVG_URL_GET_STOCK_NEWS +stockCode;
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug("get Stocks News (): " + response);
						if(getStatusResponse(response)){
							ArrayList<News> arrayNews = new ArrayList<News>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								News news;
								try {
									news = new Gson().fromJson(jsonArray.get(i).toString(), News.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
									arrayNews.add(news);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayNews);							
						}					
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_stock_news,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_stock_news);
	}

	@Override
	public void getNewDetails(int idNews,
			final IStockResponseListener<ArrayList<News>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER, SVG_URL_GET_NEW_DETAIL);
		String url = SVG_SERVER + SVG_URL_GET_NEW_DETAIL +idNews;
		logger.debug("get News Detail (): " + url);
		StringRequest strRequest = new StringRequest(Method.GET, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				ArrayList<News> news = new ArrayList<News>();
				logger.debug("get News Detail (): " + response);
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(response);
					if(getStatusResponse(jsonObject)){						
						JSONArray jsonArray =getJSONArrayObject(jsonObject);
						for (int i = 0; i < jsonArray.length(); i++) {
							News newDetail;
							try {
								newDetail = new Gson().fromJson(jsonArray.get(i).toString(), News.class);
								news.add(newDetail);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}						
					}					
					responseListener.onResponse(news);
				} catch (JSONException e) {
					responseListener.onError(e);
					e.printStackTrace();
				}
			}			
			
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				logger.debug("get News Detail () failed : " + error);
				responseListener.onError(error);
			}
		});
		
		AppController.getInstance().addToRequestQueue(strRequest, tag_get_new_details);
	}

	@Override
	public void getStockStatistic(String stockCode,
			final IStockResponseListener<ArrayList<StockStatistic>> responseListener) {
		String url = SVG_SERVER + SVG_URL_GET_STOCK +stockCode + SVG_URL_GET_STOCK_STATISTIC;
		StringRequest strRequest = new StringRequest(Method.GET, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				logger.debug("get Stocks Statistic (): " + response);
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(response);
					if(getStatusResponse(jsonObject)){
						ArrayList<StockStatistic> consults = new ArrayList<StockStatistic>();
						JSONArray jsonArray =getJSONArrayObject(jsonObject);
						for (int i = 0; i < jsonArray.length(); i++) {
							StockStatistic stockStatistic;
							try {
								stockStatistic = new Gson().fromJson(jsonArray.get(i).toString(), StockStatistic.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
								consults.add(stockStatistic);
							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
						responseListener.onResponse(consults);
					}					
					
				} catch (JSONException e) {
					responseListener.onError(e);
					e.printStackTrace();
				}								
			}			
			
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				logger.debug(tag_get_stock_statistic  + error);
				responseListener.onError(error);
			}
		});
		
		AppController.getInstance().addToRequestQueue(strRequest, tag_get_stock_statistic);
	}

	@Override
	public void getMarketETF(final IStockResponseListener<ArrayList<ETF>> responseListener) {
		String url = SVG_SERVER + SVG_URL_GET_MARKET_ETF;
		StringRequest strRequest = new StringRequest(Method.GET, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				logger.debug("get Market ETF (): " + response);
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(response);
					if(getStatusResponse(jsonObject)){
						ArrayList<ETF> listETF = new ArrayList<ETF>();
						JSONArray jsonArray =getJSONArrayObject(jsonObject);
						for (int i = 0; i < jsonArray.length(); i++) {
							ETF etf;
							try {
								etf = new Gson().fromJson(jsonArray.get(i).toString(), ETF.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
								listETF.add(etf);
							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
						responseListener.onResponse(listETF);
					}					
					
				} catch (JSONException e) {
					e.printStackTrace();
				}								
			}			
			
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				logger.debug(tag_get_market_etf  + error);
				responseListener.onError(error);
			}
		});
		
		AppController.getInstance().addToRequestQueue(strRequest, tag_get_market_etf);
	}

	@Override
	public void getStockSnapshots(String stockCode,
			final IStockResponseListener<ArrayList<StockSnapshots>> responseListener) {
		String url = SVG_SERVER + SVG_URL_GET_STOCK +stockCode + SVG_URL_GET_STOCK_SNAPSHOTS;
		StringRequest strRequest = new StringRequest(Method.GET, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				logger.debug("get Stocks Snapshots (): " + response);
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(response);
					if(getStatusResponse(jsonObject)){
						ArrayList<StockSnapshots> stockSnapshots = new ArrayList<StockSnapshots>();
						JSONArray jsonArray =getJSONArrayObject(jsonObject);
						for (int i = 0; i < jsonArray.length(); i++) {
							StockSnapshots stockSnapshot;
							try {
								stockSnapshot = new Gson().fromJson(jsonArray.get(i).toString(), StockSnapshots.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
								stockSnapshots.add(stockSnapshot);
							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
						responseListener.onResponse(stockSnapshots);
					}					
					
				} catch (JSONException e) {
					e.printStackTrace();
				}								
			}			
			
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				logger.debug(tag_get_stock_snapshots  + error);
				responseListener.onError(error);
			}
		});
		
		AppController.getInstance().addToRequestQueue(strRequest, tag_get_stock_snapshots);
	}

	@Override
	public void getStockOverview(String stockCode,
			final IStockResponseListener<ArrayList<StockOverviews>> responseListener) {
		String url = SVG_SERVER + SVG_URL_GET_STOCK +stockCode + SVG_URL_GET_STOCK_OVERVIEW;
		StringRequest strRequest = new StringRequest(Method.GET, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				logger.debug("get Stocks Overviews (): " + response);
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(response);
					if(getStatusResponse(jsonObject)){
						ArrayList<StockOverviews> stockOverviews = new ArrayList<StockOverviews>();
						JSONArray jsonArray =getJSONArrayObject(jsonObject);
						for (int i = 0; i < jsonArray.length(); i++) {
							StockOverviews stockOverview;
							try {
								stockOverview = new Gson().fromJson(jsonArray.get(i).toString(), StockOverviews.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
								stockOverviews.add(stockOverview);
							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
						responseListener.onResponse(stockOverviews);
					}					
					
				} catch (JSONException e) {
					e.printStackTrace();
				}								
			}			
			
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				logger.debug(tag_get_stock_overview  + error);
				responseListener.onError(error);
			}
		});
		
		AppController.getInstance().addToRequestQueue(strRequest, tag_get_stock_overview);
	}

	@Override
	public void getMarketWatchList(String limit, String offset,
			final IStockResponseListener<ArrayList<News>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_WATCHLIST);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug("get Watch List (): " + response);
						if(getStatusResponse(response)){
							ArrayList<News> arrayNews = new ArrayList<News>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								News news;
								try {
									news = new Gson().fromJson(jsonArray.get(i).toString(), News.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
									arrayNews.add(news);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayNews);							
						}					
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_watchlist,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_watchlist);
	}

	@Override
	public void getWatchlistFundamental(int limit, int offset,
			final IStockResponseListener<ArrayList<WatchList>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_WATCHLIST_FUNDAMENTAL);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug("get Watch List Fundamental (): " + response);
						if(getStatusResponse(response)){
							ArrayList<WatchList> arrayWatchLists = new ArrayList<WatchList>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								WatchList watchListFundamental;
								try {
									watchListFundamental = new Gson().fromJson(jsonArray.get(i).toString(), WatchList.class);
									arrayWatchLists.add(watchListFundamental);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayWatchLists);							
						}					
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_watchlist_fundamental,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_watchlist_fundamental);
	}

	@Override
	public void getWatchlistTranding(int limit, int offset, 
			final IStockResponseListener<ArrayList<WatchList>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_WATCHLIST_TRANDING);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug("get Watch List Trending (): " + response);
						if(getStatusResponse(response)){
							ArrayList<WatchList> arrayWatchLists = new ArrayList<WatchList>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								WatchList watchListFundamental;
								try {
									watchListFundamental = new Gson().fromJson(jsonArray.get(i).toString(), WatchList.class);
									arrayWatchLists.add(watchListFundamental);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayWatchLists);							
						}					
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_watchlist_tranding,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_watchlist_tranding);
	}

	@Override
	public void getWatchlistNote(int id,
			final IStockResponseListener<ArrayList<WatchListNote>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER, SVG_URL_GET_MARKET_LIST);
		
		String url = SVG_SERVER + SVG_URL_GET_WATCHLIST + id + SVG_URL_GET_WATCHLIST_NOTE;
		logger.debug(tag_get_watchlist_note + url);
		StringRequest strRequest = new StringRequest(Method.GET, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				logger.debug(tag_get_watchlist_note + response);
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(response);
					if(getStatusResponse(jsonObject)){
						ArrayList<WatchListNote> arrayWatchListNotes = new ArrayList<WatchListNote>();
						JSONArray jsonArray =getJSONArrayObject(jsonObject);
						for (int i = 0; i < jsonArray.length(); i++) {
							WatchListNote watchListNote;
							try {
								watchListNote = new Gson().fromJson(jsonArray.get(i).toString(), WatchListNote.class);
								arrayWatchListNotes.add(watchListNote);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
						responseListener.onResponse(arrayWatchListNotes);
					}					
					
				} catch (JSONException e) {
					e.printStackTrace();
				}								
			}			
			
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				logger.debug(tag_get_watchlist_note + error);
				responseListener.onError(error);
			}
		});
		
		AppController.getInstance().addToRequestQueue(strRequest, tag_get_watchlist_note);
	}

	@Override
	public void getPortfolio(int limit, int offset,final String token,
			final IStockResponseListener<ArrayList<Portfolio>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_PORFOLIO);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_get_portfolio + response);
						if(getStatusResponse(response)){
							ArrayList<Portfolio> arrayPortfolios = new ArrayList<Portfolio>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								Portfolio portfolio;
								try {
									portfolio = new Gson().fromJson(jsonArray.get(i).toString(), Portfolio.class);
									arrayPortfolios.add(portfolio);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayPortfolios);							
						}					
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_portfolio,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				headers.put("X-Auth-Token", token);
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_portfolio);
	}

	@Override
	public void addStockPortfolio(final String token, String stockCode,
			String numberOfShare, String purchasePrice,
			final IStockResponseListener<Boolean> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_ADD_STOCK_PORFOLIO);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("stock_code", stockCode);
			jsonObject.put("number_of_share", numberOfShare);
			jsonObject.put("purchase_price", purchasePrice);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_add_stock_portfolio + response);
						if(getStatusResponse(response)){
							responseListener.onResponse(true);
						}else{
							responseListener.onResponse(false);
						}						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_add_stock_portfolio,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				headers.put("X-Auth-Token", token);
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_add_stock_portfolio);
		
	}

	@Override
	public void updateStockPortfolio(final boolean checkUpdate,final String token, int idCode,
			String stockCode, String numberOfShare, String purchasePrice,String note,
			final IStockResponseListener<Boolean> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_UPDATE_STOCK_PORFOLIO);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("portfolio_id", idCode);
			jsonObject.put("stock_code", stockCode);
			jsonObject.put("number_of_share", numberOfShare);
			jsonObject.put("purchase_price", purchasePrice);
			if(checkUpdate){
				jsonObject.put("note", note);
			}		
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_update_stock_portfolio + response);
						if(getStatusResponse(response)){
							responseListener.onResponse(true);
						}else{
							responseListener.onResponse(false);
						}						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_update_stock_portfolio,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				headers.put("X-Auth-Token", token);
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_update_stock_portfolio);
	}

	@Override
	public void deleteStockPortfolio(final String token, int idCode,
			final IStockResponseListener<Boolean> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_DELETE_STOCK_PORFOLIO);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("portfolio_id", idCode);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_delete_stock_portfolio + response);
						if(getStatusResponse(response)){
							responseListener.onResponse(true);
						}else{
							responseListener.onResponse(false);
						}						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_update_stock_portfolio,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				headers.put("X-Auth-Token", token);
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_delete_stock_portfolio);
	}

	@Override
	public void getNewsMarket(String market,String limit, String offset,
			final IStockResponseListener<ArrayList<News>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,(SVG_URL_GET_NEWS_MARKET + market));
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_get_market_news + response);
						if(getStatusResponse(response)){
							ArrayList<News> arrayNews = new ArrayList<News>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								News news;
								try {
									news = new Gson().fromJson(jsonArray.get(i).toString(), News.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
									arrayNews.add(news);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayNews);							
						}					
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_market_news,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_market_news);
	}

	@Override
	public void getMarketTradingMostActiveSell(String market,final String token, String limit,
			String offset,
			final IStockResponseListener<ArrayList<MostActive>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,(SVG_URL_GET_TRADING_SUMMARY + market + "/mostactives/" + "sell"));
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_get_trading_most_active_sell + response);
						if(getStatusResponse(response)){
							ArrayList<MostActive> arrayMostActives = new ArrayList<MostActive>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								MostActive mostActive;
								try {
									mostActive = new Gson().fromJson(jsonArray.get(i).toString(), MostActive.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
									arrayMostActives.add(mostActive);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayMostActives);
						}						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_trading_most_active_sell,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				headers.put("X-Auth-Token", token);
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_trading_most_active_sell);

	}

	@Override
	public void getMarketTradingMostActiveBuy(String market, final String token,
			String limit, String offset,
			final IStockResponseListener<ArrayList<MostActive>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,(SVG_URL_GET_TRADING_SUMMARY + market + "/mostactives/" + "buy"));
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_get_trading_most_active_buy + response);
						if(getStatusResponse(response)){
							ArrayList<MostActive> arrayMostActives = new ArrayList<MostActive>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								MostActive mostActive;
								try {
									mostActive = new Gson().fromJson(jsonArray.get(i).toString(), MostActive.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
									arrayMostActives.add(mostActive);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayMostActives);
						}						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_trading_most_active_buy,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				headers.put("X-Auth-Token", token);
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_trading_most_active_buy);

	}

	@Override
	public void getMarketForeignTrades(String market, final String token,
			String limit, String offset,
			final IStockResponseListener<ArrayList<ForeignTrades>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,(SVG_URL_GET_TRADING_SUMMARY + market + SVG_URL_GET_FOREIGN_TRADE));
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		logger.debug(tag_get_trading_foreign_trades + url);
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						
						logger.debug(tag_get_trading_foreign_trades + response);
						if(getStatusResponse(response)){
							ArrayList<ForeignTrades> arrayForeignTrades = new ArrayList<ForeignTrades>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								ForeignTrades foreignTrades;
								try {
									foreignTrades = new Gson().fromJson(jsonArray.get(i).toString(), ForeignTrades.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
									arrayForeignTrades.add(foreignTrades);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayForeignTrades);
						}						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_trading_foreign_trades,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				headers.put("X-Auth-Token", token);
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_trading_foreign_trades);

	}

	@Override
	public void getWatchlist(
			final IStockResponseListener<ArrayList<MainWatchList>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER, SVG_URL_GET_MAIN_WATCHLIST);
		String url = query.toString();
		logger.debug(tag_get_mainscreen_watchlist + url);
		StringRequest strRequest = new StringRequest(Method.GET, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				logger.debug(tag_get_mainscreen_watchlist + response);
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(response);
					if(getStatusResponse(jsonObject)){
						ArrayList<MainWatchList> arrayMainWatchLists = new ArrayList<MainWatchList>();
						JSONArray jsonArray =getJSONArrayObject(jsonObject);
						for (int i = 0; i < jsonArray.length(); i++) {
							MainWatchList mainWatchList;
							try {
								mainWatchList = new Gson().fromJson(jsonArray.get(i).toString(), MainWatchList.class);
								arrayMainWatchLists.add(mainWatchList);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
						responseListener.onResponse(arrayMainWatchLists);
					}					
					
				} catch (JSONException e) {
					e.printStackTrace();
				}								
			}			
			
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				logger.debug(tag_get_mainscreen_watchlist  + error);
				responseListener.onError(error);
			}
		});
		
		AppController.getInstance().addToRequestQueue(strRequest, tag_get_mainscreen_watchlist);
	}

	@Override
	public void getPortFolioNote(final String token, int idCode,
			final IStockResponseListener<Note> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,(SVG_URL_GET_PORFOLIO + "/" + idCode + SVG_URL_GET_PORTFOLIO_NOTE));
		String url = query.toString();
		logger.debug(tag_get_trading_foreign_trades + url);
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				null, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						Note note = new Note();
						logger.debug(tag_get_portfolio_note + response);
						if(getStatusResponse(response)){
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
								
								try {
									note = new Gson().fromJson(jsonArray.get(0).toString(), Note.class);//Utils.readValue(jsonArray.get(i).toString(),Markets.class);////
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(note);
												
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_portfolio_note,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				headers.put("X-Auth-Token", token);
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_portfolio_note);
	}

	@Override
	public void getStockLastDay(String stockCode,
			final IStockResponseListener<ArrayList<StockLastDay>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER, (SVG_URL_GET_STOCK + stockCode + SVG_URL_GET_STOCK_LASTDAY));
		
		String url = query.toString();
		StringRequest strRequest = new StringRequest(Method.GET, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				logger.debug(tag_get_stock_last_day + response);
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(response);
					if(getStatusResponse(jsonObject)){
						ArrayList<StockLastDay> arrayStockLastDays = new ArrayList<StockLastDay>();
						JSONArray jsonArray = getJSONArrayObject(jsonObject);
						JSONObject jsonObject2 = jsonArray.getJSONObject(0);
						JSONArray jsonArray2 = jsonObject2.getJSONArray("Trades");
						for (int i = 0; i < jsonArray2.length(); i++) {
							StockLastDay stockLastDay;
							try {
								stockLastDay = new Gson().fromJson(jsonArray2.get(i).toString(), StockLastDay.class);
								arrayStockLastDays.add(stockLastDay);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
						responseListener.onResponse(arrayStockLastDays);
					}					
					
				} catch (JSONException e) {
					e.printStackTrace();
					responseListener.onError(e);
				}								
			}			
			
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				logger.debug(tag_get_stock_last_day + error);
				responseListener.onError(error);
			}
		});
		
		AppController.getInstance().addToRequestQueue(strRequest, tag_get_stock_last_day);	
	}

	@Override
	public void getListEvent(final String token,String date,
			final IStockResponseListener<ArrayList<Event>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_LIST_EVENT);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("date", date);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		logger.debug(tag_get_list_event + url);
		logger.debug(tag_get_list_event + date);
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_get_list_event + response);
						if(getStatusResponse(response)){
							ArrayList<Event> arrayEvents = new ArrayList<Event>();
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							for (int i = 0; i < jsonArray.length(); i++) {
								Event event;
								try {
									event = new Gson().fromJson(jsonArray.get(i).toString(), Event.class);
									arrayEvents.add(event);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							responseListener.onResponse(arrayEvents);
						}						
												
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_list_event,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				headers.put("X-Auth-Token", token);
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_list_event);
	}

	@Override
	public void getConsensus(final String token, String limit, String offset,
			final IStockResponseListener<ArrayList<Consensus>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_CONSENSUS);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_get_consensus + response);
						ArrayList<Consensus> arrayConsensus = new ArrayList<Consensus>();
						if(getStatusResponse(response)){							
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							if(jsonArray.length()> 0){
								for (int i = 0; i < jsonArray.length(); i++) {
									Consensus consensus;
									try {
										consensus = new Gson().fromJson(jsonArray.get(i).toString(), Consensus.class);
										arrayConsensus.add(consensus);
									} catch (JSONException e) {
										e.printStackTrace();
									}
								}		
							}
																			
						}
						responseListener.onResponse(arrayConsensus);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_consensus,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				headers.put("X-Auth-Token", token);
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_consensus);
	}

	@Override
	public void getDailyMarket(String limit, String offset,String idMarket,
			final IStockResponseListener<ArrayList<MarketChart>> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_GET_DAILYMARKET);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("limit", limit);
			jsonObject.put("offset", offset);	
			jsonObject.put("sm_id", idMarket);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_get_daily_market + response);
						ArrayList<MarketChart> arrayMarketCharts = new ArrayList<MarketChart>();
						if(getStatusResponse(response)){							
							JSONArray jsonArray;
							jsonArray = getJSONArrayObject(response);
							if(jsonArray.length()> 0){
								for (int i = 0; i < jsonArray.length(); i++) {
									MarketChart marketChart;
									try {
										marketChart = new Gson().fromJson(jsonArray.get(i).toString(), MarketChart.class);
										arrayMarketCharts.add(marketChart);
									} catch (JSONException e) {
										e.printStackTrace();
									}
								}		
							}
																			
						}
						responseListener.onResponse(arrayMarketCharts);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_get_daily_market,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_get_daily_market);
	}

	@Override
	public void updateUserProfile(String fullName, String username,
			String password,  String newPass, String phone,
			String email, final IStockResponseListener<JSONObject> responseListener) {
		QueryString query = new QueryString(SVG_SERVER,SVG_URL_UPDATE_PROFILE);
		String url = query.toString();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("fullname", fullName);
			jsonObject.put("username", username);	
			jsonObject.put("password", password);
			jsonObject.put("new_password", newPass);
			jsonObject.put("phone", phone);	
			jsonObject.put("email", email);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						logger.debug(tag_update_profile + response);
						if(getStatusResponse(response)){							
							responseListener.onResponse(response);																			
						}else{
							responseListener.onResponse(null);
						}
						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(tag_update_profile,"Error: " + error.getMessage());
						responseListener.onError(error);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjReq,tag_update_profile);
	}
	
}

package com.hotstock.app.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.hotstock.R;

public class WatchlistFragment extends Fragment implements OnClickListener {	

	private Button btnExpert;
	private Button btnConsensus;
	public static WatchlistFragment instant;
	
	public WatchlistFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
        View rootView = inflater.inflate(R.layout.fragment_watchlist, container, false);
        initView(rootView);
        return rootView;
    }
	
	private void initView(View v){
		instant = this;
		if(btnExpert == null){
			btnExpert = (Button) v.findViewById(R.id.fragment_watchlist_btn_expert);
		}
		
		if(btnConsensus == null){
			btnConsensus = (Button) v.findViewById(R.id.fragment_watchlist_btn_consensus);
		}
		
		listener();
		displayExpert();		
	}
	
	private void listener(){
		btnExpert.setOnClickListener(this);
		btnConsensus.setOnClickListener(this);
	}
	
	private void displayFragment(Fragment fragment, Bundle args, String name) {
		fragment.setArguments(args);
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.fragment_watchlist_container, fragment, name).commit();
	}
	
	private void displayExpert() {
		displayFragment(new WatchlistExpertsFragment(),new Bundle(),"experts");
	}

	private void displayConsensus() {
		displayFragment(new WatchlistConsensusFragment(),new Bundle(),"consensus");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.fragment_watchlist_btn_expert:
			btnExpert.setTextColor(getActivity().getResources().getColor(android.R.color.black));
			btnExpert.setBackgroundColor(getResources().getColor(R.color.text_color_yellow));
			btnConsensus.setTextColor(getActivity().getResources().getColor(android.R.color.white));
			btnConsensus.setBackgroundColor(getResources().getColor(android.R.color.transparent));
			displayExpert();
			break;
			
		case R.id.fragment_watchlist_btn_consensus:
			btnConsensus.setTextColor(getActivity().getResources().getColor(android.R.color.black));
			btnConsensus.setBackgroundColor(getResources().getColor(R.color.text_color_yellow));
			btnExpert.setTextColor(getActivity().getResources().getColor(android.R.color.white));
			btnExpert.setBackgroundColor(getResources().getColor(android.R.color.transparent));
			displayConsensus();
			break;
			
		default:
			break;
		}
	}
}

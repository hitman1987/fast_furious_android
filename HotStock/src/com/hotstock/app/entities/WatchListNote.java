package com.hotstock.app.entities;

public class WatchListNote {
	public int id;
	public String code;
	public String rate;
	public double targeted_price;
	public String type;
	public double basic_price;
    public String note;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public double getTargeted_price() {
		return targeted_price;
	}
	public void setTargeted_price(Double targeted_price) {
		this.targeted_price = targeted_price;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getBasic_price() {
		return basic_price;
	}
	public void setBasic_price(Double basic_price) {
		this.basic_price = basic_price;
	}
}

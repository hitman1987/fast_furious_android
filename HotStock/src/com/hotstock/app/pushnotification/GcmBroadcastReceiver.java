package com.hotstock.app.pushnotification;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.hotstock.app.views.PushPopupActivity;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// Explicitly specify that GcmIntentService will handle the intent.
		ComponentName comp = new ComponentName(context.getPackageName(), GcmIntentService.class.getName());
		// Start the service, keeping the device awake while it is launching.
		startWakefulService(context, (intent.setComponent(comp)));
		if(android.os.Build.VERSION.SDK_INT < 21){
			setResultCode(Activity.RESULT_OK);
			Intent intentPush = PushPopupActivity.getBidPopupIntent(context);
			intentPush.putExtra("pushinfo", intent.getExtras());
			context.startActivity(intentPush);
		}		
	}
}

package com.hotstock.app.utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;

import com.hotstock.R;
import com.hotstock.app.entities.StockList;
import com.hotstock.app.pushnotification.PushReceiveItem;

public class Utils {
	
	public static ArrayList<StockList> mDataStockList = new ArrayList<StockList>();
	
	private static ObjectMapper objectMapper = new ObjectMapper();
	private static NumberFormat nf = NumberFormat.getInstance();
	private static int langId;
	
	public static <T> T readValue(String data, Class<T> classType) {
		try {
			return objectMapper.readValue(data, classType);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static String formatStockPrice(double price){//, int vol, String code) {
//		if (price == 0 && vol > 0) {
//			if (code.equals("P"))
//				return "ATO";
//			else if (code.equals("O"))
//				return "MP";
//			else if (code.equals("A"))
//				return "ATC";
//		} else if (price == 0) {
//			
//		}

		String strAfterFormat = new DecimalFormat("###.0#").format(price);
		// Double check with current language
		int locale = Enums.kLang_EN;//locale = AppConfig.sharedInstance().resolveLangId();
		if (locale == Enums.kLang_EN && strAfterFormat.contains(",")) {
			strAfterFormat = strAfterFormat.replace(',', '.');
		} else if (locale == Enums.kLang_VN && strAfterFormat.contains(".")) {
			strAfterFormat = strAfterFormat.replace('.', ',');
		}

		return strAfterFormat; // new DecimalFormat("###.0#").format(price);
	}
	

	public static String formatStockChange(double change, double pct) {
		return (formatStockChange(change) + "(" + formatStockChangePct(pct) + ")");
	}

	public static String formatStockChange(double priceChange) {
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);
		String output = nf.format(priceChange);
		if (priceChange > 0) {
			output = "+" + output;
		}

		// Double check with current language
		int locale = resolveLangId();
		if (locale == Enums.kLang_EN && output.contains(",")) {
			output = output.replace(',', '.');
		} else if (locale == Enums.kLang_VN && output.contains(".")) {
			output = output.replace('.', ',');
		}

		return output;
	}

	public static String formatStockChangePct(double pct) {
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);
		String output = nf.format(pct) + "%";
		if (pct > 0) {
			output = "+" + output;
		}

		// Double check with current language
		int locale = resolveLangId();
		if (locale == Enums.kLang_EN && output.contains(",")) {
			output = output.replace(',', '.');
		} else if (locale == Enums.kLang_VN && output.contains(".")) {
			output = output.replace('.', ',');
		}

		return output;
	}

	public static String formatStockChangePct_ForPieChart(double pct) {
		nf.setMinimumFractionDigits(1);
		nf.setMaximumFractionDigits(1);
		String output = nf.format(pct) + "%";

		// Double check with current language
		int locale = resolveLangId();
		if (locale == Enums.kLang_EN && output.contains(",")) {
			output = output.replace(',', '.');
		} else if (locale == Enums.kLang_VN && output.contains(".")) {
			output = output.replace('.', ',');
		}

		return output;
	}

	/**
	 * Format the double value to the output like ###.## format
	 * 
	 * @param price
	 * @return
	 */
	public static String formatMarketIndex(double price) {
		String strAfterFormat = new DecimalFormat("##.##").format(price);
		// Double check with current language
		int locale = resolveLangId();
		if (locale == Enums.kLang_EN && strAfterFormat.contains(",")) {
			strAfterFormat = strAfterFormat.replace(',', '.');
		} else if (locale == Enums.kLang_VN && strAfterFormat.contains(".")) {
			strAfterFormat = strAfterFormat.replace('.', ',');
		}

		return strAfterFormat; // new DecimalFormat("##.##").format(price);
	}

	/**
	 * Format the double value to the output like ###,###,### format
	 * 
	 * @param price
	 * @return
	 */
	public static String formatStockVolume(long vol) {
		if (vol == 0) {
			return null;
		}

		String strAfterFormat = new DecimalFormat("###,###,###").format(vol);
		// Double check with current language
		int locale = resolveLangId();
		if (locale == Enums.kLang_EN && strAfterFormat.contains(".")) {
			strAfterFormat = strAfterFormat.replace('.', ',');
		} else if (locale == Enums.kLang_VN && strAfterFormat.contains(",")) {
			strAfterFormat = strAfterFormat.replace(',', '.');
		}

		return strAfterFormat; // new DecimalFormat("###,###,###").format(vol);
	}

	/**
	 * Format the double value to the output like ###,###,### format (for double
	 * number)
	 * 
	 * @param vol
	 * @return
	 */
	public static String formatAsStockVolume(Double vol) {
		if (vol == 0) {
			return null;
		}

		String strAfterFormat = new DecimalFormat("###,###,###").format(vol);
		// Double check with current language
		int locale = resolveLangId();
		if (locale == Enums.kLang_EN && strAfterFormat.contains(".")) {
			strAfterFormat = strAfterFormat.replace('.', ',');
		} else if (locale == Enums.kLang_VN && strAfterFormat.contains(",")) {
			strAfterFormat = strAfterFormat.replace(',', '.');
		}

		return strAfterFormat; // new DecimalFormat("###,###,###").format(vol);
	}

	public static String formatAsStockPrice(Double price) {
		if (price == 0) {
			return null;
		}

		String strAfterFormat = new DecimalFormat("###,###,###").format(price);
		// Double check with current language
		int locale = resolveLangId();
		if (locale == Enums.kLang_EN && strAfterFormat.contains(".")) {
			strAfterFormat = strAfterFormat.replace('.', ',');
		} else if (locale == Enums.kLang_VN && strAfterFormat.contains(",")) {
			strAfterFormat = strAfterFormat.replace(',', '.');
		}

		return strAfterFormat; // new
								// DecimalFormat("###,###,###").format(price);
	}
	
	public static int resolveLangId() {
		if (langId == Enums.kLangNotInited)
			return Enums.kLangFallback;
		if (langId == Enums.kLang_System) {
			String language = Locale.getDefault().getLanguage();
			if (language.startsWith("en")) {
				return Enums.kLang_EN;
			} else if (language.startsWith("vi")) {
				return Enums.kLang_VN;
			} else {
				return Enums.kLangFallback;
			}
		}
		return langId;
	}
	
	public static int getPriceTrend(double change) {
		if (change > 0)
			return Enums.PRICE_UP;
		if (change < 0)
			return Enums.PRICE_DOWN;
		return Enums.PRICE_REF;
	}

	
	public static int getStockColor(int priceTrend) {
		if (priceTrend == Enums.PRICE_REF)
			return R.color.kPriceRefColor;
		if (priceTrend == Enums.PRICE_NOC)
			return R.color.kPriceNocColor;
		if (priceTrend == Enums.PRICE_CEIL)
			return R.color.kPriceCeilColor;
		if (priceTrend == Enums.PRICE_FLOOR)
			return R.color.kPriceFloorColor;
		if (priceTrend == Enums.PRICE_UP)
			return R.color.kPriceUpColor;
		if (priceTrend == Enums.PRICE_DOWN)
			return R.color.kPriceDowncColor;
		else
			return R.color.kPriceUnknownColor;
	}
	
	public static int getStockColor(double price, double refP, double ceilP,
			double floorP) {
		if (price == 0 || refP == 0) {
			return R.color.kPriceRefColor;
		}
		if (price == ceilP) {
			return R.color.kPriceCeilColor;
		}
		if (price == floorP) {
			return R.color.kPriceFloorColor;
		}
		if (price == refP) {
			return R.color.kPriceRefColor;
		}
		if (price > refP) {
			return R.color.kPriceUpColor;
		}
		if (price < refP) {
			return R.color.kPriceDowncColor;
		}
		return R.color.kPriceUnknownColor;
	}
	
	public static int getStockStatus(double price, double refP, double ceilP,
			double floorP) {
		if (price == 0 || refP == 0) {
			return R.drawable.rectangle;
		}
		if (price == ceilP) {
			return R.drawable.triangle2;
		}
		if (price == floorP) {
			return R.drawable.triangle3;
		}
		if (price == refP) {
			return R.drawable.rectangle;
		}
		if (price > refP) {
			return R.drawable.triangle1;
		}
		if (price < refP) {
			return R.drawable.triangle;
		}
		return R.drawable.rectangle;
	}
	
	public static String convertStringtoDate(String dateStr){
		//String dateStr = "Thu Jan 19 2012 01:00 PM";2014-10-01T23:23:00+0700

		String[] parts = dateStr.split("T");
		String dateFormat = parts[0];
	    DateFormat readFormat = new SimpleDateFormat( "yyyy-MM-dd",Locale.UK);
	    DateFormat writeFormat = new SimpleDateFormat( "dd/MM/yyyy",Locale.UK);
	    Date date = null;
	    try {
	       date = readFormat.parse( dateFormat );
	    } catch ( ParseException e ) {
	        e.printStackTrace();
	    }

	    String formattedDate = "";
	    if( date != null ) {
	    formattedDate = writeFormat.format( date );
	    }

	    return formattedDate;
	}
	
	/**
	 * Format the integer value to the output like ###.## format
	 * 
	 * @param price
	 * @return
	 */
	public static String formatETFPrice(long price) {
		if (price == 0) {
			return "";
		}

		String strAfterFormat = new DecimalFormat("###,###,###").format(price);
		// Double check with current language
		int locale = resolveLangId();
		strAfterFormat = strAfterFormat.replace(',', '.');

		return strAfterFormat + "$";
	}

	public static String formatAsETFSLCCQ(Double price) {
		if (price == 0) {
			return "0";
		}

		String strAfterFormat = new DecimalFormat("###,###,###").format(price);
		// Double check with current language
		int locale = resolveLangId();
		strAfterFormat = strAfterFormat.replace(',', '.');

		return strAfterFormat; // new
								// DecimalFormat("###,###,###").format(price);
	}
	
	public static String convertStringtoETFDate(String dateStr) {
		// String dateStr = "Thu Jan 19 2012 01:00 PM";2014-10-01T23:23:00+0700
		DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

		DateFormat writeFormat = new SimpleDateFormat("dd/MM/yyyy \n HH:mm");
		Date date = null;
		try {
			date = readFormat.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		String formattedDate = "";
		if (date != null) {
			formattedDate = writeFormat.format(date);
		}

		return formattedDate;
	}
	
	public static int colorStatus(String status){
		int color = 1;
		if(status.toLowerCase().equals("buy")){
			color = R.color.kPriceUpColor;
		}else{
			color = R.color.kPriceDowncColor;
		}
		return color;
	}
	
	
	////////////////////------Random Device ID-------------/////////////////////////////
	private static final char[] alphanumeric=alphanumeric();
	private final static Random rand = new Random();
	
	public static String getRandomDeviceID(int len){
		StringBuffer out=new StringBuffer();
	
		while(out.length() < len){
		int idx=Math.abs(( rand.nextInt() % alphanumeric.length ));
		out.append(alphanumeric[idx]);
		}
		return out.toString();
	}
	
	// create alphanumeric char array
	private static char[] alphanumeric(){
		StringBuffer buf=new StringBuffer(128);
		for(int i=48; i<= 57;i++)buf.append((char)i); // 0-9
		for(int i=65; i<= 90;i++)buf.append((char)i); // A-Z
		for(int i=97; i<=122;i++)buf.append((char)i); // a-z
		return buf.toString().toCharArray();
	}
	
	public static String getUniquePsuedoID()
	{
	    // If all else fails, if the user does have lower than API 9 (lower
	    // than Gingerbread), has reset their phone or 'Secure.ANDROID_ID'
	    // returns 'null', then simply the ID returned will be solely based
	    // off their Android device information. This is where the collisions
	    // can happen.
	    // Thanks http://www.pocketmagic.net/?p=1662!
	    // Try not to use DISPLAY, HOST or ID - these items could change.
	    // If there are collisions, there will be overlapping data
	    String m_szDevIDShort = "35" + (Build.BOARD.length() % 10) + (Build.BRAND.length() % 10) + (Build.CPU_ABI.length() % 10) + (Build.DEVICE.length() % 10) + (Build.MANUFACTURER.length() % 10) + (Build.MODEL.length() % 10) + (Build.PRODUCT.length() % 10);

	    // Thanks to @Roman SL!
	    // http://stackoverflow.com/a/4789483/950427
	    // Only devices with API >= 9 have android.os.Build.SERIAL
	    // http://developer.android.com/reference/android/os/Build.html#SERIAL
	    // If a user upgrades software or roots their phone, there will be a duplicate entry
	    String serial = null;
	    try
	    {
	        serial = android.os.Build.class.getField("SERIAL").get(null).toString();

	        // Go ahead and return the serial for api => 9
	        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
	    }
	    catch (Exception e)
	    {
	        // String needs to be initialized
	        serial = "serial"; // some value
	    }

	    // Thanks @Joe!
	    // http://stackoverflow.com/a/2853253/950427
	    // Finally, combine the values we have found by using the UUID class to create a unique identifier
	    return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
	}
	
	
	public static void saveStringPreferences(String key, String value,
			SharedPreferences sharedPreferences) throws NullPointerException {
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static void saveIntPreferences(String key, int value,
			SharedPreferences sharedPreferences) throws NullPointerException {
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	/**
	 * save the value of key to preference
	 * 
	 * @param key
	 * @param value
	 * @param sharedPreferences
	 */
	public static void savePreferences(String key, boolean value,
			SharedPreferences sharedPreferences) throws NullPointerException {
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	/**
	 * 
	 * @param sharedPreferences
	 * @param key
	 * @return value of key
	 */
	public static String loadPreferences(SharedPreferences sharedPreferences,
			String key) {
		return sharedPreferences.getString(key, null);
	}

	public static int loadPreferencesInt(SharedPreferences sharedPreferences,
			String key) {
		return sharedPreferences.getInt(key, Enums.kLang_System);
	}
	
	public static int loadPreferencesInt(SharedPreferences sharedPreferences,
			String key, int defaultValue) {
		return sharedPreferences.getInt(key, defaultValue);
	}

	/**
	 * 
	 * @param sharedPreferences
	 * @param key
	 * @return value of key
	 */
	public static boolean loadPreferencesBool(
			SharedPreferences sharedPreferences, String key) {
		return sharedPreferences.getBoolean(key, false);
	}

	/**
	 * get preference with context
	 * 
	 * @param context
	 * @param preferenceKey
	 * @return sharePreference
	 */
	public static SharedPreferences getSharedPreferences(Context mContext,
			int preferenceKey) {
		return ((Activity) mContext).getPreferences(preferenceKey);
	}
	
	public static PushReceiveItem parsePushJson(String json) {
		PushReceiveItem item = new PushReceiveItem();
		try {
			JSONObject jsonOb = new JSONObject(json);
			JSONObject jsonObApns = jsonOb.getJSONObject("APNS");
			JSONObject jsonObApp = jsonObApns.getJSONObject("aps");
			item.aps.alert = jsonObApp.getString("alert");
			JSONObject jsonObNews = jsonObApns.getJSONObject("news");
			item.news.id = jsonObNews.getInt("id");
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return item;
	}
	
	public static String getAppVersionName(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionName;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}	
}

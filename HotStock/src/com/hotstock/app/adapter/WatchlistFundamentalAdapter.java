package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.WatchList;
import com.hotstock.app.utils.Utils;

public class WatchlistFundamentalAdapter extends BaseAdapter{
		
	private ArrayList<WatchList> mData;
	private LayoutInflater mLayoutInflater;
	private Context mContext;

	public WatchlistFundamentalAdapter(Context c,ArrayList<WatchList> d) {
		this.mContext = c;
		this.mData = d;
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(com.hotstock.R.layout.fragment_watchlist_experts_fundamental, null);
			viewHolder.layoutMain = (LinearLayout) convertView.findViewById(R.id.fragment_watchlist_expert_fundamental_layout_main);
			viewHolder.txtStock = (TextView) convertView.findViewById(R.id.fragment_watchlist_fundamental_txt_stock);
			viewHolder.txtStatus = (TextView) convertView.findViewById(R.id.fragment_watchlist_fundamental_txt_status);
			viewHolder.txtCurrentPrice = (TextView) convertView.findViewById(R.id.fragment_watchlist_fundamental_txt_current_price);
			viewHolder.txtTargetPrice = (TextView) convertView.findViewById(R.id.fragment_watchlist_fundamental_txt_target_price);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.txtStock.setText(mData.get(position).getCode());
		viewHolder.txtStatus.setText(mData.get(position).getRate());
		viewHolder.txtStatus.setTextColor(mContext.getResources().getColor(Utils.colorStatus(mData.get(position).getRate())));
		viewHolder.txtCurrentPrice.setText(Utils.formatAsStockVolume(mData.get(position).getBasic_price()));
		viewHolder.txtTargetPrice.setText(Utils.formatAsStockVolume(mData.get(position).getTargeted_price()));
		if (position % 2 == 0) {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_1));
		} else {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_2));
		}

		return convertView;
	}

	private class ViewHolder {
		private LinearLayout layoutMain;
		private TextView txtStock;
		private TextView txtStatus;
		private TextView txtCurrentPrice;
		private TextView txtTargetPrice;
	}
}

package com.hotstock.app.api;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

import com.hotstock.app.entities.Consensus;
import com.hotstock.app.entities.ETF;
import com.hotstock.app.entities.Event;
import com.hotstock.app.entities.ForeignTrades;
import com.hotstock.app.entities.MainWatchList;
import com.hotstock.app.entities.MarketChart;
import com.hotstock.app.entities.Markets;
import com.hotstock.app.entities.MostActive;
import com.hotstock.app.entities.News;
import com.hotstock.app.entities.Note;
import com.hotstock.app.entities.Portfolio;
import com.hotstock.app.entities.StockLastDay;
import com.hotstock.app.entities.StockList;
import com.hotstock.app.entities.StockOverviews;
import com.hotstock.app.entities.StockSnapshots;
import com.hotstock.app.entities.StockStatistic;
import com.hotstock.app.entities.StockTrades;
import com.hotstock.app.entities.WatchList;
import com.hotstock.app.entities.WatchListNote;


public interface IStockRestfulService {
	
	//--------------------- LOGIN --------------------------------------//
	public void login(String userName, String password,String device_id,String idPushNotification, IStockResponseListener<String> responseListener);
	
	//--------------------- REGISTER ----------------------------------//
	public void register(String username,String password,String fullName,String email,String deviceId,String referralCode,String idPushNotification,IStockResponseListener<String> responseListener);
	
	//--------------------- MAIN SCREEN -----------------------------//
	public void getWatchlist(IStockResponseListener<ArrayList<MainWatchList>> responseListener);
	
	//--------------------MARKET ------------------------------------//
	public void getMarketList(IStockResponseListener<ArrayList<Markets>> responseListener);
	public void getMarketChart(String marketName,String fromDate,String toDate, IStockResponseListener<ArrayList<Markets>> responseListener);
	public void getMarketETF(IStockResponseListener<ArrayList<ETF>> responseListener);
	public void getMarketWatchList(String limit,String offset, IStockResponseListener<ArrayList<News>> responseListener);
	public void getMarketForeignTrades(String market,String token,String limit,String offset,IStockResponseListener<ArrayList<ForeignTrades>> responseListener);
	public void getMarketTradingMostActiveSell(String market,String token,String limit,String offset,IStockResponseListener<ArrayList<MostActive>> responseListener);
	public void getMarketTradingMostActiveBuy(String market,String token,String limit,String offset,IStockResponseListener<ArrayList<MostActive>> responseListener);
	public void getDailyMarket(String limit,String offset,String idMarket,IStockResponseListener<ArrayList<MarketChart>> responseListener);
	
	//-------------------NEWS --------------------------------------//
	public void getNewsList(String limit,String offset, IStockResponseListener<ArrayList<News>> responseListener);
	public void getNewDetails(int idNew,IStockResponseListener<ArrayList<News>> responseListener);
	public void getNewsMarket(String market,String limit,String offset, IStockResponseListener<ArrayList<News>> responseListener);
	
	//---------------------STOCKS ----------------------------------//
	public void getStock(String stockCode,IStockResponseListener<ArrayList<StockTrades>> responseListener);
	public void getStockTransaction(String stockCode,String fromDate,String toDate,IStockResponseListener<ArrayList<StockTrades>> responseListener);
	public void getListStock(Context mContext,IStockResponseListener<ArrayList<StockList>> responseListener);
	public void getStockFinancialQuarter(String stockCode,JSONArray years,IStockResponseListener<String> responseListener);
	public void getStockFinancialYear(String stockCode,JSONArray years,IStockResponseListener<String> responseListener);
	public void getStockNews(int limit,int offset,String stockCode,IStockResponseListener<ArrayList<News>> responseListener);	
	public void getStockStatistic(String stockCode,IStockResponseListener<ArrayList<StockStatistic>> responseListener);
	public void getStockSnapshots(String stockCode, IStockResponseListener<ArrayList<StockSnapshots>> responseListener);
	public void getStockOverview(String stockCode, IStockResponseListener<ArrayList<StockOverviews>> responseListener);
	public void getStockLastDay(String stockCode,IStockResponseListener<ArrayList<StockLastDay>> responseListener);
	
	//-------------------WATCHLIST---------------------------------//
	public void getWatchlistFundamental(int limit,int offset,IStockResponseListener<ArrayList<WatchList>> responseListener);
	public void getWatchlistTranding(int limit,int offset,IStockResponseListener<ArrayList<WatchList>> responseListener);
	public void getWatchlistNote(int id,IStockResponseListener<ArrayList<WatchListNote>> responseListener);
	public void getPortfolio(int limit,int offset,String token,IStockResponseListener<ArrayList<Portfolio>> responseListener);
	public void addStockPortfolio(String token,String stockCode,String numberOfShare,String purchasePrice,IStockResponseListener<Boolean> responseListener);
	public void updateStockPortfolio(boolean checkUpdate,String token,int idCode,String stockCode,String numberOfShare,String purchasePrice,String note,IStockResponseListener<Boolean> responseListener);
	public void deleteStockPortfolio(String token,int idCode,IStockResponseListener<Boolean> responseListener);
	public void getPortFolioNote(String token,int idCode,IStockResponseListener<Note> responseListener);
	public void getConsensus(String token,String limit,String offset,IStockResponseListener<ArrayList<Consensus>> responseListener);
	
	//-------------------EVENT---------------------------------//
	public void getListEvent(String token,String date,IStockResponseListener<ArrayList<Event>> responseListener);
	
	//--------------------- UPDATE -----------------------------//
		public void updateUserProfile(String fullName,String username,String password,String newPass,String phone,String email,IStockResponseListener<JSONObject> responseListener);

}

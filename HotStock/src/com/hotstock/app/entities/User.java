package com.hotstock.app.entities;

import com.google.gson.annotations.SerializedName;

public class User {
    
    @SerializedName("id")
    public String id;
    
    @SerializedName("username")
    public String username;
    
    @SerializedName("password")
    public String password;
    
    @SerializedName("fullname")
    public String fullname;
    
    @SerializedName("created_time")
    public String created_time;
     
    @SerializedName("role")
    public String role;
    
    @SerializedName("referral_code")
    public String referral_code;
    
    @SerializedName("referrer")
    public String referrer;
    
    @SerializedName("identities")
    public String identities;

}

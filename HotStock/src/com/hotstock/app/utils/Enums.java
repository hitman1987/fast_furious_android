package com.hotstock.app.utils;

import android.content.SharedPreferences;

public class Enums {
	
	public static SharedPreferences sharedPreferences = null;
	
	// preference
		public final static String PREF_CURRENT_PRICE_CODE = "pref current price code";
		public final static String PREF_CURRENT_PRICE_TAB_SELECTED = "PREF_CURRENT_PRICE_TAB_SELECTED";
		public final static String PREF_LIST_STOCK="list stocks";
		public final static String PREF_STOCK_CODE="stock_code";
		public final static String PREF_NOTIFICATION="push_notification";
		public final static String PREF_DEVICE_ID="device_id";
		public final static String PREF_USERNAME = "username";
		public final static String PREF_APP_VERSION="app_version";
	
	// Language indexes
		public static int kLang_System = 0; // Auto detect system language
		public static int kLang_VN = 1;
		public static int kLang_EN = 2;
		public static int kLangFallback = kLang_VN;
		public static int kLangNotInited = -1;
		
		public static String HOSE_MARKET = "HOSE";
		public static String HNX_MARKET = "HNX";
		
		// Data for colors
		public static int PRICE_UNKNOWN = 0;
		public static int PRICE_UP = 1;
		public static int PRICE_DOWN = 2;
		public static int PRICE_NOC = 3;
		public static int PRICE_REF = 4;
		public static int PRICE_CEIL = 5;
		public static int PRICE_FLOOR = 6;
		
		// For search Stock
		public static String searchSym = "";
		public static String searchSymDetail = "";
		public static boolean checkGroupSearch = false;
		
		public static final int MAINVIEW = 0;
		public static final int MARKETVIEW = 1;
		public static final int STOCKSVIEW = 2;
		public static final int WATCHLISTVIEW = 3;
		public static final int PORTFORLIOVIEW = 4;
		public static final int CALENDARVIEW = 5;
		public static final int SETTINGSVIEW = 6;
		public static final int ABOUTVIEW = 7;
		
		public static final String GROUP_CODE ="istock";
		public static final String CODE_RETURN_SUCCESS = "200";
	
}

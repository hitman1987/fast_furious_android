package com.hotstock.app.entities;

import java.util.ArrayList;

public class StockOverviews {
	public int id;
	public String code;
	public String name;
	public ArrayList<Overviews> overviews;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Overviews> getOverviews() {
		return overviews;
	}
	public void setOverviews(ArrayList<Overviews> overviews) {
		this.overviews = overviews;
	}
}

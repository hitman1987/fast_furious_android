package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.model.NavDrawerItem;
import com.hotstock.app.views.MainActivity;

public class NavDrawerListAdapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<NavDrawerItem> navDrawerItems;
	public int color = -1;
	public boolean status = false;
	
	
	public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {		
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }
         
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        final TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        LinearLayout layoutItem = (LinearLayout) convertView.findViewById(R.id.layout_item);
        
        if(color == position && status == true){
        	txtTitle.setTextColor(context.getResources().getColor(R.color.title_main));
        }else{
        	txtTitle.setTextColor(context.getResources().getColor(android.R.color.white));
        }
         
        imgIcon.setImageResource(navDrawerItems.get(position).getIcon());        
        txtTitle.setText(navDrawerItems.get(position).getTitle());
        //changeColor(color,position,txtTitle);
        layoutItem.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				color = position;
				status = true;
				MainActivity.constant.displayView(color);
			}
		});
        
        return convertView;
	}
}

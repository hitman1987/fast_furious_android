package com.hotstock.app.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.hotstock.R;
import com.hotstock.app.AppController;
import com.hotstock.app.adapter.StockNewsAdapter;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.News;
import com.hotstock.app.utils.Enums;
import com.hotstock.app.views.NewDetailsActivity;

public class StocksNewsFragment extends Fragment {

	private IStockRestfulService restService = new IStockRestfulServiceImpl();

	private ListView lvNews;
	private StockNewsAdapter newsListAdapter;
	private String stockCode;
	private ProgressDialog pDialog;
	
	private ArrayList<News> mDataNews= new ArrayList<News>();
	
	private final int LIMITLOAD = 20;
	private int currentNewOffset = 0;
	private boolean isLoading = false;
	
	private static final int GET_NEWS_SUCCESS = 0;
	private static final int GET_NEWS_FAIL = 1;

	public StocksNewsFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_stocks_news, container, false);
		stockCode = getArguments().getString(Enums.PREF_STOCK_CODE, null);
		if(stockCode == null){
			stockCode = "VNM";
		}
		initView(rootView);
		loadDataStockNews(stockCode,false);
		return rootView;
	}
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}

	private void initView(View view) {
		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		
		lvNews = (ListView) view.findViewById(R.id.fragment_stocks_news_lv_main);
		lvNews.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intentMainScreen = new Intent(getActivity(), NewDetailsActivity.class);
				intentMainScreen.putExtra("idNews", mDataNews.get(position).getId());
				startActivity(intentMainScreen);
			}
		});
		
		lvNews.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if (totalItemCount == 0 || visibleItemCount == 0) {
					return;
				}
				if (totalItemCount == firstVisibleItem + visibleItemCount && !isLoading) {
					// 排他制御フラグをON
					isLoading = true;
					loadDataStockNews(stockCode,true);
				}
			}
		});
	}
	
	private Handler mHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case GET_NEWS_SUCCESS:
				hideProgressDialog();
				if(newsListAdapter == null){
					newsListAdapter = new StockNewsAdapter(AppController.getAppContext(),mDataNews);
					lvNews.setAdapter(newsListAdapter);
				}else{
					newsListAdapter.notifyDataSetChanged();
				}
				break;
				
			case GET_NEWS_FAIL:
				
				break;

			default:
				break;
			}
			return false;
		}
	});
	
	private void loadDataStockNews(String stockCode,final boolean isLoadMore) {
		showProgressDialog();
		isLoading = true;
		if (!isLoadMore) {
			currentNewOffset = 0;
		} else {
			currentNewOffset += LIMITLOAD;
		}
		
		restService.getStockNews(LIMITLOAD, currentNewOffset,stockCode, new IStockResponseListener<ArrayList<News>>() {
			
			@Override
			public void onResponse(ArrayList<News> data) {			
				hideProgressDialog();
				isLoading = false;
				if (!isLoadMore) {
					mDataNews.clear();
				}
				if(data.size()> 0){
					for (News item : data) {
						mDataNews.add(item);
					}
				}else{
					isLoading = true;
				}
				
				//mDataNews = data;
				mHandler.sendEmptyMessage(GET_NEWS_SUCCESS);
			}
			
			@Override
			public void onError(Exception ex) {
				hideProgressDialog();
				ex.printStackTrace();
				isLoading = false;
			}
		});
	}

}

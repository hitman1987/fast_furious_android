package com.hotstock.app.views;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.hotstock.R;
import com.hotstock.app.adapter.StockSearchAdapter;
import com.hotstock.app.entities.StockList;
import com.hotstock.app.utils.Enums;
import com.hotstock.app.utils.PreferenceUtils;
import com.hotstock.app.utils.Utils;

public class SearchStockActivity extends Activity implements OnClickListener{
	
	private ListView listAll;
	private ListView listHOSE;
	private ListView listHNX;
	private ListView listUPCOM;
	
	private Button btnAll;
	private Button btnHOSE;
	private Button btnHNX;
	private Button btnUPCOM;
	private ImageButton btnBack;
	private EditText edtSearchStock;
	
	private Context mContext;
	//private ArrayList<StockList> dataStocksAll = new ArrayList<StockList>();
	private ArrayList<StockList> dataSearchAll = new ArrayList<StockList>();
	private ArrayList<StockList> dataStocksHOSE = new ArrayList<StockList>();
	private ArrayList<StockList> dataSearchHOSE = new ArrayList<StockList>();
	private ArrayList<StockList> dataStocksHNX = new ArrayList<StockList>();
	private ArrayList<StockList> dataSearchHNX = new ArrayList<StockList>();
	private ArrayList<StockList> dataStocksUPCOM = new ArrayList<StockList>();
	private ArrayList<StockList> dataSearchUPCOM = new ArrayList<StockList>();
	
	private StockSearchAdapter stockSearchAdapter;
	private StockSearchAdapter adapterSearch;
	private StockSearchAdapter mAdapterHOSE;
	private StockSearchAdapter mAdapterHNX;
	private StockSearchAdapter mAdapterUPCOME;
	private ProgressDialog pDialog;
	
	private static final int SHOW_ALL = 0;
	private static final int SHOW_HOSE = 1;
	private static final int SHOW_HNX = 2;
	private static final int SHOW_UPCOM = 3;
	
	private static final Logger logger = LoggerFactory.getLogger(SearchStockActivity.class);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_stock);
		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		mContext = this;
		initView();
		loadData();
	}
	
	private void loadData(){
		for (int i = 0; i < Utils.mDataStockList.size(); i++) {
			dataSearchAll.add(Utils.mDataStockList.get(i));
			System.out.println("Stock Code" + Utils.mDataStockList.get(i).getCode());
			if (Utils.mDataStockList.get(i).getMarkets().get(0).getId() == 1) {
				dataStocksHOSE.add(Utils.mDataStockList.get(i));
				dataSearchHOSE.add(Utils.mDataStockList.get(i));
			} else if (Utils.mDataStockList.get(i).getMarkets().get(0).getId() == 2) {
				dataStocksHNX.add(Utils.mDataStockList.get(i));
				dataSearchHNX.add(Utils.mDataStockList.get(i));
			} else if (Utils.mDataStockList.get(i).getMarkets().get(0).getId() == 3) {
				dataStocksUPCOM.add(Utils.mDataStockList.get(i));
				dataSearchUPCOM.add(Utils.mDataStockList.get(i));
			}
		}
		mHandler.sendEmptyMessage(SHOW_ALL);
	}
	
	private void initView(){
		showProgressDialog();
		if (edtSearchStock == null) {
			edtSearchStock = (EditText) findViewById(R.id.edt_search_stock);
		}

		if (btnAll == null) {
			btnAll = (Button) findViewById(R.id.search_stock_btn_all);
		}

		if (btnHOSE == null) {
			btnHOSE = (Button) findViewById(R.id.search_stock_btn_hose);
		}

		if (btnHNX == null) {
			btnHNX = (Button) findViewById(R.id.search_stock_btn_hnx);
		}

		if (btnUPCOM == null) {
			btnUPCOM = (Button) findViewById(R.id.search_stock_btn_upcom);
		}

		if (listAll == null) {
			listAll = (ListView) findViewById(R.id.list_search_stock_all);
		}

		if (listHOSE == null) {
			listHOSE = (ListView) findViewById(R.id.list_search_stock_hose);
		}

		if (listHNX == null) {
			listHNX = (ListView) findViewById(R.id.list_search_stock_hnx);
		}

		if (listUPCOM == null) {
			listUPCOM = (ListView) findViewById(R.id.list_search_stock_upcom);
		}
		
		if(btnBack == null){
			btnBack = (ImageButton) findViewById(R.id.search_stock_img_back);
		}
		onTabPress(0);
		listener();
		//loadData();
		
		edtSearchStock.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				
				if(KeyEvent.ACTION_DOWN==event.getAction()&&(KeyEvent.KEYCODE_ENTER==keyCode||Keyboard.KEYCODE_DONE==keyCode)){
					if (listAll.getVisibility() == View.VISIBLE) {
						enter(dataSearchAll, Utils.mDataStockList);
					}
					if (listHNX.getVisibility() == View.VISIBLE) {
						enter(dataSearchHNX, dataStocksHNX);
					}
					if (listHOSE.getVisibility() == View.VISIBLE) {
						enter(dataSearchHOSE, dataStocksHOSE);
					}
					if (listUPCOM.getVisibility() == View.VISIBLE) {
						enter(dataSearchUPCOM, dataStocksUPCOM);
					}
				}
				return false;
			}
		});
		
//		edtSearchStock.addTextChangedListener(new TextWatcher() {
//
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before,
//					int count) {
//				int textlength = edtSearchStock.getText().length();
//				if (textlength != 0) {
//					dataSearchAll.clear();
//					for (int i = 0; i < dataStocksAll.size(); i++) {
//						if (textlength <= dataStocksAll.get(i).getCode()
//								.length()) {
//							if (edtSearchStock
//									.getText()
//									.toString()
//									.equalsIgnoreCase(
//											(String) dataStocksAll
//													.get(i)
//													.getCode()
//													.subSequence(0,
//															textlength))) {
//								dataSearchAll.add(dataStocksAll
//										.get(i));
//							}
//						}
//					}
//					mHandler.sendEmptyMessage(LOAD_DATA_SEARCH);
//				} else {
//					listSearch.setVisibility(View.GONE);
//					listAll.setVisibility(View.VISIBLE);
//				}
				
				listAll.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						if (dataSearchAll.size() > 0) {
							Enums.searchSym = dataSearchAll.get(position).getCode().toString();
							Enums.searchSymDetail = dataSearchAll.get(position).getName().toString();
						} else {
							Enums.searchSym = Utils.mDataStockList.get(position).getCode().toString();
							Enums.searchSymDetail = Utils.mDataStockList.get(position).getName().toString();
						}				
						saveSearchSym(Enums.searchSym);
						
						finish();
					}
				});
				
				listHNX.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						if (dataSearchHNX.size() > 0) {
							Enums.searchSym = dataSearchHNX.get(position).getCode().toString();
							Enums.searchSymDetail = dataSearchHNX.get(position).getName().toString();
						} else {
							Enums.searchSym = Utils.mDataStockList.get(position).getCode().toString();
							Enums.searchSymDetail = Utils.mDataStockList.get(position).getName().toString();
						}				
						saveSearchSym(Enums.searchSym);
						
						finish();
					}
				});
				
				listHOSE.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						if (dataSearchHOSE.size() > 0) {
							Enums.searchSym = dataSearchHOSE.get(position).getCode().toString();
							Enums.searchSymDetail = dataSearchHOSE.get(position).getName().toString();
						} else {
							Enums.searchSym = Utils.mDataStockList.get(position).getCode().toString();
							Enums.searchSymDetail = Utils.mDataStockList.get(position).getName().toString();
						}				
						saveSearchSym(Enums.searchSym);
						
						finish();
					}
				});
				
				listUPCOM.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						if (dataSearchUPCOM.size() > 0) {
							Enums.searchSym = dataSearchUPCOM.get(position).getCode().toString();
							Enums.searchSymDetail = dataSearchUPCOM.get(position).getName().toString();
						} else {
							Enums.searchSym = Utils.mDataStockList.get(position).getCode().toString();
							Enums.searchSymDetail = Utils.mDataStockList.get(position).getName().toString();
						}				
						saveSearchSym(Enums.searchSym);
						
						finish();
					}
				});
				
				edtSearchStock.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start, int before,
							int count) {
				
				if (s.length() > 0) {
					ArrayList<StockList> dataTemp = new ArrayList<StockList>();

					if (listAll.getVisibility() == View.VISIBLE) {
						if (dataSearchAll.size() > 0) {
							dataSearchAll.clear();
						}
						dataTemp = Utils.mDataStockList;
					}
					if (listHNX.getVisibility() == View.VISIBLE) {
						if (dataSearchHNX.size() > 0) {
							dataSearchHNX.clear();
						}
						dataTemp = dataStocksHNX;
					}
					if (listHOSE.getVisibility() == View.VISIBLE) {
						if (dataSearchHOSE.size() > 0) {
							dataSearchHOSE.clear();
						}
						dataTemp = dataStocksHOSE;
					}
					if (listUPCOM.getVisibility() == View.VISIBLE) {
						if (dataSearchUPCOM.size() > 0) {
							dataSearchUPCOM.clear();
						}
						dataTemp = dataStocksUPCOM;
					}

					if (dataTemp != null) {
						for (StockList item : dataTemp) {
							String code = item.getCode().toString();
							s = s.toString().toUpperCase();
							if (code.contains(s)) {
								if (listAll.getVisibility() == View.VISIBLE) {
									dataSearchAll.add(item);
									
								}
								if (listHNX.getVisibility() == View.VISIBLE) {
									dataSearchHNX.add(item);
								}
								if (listHOSE.getVisibility() == View.VISIBLE) {
									dataSearchHOSE.add(item);
								}
								if (listUPCOM.getVisibility() == View.VISIBLE) {
									dataSearchUPCOM.add(item);
								}
							}
						}

						if (listAll.getVisibility() == View.VISIBLE) {
							mHandler.sendEmptyMessage(SHOW_ALL);
						}
						if (listHNX.getVisibility() == View.VISIBLE) {
							mHandler.sendEmptyMessage(SHOW_HNX);
						}
						if (listHOSE.getVisibility() == View.VISIBLE) {
							mHandler.sendEmptyMessage(SHOW_HOSE);
						}
						if (listUPCOM.getVisibility() == View.VISIBLE) {
							mHandler.sendEmptyMessage(SHOW_UPCOM);
						}
					}
				} else {
					if (listAll.getVisibility() == View.VISIBLE
							&& dataSearchAll.size() > 0) {
						// dataSearchALL.clear();
						mHandler.sendEmptyMessage(SHOW_ALL);
					}
					if (listHNX.getVisibility() == View.VISIBLE
							&& dataSearchHNX.size() > 0) {
						// dataSearchHNX.clear();
						mHandler.sendEmptyMessage(SHOW_HNX);
					}
					if (listHOSE.getVisibility() == View.VISIBLE
							&& dataSearchHOSE.size() > 0) {
						// dataSearchHOSE.clear();
						mHandler.sendEmptyMessage(SHOW_HOSE);
					}
					if (listUPCOM.getVisibility() == View.VISIBLE
							&& dataSearchUPCOM.size() > 0) {
						// dataSearchUPCOM.clear();
						mHandler.sendEmptyMessage(SHOW_UPCOM);
					}
				}
			}
				

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}
	
	private void onTabPress(int index) {
		// Initialize default
		btnAll.setBackgroundResource(R.drawable.tab_left);
		btnAll.setTextColor(getResources().getColor(R.color.text_color_grey));
		btnHOSE.setBackgroundResource(R.drawable.tab_center);
		btnHOSE.setTextColor(getResources().getColor(R.color.text_color_grey));		
		btnHNX.setBackgroundResource(R.drawable.tab_center);
		btnHNX.setTextColor(getResources().getColor(R.color.text_color_grey));
		btnUPCOM.setBackgroundResource(R.drawable.tab_right);
		btnUPCOM.setTextColor(getResources().getColor(R.color.text_color_grey));

		// Set tab depend on index
		switch (index) {
		case 0:
			mHandler.sendEmptyMessage(SHOW_ALL);
			btnAll.setBackgroundResource(R.drawable.tab_pressed);
			btnAll.setTextColor(getResources().getColor(R.color.text_color_yellow));
			listAll.setVisibility(View.VISIBLE);
			listHNX.setVisibility(View.GONE);
			listUPCOM.setVisibility(View.GONE);
			listHOSE.setVisibility(View.GONE);
			break;

		case 1:
			mHandler.sendEmptyMessage(SHOW_HOSE);
			btnHOSE.setBackgroundResource(R.drawable.tab_pressed);
			btnHOSE.setTextColor(getResources().getColor(R.color.text_color_yellow));
			listAll.setVisibility(View.GONE);
			listHNX.setVisibility(View.GONE);
			listUPCOM.setVisibility(View.GONE);
			listHOSE.setVisibility(View.VISIBLE);
			break;

		case 2:
			mHandler.sendEmptyMessage(SHOW_HNX);
			btnHNX.setBackgroundResource(R.drawable.tab_pressed);
			btnHNX.setTextColor(getResources().getColor(R.color.text_color_yellow));
			listAll.setVisibility(View.GONE);
			listHNX.setVisibility(View.VISIBLE);
			listUPCOM.setVisibility(View.GONE);
			listHOSE.setVisibility(View.GONE);
			break;

		case 3:
			mHandler.sendEmptyMessage(SHOW_UPCOM);
			btnUPCOM.setBackgroundResource(R.drawable.tab_pressed);
			btnUPCOM.setTextColor(getResources().getColor(R.color.text_color_yellow));
			listAll.setVisibility(View.GONE);
			listHNX.setVisibility(View.GONE);
			listUPCOM.setVisibility(View.VISIBLE);
			listHOSE.setVisibility(View.GONE);
			break;
		}
	}


	
	private void saveSearchSym(String sym) {
		if (!TextUtils.isEmpty(sym)) {
			logger.debug(SearchStockActivity.class.getSimpleName(), "sym save to preference: " + sym);
			// save in preference.
			PreferenceUtils.writeString(getApplicationContext(),Enums.PREF_CURRENT_PRICE_CODE, sym);
		}
	}
	
	private void enter(ArrayList<StockList> data,ArrayList<StockList> dataFull){
		if (data.size() > 0) {
			Enums.searchSym = data.get(0).getCode();
			Enums.searchSymDetail = data.get(0).getName();
		} else {
			if(dataFull.size() > 0){
				Enums.searchSym = dataFull.get(0).getCode();
				Enums.searchSymDetail = dataFull.get(0).getName();
			}			
		}
		saveSearchSym(Enums.searchSym);		
		finish();
	}
	
	private void listener(){
		btnAll.setOnClickListener(this);
		btnHOSE.setOnClickListener(this);
		btnHNX.setOnClickListener(this);
		btnUPCOM.setOnClickListener(this);
		btnBack.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.search_stock_btn_all:
			onTabPress(0);
			break;
		case R.id.search_stock_btn_hose:
			onTabPress(1);
			break;
		case R.id.search_stock_btn_hnx:
			onTabPress(2);
			break;
		case R.id.search_stock_btn_upcom:
			onTabPress(3);
			break;
		case R.id.search_stock_img_back:
			finish();
			break;
		default:
			break;
		}
	}
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}
	
	private Handler mHandler = new Handler(new Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case SHOW_ALL:
				hideProgressDialog();
				if (dataSearchAll.size() > 0) {
					adapterSearch = new StockSearchAdapter(mContext,dataSearchAll);
					listAll.setAdapter(adapterSearch);
				} else {
					adapterSearch = new StockSearchAdapter(mContext, Utils.mDataStockList);
					listAll.setAdapter(adapterSearch);
				}
				break;
				
			case SHOW_HOSE:
				if (dataSearchHOSE.size() > 0) {
					mAdapterHOSE = new StockSearchAdapter(mContext,
							dataSearchHOSE);
					listHOSE.setAdapter(mAdapterHOSE);
				} else {
					// if (mAdapterHOSE == null) {
					mAdapterHOSE = new StockSearchAdapter(mContext, dataStocksHOSE);
					listHOSE.setAdapter(mAdapterHOSE);
					// } else {
					// mAdapterHOSE.notifyDataSetChanged();
					// }
				}
				break;

			case SHOW_HNX:
				if (dataSearchHNX.size() > 0) {
					mAdapterHNX = new StockSearchAdapter(mContext,
							dataSearchHNX);
					listHNX.setAdapter(mAdapterHNX);
				} else {
					// if (mAdapterHNX == null) {
					mAdapterHNX = new StockSearchAdapter(mContext, dataStocksHNX);
					listHNX.setAdapter(mAdapterHNX);
					// } else {
					// mAdapterHNX.notifyDataSetChanged();
					// }
				}
				break;

			case SHOW_UPCOM:
				if (dataSearchUPCOM.size() > 0) {
					mAdapterUPCOME = new StockSearchAdapter(mContext,
							dataSearchUPCOM);
					listUPCOM.setAdapter(mAdapterUPCOME);
				} else {
					// if (mAdapterUPCOME == null) {
					mAdapterUPCOME = new StockSearchAdapter(mContext, dataStocksUPCOM);
					listUPCOM.setAdapter(mAdapterUPCOME);
					// } else {
					// mAdapterUPCOME.notifyDataSetChanged();
					// }
				}


			default:
				break;
			}
			return false;
		}
	});
}

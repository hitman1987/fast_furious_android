package com.hotstock.app.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hotstock.R;

public class MarketFragmentFirst extends Fragment {
	
	public MarketFragmentFirst(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_market_first, container, false);
         
        return rootView;
    }
}

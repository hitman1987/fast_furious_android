package com.hotstock.app.entities;

import java.util.List;

public class StockStatistic {
	public int id;
	public String code;
	public String name;
	public List<Consults> Consults;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Consults> getConsults() {
		return Consults;
	}

	public void setConsults(List<Consults> consults) {
		Consults = consults;
	}
}

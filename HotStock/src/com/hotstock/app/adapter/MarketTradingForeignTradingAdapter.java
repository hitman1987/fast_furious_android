package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.ForeignTrades;
import com.hotstock.app.utils.Utils;

public class MarketTradingForeignTradingAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private ArrayList<ForeignTrades> mData = new ArrayList<ForeignTrades>();
	private Context mContext;

	public MarketTradingForeignTradingAdapter(Context c, ArrayList<ForeignTrades> d) {
		this.mContext = c;
		this.mData = d;
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(
					R.layout.adapter_foreign_trading, null);
			viewHolder.layoutMain = (ViewGroup) convertView
					.findViewById(R.id.item_trading_overview);
			viewHolder.txtTotalPurchase = (TextView) convertView
					.findViewById(R.id.foreign_trading_total_purchase);
			viewHolder.txtTotalSale = (TextView) convertView
					.findViewById(R.id.foreign_trading_total_sale);
			viewHolder.txtNetPurChase = (TextView) convertView
					.findViewById(R.id.foreign_trading_net_purchase);
			viewHolder.txtTotalPurchase.setTextColor(mContext.getResources().getColor(android.R.color.white));
			viewHolder.txtTotalSale.setTextColor(mContext.getResources().getColor(android.R.color.white));
			viewHolder.txtNetPurChase.setTextColor(mContext.getResources().getColor(android.R.color.white));
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		ForeignTrades currentItem = mData.get(position);
		viewHolder.txtTotalPurchase.setText(Utils.formatAsStockVolume(currentItem.getTotal_purchases()));
		viewHolder.txtTotalSale.setText(Utils.formatAsStockVolume(currentItem.getTotal_sales()));
		viewHolder.txtNetPurChase.setText(Utils.formatAsStockVolume(currentItem.getNet_purchases()));

		if (position % 2 == 0) {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_1));
		} else {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_2));
		}

		return convertView;
	}
	private class ViewHolder {
		private ViewGroup layoutMain;
		private TextView txtTotalPurchase;
		private TextView txtTotalSale;
		private TextView txtNetPurChase;
	}

}

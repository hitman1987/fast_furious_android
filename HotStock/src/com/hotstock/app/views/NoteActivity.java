package com.hotstock.app.views;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.Note;
import com.hotstock.app.entities.WatchListNote;
import com.hotstock.app.utils.AutofitHelper;
import com.hotstock.app.utils.IStockModel;
import com.hotstock.app.utils.Utils;

public class NoteActivity extends Activity implements OnClickListener{
	
	private ImageButton btnBack;
	private TextView txtStock;
	
	private TextView txtValue2;
	private TextView txtValue3;
	private TextView txtValue4;
	
	private TextView txtStockCode;
	private TextView txtStockStatus;
	private TextView txtStockCurrentPrice;
	private TextView txtStockTargetPrice;
	private EditText edtStockContent;
	private TextView txtStockContent;
	
	private boolean statusEdit;
	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	private WatchListNote mWatchListNote = new WatchListNote();
	private Note mDataNote = new Note();
	
	private int id;
	private ProgressDialog pDialog;
	
	private static final int GET_NOTE_SUCCESS = 0;
	private static final int GET_NOTE_FAIL = 1;
	private static final int UPDATE_STOCK_SUCCESS = 2;
	private static final int UPDATE_STOCK_FAIL = 3;
	private static final int GET_PORTFOLIO_NOTE_SUCCESS = 4;
	private static final int GET_PORTFOLIO_NOTE_FAIL = 5;
	
	//----------portfolio------------------------
	private FrameLayout layoutButton;
	private RelativeLayout layoutTitleMarketPrice;
	private ImageView imgTitleMarketPrice;
	private RelativeLayout layoutValueMarketPrice;
	private TextView txtMarketPrice;
	private String code;
	private double numberOfShare;
	private double purchasePrice;
	private double marketPrice;
	private double percent;
	private ImageButton btnEditNote;
	private ImageButton btnUpdateNote;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note);
		initView();
	}
	
	private void initView(){
		
		statusEdit = getIntent().getBooleanExtra("statusEdit", false);
		id = getIntent().getExtras().getInt("id", -1);
		
		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		
		if(btnBack == null){
			btnBack = (ImageButton) findViewById(R.id.activity_note_img_back);
		}
		
		if(btnEditNote == null){
			btnEditNote = (ImageButton) findViewById(R.id.activity_note_btn_edit);
		}
		
		if(btnUpdateNote == null){
			btnUpdateNote = (ImageButton) findViewById(R.id.activity_note_btn_update);
		}
		
		if(txtStock == null){
			txtStock = (TextView) findViewById(R.id.activity_note_txt_stock);
		}
		
		///////////////////-----UI PORTFOLIO------/////////////////////////////
		if(txtValue2 == null){
			txtValue2 = (TextView) findViewById(R.id.activity_note_txt_value_2);
		}
		if(txtValue3 == null){
			txtValue3 = (TextView) findViewById(R.id.activity_note_txt_value_3);
		}
		if(txtValue4 == null){
			txtValue4 = (TextView) findViewById(R.id.activity_note_txt_value_4);
		}
		
		
		if(txtStockCode == null){
			txtStockCode = (TextView) findViewById(R.id.activity_note_txt_stock_code);
		}
		if(txtStockStatus == null){
			txtStockStatus = (TextView) findViewById(R.id.activity_note_txt_status);
		}
		if(txtStockCurrentPrice == null){
			txtStockCurrentPrice = (TextView) findViewById(R.id.activity_note_txt_current_price);
		}
		if(txtStockTargetPrice == null){
			txtStockTargetPrice = (TextView) findViewById(R.id.activity_note_txt_target_price);
		}
		if(txtStockContent == null){
			txtStockContent = (TextView) findViewById(R.id.activity_note_txt_content);
			txtStockContent.setMovementMethod(new ScrollingMovementMethod());
		}
		if(edtStockContent == null){
			edtStockContent = (EditText) findViewById(R.id.activity_note_edt_content);
		}
		
		//---------------portfolio----------------------------------------------------------
		
		if(layoutButton == null){
			layoutButton = (FrameLayout) findViewById(R.id.activity_note_layout_edit_update);
		}
		
		if(layoutTitleMarketPrice == null){
			layoutTitleMarketPrice = (RelativeLayout) findViewById(R.id.activity_note_layout_title_market_price);
		}
		if(imgTitleMarketPrice == null){
			imgTitleMarketPrice = (ImageView) findViewById(R.id.activity_note_img_market_price);
		}
		if(layoutValueMarketPrice == null){
			layoutValueMarketPrice = (RelativeLayout) findViewById(R.id.activity_note_layout_value_market_price);
		}
		if(txtMarketPrice == null){
			txtMarketPrice = (TextView) findViewById(R.id.activity_note_txt_market_price);
		}		
		
		if(statusEdit){
			layoutButton.setVisibility(View.VISIBLE);
			layoutTitleMarketPrice.setVisibility(View.VISIBLE);
			imgTitleMarketPrice.setVisibility(View.VISIBLE);
			layoutValueMarketPrice.setVisibility(View.VISIBLE);
			txtMarketPrice.setVisibility(View.VISIBLE);
			code = getIntent().getExtras().getString("code", null);
			numberOfShare = getIntent().getExtras().getDouble("number", 0);
			purchasePrice = getIntent().getExtras().getDouble("purchase", 0);
			marketPrice = getIntent().getExtras().getDouble("marketPrice", 0);
			percent = getIntent().getExtras().getDouble("percent", 0);
			
			txtValue2.setText(getApplication().getResources().getString(R.string.fragment_watchlist_consensus_title_buy));
			txtValue3.setText(getApplication().getResources().getString(R.string.fragment_watchlist_consensus_title_sell));
			txtValue4.setText(getApplication().getResources().getString(R.string.fragment_watchlist_consensus_title_hold));
			
			AutofitHelper.create(txtStockStatus);
			AutofitHelper.create(txtStockCurrentPrice);
			AutofitHelper.create(txtMarketPrice);
			AutofitHelper.create(txtStockTargetPrice);
			
			txtStock.setText(code);
			txtStockCode.setText(code);
			txtStockStatus.setText(Utils.formatAsStockVolume(numberOfShare));
			txtStockCurrentPrice.setText(Utils.formatAsStockVolume(purchasePrice));
			txtMarketPrice.setText(Utils.formatAsStockVolume(marketPrice));
			txtStockTargetPrice.setText(percent + "%");
			loadPortFolioNote(IStockModel.getInstance().getAccessToken(),Integer.valueOf(id));
	
		}else{
			loadData();
		}
		
		listener();
		
	}
	
	private void listener(){
		btnBack.setOnClickListener(this);
		btnEditNote.setOnClickListener(this);
		btnUpdateNote.setOnClickListener(this);
	}
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}
	
	private void loadData(){
		showProgressDialog();
		restService.getWatchlistNote(id, new IStockResponseListener<ArrayList<WatchListNote>>() {
			
			@Override
			public void onResponse(ArrayList<WatchListNote> data) {
				if(data!= null && data.size()>0){
					mWatchListNote = data.get(0);
					mHandler.sendEmptyMessage(GET_NOTE_SUCCESS);
				}else{
					mHandler.sendEmptyMessage(GET_NOTE_FAIL);
				}			
			}
			
			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
				mHandler.sendEmptyMessage(GET_NOTE_FAIL);
			}
		});
	}
	
	
	private void updatePortfolio(boolean checkUpdate,String token,int idStock,String stockCode,String numberOfShare,String purchasePrice,String note){
		showProgressDialog();
		restService.updateStockPortfolio(checkUpdate, token, idStock, stockCode, numberOfShare, purchasePrice,note, new IStockResponseListener<Boolean>() {
			
			@Override
			public void onResponse(Boolean data) {
				hideProgressDialog();
				if(data){
					mHandler.sendEmptyMessage(UPDATE_STOCK_SUCCESS);
				}else{
					mHandler.sendEmptyMessage(UPDATE_STOCK_FAIL);
				}
			}
			
			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
				hideProgressDialog();
				mHandler.sendEmptyMessage(UPDATE_STOCK_FAIL);
			}
		});
	}
	
	
	private void loadPortFolioNote(String token,int id){
		restService.getPortFolioNote(token, id, new IStockResponseListener<Note>() {
			
			@Override
			public void onResponse(Note data) {
				if(data!=null){
					mDataNote = data;
					mHandler.sendEmptyMessage(GET_PORTFOLIO_NOTE_SUCCESS);
				}
			}
			
			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
				mHandler.sendEmptyMessage(GET_PORTFOLIO_NOTE_FAIL);
			}
		});
	}
	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.activity_note_img_back:
			finish();
			break;
			
		case R.id.activity_note_btn_edit:
			txtStockContent.setVisibility(View.GONE);
			edtStockContent.setVisibility(View.VISIBLE);
			btnEditNote.setVisibility(View.GONE);
			btnUpdateNote.setVisibility(View.VISIBLE);
			break;
			
		case R.id.activity_note_btn_update:
			String note = edtStockContent.getText().toString().trim();
			updatePortfolio(true,IStockModel.getInstance().getAccessToken(),id,code,String.valueOf(numberOfShare),String.valueOf(purchasePrice),note);
			break;

		default:
			break;
		}
	}
	
	private Handler mHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case GET_NOTE_SUCCESS:
				hideProgressDialog();
				txtStock.setText(mWatchListNote.getCode());
				txtStockCode.setText(mWatchListNote.getCode());
				txtStockStatus.setText(mWatchListNote.getRate());
				txtStockStatus.setTextColor(getResources().getColor(Utils.colorStatus(mWatchListNote.getRate())));
				txtStockCurrentPrice.setText(Utils.formatAsStockVolume(mWatchListNote.getBasic_price()));
				txtStockTargetPrice.setText(Utils.formatAsStockVolume(mWatchListNote.getTargeted_price()));
				txtStockContent.setText(mWatchListNote.getNote());
				edtStockContent.setText(mWatchListNote.getNote());
				break;
				
			case GET_NOTE_FAIL:
				hideProgressDialog();
				break;
				
			case UPDATE_STOCK_SUCCESS:
				btnEditNote.setVisibility(View.VISIBLE);
				btnUpdateNote.setVisibility(View.GONE);
				txtStockContent.setVisibility(View.VISIBLE);
				edtStockContent.setVisibility(View.GONE);
				loadPortFolioNote(IStockModel.getInstance().getAccessToken(),Integer.valueOf(id));
				break;
				
			case UPDATE_STOCK_FAIL:
				
				break;
				
			case GET_PORTFOLIO_NOTE_SUCCESS:
				txtStockContent.setText(mDataNote.getNote());
				edtStockContent.setText(mDataNote.getNote());
				break;
				
			case GET_PORTFOLIO_NOTE_FAIL:
				
				break;

			default:
				break;
			}
			return false;
		}
	});
}

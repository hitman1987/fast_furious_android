package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.StockList;
import com.hotstock.app.utils.AutofitHelper;

public class StockSearchAdapter extends BaseAdapter{
	
	private ArrayList<StockList> mData;
	private Context mContext;
	private LayoutInflater mInflater = null;
	
	public StockSearchAdapter(Context context,ArrayList<StockList> data){
		this.mContext = context;
		this.mData = data;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.search_stock_items, null);
			viewHolder = new ViewHolder();
			viewHolder.txtCode = (TextView) convertView.findViewById(R.id.search_stock_items_code);
			viewHolder.txtName = (TextView) convertView.findViewById(R.id.search_stock_items_name);
			viewHolder.layoutMain = (LinearLayout) convertView.findViewById(R.id.search_stock_items_layout_main);
			AutofitHelper.create(viewHolder.txtName);
			convertView.setTag(viewHolder);
		}else {
			viewHolder = (ViewHolder) convertView.getTag();
		}			
		viewHolder.txtCode.setText(mData.get(position).getCode());
		viewHolder.txtName.setText(mData.get(position).getName());
		
		if (position % 2 == 0) {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources().getColor(R.color.market_list_item_1));
		} else {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources().getColor(R.color.market_list_item_2));
		}
		
		return convertView;
	}
	
	private class ViewHolder{
		private LinearLayout layoutMain;
		private TextView txtCode;
		private TextView txtName;		
	}

}

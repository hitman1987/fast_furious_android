package com.hotstock.app.entities;

public class MainWatchList {
	 public int id;
	 public String code;
	 public String title;
	 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getNote() {
		return title;
	}
	public void setNote(String note) {
		this.title = note;
	}
}

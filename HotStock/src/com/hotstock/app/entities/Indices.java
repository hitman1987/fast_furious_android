package com.hotstock.app.entities;


public class Indices {
	public double index;
	public double percentage_change;
	public double volume;
    public String updated_time;
    public String value;
    
	public double getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public double getPercentage_change() {
		return percentage_change;
	}
	public void setPercentage_change(double percentage_change) {
		this.percentage_change = percentage_change;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public String getUpdated_time() {
		return updated_time;
	}
	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}
	public void setIndex(double index) {
		this.index = index;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}

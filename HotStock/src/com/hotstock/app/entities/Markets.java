package com.hotstock.app.entities;

import java.util.List;

public class Markets {
	public int id;
	public String code;
	public String name;
	public List<Indices> Indices;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Indices> getIndices() {
		return Indices;
	}

	public void setIndices(List<Indices> indices) {
		Indices = indices;
	}
}

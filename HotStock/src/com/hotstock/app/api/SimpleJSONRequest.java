package com.hotstock.app.api;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Volley adapter for JSON requests that will be parsed into Java objects by
 * Gson.
 */
public class SimpleJSONRequest<T> extends Request<T> {
	private final Gson gson = new Gson();
	private final Listener<T> listener;
	private final Class<T> mClazz;
	private Map<String, String> mHeaders;

	protected Map<String, String> mParams;

	/**
	 * Make a GET request and return a parsed object from JSON.
	 * 
	 * @param url
	 *            URL of the request to make
	 * @param mClazz
	 *            Relevant class object, for Gson's reflection
	 * @param mHeaders
	 *            Map of request mHeaders
	 */
	public SimpleJSONRequest(String url, Class<T> clazz, Map<String, String> headers, Listener<T> listener, ErrorListener errorListener) {
		super(Method.GET, url, errorListener);
		this.mClazz = clazz;
		this.mHeaders = headers;
		this.listener = listener;
	}

	@Override
	public void deliverError(VolleyError error) {
		super.deliverError(error);
	}

	@Override
	protected void deliverResponse(T response) {
		if (listener == null) {
			return;
		}
		listener.onResponse(response);
	}

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		mHeaders = new HashMap<String, String>();

		mHeaders.put("Authorization","auth");

		return mHeaders;
	}

	@Override
	protected Map<String, String> getParams() throws AuthFailureError {
		// TODO return the params
		return mParams;
	}

	@Override
	protected Response<T> parseNetworkResponse(NetworkResponse response) {
		try {
			String json = new String(response.data, "UTF-8");
			return Response.success(gson.fromJson(json, mClazz), HttpHeaderParser.parseCacheHeaders(response));
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		} catch (JsonSyntaxException e) {
			return Response.error(new ParseError(e));
		}
	}
	
	@Override
	protected VolleyError parseNetworkError(VolleyError volleyError) {
		return volleyError;
	}

	/**
	 * set params for request
	 * 
	 * @param params
	 */
	public void setParams(Map<String, String> params) {
		mParams = params;
	}
}

package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.MainWatchList;

public class WatchListTrendAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private Context mContext;
	private ArrayList<MainWatchList> mData = new ArrayList<MainWatchList>();

	public WatchListTrendAdapter(Context c, ArrayList<MainWatchList> d) {
		this.mContext = c;
		this.mData = d;
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(
					com.hotstock.R.layout.watch_list_trend_items, null);
			viewHolder.layoutMain = (LinearLayout) convertView.findViewById(R.id.watch_List_trend_layout_main);
			viewHolder.txtStockCode = (TextView) convertView.findViewById(R.id.watch_List_trend_txt_stock);
			viewHolder.txtNote = (TextView) convertView.findViewById(R.id.watch_List_trend_txt_note);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		viewHolder.txtStockCode.setText(mData.get(position).getCode());
		viewHolder.txtNote.setText(mData.get(position).getNote());
		
		if (position % 2 == 0) {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_1));
		} else {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_2));
		}

		return convertView;
	}

	private class ViewHolder {
		private LinearLayout layoutMain;
		private TextView txtStockCode;
		private TextView txtNote;
	}
}

package com.hotstock.app.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class QueryString {
	private String host = StringUtils.EMPTY_STR;
	private String query = StringUtils.EMPTY_STR;
	private String params = StringUtils.EMPTY_STR;

	public QueryString(String host, String query) {
		this.host = host;
		this.query = query;
	}

	public void add(String param, String value) {
		if (StringUtils.isNotEmpty(params)){
				params += "&";
			}
		if(value == null){
			value =" ";
		}
		encode(param, value);
	}

	private void encode(String name, String value) {
		try {
			params += URLEncoder.encode(name, "UTF-8");
			params += "=";
			params += URLEncoder.encode(value, "UTF-8");
		} catch (UnsupportedEncodingException ex) {
			throw new RuntimeException("Broken VM does not support UTF-8");
		}
	}

	public String getParams() {
		return params;
	}

	public String toString() {
		return host + query + "?" + params;
	}

}

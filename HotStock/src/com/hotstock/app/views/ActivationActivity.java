package com.hotstock.app.views;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hotstock.R;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.DataReceiver;
import com.hotstock.app.pushnotification.PushNotificationUtil;
import com.hotstock.app.utils.Enums;
import com.hotstock.app.utils.IStockModel;
import com.hotstock.app.utils.Utils;

public class ActivationActivity extends Activity implements OnClickListener{
	
	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	
	private EditText edtActivationCode;
	private EditText edtUsername;
	private EditText edtPassword;
	private EditText edtFullname;
	private EditText edtEmail;
	private TextView txtReadMore;
	private Button btnSubmit;
	private Button btnCancel;
	
	private String activationCode;
	private String userName;
	private String passWord;
	private String fullName;
	private String address;
	private String email;
	private String phoneNumber;
	
	private ImageView imageCheck;
	private ImageView imageUnCheck;
	
	private Context mContext;
	private ProgressDialog pDialog;
	private int sdk;
	
	private String errorRegister = null;
	
	private static final int ACTIVATION_SUCCESS = 0;
	private static final int ACTIVATION_FAIL = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activation);
		initView();
		sdk = android.os.Build.VERSION.SDK_INT;
		// Init Push Notification
		PushNotificationUtil.getInstance(this).register(this);
	}
	
	private void initView(){
		
		mContext = this;
		
		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		
		errorRegister = mContext.getResources().getString(R.string.activity_activiation_error);
		
		if (Enums.sharedPreferences == null) {
			Enums.sharedPreferences = Utils.getSharedPreferences(mContext,
					Context.MODE_PRIVATE);
		}
		
		if(edtActivationCode == null){
			edtActivationCode = (EditText) findViewById(R.id.activity_activation_et_active_code);
		}
		if(edtUsername == null){
			edtUsername = (EditText) findViewById(R.id.activity_activation_et_username);
		}
		if(edtPassword == null){
			edtPassword = (EditText) findViewById(R.id.activity_activation_et_password);
		}
		if(edtFullname == null){
			edtFullname = (EditText) findViewById(R.id.activity_activation_et_fullname);
		}
		
		if(edtEmail == null){
			edtEmail = (EditText) findViewById(R.id.activity_activation_edt_email);
		}
		
		if(txtReadMore == null){
			txtReadMore = (TextView) findViewById(R.id.activity_activation_tv_read_more);
		}
		
		if(imageCheck == null){
			imageCheck = (ImageView) findViewById(R.id.activity_activation_img_check);
		}
		
		if(imageUnCheck == null){
			imageUnCheck = (ImageView) findViewById(R.id.activity_activation_img_un_check);
		}
		
		if(btnSubmit == null){
			btnSubmit = (Button) findViewById(R.id.activity_activation_btn_sumbit);
		}
		if(btnCancel == null){
			btnCancel = (Button) findViewById(R.id.activity_activation_btn_cancel);
		}
		
		listener();
	}
	
	private void listener(){
		btnSubmit.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		imageCheck.setOnClickListener(this);
		imageUnCheck.setOnClickListener(this);
		txtReadMore.setOnClickListener(this);
	}
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}
	
	private void register(String activattionCode,String userName,String passWord,String fullName,String email,
			String address,String tel,final String deviceId,final String idPushNotification){
		showProgressDialog();
		restService.register(userName, passWord, fullName, email, deviceId, activattionCode,idPushNotification, new IStockResponseListener<String>() {
			
			@Override
			public void onResponse(String data) {
				System.out.println("data = " + data);
				try {
					hideProgressDialog();
					JSONObject jsonObject = new JSONObject(data);
					DataReceiver register = new Gson().fromJson((new JSONObject(jsonObject.getString("meta"))).toString(), DataReceiver.class);
					if(register.code.equals(Enums.CODE_RETURN_SUCCESS)){
						JSONObject jsonObject2 = new JSONObject(jsonObject.getString("data"));
						String token = jsonObject2.optString("token", null);
						if(token != null && token.length() > 0){
//							Utils.saveStringPreferences(Enums.PREF_NOTIFICATION, idPushNotification, Enums.sharedPreferences);
							Utils.saveStringPreferences(Enums.PREF_DEVICE_ID, deviceId, Enums.sharedPreferences);
							IStockModel.getInstance().setNotificationToken(idPushNotification);
							IStockModel.getInstance().setDeviceId(deviceId);
							IStockModel.getInstance().setAccessToken(token);
							mHandler.sendEmptyMessage(ACTIVATION_SUCCESS);
						}
					}else{
						hideProgressDialog();
						errorRegister = register.error;
						mHandler.sendEmptyMessage(ACTIVATION_FAIL);
					}
					
				} catch (JSONException e) {
					hideProgressDialog();
					mHandler.sendEmptyMessage(ACTIVATION_FAIL);
					e.printStackTrace();
				}			
			}
			
			@Override
			public void onError(Exception ex) {
				hideProgressDialog();
				ex.printStackTrace();
				mHandler.sendEmptyMessage(ACTIVATION_FAIL);
			}
		});
	}

	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.activity_activation_btn_sumbit:
			activationCode = edtActivationCode.getText().toString().trim();
			userName = edtUsername.getText().toString();
			passWord = edtPassword.getText().toString();
			fullName = edtFullname.getText().toString();
			email = edtEmail.getText().toString();
			
			if(activationCode == null || activationCode.length() == 0){
				Toast.makeText(mContext, "Activation code is not blank!", Toast.LENGTH_SHORT).show();
				return;
			}
			if(userName == null || userName.length() == 0){
				Toast.makeText(mContext, "UserName is not blank!", Toast.LENGTH_SHORT).show();
				return;
			}
			if(passWord == null || passWord.length() == 0){
				Toast.makeText(mContext, "Password is not blank!", Toast.LENGTH_SHORT).show();
				return;
			}
			if(fullName == null || fullName.length() == 0){
				Toast.makeText(mContext, "FullName is not blank!", Toast.LENGTH_SHORT).show();
				return;
			}
			if(email == null || email.length() == 0){
				Toast.makeText(mContext, "Email is not blank!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if(imageUnCheck.getVisibility() == View.VISIBLE){
				Toast.makeText(mContext, "Please check accept agreement!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			register(activationCode, userName, passWord, fullName, email, address, phoneNumber, Utils.getUniquePsuedoID(),Utils.loadPreferences(Enums.sharedPreferences, Enums.PREF_NOTIFICATION));//Utils.getUniquePsuedoID(),idPush);
			break;
			
		case R.id.activity_activation_btn_cancel:
			finish();
			break;
			
		case R.id.activity_activation_img_un_check:
			
			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				btnSubmit.setBackgroundDrawable((getResources().getDrawable(R.drawable.activation_activity_button)));
			} else {
				btnSubmit.setBackground(getResources().getDrawable(R.drawable.activation_activity_button));
			}
			imageCheck.setVisibility(View.VISIBLE);
			imageUnCheck.setVisibility(View.GONE);
			break;
			
		case R.id.activity_activation_img_check:
			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				btnSubmit.setBackgroundDrawable((getResources().getDrawable(R.drawable.inactive_button)));
			} else {
				btnSubmit.setBackground(getResources().getDrawable(R.drawable.inactive_button));
			}
			imageCheck.setVisibility(View.GONE);
			imageUnCheck.setVisibility(View.VISIBLE);
			break;
			
		case R.id.activity_activation_tv_read_more:
			Intent intentActivation = new Intent(mContext, TermsAndConditionsActivity.class);
			startActivity(intentActivation);
			break;

		default:
			break;
		}
	}
	
	private void showDialog(final boolean status){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Message
		if(status){
			alertDialog.setMessage(getResources().getString(R.string.activity_activiation_success));
		}else{
			alertDialog.setMessage(errorRegister);
		}
        
 
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.icon_app);
 
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	if(status){
            		finish();
            	}
            }
        });
        // Showing Alert Message
        AlertDialog message =  alertDialog.show();
        TextView messageView = (TextView)message.findViewById(android.R.id.message);
		messageView.setGravity(Gravity.CENTER);
	}
	
private Handler mHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case ACTIVATION_SUCCESS:
				showDialog(true);
				break;
				
			case ACTIVATION_FAIL:
				showDialog(false);
				break;

			default:
				break;
			}
			return false;
		}
	});
	
}

package com.hotstock.app.fragment;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.hotstock.R;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.utils.IStockModel;
import com.hotstock.app.utils.StringUtils;

public class SettingsFragment extends Fragment implements OnClickListener{
	
	private EditText edtFullName;
	private EditText edtEmail;
	private EditText edtTel;
	private EditText edtCurrentPassword;
	private EditText edtNewPassword;
	private EditText edtConfirmPassword;
	private Button btnUpdate;
	private Button btnCancel;
	public static SettingsFragment instant;
	private String message;
	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	private JSONObject jsonData;
	
	private static final int UPDATE_SUCCESS = 0;
	private static final int UPDATE_FAIL = 1;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instant = this;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_user_profile, container, false);
		initView(rootView);
        return rootView;
	}
	
	private void initView(View v){
		if(edtFullName == null){
			edtFullName = (EditText) v.findViewById(R.id.activity_user_profile_edt_fullName);
		}
		if(edtEmail == null){
			edtEmail = (EditText) v.findViewById(R.id.activity_user_profile_edt_email);
		}
		if(edtTel == null){
			edtTel = (EditText) v.findViewById(R.id.activity_user_profile_edt_tel);
		}
		if(edtCurrentPassword == null){
			edtCurrentPassword = (EditText) v.findViewById(R.id.activity_user_profile_edt_current_password);
		}
		if(edtNewPassword == null){
			edtNewPassword = (EditText) v.findViewById(R.id.activity_user_profile_edt_new_password);
		}
		if(edtConfirmPassword == null){
			edtConfirmPassword = (EditText) v.findViewById(R.id.activity_user_profile_edt_confirm_password);
		}
		if(btnUpdate == null){
			btnUpdate = (Button) v.findViewById(R.id.activity_user_profile_btn_update);
		}
		if(btnCancel == null){
			btnCancel = (Button) v.findViewById(R.id.activity_user_profile_btn_refresh);
		}
		listener();
		loadData();
	}
	
	private void listener(){
		btnUpdate.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
	}
	
	private void updateProfile(String fullName,String username,String password,String newPassword,String phone,String email){
		restService.updateUserProfile(fullName, username, password, newPassword, phone, email, new IStockResponseListener<JSONObject>() {
			
			@Override
			public void onResponse(JSONObject data) {
				if(data!= null && data.length() > 0){
					jsonData = data;
					mHandler.sendEmptyMessage(UPDATE_SUCCESS);
				}else{
					mHandler.sendEmptyMessage(UPDATE_FAIL);
				}
			}
			
			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
				mHandler.sendEmptyMessage(UPDATE_FAIL);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.activity_user_profile_btn_update:
			String fullName = edtFullName.getText().toString().trim();			
			String password = edtCurrentPassword.getText().toString().trim();			
			String newPassword = edtNewPassword.getText().toString().trim();			
			String confirmPassword = edtConfirmPassword.getText().toString().trim();			
			String phone = edtTel.getText().toString().trim();			
			String email = edtEmail.getText().toString().trim();
			
			if(fullName ==null || fullName.length() == 0){
				message = getActivity().getResources().getString(R.string.activity_userprofile_str_error_fullname);
				showDialog(message);
				return;
			}
			if(password ==null || password.length() == 0){
				message = getActivity().getResources().getString(R.string.activity_userprofile_str_error_password_empty);
				showDialog(message);
				return;
			}
			
			if(phone ==null || phone.length() == 0){
				message = getActivity().getResources().getString(R.string.activity_userprofile_str_error_phone);
				showDialog(message);
				return;
			}
			if(email == null || email.length() == 0){
				message = getActivity().getResources().getString(R.string.activity_userprofile_str_error_email);
				showDialog(message);
				return;
			}						
			
			if (newPassword.trim().length() == 0 && confirmPassword.trim().length() == 0) {
				newPassword = password;
			}		
			
			if (!(newPassword.equals(confirmPassword))) {
				message = getActivity().getResources().getString(R.string.activity_userprofile_str_error_password);
				showDialog(message);
				return;
			}
			updateProfile(fullName, IStockModel.getInstance().getUserName(), password, newPassword, phone, email);
			break;
			
		case R.id.activity_user_profile_btn_refresh:
			loadData();
			edtNewPassword.setText("");
			edtConfirmPassword.setText("");
			break;

		default:
			break;
		}
	}
	
	private void showDialog(final String message) {
		new Handler().post(new Runnable() {
			
			@Override
			public void run() {
				DialogFragment newFragment = MyAlertDialogFragment.newInstance(R.string.app_name,message);
				newFragment.show(getFragmentManager(), "dialog");
			}
		});
	}
	
	private void loadData(){
		edtFullName.setText(IStockModel.getInstance().getFullName());
		edtEmail.setText(IStockModel.getInstance().getEmail());
		edtTel.setText(IStockModel.getInstance().getPhoneNumber());
		edtCurrentPassword.setText(IStockModel.getInstance().getPassWord());
	}

	public void doPositiveClick() {
		
	}
	
	private static class MyAlertDialogFragment extends DialogFragment {

		public static MyAlertDialogFragment newInstance(int title,String message) {
			MyAlertDialogFragment fragmentDialogFragment = new MyAlertDialogFragment();
			Bundle args = new Bundle();
			args.putInt("title", title);
			args.putString("message",message);
			fragmentDialogFragment.setArguments(args);
			return fragmentDialogFragment;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			int title = getArguments().getInt("title");
			String message = getArguments().getString("message", null);
			Dialog dialog = null;
				dialog = new AlertDialog.Builder(getActivity())
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int whichButton) {
								SettingsFragment.instant.doPositiveClick();
							}
						}).create();
				dialog.setCancelable(false);
				dialog.setCanceledOnTouchOutside(false);
			 
			return dialog;
		}
	}
	
	private Handler mHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case UPDATE_SUCCESS:
				message = getActivity().getResources().getString(R.string.activity_userprofile_str_success);
				showDialog(message);
				try {
					JSONObject jsonObject = new JSONObject(jsonData.getString("data"));
					IStockModel.getInstance().setPassWord(jsonObject.optString("password", StringUtils.EMPTY_STR));
					IStockModel.getInstance().setFullName(jsonObject.optString("fullname", StringUtils.EMPTY_STR));
					IStockModel.getInstance().setPhoneNumber(jsonObject.optString("phone", StringUtils.EMPTY_STR));
					IStockModel.getInstance().setEmail(jsonObject.optString("email", StringUtils.EMPTY_STR));
					loadData();
					edtNewPassword.setText("");
					edtConfirmPassword.setText("");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				break;
				
			case UPDATE_FAIL:
				message = getActivity().getResources().getString(R.string.activity_userprofile_str_failed);
				showDialog(message);
				break;

			default:
				break;
			}
			return false;
		}
	});
}

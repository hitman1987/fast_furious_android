package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.Snapshots;
import com.hotstock.app.utils.Utils;

public class StockSnapshotListAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private Context mContext;
	private ArrayList<Snapshots> Snapshots;

	public StockSnapshotListAdapter(Context c, ArrayList<Snapshots> Snapshots) {
		this.mContext = c;
		this.Snapshots = Snapshots;
		mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		if (Snapshots == null || Snapshots.size() <= 0) {
			return 0;
		}
		return Snapshots.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(com.hotstock.R.layout.item_stock_snapshot, null);
			viewHolder.tvLowestin52weeks = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_lowestin52weeks);
			viewHolder.tvHighestin52weeks = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_highestin52weeks);
			viewHolder.tvAvaragevolumein10days = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_avaragevolumein10days);
			viewHolder.tvDividendYield = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_dividend_yield);
			viewHolder.tvMarketCapitalization = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_market_capitalization);
			viewHolder.tvOutstandingShares = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_outstanding_shares);
			viewHolder.tvListedShares = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_listed_shares);
			viewHolder.tvUpdateTime = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_updated_time);
			viewHolder.tvEps4q = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_eps_4q);
			viewHolder.tvRoaTrailing = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_roa_trailing);
			viewHolder.tvRoeTrailing = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_roe_trailing);
			viewHolder.tvPeTrailing = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_pe_trailing);
			viewHolder.tvPb = (TextView) convertView.findViewById(R.id.item_stock_snapshot_tv_pb);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		Snapshots dto = Snapshots.get(position);

		viewHolder.tvLowestin52weeks.setText(dto.getLowestin52weeks());
		viewHolder.tvHighestin52weeks.setText(dto.getHighestin52weeks());
		viewHolder.tvAvaragevolumein10days.setText(Utils.formatStockVolume(dto.getAvaragevolumein10days()));
		viewHolder.tvDividendYield.setText(dto.getDividend_yield());
		viewHolder.tvMarketCapitalization.setText(Utils.formatMarketIndex(dto.getMarket_capitalization()));
		viewHolder.tvOutstandingShares.setText(Utils.formatStockVolume(dto.getOutstanding_shares()));
		viewHolder.tvListedShares.setText(Utils.formatStockVolume(dto.getListed_shares()));
		viewHolder.tvUpdateTime.setText(Utils.convertStringtoDate(dto.getUpdated_time()));
		viewHolder.tvEps4q.setText(Utils.formatStockVolume(dto.getEps_4q()));
		viewHolder.tvRoaTrailing.setText(dto.getRoa_trailing());
		viewHolder.tvRoeTrailing.setText(dto.getRoe_trailing());
		viewHolder.tvPeTrailing.setText(Utils.formatMarketIndex(dto.getPe_trailing()));
		viewHolder.tvPb.setText(Utils.formatMarketIndex(dto.getPb()));

		return convertView;
	}

	private class ViewHolder {
		private TextView tvLowestin52weeks;
		private TextView tvHighestin52weeks;
		private TextView tvAvaragevolumein10days;
		private TextView tvDividendYield;
		private TextView tvMarketCapitalization;
		private TextView tvOutstandingShares;
		private TextView tvListedShares;
		private TextView tvUpdateTime;
		private TextView tvEps4q;
		private TextView tvRoaTrailing;
		private TextView tvRoeTrailing;
		private TextView tvPeTrailing;
		private TextView tvPb;
	}

}

package com.hotstock.app.pushnotification;

public class PushReceiveItem {

	public Aps aps;
	public News news;
	
	public PushReceiveItem() {
		aps = new Aps();
		news = new News();
	}
	
	public class Aps {
		public String alert = "";
	}
	
	public class News {
		public int id;
	}
}

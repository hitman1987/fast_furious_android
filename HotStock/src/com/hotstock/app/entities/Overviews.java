package com.hotstock.app.entities;

public class Overviews {
	public int id;
	public String published_time;
	public String description;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPublished_time() {
		return published_time;
	}
	public void setPublished_time(String published_time) {
		this.published_time = published_time;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}

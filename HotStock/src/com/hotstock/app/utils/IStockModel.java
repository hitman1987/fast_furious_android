package com.hotstock.app.utils;

/**
 * Global data.
 * 
 * @author tamnguyen
 */
public class IStockModel {

	private String accessToken = StringUtils.EMPTY_STR;
	private String userId = StringUtils.EMPTY_STR;
	private String notificationToken = StringUtils.EMPTY_STR;
	private String fullName = StringUtils.EMPTY_STR;
	private String deviceId = StringUtils.EMPTY_STR;
	private String userName = StringUtils.EMPTY_STR;
	private String passWord = StringUtils.EMPTY_STR;
	private String email = StringUtils.EMPTY_STR;
	private String phoneNumber = StringUtils.EMPTY_STR;

	private static IStockModel instance;
	public static synchronized IStockModel getInstance() {
		if (instance == null) {
			instance = new IStockModel();
		}
		return instance;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getNotificationToken() {
		return notificationToken;
	}

	public void setNotificationToken(String notificationToken) {
		this.notificationToken = notificationToken;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	
}

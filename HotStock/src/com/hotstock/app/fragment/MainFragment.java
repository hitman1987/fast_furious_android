package com.hotstock.app.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.android.volley.VolleyLog;
import com.hotstock.R;
import com.hotstock.app.AppController;
import com.hotstock.app.adapter.MarketAdapter;
import com.hotstock.app.adapter.TopNewsAdapter;
import com.hotstock.app.adapter.WatchListTrendAdapter;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.MainWatchList;
import com.hotstock.app.entities.Markets;
import com.hotstock.app.entities.News;
import com.hotstock.app.views.NewDetailsActivity;
import com.hotstock.app.views.WatchListNoteActivity;

public class MainFragment extends Fragment {

	private View viewFragment;
	private ListView listMarket;
	private ListView listNews;
	private ListView listWatchList;
	
	private MarketAdapter mAdapterMarket;
	private TopNewsAdapter mAdapterTopNews;
	private WatchListTrendAdapter mAdapterWatchList;
	
	private ArrayList<Markets> mDataMarkets = new ArrayList<Markets>();
	private ArrayList<News> mDataNews= new ArrayList<News>();
	private ArrayList<MainWatchList> mDataWatchLists = new ArrayList<MainWatchList>();
	
	private static final String TAG = "Main Fragment";
	
	private static final int GET_MARKET_SUCCESS = 0;
	private static final int GET_MARKET_FAIL = 1;
	private static final int GET_NEWS_SUCCESS = 2;
	private static final int GET_NEWS_FAIL = 3;
	private static final int GET_WATCHLIST_TREND_SUCCESS = 4;
	private static final int GET_WATCHLIST_TREND_FAIL = 5;
	
	private Intent intentMainScreen;
	private ProgressDialog pDialog;
	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	
	private final int LIMITLOAD = 20;
	private int currentNewOffset = 0;
	private boolean isLoading = false;
	
	private boolean checkFinishMarket = false;
	private boolean checkFinishNews = false;
	private boolean checkFinishWatchlist = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		viewFragment = inflater.inflate(R.layout.activity_main_screen, container,false);
		initView(viewFragment);
		return viewFragment;
	}

	private void initView(View view) {
		if(listMarket == null){
			listMarket = (ListView)view.findViewById(R.id.fragment_main_screen_list_market);
		}
		
		if(listNews == null){
			listNews = (ListView)view.findViewById(R.id.fragment_main_screen_list_news);
			listNews.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					intentMainScreen = new Intent(AppController.getAppContext(), NewDetailsActivity.class);
					intentMainScreen.putExtra("idNews", mDataNews.get(position).getId());
					startActivity(intentMainScreen);
				}
			});
			
			listNews.setOnScrollListener(new OnScrollListener() {
				
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					
				}
				
				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,
						int visibleItemCount, int totalItemCount) {
					if (totalItemCount == 0 || visibleItemCount == 0) {
						return;
					}
					if (totalItemCount == firstVisibleItem + visibleItemCount && !isLoading) {
						// 排他制御フラグをON
						isLoading = true;
						loadDataMarketNews(true);
					}
				}
			});
		}
		
		if(listWatchList == null){
			listWatchList = (ListView)view.findViewById(R.id.fragment_main_screen_list_watch_list);
			listWatchList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
					intentMainScreen = new Intent(AppController.getAppContext(), WatchListNoteActivity.class);
					intentMainScreen.putExtra("id", mDataWatchLists.get(position).id);
					intentMainScreen.putExtra("statusEdit", false);
					startActivity(intentMainScreen);
				}
			});;
		}
		
		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		
		loadDataMarketNews(false);
		loadData();	
	}
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}
	
	private void loadData(){
		showProgressDialog();
		
		/////////// Get MarKet Main //////////////////////////
		restService.getMarketList(new IStockResponseListener<ArrayList<Markets>>() {
			
			@Override
			public void onResponse(ArrayList<Markets> data) {
				checkFinishMarket = true;
				mDataMarkets = data;
				mHandler.sendEmptyMessage(GET_MARKET_SUCCESS);
			}
			
			@Override
			public void onError(Exception ex) {
				checkFinishMarket = true;
				VolleyLog.d(TAG, ex);
				mHandler.sendEmptyMessage(GET_MARKET_FAIL);
			}
		});
		
		///////////// Get WatchList ./////////////////////
		restService.getWatchlist(new IStockResponseListener<ArrayList<MainWatchList>>() {
			
			@Override
			public void onResponse(ArrayList<MainWatchList> data) {
				checkFinishWatchlist = true;
				mDataWatchLists = data;
				mHandler.sendEmptyMessage(GET_WATCHLIST_TREND_SUCCESS);
			}
			
			@Override
			public void onError(Exception ex) {
				checkFinishWatchlist = true;
				ex.printStackTrace();
				mHandler.sendEmptyMessage(GET_WATCHLIST_TREND_FAIL);
			}
		});
	}
	
	private void loadDataMarketNews(final boolean isLoadMore) {
		showProgressDialog();
		isLoading = true;
		if (!isLoadMore) {
			currentNewOffset = 0;
		} else {
			currentNewOffset += LIMITLOAD;
		}
		
		restService.getNewsList(String.valueOf(LIMITLOAD), String.valueOf(currentNewOffset), new IStockResponseListener<ArrayList<News>>() {
			
			@Override
			public void onResponse(ArrayList<News> data) {
				checkFinishNews = true;
				isLoading = false;
				if (!isLoadMore) {
					mDataNews.clear();
				}
				for (News item : data) {
					mDataNews.add(item);
				}
				
				//mDataNews = data;
				mHandler.sendEmptyMessage(GET_NEWS_SUCCESS);
			}
			
			@Override
			public void onError(Exception ex) {
				checkFinishNews = true;
				mHandler.sendEmptyMessage(GET_NEWS_FAIL);
				ex.printStackTrace();
				isLoading = false;
			}
		});
	}
	
	private Handler mHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case GET_MARKET_SUCCESS:
				checkLoadProgress();
				if(mAdapterMarket == null){
					mAdapterMarket = new MarketAdapter(AppController.getAppContext(),mDataMarkets);
					listMarket.setAdapter(mAdapterMarket);
				}else{
					mAdapterMarket.notifyDataSetChanged();
				}
				break;
			case GET_MARKET_FAIL:
				checkLoadProgress();			
				break;
			case GET_NEWS_SUCCESS:
				checkLoadProgress();
				if(mAdapterTopNews == null){
					mAdapterTopNews = new TopNewsAdapter(AppController.getAppContext(),mDataNews);
					listNews.setAdapter(mAdapterTopNews);
				}else{
					mAdapterTopNews.notifyDataSetChanged();
				}
				break;
			case GET_NEWS_FAIL:
				checkLoadProgress();
				break;
			case GET_WATCHLIST_TREND_SUCCESS:
				checkLoadProgress();
				if(mAdapterWatchList == null){
					mAdapterWatchList = new WatchListTrendAdapter(AppController.getAppContext(),mDataWatchLists);
					listWatchList.setAdapter(mAdapterWatchList);
				}else{
					mAdapterWatchList.notifyDataSetChanged();
				}
				break;
			case GET_WATCHLIST_TREND_FAIL:
				checkLoadProgress();
				break;

			default:
				break;
			}
			return false;
		}
	});
	
	private void checkLoadProgress(){
		if(checkFinishMarket && checkFinishNews && checkFinishWatchlist){
			hideProgressDialog();
		}
	}

}

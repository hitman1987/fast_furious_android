package com.hotstock.app.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hotstock.R;

public class MarketFragmentSecond extends Fragment {
	
	public MarketFragmentSecond(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_market_second, container, false);
         
        return rootView;
    }
}

package com.hotstock.app.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.hotstock.R;

public class PushPopupActivity extends Activity implements View.OnClickListener {

	private String idNews = "";
	private String contenNews = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.push_notification_popup);

		Bundle extras = getIntent().getBundleExtra("pushinfo");
		if (extras != null) {
			idNews = extras.getString("news", "1");
			contenNews = extras.getString("message", null);
		}
		setupViews();
	}

	@Override
	public void onResume() {
		super.onResume();

		if (getIntent().getBooleanExtra("newIntent", false)) {
			setCurrentValue();
			getIntent().putExtra("newIntent", false);
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Bundle extras = intent.getBundleExtra("pushinfo");
		if (extras != null) {
			idNews = extras.getString("news", "1");
			contenNews = extras.getString("message", null);
		}
		setupViews();
	}

	public static Intent getBidPopupIntent(Context context) {
		Intent intent = new Intent(context, PushPopupActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		return intent;
	}

	private void setupViews() {
		((TextView) this.findViewById(R.id.push_notification_popup_txt_content)).setText(contenNews);
		this.findViewById(R.id.push_layout_popup).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						try {
							Intent intentMainScreen = new Intent(
									PushPopupActivity.this,
									NewDetailsActivity.class);
							intentMainScreen.putExtra("idNews",
									Integer.parseInt(idNews));
							startActivity(intentMainScreen);
							android.util.Log
									.i("tuancuong", "idNews: " + idNews);
							PushPopupActivity.this.finish();
						} catch (Exception e) {
						}
					}

				});
		this.findViewById(R.id.push_dialog_parent_view).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						try {
							PushPopupActivity.this.finish();
						} catch (Exception e) {
						}
					}

				});
	}

	@Override
	public void onClick(View v) {
	}

	private void setCurrentValue() {
	}

}
package com.hotstock.app.entities;

public class StockLastDay {
	
	public double last_price;
	public double volume;
	public double total_volume;
	public String trading_time;
	
	public double getLast_price() {
		return last_price;
	}
	public void setLast_price(double last_price) {
		this.last_price = last_price;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public double getTotal_volume() {
		return total_volume;
	}
	public void setTotal_volume(double total_volume) {
		this.total_volume = total_volume;
	}
	public String getTrading_time() {
		return trading_time;
	}
	public void setTrading_time(String trading_time) {
		this.trading_time = trading_time;
	}
	
	
}

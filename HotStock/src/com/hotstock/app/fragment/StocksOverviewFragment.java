package com.hotstock.app.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hotstock.R;
import com.hotstock.app.AppController;
import com.hotstock.app.adapter.StockOverviewListAdapter;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.Overviews;
import com.hotstock.app.entities.StockOverviews;
import com.hotstock.app.model.OverviewTestDto;
import com.hotstock.app.utils.Enums;
import com.hotstock.app.utils.Utils;

public class StocksOverviewFragment extends Fragment {

	private IStockRestfulService restService = new IStockRestfulServiceImpl();

	private ListView lvOverview;
	private StockOverviewListAdapter overviewListAdapter;

	private String stockCode;
	
	private ProgressDialog pDialog;

	public StocksOverviewFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_stocks_overview, container, false);

		stockCode = getArguments().getString(Enums.PREF_STOCK_CODE, null);
		if (stockCode == null) {
			stockCode = "VNM";
		}
		initView(rootView);
		loadData(stockCode);

		return rootView;
	}

	private void initView(View view) {
		lvOverview = (ListView) view.findViewById(R.id.fragment_stocks_overview_lv_main);
		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
	}
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}

	private void loadData(String stockCode) {
		showProgressDialog();
		final ArrayList<OverviewTestDto> dtos = new ArrayList<OverviewTestDto>();

		restService.getStockOverview(stockCode, new IStockResponseListener<ArrayList<StockOverviews>>() {

			@Override
			public void onResponse(ArrayList<StockOverviews> data) {
				if (data.get(0).getOverviews().size() > 0) {
					OverviewTestDto dto;
					String prevTime = "";
					for (Overviews overviewItem : data.get(0).getOverviews()) {
						String time = Utils.convertStringtoDate(overviewItem.getPublished_time());
						time = time.substring(time.length() - 4, time.length());
						String description = overviewItem.getDescription();

						if (dtos.size() > 1) {
							if (!prevTime.equals(time)) {
								dto = new OverviewTestDto(0, time, "");
								prevTime = time;
								dtos.add(dto);
							}
						} else {
							dto = new OverviewTestDto(0, time, "");
							prevTime = time;
							dtos.add(dto);
						}
						dto = new OverviewTestDto(1, "", description);
						dtos.add(dto);
					}
				}

				overviewListAdapter = new StockOverviewListAdapter(AppController.getAppContext(), dtos);
				lvOverview.setAdapter(overviewListAdapter);
				hideProgressDialog();
			}

			@Override
			public void onError(Exception ex) {
				hideProgressDialog();
			}
		});
	}

}

package com.hotstock.app.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hotstock.R;
import com.hotstock.app.AppController;
import com.hotstock.app.adapter.PortfolioAdapter;
import com.hotstock.app.animation.BaseSwipeListViewListener;
import com.hotstock.app.animation.SwipeListView;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.Portfolio;
import com.hotstock.app.utils.IStockModel;
import com.hotstock.app.utils.Utils;
import com.hotstock.app.views.NoteActivity;

public class PortfolioFragment extends Fragment implements OnClickListener {

	private SwipeListView listPortfolio;
	private LinearLayout layoutAddNew;
	private TextView txtAddNew;
	private EditText edtAddStock;
	private EditText edtPurcharsePrice;
	private EditText edtNumberOfShare;
	private Button btnOK;
	private Button btnCancel;
	private ImageButton imgOpenLayoutAdd;
	private ImageButton imgCloseLayoutAdd;
	private Boolean statusAdd = true;

	private View rootView;
	private ProgressDialog pDialog;

	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	private ArrayList<Portfolio> mData = new ArrayList<Portfolio>();
	private Portfolio mPortfolio = new Portfolio();
	private PortfolioAdapter mAdapter;
	private int idStockUpdate = -1;

	private static final int LOAD_PORTFOLIO_SUCCESS = 0;
	private static final int LOAD_PORTFOLIO_FAIL = 1;
	private static final int ADD_STOCK_SUCCESS = 2;
	private static final int ADD_STOCK_FAIL = 3;
	private static final int UPDATE_STOCK_SUCCESS = 4;
	private static final int UPDATE_STOCK_FAIL = 5;
	private static final int DELETE_STOCK_SUCCESS = 6;
	private static final int DELETE_STOCK_FAIL = 7;
	
	public static PortfolioFragment instant;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		instant = this;
		rootView = inflater.inflate(R.layout.fragment_portfolio,
				container, false);
		initView(rootView);
		return rootView;
	}

	private void initView(View v) {
		if (listPortfolio == null) {
			listPortfolio = (SwipeListView) v
					.findViewById(R.id.fragment_portfolio_list);
			listPortfolio
					.setSwipeListViewListener(new BaseSwipeListViewListener() {
						@Override
						public void onOpened(int position, boolean toRight) {
						}

						@Override
						public void onClosed(int position, boolean fromRight) {
						}

						@Override
						public void onListChanged() {
						}

						@Override
						public void onMove(int position, float x) {
						}

						@Override
						public void onStartOpen(int position, int action,
								boolean right) {
							Log.d("swipe", String.format(
									"onStartOpen %d - action %d", position,
									action));
						}

						@Override
						public void onStartClose(int position, boolean right) {
							Log.d("swipe",
									String.format("onStartClose %d", position));
						}

						@Override
						public void onClickFrontView(int position) {
							Intent intentPortfolio = new Intent(AppController.getAppContext(),
									NoteActivity.class);
							intentPortfolio.putExtra("statusEdit", true);
							intentPortfolio.putExtra("id", mData.get(position)
									.getId());
							intentPortfolio.putExtra("code", mData
									.get(position).getCode());
							intentPortfolio.putExtra("number",
									mData.get(position).getNumberofshares());
							intentPortfolio.putExtra("purchase",
									mData.get(position).getPurchase_price());
							intentPortfolio.putExtra("marketPrice",
									mData.get(position).getMarket_price());
							intentPortfolio.putExtra("percent",
									mData.get(position).getPercentage_change());
							startActivity(intentPortfolio);
							// Toast.makeText(AppController.getAppContext(),
							// "Comming soon....", Toast.LENGTH_SHORT).show();

							// listPortfolio.openAnimate(position); //when you
							// touch front view it will open

						}

						@Override
						public void onClickBackView(int position) {
							Log.d("swipe", String.format("onClickBackView %d",
									position));

							listPortfolio.closeAnimate(position);// when you
																	// touch
																	// back view
																	// it will
																	// close
						}

						@Override
						public void onDismiss(int[] reverseSortedPositions) {

						}
					});

			// These are the swipe listview settings. you can change these
			// setting as your requirement
			listPortfolio.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
			listPortfolio.setOffsetLeft(convertDpToPixel(200f)); // left side
																	// offset
			listPortfolio.setOffsetRight(convertDpToPixel(80f)); // right side
																	// offset
			listPortfolio.setAnimationTime(100); // Animation time
			listPortfolio.setSwipeOpenOnLongPress(true); // enable or disable
															// SwipeOpenOnLongPress

		}

		if (layoutAddNew == null) {
			layoutAddNew = (LinearLayout) v.findViewById(R.id.fragment_portfolio_layout_add_new);
		}

		if (txtAddNew == null) {
			txtAddNew = (TextView) v.findViewById(R.id.fragment_portfolio_txt_add);
		}

		if (edtAddStock == null) {
			edtAddStock = (EditText) v.findViewById(R.id.fragment_portfolio_edt_stock);
		}

		if (edtNumberOfShare == null) {
			edtNumberOfShare = (EditText) v.findViewById(R.id.fragment_portfolio_edt_number_of_share);
		}

		if (edtPurcharsePrice == null) {
			edtPurcharsePrice = (EditText) v.findViewById(R.id.fragment_portfolio_edt_purchase_price);
		}
		
		if (imgOpenLayoutAdd == null) {
			imgOpenLayoutAdd = (ImageButton) v.findViewById(R.id.fragment_portfolio_img_open_layout_add_new_portfolio);
		}
		
		if (imgCloseLayoutAdd == null) {
			imgCloseLayoutAdd = (ImageButton) v.findViewById(R.id.fragment_portfolio_img_close_layout_add_new_portfolio);
		}
		
		if (btnOK == null) {
			btnOK = (Button) v
					.findViewById(R.id.fragment_portfolio_btn_ok);
		}

		if (btnOK == null) {
			btnOK = (Button) v
					.findViewById(R.id.fragment_portfolio_btn_ok);
		}

		if (btnCancel == null) {
			btnCancel = (Button) v
					.findViewById(R.id.fragment_portfolio_btn_cancel);
		}

		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);

		listener();
		loadData();
	}

	private void listener() {
		btnOK.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		imgOpenLayoutAdd.setOnClickListener(this);
		imgCloseLayoutAdd.setOnClickListener(this);
	}

	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}

	public void updateStock(int position) {
		imgCloseLayoutAdd.setVisibility(View.VISIBLE);
		imgOpenLayoutAdd.setVisibility(View.GONE);
		layoutAddNew.setVisibility(View.VISIBLE);
		statusAdd = false;
		txtAddNew.setText(AppController.getAppContext().getResources().getString(
				R.string.fragment_portfolio_title_update));
		btnOK.setText(AppController.getAppContext().getResources().getString(
				R.string.fragment_portfolio_title_update));
		edtAddStock.setText(mData.get(position).getCode());
		edtNumberOfShare.setText(Utils.formatAsStockVolume(mData.get(position)
				.getNumberofshares()));
		edtPurcharsePrice.setText(Utils.formatAsStockVolume(mData.get(position)
				.getPurchase_price()));
		idStockUpdate = mData.get(position).getId();
		listPortfolio.closeAnimate(position);
	}

	public void deleteStock(int position) {
		showProgressDialog();
		int portfolioID = mData.get(position).getId();
		if (portfolioID > 0) {
			restService.deleteStockPortfolio(IStockModel.getInstance()
					.getAccessToken(), portfolioID,
					new IStockResponseListener<Boolean>() {

						@Override
						public void onResponse(Boolean data) {

							if (data) {
								mHandler.sendEmptyMessage(ADD_STOCK_SUCCESS);
							} else {
								mHandler.sendEmptyMessage(ADD_STOCK_FAIL);
							}
						}

						@Override
						public void onError(Exception ex) {
							hideProgressDialog();
							ex.printStackTrace();
							mHandler.sendEmptyMessage(DELETE_STOCK_FAIL);
						}
					});
		}

	}

	private void loadData() {
		showProgressDialog();
		restService.getPortfolio(10, 0, IStockModel.getInstance()
				.getAccessToken(),
				new IStockResponseListener<ArrayList<Portfolio>>() {

					@Override
					public void onResponse(ArrayList<Portfolio> data) {
						hideProgressDialog();
						mData.clear();
						if (data != null && data.size() > 0) {
							listPortfolio.setVisibility(View.VISIBLE);
							mData = data;
							mHandler.sendEmptyMessage(LOAD_PORTFOLIO_SUCCESS);
						} else if (data.size() == 0) {
							listPortfolio.setVisibility(View.GONE);
						}
					}

					@Override
					public void onError(Exception ex) {
						hideProgressDialog();
						mHandler.sendEmptyMessage(LOAD_PORTFOLIO_FAIL);
						ex.printStackTrace();
					}
				});
	}

	private void addStock(String token, String stockCode, String numberOfShare,
			String purchasePrice) {
		showProgressDialog();
		restService.addStockPortfolio(token, stockCode, numberOfShare,
				purchasePrice, new IStockResponseListener<Boolean>() {

					@Override
					public void onResponse(Boolean data) {
						hideProgressDialog();
						if (data) {
							mHandler.sendEmptyMessage(ADD_STOCK_SUCCESS);
						} else {
							mHandler.sendEmptyMessage(ADD_STOCK_FAIL);
						}
					}

					@Override
					public void onError(Exception ex) {
						hideProgressDialog();
						ex.printStackTrace();
						mHandler.sendEmptyMessage(ADD_STOCK_FAIL);
					}
				});
	}

	private void updateStock(boolean checkUpdate,String token, int idStock, String stockCode,
			String numberOfShare, String purchasePrice, String note) {
		showProgressDialog();
		restService.updateStockPortfolio(checkUpdate,token, idStock, stockCode,
				numberOfShare, purchasePrice, note,
				new IStockResponseListener<Boolean>() {

					@Override
					public void onResponse(Boolean data) {
						hideProgressDialog();
						if (data) {
							mHandler.sendEmptyMessage(UPDATE_STOCK_SUCCESS);
						} else {
							mHandler.sendEmptyMessage(UPDATE_STOCK_FAIL);
						}
					}

					@Override
					public void onError(Exception ex) {
						ex.printStackTrace();
						hideProgressDialog();
						mHandler.sendEmptyMessage(UPDATE_STOCK_FAIL);
					}
				});
	}

	private Handler mHandler = new Handler(new Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case LOAD_PORTFOLIO_SUCCESS:
				// if(mAdapter == null){
				mAdapter = new PortfolioAdapter(AppController.getAppContext(),
						mData);
				listPortfolio.setAdapter(mAdapter);
				// }else{
				// mAdapter.notifyDataSetChanged();
				// }
				break;

			case LOAD_PORTFOLIO_FAIL:

				break;

			case ADD_STOCK_SUCCESS:
				edtAddStock.setText("");
				edtNumberOfShare.setText("");
				edtPurcharsePrice.setText("");
				loadData();
				break;

			case ADD_STOCK_FAIL:
				break;

			case UPDATE_STOCK_SUCCESS:
				statusAdd = true;
				txtAddNew.setText(AppController.getAppContext().getResources().getString(
						R.string.fragment_portfolio_title_add_new));
				btnOK.setText(AppController.getAppContext().getResources().getString(
						R.string.fragment_portfolio_title_add_new));
				edtAddStock.setText("");
				edtNumberOfShare.setText("");
				edtPurcharsePrice.setText("");
				loadData();
				break;

			case UPDATE_STOCK_FAIL:

				break;

			case DELETE_STOCK_SUCCESS:
				loadData();
				break;

			case DELETE_STOCK_FAIL:

				break;

			default:
				break;
			}
			return false;
		}
	});

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.fragment_portfolio_btn_ok:
			String strCode = edtAddStock.getText().toString().trim();
			String strNumberOfShare = edtNumberOfShare.getText().toString()
					.trim();
			String strPurchase = edtPurcharsePrice.getText().toString().trim();

			if (strCode == null || strCode.length() == 0) {
				Toast.makeText(AppController.getAppContext(),
						"Invalid Stock Code!", Toast.LENGTH_SHORT).show();
				return;
			}

			if (strNumberOfShare == null || strNumberOfShare.length() == 0) {
				Toast.makeText(AppController.getAppContext(),
						"Invalid Number of Share!", Toast.LENGTH_SHORT).show();
				return;
			}

			if (strPurchase == null || strPurchase.length() == 0) {
				Toast.makeText(AppController.getAppContext(),
						"Invalid Purchase Price!", Toast.LENGTH_SHORT).show();
				return;
			}

			if (statusAdd) {
				addStock(IStockModel.getInstance().getAccessToken(), strCode,
						strNumberOfShare, strPurchase);
			} else {
				updateStock(false,IStockModel.getInstance().getAccessToken(),
						idStockUpdate, strCode, strNumberOfShare, strPurchase,
						" ");
			}
			break;

		case R.id.fragment_portfolio_btn_cancel:
//			if (statusAdd == false) {
//				statusAdd = true;
				txtAddNew.setText(AppController.getAppContext().getResources().getString(
						R.string.fragment_portfolio_title_add_new));
				btnOK.setText(AppController.getAppContext().getResources().getString(
						R.string.fragment_portfolio_title_ok));
	//		}
			edtAddStock.setText("");
			edtNumberOfShare.setText("");
			edtPurcharsePrice.setText("");
			break;
			
		case R.id.fragment_portfolio_img_open_layout_add_new_portfolio:
			imgOpenLayoutAdd.setVisibility(View.GONE);
			imgCloseLayoutAdd.setVisibility(View.VISIBLE);
			layoutAddNew.setVisibility(View.VISIBLE);
			break;
			
		case R.id.fragment_portfolio_img_close_layout_add_new_portfolio:
			imgOpenLayoutAdd.setVisibility(View.VISIBLE);
			imgCloseLayoutAdd.setVisibility(View.GONE);
			layoutAddNew.setVisibility(View.GONE);
			break;

		default:
			break;
		}
	}

	public int convertDpToPixel(float dp) {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return (int) px;
	}

}

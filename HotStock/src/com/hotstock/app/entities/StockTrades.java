package com.hotstock.app.entities;

import java.util.List;

public class StockTrades {
	
	public int id;
	public String code;
	public String name;
	public List<Trades> Trades;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Trades> getTrades() {
		return Trades;
	}
	public void setTrades(List<Trades> trades) {
		Trades = trades;
	}
	
}

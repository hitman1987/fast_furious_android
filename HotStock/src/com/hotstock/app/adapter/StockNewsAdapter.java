package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.News;
import com.hotstock.app.utils.Utils;

public class StockNewsAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private ArrayList<News> mData = new ArrayList<News>();
	private Context mContext;

	public StockNewsAdapter(Context c,ArrayList<News> d) {
		this.mContext = c;
		this.mData = d;
		mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(
					com.hotstock.R.layout.top_news_items, null);
			viewHolder.layoutMain = (RelativeLayout) convertView.findViewById(R.id.top_news_items_main);
			viewHolder.txtContent = (TextView) convertView.findViewById(R.id.top_news_items_txt_content);
			viewHolder.txtDate = (TextView) convertView.findViewById(R.id.top_news_items_txt_date);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.txtContent.setText(mData.get(position).getTitle());
		viewHolder.txtDate.setText("(" + Utils.convertStringtoDate(mData.get(position).getPublished_time())+")");
		
		if (position % 2 == 0) {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources().getColor(R.color.market_list_item_1));
		} else {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources().getColor(R.color.market_list_item_2));
		}

		return convertView;
	}

	private class ViewHolder {
		private RelativeLayout layoutMain;
		private TextView txtContent;
		private TextView txtDate;
	}

}

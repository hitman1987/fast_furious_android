package com.hotstock.app.entities;

public class Trades {
	public int transaction_id;
	public double basic_price;
	public double ceiling_price;
	public double floor_price;
	public double last_price;
	public int volume;
	public double total_volume;
	public String trading_time;
    public double fr_room;
    public double percentage_change;
    public double change;    
    public int close_price;
    public int highest_price;
    public int lowest_price;
    public int open_price;

    
	public int getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}
	public double getBasic_price() {
		return basic_price;
	}
	public void setBasic_price(double basic_price) {
		this.basic_price = basic_price;
	}
	public double getCeiling_price() {
		return ceiling_price;
	}
	public void setCeiling_price(double ceiling_price) {
		this.ceiling_price = ceiling_price;
	}
	public double getFloor_price() {
		return floor_price;
	}
	public void setFloor_price(double floor_price) {
		this.floor_price = floor_price;
	}
	public double getLast_price() {
		return last_price;
	}
	public void setLast_price(double last_price) {
		this.last_price = last_price;
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	public double getTotal_volume() {
		return total_volume;
	}
	public void setTotal_volume(double total_volume) {
		this.total_volume = total_volume;
	}
	public String getTrading_time() {
		return trading_time;
	}
	public void setTrading_time(String trading_time) {
		this.trading_time = trading_time;
	}
	public double getFr_room() {
		return fr_room;
	}
	public void setFr_room(double fr_room) {
		this.fr_room = fr_room;
	}
	public double getPercentage_change() {
		return percentage_change;
	}
	public void setPercentage_change(double percentage_change) {
		this.percentage_change = percentage_change;
	}
	public double getChange() {
		return change;
	}
	public void setChange(double change) {
		this.change = change;
	}
	public int getClose_price() {
		return close_price;
	}
	public void setClose_price(int close_price) {
		this.close_price = close_price;
	}
	public int getHighest_price() {
		return highest_price;
	}
	public void setHighest_price(int highest_price) {
		this.highest_price = highest_price;
	}
	public int getLowest_price() {
		return lowest_price;
	}
	public void setLowest_price(int lowest_price) {
		this.lowest_price = lowest_price;
	}
	public int getOpen_price() {
		return open_price;
	}
	public void setOpen_price(int open_price) {
		this.open_price = open_price;
	}
}

package com.hotstock.app.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Define common methods that used to saving in preference: contants, get,
 * write, etc..
 * 
 */
public final class PreferenceUtils {

	private static final String TAG = PreferenceUtils.class.getSimpleName();
	private static final Logger logger = LoggerFactory
			.getLogger(PreferenceUtils.class);

	/**
	 * check value of a key has been saved on Preference or not.
	 * 
	 * @param con
	 * @param key
	 * @return
	 */
	public static boolean contains(Context con, String key) {
		try {
			return pref(con).contains(key);
		} catch (Exception e) {
			logger.debug(TAG, e.toString());
			return false;
		}
	}

	/**
	 * Get value as String type of a key from Preference.
	 * 
	 * @param con
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static String getString(Context con, String key, String defValue) {
		try {
			return pref(con).getString(key, defValue);
		} catch (Exception e) {
			logger.debug(TAG, e.toString());
			return defValue;
		}
	}

	/**
	 * Get value as Boolean type of a key from Preference.
	 * 
	 * @param con
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static boolean getBoolean(Context con, String key, boolean defValue) {
		try {
			return pref(con).getBoolean(key, defValue);
		} catch (Exception e) {
			logger.debug(TAG, e.toString());
			return defValue;
		}
	}

	/**
	 * Get value as Integer type of a key from Preference.
	 * 
	 * @param con
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static int getInt(Context con, String key, int defValue) {
		try {
			return pref(con).getInt(key, defValue);
		} catch (Exception e) {
			logger.debug(TAG, e.toString());
			return defValue;
		}
	}

	/**
	 * Get value as Long type of a key from Preference.
	 * 
	 * @param con
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static long getLong(Context con, String key, long defValue) {
		try {
			return pref(con).getLong(key, defValue);
		} catch (Exception e) {
			logger.debug(TAG, e.toString());
			return defValue;
		}
	}

	/**
	 * Get value as Float type of a key from Preference.
	 * 
	 * @param con
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static float getFloat(Context con, String key, float defValue) {
		try {
			return pref(con).getFloat(key, defValue);
		} catch (Exception e) {
			logger.debug(TAG, e.toString());
			return defValue;
		}
	}

	/**
	 * write value as string type of a key from Preference.
	 * 
	 * @param con
	 * @param key
	 * @param value
	 */
	public static void writeString(Context con, String key, String value) {
		try {
			pref(con).edit().putString(key, value).commit();
		} catch (Exception e) {
			logger.debug(TAG, e.toString());
		}
	}

	/**
	 * write value as boolean type of a key from Preference.
	 * 
	 * @param con
	 * @param key
	 * @param value
	 */
	public static void writeBoolean(Context con, String key, boolean value) {
		try {
			pref(con).edit().putBoolean(key, value).commit();
		} catch (Exception e) {
			logger.debug(TAG, e.toString());
		}
	}

	/**
	 * write value as Integer type of a key from Preference.
	 * 
	 * @param con
	 * @param key
	 * @param value
	 */
	public static void writeInt(Context con, String key, int value) {
		try {
			pref(con).edit().putInt(key, value).commit();
		} catch (Exception e) {
			logger.debug(TAG, e.toString());
		}
	}

	/**
	 * write value as Long type of a key from Preference.
	 * 
	 * @param con
	 * @param key
	 * @param value
	 */
	public static void writeLong(Context con, String key, long value) {
		try {
			pref(con).edit().putLong(key, value).commit();
		} catch (Exception e) {
			logger.debug(TAG, e.toString());
		}
	}

	/**
	 * write value as Float type of a key from Preference.
	 * 
	 * @param con
	 * @param key
	 * @param value
	 */
	public static void writeFloat(Context con, String key, float value) {
		try {
			pref(con).edit().putFloat(key, value).commit();
		} catch (Exception e) {logger.debug(TAG, e.toString());
			logger.debug(TAG, e.toString());
		}
	}

	/**
	 * Remove value of a key from Preference.
	 * 
	 * @param con
	 * @param key
	 */
	public static void remove(Context con, String key) {
		try {
			pref(con).edit().remove(key).commit();
		} catch (Exception e) {
			logger.debug(TAG, e.toString());
		}
	}

	/**
	 * SharedPreferens.
	 * 
	 * @param con
	 * @return
	 */
	private static SharedPreferences pref(Context con) {
		return PreferenceManager.getDefaultSharedPreferences(con);
	}

}

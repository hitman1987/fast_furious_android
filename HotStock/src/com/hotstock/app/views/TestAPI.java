package com.hotstock.app.views;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;

import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.MarketChart;

public class TestAPI extends Activity{
	
	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		//---------------get Stock News -----------------------------------------
//		restService.getStockNews(5, 0, "ABC", new IStockResponseListener<ArrayList<News>>() {
//			
//			@Override
//			public void onResponse(ArrayList<News> data) {
//				System.out.println("data = " + data.get(0).getTitle());
//			}
//			
//			@Override
//			public void onError(Exception ex) {
//				
//			}
//		});
		
		
//		restService.getStockStatistic("FPT", new IStockResponseListener<ArrayList<StockStatistic>>() {
//			
//			@Override
//			public void onResponse(ArrayList<StockStatistic> data) {
//				System.out.println("data = "  + data.get(0).getName());
//			}
//			
//			@Override
//			public void onError(Exception ex) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
		
//		restService.getMarketETF(new IStockResponseListener<ArrayList<ETF>>() {
//			
//			@Override
//			public void onResponse(ArrayList<ETF> data) {
//				System.out.println("data = "  + data.size());
//				System.out.println("data = "  + data.get(0).getFundname());
//			}
//			
//			@Override
//			public void onError(Exception ex) {
//				
//			}
//		});
		
//		restService.getStockSnapshots("DIG", new IStockResponseListener<ArrayList<StockSnapshots>>() {
//
//			@Override
//			public void onResponse(ArrayList<StockSnapshots> data) {
//				System.out.println("data = "  + data.get(0).Snapshots.get(0).getUpdated_time());
//			}
//
//			@Override
//			public void onError(Exception ex) {
//				
//			}
//		});
		
//		restService.getStockOverview("AAM", new IStockResponseListener<ArrayList<StockOverviews>>() {
//
//			@Override
//			public void onResponse(ArrayList<StockOverviews> data) {
//				System.out.println("data = "  + data.size());
//				System.out.println("data = "  + data.get(0).getOverviews().get(0).getDescription());
//			}
//
//			@Override
//			public void onError(Exception ex) {
//				ex.printStackTrace();
//			}
//		});
		
//		restService.getNewsMarket("HOSE", "10", "0", new IStockResponseListener<ArrayList<News>>() {
//			
//			@Override
//			public void onResponse(ArrayList<News> data) {
//				
//			}
//			
//			@Override
//			public void onError(Exception ex) {
//				
//			}
//		});
		
//		restService.getMarketTradingMostActiveSell("c82fd29c24ac514c7b1495e37a83d302", "HOSE", "10", "0", new IStockResponseListener<ArrayList<MostActive>>() {
//			
//			@Override
//			public void onResponse(ArrayList<MostActive> data) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void onError(Exception ex) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
		
//		restService.getMarketForeignTrades("HOSE", "c82fd29c24ac514c7b1495e37a83d302", "10", "0", new IStockResponseListener<ArrayList<ForeignTrades>>() {
//			
//			@Override
//			public void onResponse(ArrayList<ForeignTrades> data) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void onError(Exception ex) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
		
//		restService.getWatchlist(new IStockResponseListener<ArrayList<MainWatchList>>() {
//			
//			@Override
//			public void onResponse(ArrayList<MainWatchList> data) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void onError(Exception ex) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
		
//		restService.getStockLastDay("VNM",new IStockResponseListener<ArrayList<StockLastDay>>() {
//			
//			@Override
//			public void onResponse(ArrayList<StockLastDay> data) {
//				System.out.println("data = " + data.get(0).getLast_price());
//			}
//			
//			@Override
//			public void onError(Exception ex) {
//				
//			}
//		} );
		
		restService.getDailyMarket("1000", "0","1", new IStockResponseListener<ArrayList<MarketChart>>() {
			
			@Override
			public void onResponse(ArrayList<MarketChart> data) {
				System.out.println("id Market = " + data.get(0).getId());
			}
			
			@Override
			public void onError(Exception ex) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}

package com.hotstock.app.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.AppController;
import com.hotstock.app.adapter.StockConsensusAdapter;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.StockStatistic;
import com.hotstock.app.entities.StockTrades;
import com.hotstock.app.entities.Trades;
import com.hotstock.app.utils.AutofitHelper;
import com.hotstock.app.utils.Enums;
import com.hotstock.app.utils.PreferenceUtils;
import com.hotstock.app.utils.Utils;
import com.hotstock.app.views.SearchStockActivity;

public class StocksFragment extends Fragment {

	private FragmentManager childFragmentManager;
	private ListView lvConsensus;
	private Button btnTabChart;
	private Button btnTabFinancial;
	private Button btnTabSnabShot;
	private Button btnTabNews;
	private Button btnTabOverview;

	private TextView txtStockCode;
	private TextView txtStockPrice;
	private TextView txtStockPercent;
	private TextView txtStockRatePrice;
	private TextView txtCeiling;
	private TextView txtRef;
	private TextView txtFloor;
	private TextView txtVol;
	private TextView txtTotalVol;
	private TextView txtFrRoom;

	private ImageView imgStockStatus;
	private ImageButton imgSearchStock;

	private ArrayList<StockTrades> mData = new ArrayList<StockTrades>();
	private Trades trade = new Trades();
	private Bundle bundle = new Bundle();
	private IStockRestfulService restService = new IStockRestfulServiceImpl();

	private static final int LOAD_STOCK_SUCCESS = 0;
	private static final int LOAD_STOCK_FAIL = 1;

	private Intent intentStock;
	private String sym;

	public StocksFragment() {
	}

	// runs without a timer by reposting this handler at the end of the runnable
	Handler timerHandler = new Handler();
	Runnable timerRunnable = new Runnable() {

		@Override
		public void run() {
			String symLoad = PreferenceUtils.getString(AppController.getAppContext(), Enums.PREF_CURRENT_PRICE_CODE, null);
			if (symLoad != null) {
				loadData(symLoad);
			}

			timerHandler.postDelayed(this, 3000);
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_stocks, container, false);

		initView(rootView);

		return rootView;
	}

	private void initView(View view) {
		childFragmentManager = getFragmentManager();

		lvConsensus = (ListView) view.findViewById(R.id.fragment_stock_lv_consensus);
		txtStockCode = (TextView) view.findViewById(R.id.fragment_stocks_txt_stock_code);
		txtStockPrice = (TextView) view.findViewById(R.id.tv_stock_price);
		txtStockPercent = (TextView) view.findViewById(R.id.tv_stock_percent);
		txtStockRatePrice = (TextView) view.findViewById(R.id.tv_stock_change);
		txtCeiling = (TextView) view.findViewById(R.id.tv_stock_ceiling);
		txtRef = (TextView) view.findViewById(R.id.tv_stock_rf);
		txtFloor = (TextView) view.findViewById(R.id.tv_stock_floor);
		txtVol = (TextView) view.findViewById(R.id.tv_stock_vol);
		txtTotalVol = (TextView) view.findViewById(R.id.tv_stock_total_vol);
		txtFrRoom = (TextView) view.findViewById(R.id.tv_stock_fr_room);

		AutofitHelper.create(txtCeiling);
		AutofitHelper.create(txtStockRatePrice);
		AutofitHelper.create(txtCeiling);

		btnTabChart = (Button) view.findViewById(R.id.fragment_stock_btn_tab_chart);
		btnTabFinancial = (Button) view.findViewById(R.id.fragment_stock_btn_tab_financial);
		btnTabSnabShot = (Button) view.findViewById(R.id.fragment_stock_btn_tab_snapshot);
		btnTabNews = (Button) view.findViewById(R.id.fragment_stock_btn_tab_news);
		btnTabOverview = (Button) view.findViewById(R.id.fragment_stock_btn_tab_overview);

		imgStockStatus = (ImageView) view.findViewById(R.id.imv_stock_status);
		imgSearchStock = (ImageButton) view.findViewById(R.id.fragment_stocks_img_search);

		onTabPress(0);

		OnStockFragmentClickListener onClickListener = new OnStockFragmentClickListener();
		btnTabChart.setOnClickListener(onClickListener);
		btnTabFinancial.setOnClickListener(onClickListener);
		btnTabSnabShot.setOnClickListener(onClickListener);
		btnTabNews.setOnClickListener(onClickListener);
		btnTabOverview.setOnClickListener(onClickListener);
		imgSearchStock.setOnClickListener(onClickListener);
	}

	@Override
	public void onResume() {
		super.onResume();
		// Start timer
		timerHandler.postDelayed(timerRunnable, 3000);
		sym = PreferenceUtils.getString(AppController.getAppContext(), Enums.PREF_CURRENT_PRICE_CODE, null);
		if (sym != null) {
			loadData(sym);
			bundle.putString(Enums.PREF_STOCK_CODE, sym);
			onTabPress(0);
		} else {
			sym = "VNM";
			loadData(sym);
		}
		restService.getStockStatistic(sym, new IStockResponseListener<ArrayList<StockStatistic>>() {

			@Override
			public void onResponse(ArrayList<StockStatistic> data) {
				StockConsensusAdapter adapter = new StockConsensusAdapter(AppController.getAppContext(), data.get(0).Consults);
				lvConsensus.setAdapter(adapter);
			}

			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
			}
		});
	}

	@Override
	public void onPause() {
		super.onPause();
		timerHandler.removeCallbacks(timerRunnable);
	}

	private void onTabPress(int index) {
		// Initialize default
		btnTabChart.setBackgroundResource(R.drawable.tab_left);
		btnTabChart.setTextColor(AppController.getAppContext().getResources().getColor(R.color.text_color_grey));
		btnTabFinancial.setBackgroundResource(R.drawable.tab_center);
		btnTabFinancial.setTextColor(AppController.getAppContext().getResources().getColor(R.color.text_color_grey));
		btnTabSnabShot.setBackgroundResource(R.drawable.tab_center);
		btnTabSnabShot.setTextColor(AppController.getAppContext().getResources().getColor(R.color.text_color_grey));
		btnTabNews.setBackgroundResource(R.drawable.tab_center);
		btnTabNews.setTextColor(AppController.getAppContext().getResources().getColor(R.color.text_color_grey));
		btnTabOverview.setBackgroundResource(R.drawable.tab_right);
		btnTabOverview.setTextColor(AppController.getAppContext().getResources().getColor(R.color.text_color_grey));

		// Set tab depend on index
		switch (index) {
		case 0:
			btnTabChart.setBackgroundResource(R.drawable.tab_pressed);
			btnTabChart.setTextColor(AppController.getAppContext().getResources().getColor(R.color.text_color_yellow));
			StocksChartFragment chartFragment = new StocksChartFragment();
			chartFragment.setArguments(bundle);
			childFragmentManager.beginTransaction().replace(R.id.fragment_stock_frame_container, chartFragment).commit();
			break;

		case 1:
			btnTabFinancial.setBackgroundResource(R.drawable.tab_pressed);
			btnTabFinancial.setTextColor(AppController.getAppContext().getResources().getColor(R.color.text_color_yellow));
			StocksFinancialFragment financialFragment = new StocksFinancialFragment();
			financialFragment.setArguments(bundle);
			childFragmentManager.beginTransaction().replace(R.id.fragment_stock_frame_container, financialFragment).commit();
			break;

		case 2:
			btnTabSnabShot.setBackgroundResource(R.drawable.tab_pressed);
			btnTabSnabShot.setTextColor(AppController.getAppContext().getResources().getColor(R.color.text_color_yellow));
			StocksSnapshotFragment snapshotFragment = new StocksSnapshotFragment();
			snapshotFragment.setArguments(bundle);
			childFragmentManager.beginTransaction().replace(R.id.fragment_stock_frame_container, snapshotFragment).commit();
			break;

		case 3:
			btnTabNews.setBackgroundResource(R.drawable.tab_pressed);
			btnTabNews.setTextColor(AppController.getAppContext().getResources().getColor(R.color.text_color_yellow));
			StocksNewsFragment newsFragment = new StocksNewsFragment();
			newsFragment.setArguments(bundle);
			childFragmentManager.beginTransaction().replace(R.id.fragment_stock_frame_container, newsFragment).commit();
			break;

		case 4:
			btnTabOverview.setBackgroundResource(R.drawable.tab_pressed);
			btnTabOverview.setTextColor(AppController.getAppContext().getResources().getColor(R.color.text_color_yellow));
			StocksOverviewFragment overviewFragment = new StocksOverviewFragment();
			overviewFragment.setArguments(bundle);
			childFragmentManager.beginTransaction().replace(R.id.fragment_stock_frame_container, overviewFragment).commit();
			break;
		}
	}

	private class OnStockFragmentClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.fragment_stock_btn_tab_chart:
				onTabPress(0);
				break;

			case R.id.fragment_stock_btn_tab_financial:
				onTabPress(1);
				break;

			case R.id.fragment_stock_btn_tab_snapshot:
				onTabPress(2);
				break;

			case R.id.fragment_stock_btn_tab_news:
				onTabPress(3);
				break;

			case R.id.fragment_stock_btn_tab_overview:
				onTabPress(4);
				break;

			case R.id.fragment_stocks_img_search:
				intentStock = new Intent(AppController.getAppContext(), SearchStockActivity.class);
				startActivity(intentStock);
				break;
			}
		}
	}

	// --------------load data------------------------
	private void loadData(String stockName) {
		restService.getStock(stockName, new IStockResponseListener<ArrayList<StockTrades>>() {

			@Override
			public void onResponse(ArrayList<StockTrades> data) {
				if(data!=null){
					txtStockCode.setText(sym);
				}
				if (data.get(0).getTrades() != null && data.get(0).getTrades().size() > 0) {
					trade = data.get(0).getTrades().get(0);
					mHandler.sendEmptyMessage(LOAD_STOCK_SUCCESS);
				}else{
					mHandler.sendEmptyMessage(LOAD_STOCK_FAIL);
				}
			}

			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
			}
		});
	}

	private Handler mHandler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case LOAD_STOCK_SUCCESS:
				if(AppController.getAppContext() != null){
					txtStockPrice.setText(Utils.formatStockPrice(trade.getLast_price() / 1000));
					int textColor = Utils.getStockColor(trade.getLast_price(), trade.getBasic_price(), trade.getCeiling_price(), trade.getFloor_price());
					txtStockPrice.setTextColor(AppController.getAppContext().getResources().getColor(textColor));
					txtStockRatePrice.setText(Utils.formatStockChange(trade.getChange() / 1000));
					txtStockRatePrice.setTextColor(AppController.getAppContext().getResources().getColor(textColor));
					txtStockPercent.setText(Utils.formatStockChangePct(trade.getPercentage_change()));
					txtStockPercent.setTextColor(AppController.getAppContext().getResources().getColor(textColor));
					txtVol.setText(Utils.formatStockVolume(trade.getVolume()));
					txtTotalVol.setText(Utils.formatAsStockVolume(trade.getTotal_volume()));
					txtFrRoom.setText(Utils.formatAsStockVolume(trade.getFr_room()));
					txtCeiling.setText(Utils.formatStockPrice(trade.getCeiling_price() / 1000));
					txtRef.setText(Utils.formatStockPrice(trade.getBasic_price() / 1000));
					txtFloor.setText(Utils.formatStockPrice(trade.getFloor_price() / 1000));
					int stockStatus = Utils.getStockStatus(trade.getLast_price(), trade.getBasic_price(), trade.getCeiling_price(),trade.getFloor_price());
					imgStockStatus.setVisibility(View.VISIBLE);
					imgStockStatus.setImageResource(stockStatus);
				}
				
				break;

			case LOAD_STOCK_FAIL:
				int textColorFail = AppController.getAppContext().getResources().getColor(android.R.color.white);
				imgStockStatus.setVisibility(View.GONE);
				txtStockPrice.setText("0,0");
				txtStockRatePrice.setText("0,0");
				txtStockPercent.setText("0%");
				txtStockPrice.setTextColor(textColorFail);
				txtStockRatePrice.setTextColor(textColorFail);
				txtStockPercent.setTextColor(textColorFail);
				txtVol.setText("0");
				txtTotalVol.setText("0");
				txtFrRoom.setText("0");
				txtCeiling.setText("0,0");
				txtRef.setText("0,0");
				txtFloor.setText("0,0");
				break;

			default:
				break;
			}
			return false;
		}
	});
}

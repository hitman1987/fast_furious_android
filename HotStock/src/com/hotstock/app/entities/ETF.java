package com.hotstock.app.entities;

public class ETF {
	public int id;
	public String fundname;
	public double slccq;
	public double slccq_change;
	public String updated_time;
	public int discount;
	public int premium;
	public double nav;
	public int last_price;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFundname() {
		return fundname;
	}
	public void setFundname(String fundname) {
		this.fundname = fundname;
	}
	public double getSlccq() {
		return slccq;
	}
	public void setSlccq(double slccq) {
		this.slccq = slccq;
	}
	public double getSlccq_change() {
		return slccq_change;
	}
	public void setSlccq_change(double slccq_change) {
		this.slccq_change = slccq_change;
	}
	public String getUpdated_time() {
		return updated_time;
	}
	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public int getPremium() {
		return premium;
	}
	public void setPremium(int premium) {
		this.premium = premium;
	}
	public double getNav() {
		return nav;
	}
	public void setNav(double nav) {
		this.nav = nav;
	}
	public int getLast_price() {
		return last_price;
	}
	public void setLast_price(int last_price) {
		this.last_price = last_price;
	}
}

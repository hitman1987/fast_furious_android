package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.Consensus;

public class WatchlistConsensusAdapter extends BaseAdapter{
	
	private ArrayList<Consensus> mData;
	private LayoutInflater mLayoutInflater;
	private Context mContext;
	
	public WatchlistConsensusAdapter(Context c,ArrayList<Consensus> d) {
		this.mContext = c;
		this.mData = d;
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(com.hotstock.R.layout.consensus_items, null);
			viewHolder.layoutMain = (LinearLayout) convertView.findViewById(R.id.consensus_items_layout_main);
			viewHolder.txtStock = (TextView) convertView.findViewById(R.id.consensus_items_txt_stock);
			viewHolder.txtBuy = (TextView) convertView.findViewById(R.id.consensus_items_txt_buy);
			viewHolder.txtSell = (TextView) convertView.findViewById(R.id.consensus_items_txt_sell);
			viewHolder.txtHold = (TextView) convertView.findViewById(R.id.consensus_items_txt_hold);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.txtStock.setText(mData.get(position).getCode());
		viewHolder.txtBuy.setText(String.valueOf(mData.get(position).getBuy()));
		viewHolder.txtSell.setText(String.valueOf(mData.get(position).getSell()));
		viewHolder.txtHold.setText(String.valueOf(mData.get(position).getHold()));
		if (position % 2 == 0) {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_1));
		} else {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_2));
		}

		return convertView;
	}

	private class ViewHolder {
		private LinearLayout layoutMain;
		private TextView txtStock;
		private TextView txtBuy;
		private TextView txtSell;
		private TextView txtHold;
	}

}

package com.hotstock.app.pushnotification;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.hotstock.app.utils.Enums;
import com.hotstock.app.utils.Utils;

public class PushNotificationUtil {

	private volatile static PushNotificationUtil uniqueInstance;
	private Context context;
	private Activity acivity;
	private GoogleCloudMessaging gcm;
	// private AtomicInteger msgId = new AtomicInteger();
	private String regid;

	public static final String EXTRA_MESSAGE = "message";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	/**
	 * Substitute you own sender ID here. This is the project number you got from the API Console, as described in "Getting Started."
	 */
	String SENDER_ID = "336965195061";
	// API KEY=AIzaSyBe_HZKEEkJw-1NOnYlHnMO_eUsuLHtu5A
	/**
	 * Tag used on log messages.
	 */
	static final String TAG = "HotStockAndroidPush";

	public static PushNotificationUtil getInstance(Context context) {
		if (uniqueInstance == null) {
			synchronized (PushNotificationUtil.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new PushNotificationUtil(context);
				}
			}
		}
		return uniqueInstance;
	}

	private PushNotificationUtil(Context context) {
		this.context = context;
		if (Enums.sharedPreferences == null) {
			Enums.sharedPreferences = Utils.getSharedPreferences(context, Context.MODE_PRIVATE);
		}
	}

	public void register(Activity activity) {
		this.acivity = activity;

		// Check device for Play Services APK. If check succeeds, proceed with
		// GCM registration.
		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(context);
			regid = getRegistrationId(context);

			if (regid == null || regid.equals("")) {
				registerInBackground();
			}
		} else {
			Log.e(TAG, "No valid Google Play Services APK found.");
		}
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it doesn't, display a dialog that allows users to download the APK from the Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, acivity, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.e(TAG, "This device is not supported.");
			}
			return false;
		}
		return true;
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing registration ID.
	 */
	private String getRegistrationId(Context context) {
		String registrationId = Utils.loadPreferences(Enums.sharedPreferences, Enums.PREF_NOTIFICATION);
		if (registrationId == null || registrationId.equals("")) {
			Log.e(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = Utils.loadPreferencesInt(Enums.sharedPreferences, Enums.PREF_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.e(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's shared preferences.
	 */
	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					regid = gcm.register(SENDER_ID);
					msg = "Device registered, registration ID=" + regid;

					// You should send the registration ID to your server over
					// HTTP,
					// so it can use GCM/HTTP or CCS to send messages to your
					// app.
					// The request to your server should be authenticated if
					// your app
					// is using accounts.
					// System.out.println(">>> call to send regId to server: " + regid);
					sendRegistrationIdToBackend(regid);

					// For this demo: we don't need to send it because the
					// device
					// will send upstream messages to a server that echo back
					// the
					// message using the 'from' address in the message.

					// Persist the regID - no need to register again.
					storeRegistrationId();
				} catch (IOException ex) {
					// System.out.println(">>> error: " + ex.getMessage());
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				// mDisplay.append(msg + "\n");
				Log.e(TAG, msg);

			}
		}.execute(null, null, null);
	}

	/**
	 * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send messages to your app. Not needed for this demo since the device sends upstream messages to a server that echoes back the message using the 'from' address
	 * in the message.
	 */
	private void sendRegistrationIdToBackend(String regId) {
		// if (Util.isNetworkAvailable()) {
		// String auth_token = OAthApplication.Instance().getmPrefs().getStringValue(AppConstant.PROPERTY_USERINFODATA_AUTHTOKEN, "");
		// // System.out.println(">>> auth token: " + auth_token);
		// if (!auth_token.equals("")) {
		// // Add the callback object is current activity object
		// DataHelper.getInstance().addObserver((Observer) acivity);
		// DataHelper.getInstance().updateDeviceToken(auth_token, regId);
		// }
		// }
	}

	/**
	 * Stores the registration ID and app versionCode in the application's {@code SharedPreferences}.
	 * 
	 */
	public void storeRegistrationId() {
		Utils.saveStringPreferences(Enums.PREF_NOTIFICATION, regid, Enums.sharedPreferences);
		Utils.saveIntPreferences(Enums.PREF_APP_VERSION, getAppVersion(context), Enums.sharedPreferences);
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}
}

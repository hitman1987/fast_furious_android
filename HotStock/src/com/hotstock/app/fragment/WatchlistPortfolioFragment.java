package com.hotstock.app.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hotstock.R;
import com.hotstock.app.adapter.PortfolioAdapter;
import com.hotstock.app.animation.BaseSwipeListViewListener;
import com.hotstock.app.animation.SwipeListView;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.Portfolio;
import com.hotstock.app.utils.IStockModel;
import com.hotstock.app.utils.Utils;
import com.hotstock.app.views.NoteActivity;

public class WatchlistPortfolioFragment extends Fragment implements OnClickListener{
	
	private SwipeListView listPortfolio;
	private LinearLayout layoutAddNew;
	private TextView txtAddNew;
	private EditText edtAddStock;
	private EditText edtPurcharsePrice;
	private EditText edtNumberOfShare;
	private Button btnOK;
	private Button btnCancel;
	private Boolean statusAdd = true;
	
	private View rootView;
	private ProgressDialog pDialog;
	
	private IStockRestfulService restService = new IStockRestfulServiceImpl();
	private ArrayList<Portfolio> mData = new ArrayList<Portfolio>();
	private Portfolio mPortfolio = new Portfolio();
	private PortfolioAdapter mAdapter;
	private int idStockUpdate = -1;
	private boolean statusClickAdd = false;
	
	private static final int LOAD_PORTFOLIO_SUCCESS = 0;
	private static final int LOAD_PORTFOLIO_FAIL = 1;
	private static final int ADD_STOCK_SUCCESS = 2;
	private static final int ADD_STOCK_FAIL = 3;
	private static final int UPDATE_STOCK_SUCCESS = 4;
	private static final int UPDATE_STOCK_FAIL = 5;
	private static final int DELETE_STOCK_SUCCESS = 6;
	private static final int DELETE_STOCK_FAIL = 7;
	
	public static WatchlistPortfolioFragment instant;
	
	private final int limitPortfolio = 20;
	private int offsetPortfolio = 0;
	private boolean isLoading = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		instant = this;
		rootView = inflater.inflate(R.layout.fragment_watchlist_portfolio, container, false);
		statusClickAdd = getArguments().getBoolean("ADD", false);
		initView(rootView);
		return rootView;
	}
	
	private void initView(View v){
		if(listPortfolio == null){
			listPortfolio = (SwipeListView) v.findViewById(R.id.fragment_watchlist_portfolio_list);
			listPortfolio.setSwipeListViewListener(new BaseSwipeListViewListener()
			{
				@Override
	            public void onOpened(int position, boolean toRight) {
	            }

	            @Override
	            public void onClosed(int position, boolean fromRight) {
	            }

	            @Override
	            public void onListChanged() {
	            }

	            @Override
	            public void onMove(int position, float x) {
	            }

	            @Override
	            public void onStartOpen(int position, int action, boolean right) {
	                Log.d("swipe", String.format("onStartOpen %d - action %d", position, action));
	            }

	            @Override
	            public void onStartClose(int position, boolean right) {
	                Log.d("swipe", String.format("onStartClose %d", position));
	            }

	            @Override
	            public void onClickFrontView(int position) {
	               Intent intentPortfolio = new Intent(getActivity(), NoteActivity.class);
	               intentPortfolio.putExtra("statusEdit", true);
	               intentPortfolio.putExtra("id", mData.get(position).getId());
	               intentPortfolio.putExtra("code", mData.get(position).getCode());
	               intentPortfolio.putExtra("number", mData.get(position).getNumberofshares());
	               intentPortfolio.putExtra("purchase", mData.get(position).getPurchase_price());
	               intentPortfolio.putExtra("marketPrice", mData.get(position).getMarket_price());
	               intentPortfolio.putExtra("percent", mData.get(position).getPercentage_change());
	               startActivity(intentPortfolio);
	            	// Toast.makeText(getActivity().getBaseContext(), "Comming soon....", Toast.LENGTH_SHORT).show();
	             
	              //  listPortfolio.openAnimate(position); //when you touch front view it will open
	              
	             
	            }

	            @Override
	            public void onClickBackView(int position) {
	                Log.d("swipe", String.format("onClickBackView %d", position));
	                
	                listPortfolio.closeAnimate(position);//when you touch back view it will close
	            }

	            @Override
	            public void onDismiss(int[] reverseSortedPositions) {
	            	
	            }	        
			});
			
			 //These are the swipe listview settings. you can change these
	        //setting as your requirement 
	        listPortfolio.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
	        listPortfolio.setOffsetLeft(convertDpToPixel(200f)); // left side offset
	        listPortfolio.setOffsetRight(convertDpToPixel(80f)); // right side offset
	        listPortfolio.setAnimationTime(100); // Animation time
	        listPortfolio.setSwipeOpenOnLongPress(true); // enable or disable SwipeOpenOnLongPress
	        
	        listPortfolio.setOnScrollListener(new OnScrollListener() {
				
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					
				}
				
				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,
						int visibleItemCount, int totalItemCount) {
					// TODO Auto-generated method stub
					if (totalItemCount == 0 || visibleItemCount == 0) {
						return;
					}
					if (totalItemCount == firstVisibleItem + visibleItemCount && !isLoading) {
						// 排他制御フラグをON
						isLoading = true;
						loadData(true);
					}
				}
			});
	        
		
		}
		
		if(layoutAddNew == null){
			layoutAddNew = (LinearLayout) v.findViewById(R.id.fragment_watchlist_portfolio_layout_add_new);
		}
		
		if(txtAddNew == null){
			txtAddNew = (TextView) v.findViewById(R.id.fragment_watchlist_portfolio_txt_add);
		}
		
		if(edtAddStock == null){
			edtAddStock = (EditText) v.findViewById(R.id.fragment_watchlist_portfolio_edt_stock);
		}
		
		if(edtNumberOfShare == null){
			edtNumberOfShare = (EditText) v.findViewById(R.id.fragment_watchlist_portfolio_edt_number_of_share);
		}
		
		if(edtPurcharsePrice == null){
			edtPurcharsePrice = (EditText) v.findViewById(R.id.fragment_watchlist_portfolio_edt_purchase_price);
		}		
		
		if(btnOK == null){
			btnOK = (Button) v.findViewById(R.id.fragment_watchlist_portfolio_btn_ok);
		}
		
		if(btnCancel == null){
			btnCancel = (Button) v.findViewById(R.id.fragment_watchlist_portfolio_btn_cancel);
		}
		
		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
		
		if(statusClickAdd){
			layoutAddNew.setVisibility(View.VISIBLE);
		}else{
			layoutAddNew.setVisibility(View.GONE);
		}
		
		listener();
		loadData(false);
	}
	
	private void listener(){
		btnOK.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
	}
	
	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}
	
	public void updateStock(int position){
		statusAdd = false;
		txtAddNew.setText(getActivity().getResources().getString(R.string.fragment_watchlist_portfolio_title_update));
		btnOK.setText(getActivity().getResources().getString(R.string.fragment_watchlist_portfolio_title_update));
		edtAddStock.setText(mData.get(position).getCode());
		edtNumberOfShare.setText(Utils.formatAsStockVolume(mData.get(position).getNumberofshares()));
		edtPurcharsePrice.setText(Utils.formatAsStockVolume(mData.get(position).getPurchase_price()));
		idStockUpdate = mData.get(position).getId();
		listPortfolio.closeAnimate(position);
	}
	
	public void deleteStock(int position){
		showProgressDialog();
		int portfolioID = mData.get(position).getId();
		if(portfolioID > 0){
			restService.deleteStockPortfolio(IStockModel.getInstance().getAccessToken(), portfolioID, new IStockResponseListener<Boolean>() {
				
				@Override
				public void onResponse(Boolean data) {
					
					if(data){
						mHandler.sendEmptyMessage(ADD_STOCK_SUCCESS);
					}else{
						mHandler.sendEmptyMessage(ADD_STOCK_FAIL);
					}
				}
				
				@Override
				public void onError(Exception ex) {
					hideProgressDialog();
					ex.printStackTrace();
					mHandler.sendEmptyMessage(DELETE_STOCK_FAIL);
				}
			});
		}
		
	}
	
	private void loadData(final boolean isLoadMore){
		
		
		showProgressDialog();
		isLoading = true;
		if (!isLoadMore) {
			offsetPortfolio = 0;
		} else {
			offsetPortfolio += limitPortfolio;
		}
		
		restService.getPortfolio(limitPortfolio, offsetPortfolio,IStockModel.getInstance().getAccessToken(), new IStockResponseListener<ArrayList<Portfolio>>() {
			
			@Override
			public void onResponse(ArrayList<Portfolio> data) {
				hideProgressDialog();
				isLoading = false;
				if (!isLoadMore) {
					mData.clear();
				}
				if(data.size()> 0){
					for (Portfolio item : data) {
						mData.add(item);
					}
				}else{
					isLoading = true;
				}
			
				mHandler.sendEmptyMessage(LOAD_PORTFOLIO_SUCCESS);
			}
			
			@Override
			public void onError(Exception ex) {
				hideProgressDialog();
				ex.printStackTrace();
				isLoading = false;
				mHandler.sendEmptyMessage(LOAD_PORTFOLIO_FAIL);
			}
		});
	}
	
	private void addStock(String token,String stockCode,String numberOfShare,String purchasePrice){
		showProgressDialog();
		restService.addStockPortfolio(token, stockCode, numberOfShare, purchasePrice, new IStockResponseListener<Boolean>() {
			
			@Override
			public void onResponse(Boolean data) {
				hideProgressDialog();
				if(data){
					mHandler.sendEmptyMessage(ADD_STOCK_SUCCESS);
				}else{
					mHandler.sendEmptyMessage(ADD_STOCK_FAIL);
				}
			}
			
			@Override
			public void onError(Exception ex) {
				hideProgressDialog();
				ex.printStackTrace();
				mHandler.sendEmptyMessage(ADD_STOCK_FAIL);
			}
		});
	}
	
	private void updateStock(boolean checkUpdate,String token,int idStock,String stockCode,String numberOfShare,String purchasePrice,String note){
		showProgressDialog();
		restService.updateStockPortfolio(checkUpdate, token, idStock, stockCode, numberOfShare, purchasePrice,note, new IStockResponseListener<Boolean>() {
			
			@Override
			public void onResponse(Boolean data) {
				hideProgressDialog();
				if(data){
					mHandler.sendEmptyMessage(UPDATE_STOCK_SUCCESS);
				}else{
					mHandler.sendEmptyMessage(UPDATE_STOCK_FAIL);
				}
			}
			
			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
				hideProgressDialog();
				mHandler.sendEmptyMessage(UPDATE_STOCK_FAIL);
			}
		});
	}
	
	private Handler mHandler = new Handler(new Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case LOAD_PORTFOLIO_SUCCESS:
//				if(mAdapter == null){
					mAdapter = new PortfolioAdapter(getActivity().getBaseContext(), mData);
					listPortfolio.setAdapter(mAdapter);
	//			}else{
//					mAdapter.notifyDataSetChanged();
//				}
				break;
				
			case LOAD_PORTFOLIO_FAIL:
							
				break;
							
			case ADD_STOCK_SUCCESS:
				edtAddStock.setText("");
				edtNumberOfShare.setText("");
				edtPurcharsePrice.setText("");
				loadData(false);
				break;
				
			case ADD_STOCK_FAIL:
				break;
				
			case UPDATE_STOCK_SUCCESS:
				statusAdd = true;
				txtAddNew.setText(getActivity().getResources().getString(R.string.fragment_watchlist_portfolio_title_add_new));
				btnOK.setText(getActivity().getResources().getString(R.string.fragment_watchlist_portfolio_title_add_new));
				edtAddStock.setText("");
				edtNumberOfShare.setText("");
				edtPurcharsePrice.setText("");
				loadData(false);
				break;
				
			case UPDATE_STOCK_FAIL:
				
				break;
				
			case DELETE_STOCK_SUCCESS:
				loadData(false);
				break;
				
			case DELETE_STOCK_FAIL:
				
				break;

			default:
				break;
			}
			return false;
		}
	});

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.fragment_watchlist_portfolio_btn_ok:
				String strCode = edtAddStock.getText().toString().trim();
				String strNumberOfShare = edtNumberOfShare.getText().toString().trim();
				String strPurchase = edtPurcharsePrice.getText().toString().trim();
				
				if(strCode == null || strCode.length() == 0){
					Toast.makeText(getActivity().getBaseContext(),"Invalid Stock Code!", Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(strNumberOfShare == null || strNumberOfShare.length() == 0){
					Toast.makeText(getActivity().getBaseContext(),"Invalid Number of Share!", Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(strPurchase == null || strPurchase.length() == 0){
					Toast.makeText(getActivity().getBaseContext(),"Invalid Purchase Price!", Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(statusAdd){
					addStock(IStockModel.getInstance().getAccessToken(), strCode, strNumberOfShare, strPurchase);
				}else{
					updateStock(false,IStockModel.getInstance().getAccessToken(), idStockUpdate,strCode, strNumberOfShare, strPurchase," ");
				}
			break;
			
		case R.id.fragment_watchlist_portfolio_btn_cancel:
			if(statusAdd == false){
				statusAdd = true;
				txtAddNew.setText(getActivity().getResources().getString(R.string.fragment_watchlist_portfolio_title_add_new));
				btnOK.setText(getActivity().getResources().getString(R.string.fragment_watchlist_portfolio_title_add_new));
			}
			layoutAddNew.setVisibility(View.GONE);
			edtAddStock.setText("");
			edtNumberOfShare.setText("");
			edtPurcharsePrice.setText("");
			break;

		default:
			break;
		}
	}
	
	public int convertDpToPixel(float dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }
}

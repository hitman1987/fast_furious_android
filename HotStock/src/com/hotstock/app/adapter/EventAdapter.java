package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.Event;

public class EventAdapter extends BaseAdapter{
	
	private ArrayList<Event> mData = new ArrayList<Event>();
	private LayoutInflater mLayoutInflater;
	private Context mContext;
	
	public EventAdapter(Context c,ArrayList<Event> d) {
		this.mContext = c;
		this.mData = d;
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(
					com.hotstock.R.layout.event_items, null);
			viewHolder.layoutMain = (LinearLayout) convertView.findViewById(R.id.event_items_main);
			viewHolder.txtContent = (TextView) convertView.findViewById(R.id.event_items_txt_content);
			viewHolder.txtStockCode = (TextView) convertView.findViewById(R.id.event_items_txt_stock_code);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.txtStockCode.setText(mData.get(position).getTag_name());
		viewHolder.txtContent.setText(mData.get(position).getTitle());
		
		if (position % 2 == 0) {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources().getColor(R.color.market_list_item_1));
		} else {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources().getColor(R.color.market_list_item_2));
		}

		return convertView;
	}

	private class ViewHolder {
		private LinearLayout layoutMain;
		private TextView txtStockCode;
		private TextView txtContent;
	}

}

package com.hotstock.app.viewcontrol;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ViewFlipper;

public class FlipViewSupportList extends ViewFlipper {

	public GestureDetector detector;

	public FlipViewSupportList(Context context) {
		super(context);
	}

	public FlipViewSupportList(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setUpGestureDetector(GestureDetector detector) {
		this.detector = detector;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		if (detector != null && detector.onTouchEvent(event)) {
			return true;
		}
		return super.onInterceptTouchEvent(event);
	}

}
package com.hotstock.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotstock.R;
import com.hotstock.app.entities.Portfolio;
import com.hotstock.app.fragment.PortfolioFragment;
import com.hotstock.app.utils.AutofitHelper;
import com.hotstock.app.utils.Utils;

public class PortfolioAdapter extends BaseAdapter{
	private ArrayList<Portfolio> mData;
	private LayoutInflater mLayoutInflater;
	private Context mContext;

	public PortfolioAdapter(Context c,ArrayList<Portfolio> d) {
		this.mContext = c;
		this.mData = d;
		mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mLayoutInflater.inflate(R.layout.portfolio_items, null);
			viewHolder.layoutMain = (FrameLayout) convertView.findViewById(R.id.portfolio_item_layout_main);
			viewHolder.layoutMainData = (LinearLayout) convertView.findViewById(R.id.front);
			viewHolder.txtStock = (TextView) convertView.findViewById(R.id.portfolio_item_txt_stock_code);
			viewHolder.txtNumberOfShare = (TextView) convertView.findViewById(R.id.portfolio_item_txt_number_of_share);
			viewHolder.txtPurchasePrice = (TextView) convertView.findViewById(R.id.portfolio_item_txt_purchase_price);
			viewHolder.txtMarketPrice = (TextView) convertView.findViewById(R.id.portfolio_item_txt_market_price);
			viewHolder.txtIncrease = (TextView) convertView.findViewById(R.id.portfolio_item_txt_increase);
			viewHolder.btnDeleteStock = (Button) convertView.findViewById(R.id.portfolio_items_btn_delete);
			viewHolder.btnEditStock = (Button) convertView.findViewById(R.id.portfolio_items_btn_edit);
			AutofitHelper.create(viewHolder.txtIncrease);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.txtStock.setText(mData.get(position).getCode());
		viewHolder.txtNumberOfShare.setText(Utils.formatAsStockVolume(mData.get(position).getNumberofshares()));
		viewHolder.txtPurchasePrice.setText(Utils.formatAsStockVolume(mData.get(position).getPurchase_price()));
		viewHolder.txtMarketPrice.setText(Utils.formatAsStockVolume(mData.get(position).getMarket_price()));
		viewHolder.txtIncrease.setText(mData.get(position).getPercentage_change() + "%");
		if (position % 2 == 0) {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_1));
			viewHolder.layoutMainData.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_1));
		} else {
			viewHolder.layoutMain.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_2));
			viewHolder.layoutMainData.setBackgroundColor(mContext.getResources()
					.getColor(R.color.market_list_item_2));
		}
		
		viewHolder.btnDeleteStock.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				PortfolioFragment.instant.deleteStock(position);
			}
		});
		
		viewHolder.btnEditStock.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				PortfolioFragment.instant.updateStock(position);
			}
		});

		return convertView;
	}

	private class ViewHolder {
		private FrameLayout layoutMain;
		private LinearLayout layoutMainData;
		private TextView txtStock;
		private TextView txtNumberOfShare;
		private TextView txtPurchasePrice;
		private TextView txtMarketPrice;
		private TextView txtIncrease;
		private Button btnDeleteStock;
		private Button btnEditStock;
	}

}

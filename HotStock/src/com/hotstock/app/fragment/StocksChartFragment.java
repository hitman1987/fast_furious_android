package com.hotstock.app.fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer.FillOutsideLine;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;

import com.hotstock.R;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.StockLastDay;
import com.hotstock.app.entities.StockTrades;
import com.hotstock.app.entities.Trades;
import com.hotstock.app.utils.Enums;
import com.hotstock.app.utils.Utils;

public class StocksChartFragment extends Fragment {

	private IStockRestfulService restService = new IStockRestfulServiceImpl();

	private CheckedTextView ctvADay, ctvAMonth, ctvSixMonths, ctvAYear, ctvFiveYears;

	public static final int ONE_DAY = 0xa1;
	public static final int ONE_MONTH = ONE_DAY + 1;
	public static final int SIX_MONTHS = ONE_MONTH + 1;
	public static final int ONE_YEAR = SIX_MONTHS + 1;
	public static final int FIVE_YEARS = ONE_YEAR + 1;

	private int currentDate = ONE_DAY;
	
	private ArrayList<StockLastDay> mDataChartLastDay = new ArrayList<StockLastDay>();

	// //////////////////
	// For chart engine
	// /////////////////
	/** The main dataset that includes all the series that go into a chart. */
	private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();
	/** The main renderer that includes all the renderers customizing a chart. */
	private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
	/** The most recently added series. */
	private XYSeries mCurrentSeries;
	/** The most recently created renderer, customizing the current series. */
	private XYSeriesRenderer mCurrentRenderer;
	/** The chart view that displays the data. */
	private GraphicalView mChartView;
	private ProgressDialog pDialog;
	private String stockCode;
	
	private boolean checkChartDaily = false;

	public StocksChartFragment() {
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_stocks_chart, container, false);
		stockCode = getArguments().getString(Enums.PREF_STOCK_CODE, null);
		if (savedInstanceState != null) {
			mDataset = (XYMultipleSeriesDataset) savedInstanceState.getSerializable("dataset");
			mRenderer = (XYMultipleSeriesRenderer) savedInstanceState.getSerializable("renderer");
			mCurrentSeries = (XYSeries) savedInstanceState.getSerializable("current_series");
			mCurrentRenderer = (XYSeriesRenderer) savedInstanceState.getSerializable("current_renderer");
		}

		ctvADay = (CheckedTextView) rootView.findViewById(R.id.market_tab_one_day);
		ctvAMonth = (CheckedTextView) rootView.findViewById(R.id.market_tab_one_month);
		ctvSixMonths = (CheckedTextView) rootView.findViewById(R.id.market_tab_six_months);
		ctvAYear = (CheckedTextView) rootView.findViewById(R.id.market_tab_one_year);
		ctvFiveYears = (CheckedTextView) rootView.findViewById(R.id.market_tab_five_years);

		OnStockChartFragmentClickListener onClickListener = new OnStockChartFragmentClickListener();
		ctvADay.setOnClickListener(onClickListener);
		ctvAMonth.setOnClickListener(onClickListener);
		ctvSixMonths.setOnClickListener(onClickListener);
		ctvAYear.setOnClickListener(onClickListener);
		ctvFiveYears.setOnClickListener(onClickListener);

		if (stockCode == null) {
			stockCode = "VNM";
		}
		
		// prevent null pointer exception
		if (mRenderer == null)
			mRenderer = new XYMultipleSeriesRenderer();
		
		initChart(rootView, stockCode);
		return rootView;
	}

	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}

	private void initChart(View v, String marketName) {

		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);

		// set some properties on the main renderer
		mRenderer.setApplyBackgroundColor(true);
		mRenderer.setBackgroundColor(Color.TRANSPARENT);
		mRenderer.setMarginsColor(getActivity().getResources().getColor(android.R.color.transparent));
		mRenderer.setAxisTitleTextSize(16);
		mRenderer.setChartTitleTextSize(20);
		mRenderer.setLabelsTextSize(15);
		mRenderer.setLegendTextSize(15);
		mRenderer.setMargins(new int[] { 40, 40, 40, 40 });
		mRenderer.setZoomButtonsVisible(false);
		mRenderer.setPointSize(5);
		mRenderer.setShowGridX(true);
		mRenderer.setGridColor(Color.BLACK);
		mRenderer.setPanEnabled(false);
		mRenderer.setYLabelsAlign(Align.RIGHT);
		mRenderer.setPanEnabled(false, false);
		mRenderer.setZoomEnabled(false, false);

		if (mChartView == null) {
			if(mDataset != null && mRenderer != null){
				LinearLayout layout = (LinearLayout) v.findViewById(R.id.stock_chart);
				mChartView = ChartFactory.getLineChartView(getActivity(), mDataset, mRenderer);
				mChartView.setBackgroundColor(Color.TRANSPARENT);
				mRenderer.setSelectableBuffer(10);
				layout.addView(mChartView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			} else {
				return;
			}
		} else {
			mChartView.repaint();
		}

		String seriesTitle = "Series " + (mDataset.getSeriesCount() + 1);
		// create a new series of data
		XYSeries series = new XYSeries(seriesTitle);
		mDataset.addSeries(series);
		mCurrentSeries = series;
		// create a new renderer for the new series
		XYSeriesRenderer renderer = new XYSeriesRenderer();
		mRenderer.addSeriesRenderer(renderer);
		// set some renderer properties
		renderer.setPointStyle(PointStyle.POINT);
		renderer.setColor(getActivity().getResources().getColor(R.color.chart_color));
		renderer.setFillPoints(false);
		renderer.setDisplayChartValues(false);
		renderer.setDisplayChartValuesDistance(10);
		renderer.setShowLegendItem(false);
		FillOutsideLine fill = new FillOutsideLine(FillOutsideLine.Type.BOUNDS_ABOVE);
		fill.setColor(getActivity().getResources().getColor(R.color.chart_color));
		renderer.addFillOutsideLine(fill);
		fill = new FillOutsideLine(FillOutsideLine.Type.BOUNDS_BELOW);
		fill.setColor(getActivity().getResources().getColor(R.color.chart_color));
		renderer.addFillOutsideLine(fill);
		mCurrentRenderer = renderer;
		mChartView.repaint();

		drawChartView(marketName);
	}

	private void drawChartView(final String marketName) {
		showProgressDialog();
		mCurrentSeries.clear();

		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ",Locale.US);
		final String now = df.format(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		switch (currentDate) {
		case ONE_DAY:
			checkChartDaily = true;
			//cal.add(Calendar.DATE, -1);
			break;
		case ONE_MONTH:
			checkChartDaily = false;
			cal.add(Calendar.MONTH, -1);
			break;
		case SIX_MONTHS:
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous3Months = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			String previous3Months1Day = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			String previous6Months = df.format(date);
			// System.out.println(">>> now: " + now);
			// System.out.println(">>> prev 3 months: " + previous3Months);
			// System.out.println(">>> prev 3 months and one day: " + previous3Months1Day);
			// System.out.println(">>> prev 6 months: " + previous6Months);
			final List<Trades> listTradesFinal = new ArrayList<Trades>();
			restService.getStockTransaction(marketName, previous6Months, previous3Months1Day, new IStockResponseListener<ArrayList<StockTrades>>() {

				@Override
				public void onResponse(ArrayList<StockTrades> data) {
					if (data != null && data.size() > 0) {
						List<Trades> listTrades = data.get(0).Trades;
						if (listTrades != null && listTrades.size() > 0) {
							Collections.reverse(listTrades);
							for (Trades trade : listTrades) {
								listTradesFinal.add(trade);
							}
						}
					}

					restService.getStockTransaction(marketName, previous3Months, now, new IStockResponseListener<ArrayList<StockTrades>>() {

						@Override
						public void onResponse(ArrayList<StockTrades> data) {
							hideProgressDialog();
							if (data != null && data.size() > 0) {
								List<Trades> listTrades = data.get(0).Trades;
								if (listTrades != null && listTrades.size() > 0) {
									Collections.reverse(listTrades);
									for (Trades trade : listTrades) {
										listTradesFinal.add(trade);
									}

									double x = -1;
									double minY = Double.MAX_VALUE;
									int displayDateStep = listTradesFinal.size() / 5;
									int countStep = 0;
									for (Trades trade : listTradesFinal) {
										x++;
										double y = trade.last_price / 1000;
										if (minY > y) {
											minY = y;
										}
										// add a new data point to the current series
										mCurrentSeries.add(x, y);
										// repaint the chart such as the newly added
										// point to be visible
										mChartView.repaint();
									}
									if (displayDateStep != 0) {
										for (int i = 0; i < listTradesFinal.size(); i++) {
											if (i % displayDateStep == 0) {
												countStep++;
												mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal.get(i).trading_time));
											} else {
												if (i == listTradesFinal.size() - 1 && countStep < 6) {
													mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal.get(i).trading_time));
												} else {
													mRenderer.addXTextLabel(i, "");
												}
											}
										}
									} else {
										for (int i = 0; i < listTradesFinal.size(); i++) {
											mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal.get(i).trading_time));
										}
									}
									mRenderer.setYAxisMin(minY - 0.1);
									mRenderer.setXLabels(0);
								}
							}
						}

						@Override
						public void onError(Exception ex) {
							ex.printStackTrace();
							hideProgressDialog();
						}
					});
				}

				@Override
				public void onError(Exception ex) {
					ex.printStackTrace();
					hideProgressDialog();
				}
			});
			return;
		case ONE_YEAR:
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous3Months1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous3Months1Day1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous6Months1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous6Months1Day1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous9Months1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous9Months1Day1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous12Months1 = df.format(date);
			System.out.println(">>> now: " + now);
			System.out.println(">>> prev 3 months: " + previous3Months1);
			System.out.println(">>> prev 3 months and one day: " + previous3Months1Day1);
			System.out.println(">>> prev 6 months: " + previous6Months1);
			System.out.println(">>> prev 6 months and one day: " + previous6Months1Day1);
			System.out.println(">>> prev 9 months: " + previous9Months1);
			System.out.println(">>> prev 9 months and one day: " + previous9Months1Day1);
			System.out.println(">>> prev 12 months: " + previous12Months1);
			final List<Trades> listTradesFinal1 = new ArrayList<Trades>();
			restService.getStockTransaction(marketName, previous12Months1, previous9Months1Day1,
					new IStockResponseListener<ArrayList<StockTrades>>() {

						@Override
						public void onResponse(ArrayList<StockTrades> data) {
							if (data != null && data.size() > 0) {
								List<Trades> listTrades = data.get(0).Trades;
								if (listTrades != null && listTrades.size() > 0) {
									Collections.reverse(listTrades);
									for (Trades trade : listTrades) {
										// System.out.println(">>>>>>> " + trade.trading_time);
										listTradesFinal1.add(trade);
									}
								}
							}

							restService.getStockTransaction(marketName, previous9Months1, previous6Months1Day1,
									new IStockResponseListener<ArrayList<StockTrades>>() {

										@Override
										public void onResponse(ArrayList<StockTrades> data) {
											if (data != null && data.size() > 0) {
												List<Trades> listTrades = data.get(0).Trades;
												if (listTrades != null && listTrades.size() > 0) {
													Collections.reverse(listTrades);
													for (Trades trade : listTrades) {
														// System.out.println(">>>>>>>> " + trade.trading_time);
														listTradesFinal1.add(trade);
													}
												}
											}

											restService.getStockTransaction(marketName, previous6Months1, previous3Months1Day1,
													new IStockResponseListener<ArrayList<StockTrades>>() {

														@Override
														public void onResponse(ArrayList<StockTrades> data) {
															if (data != null && data.size() > 0) {
																List<Trades> listTrades = data.get(0).Trades;
																if (listTrades != null && listTrades.size() > 0) {
																	Collections.reverse(listTrades);
																	for (Trades trade : listTrades) {
																		// System.out.println(">>>>>>>> " + trade.trading_time);
																		listTradesFinal1.add(trade);
																	}
																}
															}

															restService.getStockTransaction(marketName, previous3Months1, now,
																	new IStockResponseListener<ArrayList<StockTrades>>() {

																		@Override
																		public void onResponse(ArrayList<StockTrades> data) {
																			hideProgressDialog();
																			if (data != null && data.size() > 0) {
																				List<Trades> listTrades = data.get(0).Trades;
																				if (listTrades != null && listTrades.size() > 0) {
																					Collections.reverse(listTrades);
																					for (Trades trade : listTrades) {
																						// System.out.println(">>>>>>>> " + trade.trading_time);
																						listTradesFinal1.add(trade);
																					}
																				}

																				double x = -1;
																				double minY = Double.MAX_VALUE;
																				int displayDateStep = listTradesFinal1.size() / 5;
																				int countStep = 0;
																				for (Trades trade : listTradesFinal1) {
																					x++;
																					double y = trade.last_price / 1000;
																					if (minY > y) {
																						minY = y;
																					}
																					// add a new data point to the current series
																					mCurrentSeries.add(x, y);
																					// repaint the chart such as the newly added
																					// point to be visible
																					mChartView.repaint();
																				}
																				if (displayDateStep != 0) {
																					for (int i = 0; i < listTradesFinal1.size(); i++) {
																						if (i % displayDateStep == 0) {
																							countStep++;
																							mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal1.get(i).trading_time));
																						} else {
																							if (i == listTradesFinal1.size() - 1 && countStep < 6) {
																								mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal1.get(i).trading_time));
																							} else {
																								mRenderer.addXTextLabel(i, "");
																							}
																						}
																					}
																				} else {
																					for (int i = 0; i < listTradesFinal1.size(); i++) {
																						mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal1.get(i).trading_time));
																					}
																				}
																				mRenderer.setYAxisMin(minY - 0.1);
																				mRenderer.setXLabels(0);
																			}
																		}

																		@Override
																		public void onError(Exception ex) {
																			ex.printStackTrace();
																			hideProgressDialog();
																		}
																	});
														}

														@Override
														public void onError(Exception ex) {
															ex.printStackTrace();
															hideProgressDialog();
														}
													});
										}

										@Override
										public void onError(Exception ex) {
											ex.printStackTrace();
											hideProgressDialog();
										}
									});
						}

						@Override
						public void onError(Exception ex) {
							ex.printStackTrace();
							hideProgressDialog();
						}
					});
			return;
		case FIVE_YEARS:
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous3Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous3Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous6Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous6Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous9Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous9Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous12Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous12Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous15Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous15Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous18Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous18Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous21Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous21Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous24Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous24Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous27Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous27Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous30Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous30Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous33Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous33Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous36Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous36Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous39Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous39Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous42Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous42Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous45Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous45Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous48Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous48Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous51Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous51Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous54Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous54Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous57Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous57Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous60Months2 = df.format(date);
			System.out.println(">>> now: " + now);
			System.out.println(">>> prev 3 months: " + previous3Months2);
			System.out.println(">>> prev 3 months and one day: " + previous3Months1Day2);
			System.out.println(">>> prev 6 months: " + previous6Months2);
			System.out.println(">>> prev 6 months and one day: " + previous6Months1Day2);
			System.out.println(">>> prev 9 months: " + previous9Months2);
			System.out.println(">>> prev 9 months and one day: " + previous9Months1Day2);
			System.out.println(">>> prev 12 months: " + previous12Months2);
			System.out.println(">>> prev 12 months and one day: " + previous12Months1Day2);
			System.out.println(">>> prev 15 months: " + previous15Months2);
			System.out.println(">>> prev 15 months and one day: " + previous15Months1Day2);
			System.out.println(">>> prev 18 months: " + previous18Months2);
			System.out.println(">>> prev 18 months and one day: " + previous18Months1Day2);
			System.out.println(">>> prev 21 months: " + previous21Months2);
			System.out.println(">>> prev 21 months and one day: " + previous21Months1Day2);
			System.out.println(">>> prev 24 months: " + previous24Months2);
			System.out.println(">>> prev 24 months and one day: " + previous24Months1Day2);
			System.out.println(">>> prev 27 months: " + previous27Months2);
			System.out.println(">>> prev 27 months and one day: " + previous27Months1Day2);
			System.out.println(">>> prev 30 months: " + previous30Months2);
			System.out.println(">>> prev 30 months and one day: " + previous30Months1Day2);
			System.out.println(">>> prev 33 months: " + previous33Months2);
			System.out.println(">>> prev 33 months and one day: " + previous33Months1Day2);
			System.out.println(">>> prev 36 months: " + previous36Months2);
			System.out.println(">>> prev 36 months and one day: " + previous36Months1Day2);
			System.out.println(">>> prev 39 months: " + previous39Months2);
			System.out.println(">>> prev 39 months and one day: " + previous39Months1Day2);
			System.out.println(">>> prev 42 months: " + previous42Months2);
			System.out.println(">>> prev 42 months and one day: " + previous42Months1Day2);
			System.out.println(">>> prev 45 months: " + previous45Months2);
			System.out.println(">>> prev 45 months and one day: " + previous45Months1Day2);
			System.out.println(">>> prev 48 months: " + previous48Months2);
			System.out.println(">>> prev 48 months and one day: " + previous48Months1Day2);
			System.out.println(">>> prev 51 months: " + previous51Months2);
			System.out.println(">>> prev 51 months and one day: " + previous51Months1Day2);
			System.out.println(">>> prev 54 months: " + previous54Months2);
			System.out.println(">>> prev 54 months and one day: " + previous54Months1Day2);
			System.out.println(">>> prev 57 months: " + previous57Months2);
			System.out.println(">>> prev 57 months and one day: " + previous57Months1Day2);
			System.out.println(">>> prev 60 months: " + previous60Months2);
			final List<Trades> listTradesFinal2 = new ArrayList<Trades>();
			restService.getStockTransaction(marketName, previous60Months2, previous57Months1Day2,
					new IStockResponseListener<ArrayList<StockTrades>>() {

						@Override
						public void onResponse(ArrayList<StockTrades> data) {
							if (data != null && data.size() > 0) {
								List<Trades> listTrades = data.get(0).Trades;
								if (listTrades != null && listTrades.size() > 0) {
									Collections.reverse(listTrades);
									for (Trades trade : listTrades) {
										System.out.println(">>>>>>> " + trade.trading_time);
										listTradesFinal2.add(trade);
									}
								}
							}

							restService.getStockTransaction(marketName, previous57Months2, previous54Months1Day2,
									new IStockResponseListener<ArrayList<StockTrades>>() {

										@Override
										public void onResponse(ArrayList<StockTrades> data) {
											if (data != null && data.size() > 0) {
												List<Trades> listTrades = data.get(0).Trades;
												if (listTrades != null && listTrades.size() > 0) {
													Collections.reverse(listTrades);
													for (Trades trade : listTrades) {
														System.out.println(">>>>>>>> " + trade.trading_time);
														listTradesFinal2.add(trade);
													}
												}
											}

											restService.getStockTransaction(marketName, previous54Months2, previous51Months1Day2,
													new IStockResponseListener<ArrayList<StockTrades>>() {

														@Override
														public void onResponse(ArrayList<StockTrades> data) {
															if (data != null && data.size() > 0) {
																List<Trades> listTrades = data.get(0).Trades;
																if (listTrades != null && listTrades.size() > 0) {
																	Collections.reverse(listTrades);
																	for (Trades trade : listTrades) {
																		System.out.println(">>>>>>>> " + trade.trading_time);
																		listTradesFinal2.add(trade);
																	}
																}
															}

															restService.getStockTransaction(marketName, previous51Months2, previous48Months1Day2,
																	new IStockResponseListener<ArrayList<StockTrades>>() {

																		@Override
																		public void onResponse(ArrayList<StockTrades> data) {
																			if (data != null && data.size() > 0) {
																				List<Trades> listTrades = data.get(0).Trades;
																				if (listTrades != null && listTrades.size() > 0) {
																					Collections.reverse(listTrades);
																					for (Trades trade : listTrades) {
																						System.out.println(">>>>>>>> " + trade.trading_time);
																						listTradesFinal2.add(trade);
																					}
																				}
																			}

																			restService.getStockTransaction(marketName, previous48Months2,
																					previous45Months1Day2,
																					new IStockResponseListener<ArrayList<StockTrades>>() {

																						@Override
																						public void onResponse(ArrayList<StockTrades> data) {
																							if (data != null && data.size() > 0) {
																								List<Trades> listTrades = data.get(0).Trades;
																								if (listTrades != null && listTrades.size() > 0) {
																									Collections.reverse(listTrades);
																									for (Trades trade : listTrades) {
																										System.out.println(">>>>>>>> "
																												+ trade.trading_time);
																										listTradesFinal2.add(trade);
																									}
																								}
																							}

																							restService
																									.getStockTransaction(
																											marketName,
																											previous45Months2,
																											previous42Months1Day2,
																											new IStockResponseListener<ArrayList<StockTrades>>() {

																												@Override
																												public void onResponse(
																														ArrayList<StockTrades> data) {
																													if (data != null
																															&& data.size() > 0) {
																														List<Trades> listTrades = data
																																.get(0).Trades;
																														if (listTrades != null
																																&& listTrades.size() > 0) {
																															Collections
																																	.reverse(listTrades);
																															for (Trades trade : listTrades) {
																																System.out
																																		.println(">>>>>>>> "
																																				+ trade.trading_time);
																																listTradesFinal2
																																		.add(trade);
																															}
																														}
																													}

																													restService
																															.getStockTransaction(
																																	marketName,
																																	previous42Months2,
																																	previous39Months1Day2,
																																	new IStockResponseListener<ArrayList<StockTrades>>() {

																																		@Override
																																		public void onResponse(
																																				ArrayList<StockTrades> data) {
																																			if (data != null
																																					&& data.size() > 0) {
																																				List<Trades> listTrades = data
																																						.get(0).Trades;
																																				if (listTrades != null
																																						&& listTrades
																																								.size() > 0) {
																																					Collections
																																							.reverse(listTrades);
																																					for (Trades trade : listTrades) {
																																						System.out
																																								.println(">>>>>>>> "
																																										+ trade.trading_time);
																																						listTradesFinal2
																																								.add(trade);
																																					}
																																				}
																																			}

																																			restService
																																					.getStockTransaction(
																																							marketName,
																																							previous39Months2,
																																							previous36Months1Day2,
																																							new IStockResponseListener<ArrayList<StockTrades>>() {

																																								@Override
																																								public void onResponse(
																																										ArrayList<StockTrades> data) {
																																									if (data != null
																																											&& data.size() > 0) {
																																										List<Trades> listTrades = data
																																												.get(0).Trades;
																																										if (listTrades != null
																																												&& listTrades
																																														.size() > 0) {
																																											Collections
																																													.reverse(listTrades);
																																											for (Trades trade : listTrades) {
																																												System.out
																																														.println(">>>>>>>> "
																																																+ trade.trading_time);
																																												listTradesFinal2
																																														.add(trade);
																																											}
																																										}
																																									}

																																									restService
																																											.getStockTransaction(
																																													marketName,
																																													previous36Months2,
																																													previous33Months1Day2,
																																													new IStockResponseListener<ArrayList<StockTrades>>() {

																																														@Override
																																														public void onResponse(
																																																ArrayList<StockTrades> data) {
																																															if (data != null
																																																	&& data.size() > 0) {
																																																List<Trades> listTrades = data
																																																		.get(0).Trades;
																																																if (listTrades != null
																																																		&& listTrades
																																																				.size() > 0) {
																																																	Collections
																																																			.reverse(listTrades);
																																																	for (Trades trade : listTrades) {
																																																		System.out
																																																				.println(">>>>>>>> "
																																																						+ trade.trading_time);
																																																		listTradesFinal2
																																																				.add(trade);
																																																	}
																																																}
																																															}

																																															restService
																																																	.getStockTransaction(
																																																			marketName,
																																																			previous33Months2,
																																																			previous30Months1Day2,
																																																			new IStockResponseListener<ArrayList<StockTrades>>() {

																																																				@Override
																																																				public void onResponse(
																																																						ArrayList<StockTrades> data) {
																																																					if (data != null
																																																							&& data.size() > 0) {
																																																						List<Trades> listTrades = data
																																																								.get(0).Trades;
																																																						if (listTrades != null
																																																								&& listTrades
																																																										.size() > 0) {
																																																							Collections
																																																									.reverse(listTrades);
																																																							for (Trades trade : listTrades) {
																																																								System.out
																																																										.println(">>>>>>>> "
																																																												+ trade.trading_time);
																																																								listTradesFinal2
																																																										.add(trade);
																																																							}
																																																						}
																																																					}

																																																					restService
																																																							.getStockTransaction(
																																																									marketName,
																																																									previous30Months2,
																																																									previous27Months1Day2,
																																																									new IStockResponseListener<ArrayList<StockTrades>>() {

																																																										@Override
																																																										public void onResponse(
																																																												ArrayList<StockTrades> data) {
																																																											if (data != null
																																																													&& data.size() > 0) {
																																																												List<Trades> listTrades = data
																																																														.get(0).Trades;
																																																												if (listTrades != null
																																																														&& listTrades
																																																																.size() > 0) {
																																																													Collections
																																																															.reverse(listTrades);
																																																													for (Trades trade : listTrades) {
																																																														System.out
																																																																.println(">>>>>>>> "
																																																																		+ trade.trading_time);
																																																														listTradesFinal2
																																																																.add(trade);
																																																													}
																																																												}
																																																											}

																																																											restService
																																																													.getStockTransaction(
																																																															marketName,
																																																															previous27Months2,
																																																															previous24Months1Day2,
																																																															new IStockResponseListener<ArrayList<StockTrades>>() {

																																																																@Override
																																																																public void onResponse(
																																																																		ArrayList<StockTrades> data) {
																																																																	if (data != null
																																																																			&& data.size() > 0) {
																																																																		List<Trades> listTrades = data
																																																																				.get(0).Trades;
																																																																		if (listTrades != null
																																																																				&& listTrades
																																																																						.size() > 0) {
																																																																			Collections
																																																																					.reverse(listTrades);
																																																																			for (Trades trade : listTrades) {
																																																																				System.out
																																																																						.println(">>>>>>>> "
																																																																								+ trade.trading_time);
																																																																				listTradesFinal2
																																																																						.add(trade);
																																																																			}
																																																																		}
																																																																	}

																																																																	restService
																																																																			.getStockTransaction(
																																																																					marketName,
																																																																					previous24Months2,
																																																																					previous21Months1Day2,
																																																																					new IStockResponseListener<ArrayList<StockTrades>>() {

																																																																						@Override
																																																																						public void onResponse(
																																																																								ArrayList<StockTrades> data) {
																																																																							if (data != null
																																																																									&& data.size() > 0) {
																																																																								List<Trades> listTrades = data
																																																																										.get(0).Trades;
																																																																								if (listTrades != null
																																																																										&& listTrades
																																																																												.size() > 0) {
																																																																									Collections
																																																																											.reverse(listTrades);
																																																																									for (Trades trade : listTrades) {
																																																																										System.out
																																																																												.println(">>>>>>>> "
																																																																														+ trade.trading_time);
																																																																										listTradesFinal2
																																																																												.add(trade);
																																																																									}
																																																																								}
																																																																							}

																																																																							restService
																																																																									.getStockTransaction(
																																																																											marketName,
																																																																											previous21Months2,
																																																																											previous18Months1Day2,
																																																																											new IStockResponseListener<ArrayList<StockTrades>>() {

																																																																												@Override
																																																																												public void onResponse(
																																																																														ArrayList<StockTrades> data) {
																																																																													if (data != null
																																																																															&& data.size() > 0) {
																																																																														List<Trades> listTrades = data
																																																																																.get(0).Trades;
																																																																														if (listTrades != null
																																																																																&& listTrades
																																																																																		.size() > 0) {
																																																																															Collections
																																																																																	.reverse(listTrades);
																																																																															for (Trades trade : listTrades) {
																																																																																System.out
																																																																																		.println(">>>>>>>> "
																																																																																				+ trade.trading_time);
																																																																																listTradesFinal2
																																																																																		.add(trade);
																																																																															}
																																																																														}
																																																																													}

																																																																													restService
																																																																															.getStockTransaction(
																																																																																	marketName,
																																																																																	previous18Months2,
																																																																																	previous15Months1Day2,
																																																																																	new IStockResponseListener<ArrayList<StockTrades>>() {

																																																																																		@Override
																																																																																		public void onResponse(
																																																																																				ArrayList<StockTrades> data) {
																																																																																			if (data != null
																																																																																					&& data.size() > 0) {
																																																																																				List<Trades> listTrades = data
																																																																																						.get(0).Trades;
																																																																																				if (listTrades != null
																																																																																						&& listTrades
																																																																																								.size() > 0) {
																																																																																					Collections
																																																																																							.reverse(listTrades);
																																																																																					for (Trades trade : listTrades) {
																																																																																						System.out
																																																																																								.println(">>>>>>>> "
																																																																																										+ trade.trading_time);
																																																																																						listTradesFinal2
																																																																																								.add(trade);
																																																																																					}
																																																																																				}
																																																																																			}

																																																																																			restService
																																																																																					.getStockTransaction(
																																																																																							marketName,
																																																																																							previous15Months2,
																																																																																							previous12Months1Day2,
																																																																																							new IStockResponseListener<ArrayList<StockTrades>>() {

																																																																																								@Override
																																																																																								public void onResponse(
																																																																																										ArrayList<StockTrades> data) {
																																																																																									if (data != null
																																																																																											&& data.size() > 0) {
																																																																																										List<Trades> listTrades = data
																																																																																												.get(0).Trades;
																																																																																										if (listTrades != null
																																																																																												&& listTrades
																																																																																														.size() > 0) {
																																																																																											Collections
																																																																																													.reverse(listTrades);
																																																																																											for (Trades trade : listTrades) {
																																																																																												System.out
																																																																																														.println(">>>>>>>> "
																																																																																																+ trade.trading_time);
																																																																																												listTradesFinal2
																																																																																														.add(trade);
																																																																																											}
																																																																																										}
																																																																																									}

																																																																																									restService
																																																																																											.getStockTransaction(
																																																																																													marketName,
																																																																																													previous12Months2,
																																																																																													previous9Months1Day2,
																																																																																													new IStockResponseListener<ArrayList<StockTrades>>() {

																																																																																														@Override
																																																																																														public void onResponse(
																																																																																																ArrayList<StockTrades> data) {
																																																																																															if (data != null
																																																																																																	&& data.size() > 0) {
																																																																																																List<Trades> listTrades = data
																																																																																																		.get(0).Trades;
																																																																																																if (listTrades != null
																																																																																																		&& listTrades
																																																																																																				.size() > 0) {
																																																																																																	Collections
																																																																																																			.reverse(listTrades);
																																																																																																	for (Trades trade : listTrades) {
																																																																																																		System.out
																																																																																																				.println(">>>>>>>> "
																																																																																																						+ trade.trading_time);
																																																																																																		listTradesFinal2
																																																																																																				.add(trade);
																																																																																																	}
																																																																																																}
																																																																																															}

																																																																																															restService
																																																																																																	.getStockTransaction(
																																																																																																			marketName,
																																																																																																			previous9Months2,
																																																																																																			previous6Months1Day2,
																																																																																																			new IStockResponseListener<ArrayList<StockTrades>>() {

																																																																																																				@Override
																																																																																																				public void onResponse(
																																																																																																						ArrayList<StockTrades> data) {
																																																																																																					if (data != null
																																																																																																							&& data.size() > 0) {
																																																																																																						List<Trades> listTrades = data
																																																																																																								.get(0).Trades;
																																																																																																						if (listTrades != null
																																																																																																								&& listTrades
																																																																																																										.size() > 0) {
																																																																																																							Collections
																																																																																																									.reverse(listTrades);
																																																																																																							for (Trades trade : listTrades) {
																																																																																																								System.out
																																																																																																										.println(">>>>>>>> "
																																																																																																												+ trade.trading_time);
																																																																																																								listTradesFinal2
																																																																																																										.add(trade);
																																																																																																							}
																																																																																																						}
																																																																																																					}

																																																																																																					restService
																																																																																																							.getStockTransaction(
																																																																																																									marketName,
																																																																																																									previous6Months2,
																																																																																																									previous3Months1Day2,
																																																																																																									new IStockResponseListener<ArrayList<StockTrades>>() {

																																																																																																										@Override
																																																																																																										public void onResponse(
																																																																																																												ArrayList<StockTrades> data) {
																																																																																																											if (data != null
																																																																																																													&& data.size() > 0) {
																																																																																																												List<Trades> listTrades = data
																																																																																																														.get(0).Trades;
																																																																																																												if (listTrades != null
																																																																																																														&& listTrades
																																																																																																																.size() > 0) {
																																																																																																													Collections
																																																																																																															.reverse(listTrades);
																																																																																																													for (Trades trade : listTrades) {
																																																																																																														System.out
																																																																																																																.println(">>>>>>>> "
																																																																																																																		+ trade.trading_time);
																																																																																																														listTradesFinal2
																																																																																																																.add(trade);
																																																																																																													}
																																																																																																												}
																																																																																																											}

																																																																																																											restService
																																																																																																													.getStockTransaction(
																																																																																																															marketName,
																																																																																																															previous3Months2,
																																																																																																															now,
																																																																																																															new IStockResponseListener<ArrayList<StockTrades>>() {

																																																																																																																@Override
																																																																																																																public void onResponse(
																																																																																																																		ArrayList<StockTrades> data) {
																																																																																																																	hideProgressDialog();
																																																																																																																	if (data != null
																																																																																																																			&& data.size() > 0) {
																																																																																																																		List<Trades> listTrades = data
																																																																																																																				.get(0).Trades;
																																																																																																																		if (listTrades != null
																																																																																																																				&& listTrades
																																																																																																																						.size() > 0) {
																																																																																																																			Collections
																																																																																																																					.reverse(listTrades);
																																																																																																																			for (Trades trade : listTrades) {
																																																																																																																				System.out
																																																																																																																						.println(">>>>>>>> "
																																																																																																																								+ trade.trading_time);
																																																																																																																				listTradesFinal2
																																																																																																																						.add(trade);
																																																																																																																			}
																																																																																																																		}

																																																																																																																		double x = -1;
																																																																																																																		double minY = Double.MAX_VALUE;
																																																																																																																		int displayDateStep = listTradesFinal2.size() / 5;
																																																																																																																		int countStep = 0;
																																																																																																																		for (Trades trade : listTradesFinal2) {
																																																																																																																			x++;
																																																																																																																			double y = trade.last_price / 1000;
																																																																																																																			if (minY > y) {
																																																																																																																				minY = y;
																																																																																																																			}
																																																																																																																			// add a new data point to the current series
																																																																																																																			mCurrentSeries.add(x, y);
																																																																																																																			// repaint the chart such as the newly added
																																																																																																																			// point to be visible
																																																																																																																			mChartView.repaint();
																																																																																																																		}
																																																																																																																		if (displayDateStep != 0) {
																																																																																																																			for (int i = 0; i < listTradesFinal2.size(); i++) {
																																																																																																																				if (i % displayDateStep == 0) {
																																																																																																																					countStep++;
																																																																																																																					mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal2.get(i).trading_time));
																																																																																																																				} else {
																																																																																																																					if (i == listTradesFinal2.size() - 1 && countStep < 6) {
																																																																																																																						mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal2.get(i).trading_time));
																																																																																																																					} else {
																																																																																																																						mRenderer.addXTextLabel(i, "");
																																																																																																																					}
																																																																																																																				}
																																																																																																																			}
																																																																																																																		} else {
																																																																																																																			for (int i = 0; i < listTradesFinal2.size(); i++) {
																																																																																																																				mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal2.get(i).trading_time));
																																																																																																																			}
																																																																																																																		}
																																																																																																																		mRenderer.setYAxisMin(minY - 0.1);
																																																																																																																		mRenderer
																																																																																																																				.setXLabels(0);
																																																																																																																	}
																																																																																																																}

																																																																																																																@Override
																																																																																																																public void onError(
																																																																																																																		Exception ex) {
																																																																																																																	ex.printStackTrace();
																																																																																																																	hideProgressDialog();
																																																																																																																}
																																																																																																															});
																																																																																																										}

																																																																																																										@Override
																																																																																																										public void onError(
																																																																																																												Exception ex) {
																																																																																																											ex.printStackTrace();
																																																																																																											hideProgressDialog();
																																																																																																										}
																																																																																																									});
																																																																																																				}

																																																																																																				@Override
																																																																																																				public void onError(
																																																																																																						Exception ex) {
																																																																																																					ex.printStackTrace();
																																																																																																					hideProgressDialog();
																																																																																																				}
																																																																																																			});
																																																																																														}

																																																																																														@Override
																																																																																														public void onError(
																																																																																																Exception ex) {
																																																																																															ex.printStackTrace();
																																																																																															hideProgressDialog();
																																																																																														}
																																																																																													});
																																																																																								}

																																																																																								@Override
																																																																																								public void onError(
																																																																																										Exception ex) {
																																																																																									ex.printStackTrace();
																																																																																									hideProgressDialog();
																																																																																								}
																																																																																							});
																																																																																		}

																																																																																		@Override
																																																																																		public void onError(
																																																																																				Exception ex) {
																																																																																			ex.printStackTrace();
																																																																																			hideProgressDialog();
																																																																																		}
																																																																																	});
																																																																												}

																																																																												@Override
																																																																												public void onError(
																																																																														Exception ex) {
																																																																													ex.printStackTrace();
																																																																													hideProgressDialog();
																																																																												}
																																																																											});
																																																																						}

																																																																						@Override
																																																																						public void onError(
																																																																								Exception ex) {
																																																																							ex.printStackTrace();
																																																																							hideProgressDialog();
																																																																						}
																																																																					});
																																																																}

																																																																@Override
																																																																public void onError(
																																																																		Exception ex) {
																																																																	ex.printStackTrace();
																																																																	hideProgressDialog();
																																																																}
																																																															});
																																																										}

																																																										@Override
																																																										public void onError(
																																																												Exception ex) {
																																																											ex.printStackTrace();
																																																											hideProgressDialog();
																																																										}
																																																									});
																																																				}

																																																				@Override
																																																				public void onError(
																																																						Exception ex) {
																																																					ex.printStackTrace();
																																																					hideProgressDialog();
																																																				}
																																																			});
																																														}

																																														@Override
																																														public void onError(
																																																Exception ex) {
																																															ex.printStackTrace();
																																															hideProgressDialog();
																																														}
																																													});
																																								}

																																								@Override
																																								public void onError(
																																										Exception ex) {
																																									ex.printStackTrace();
																																									hideProgressDialog();
																																								}
																																							});
																																		}

																																		@Override
																																		public void onError(
																																				Exception ex) {
																																			ex.printStackTrace();
																																			hideProgressDialog();
																																		}
																																	});
																												}

																												@Override
																												public void onError(Exception ex) {
																													ex.printStackTrace();
																													hideProgressDialog();
																												}
																											});
																						}

																						@Override
																						public void onError(Exception ex) {
																							ex.printStackTrace();
																							hideProgressDialog();
																						}
																					});
																		}

																		@Override
																		public void onError(Exception ex) {
																			ex.printStackTrace();
																			hideProgressDialog();
																		}
																	});
														}

														@Override
														public void onError(Exception ex) {
															ex.printStackTrace();
															hideProgressDialog();
														}
													});
										}

										@Override
										public void onError(Exception ex) {
											ex.printStackTrace();
											hideProgressDialog();
										}
									});
						}

						@Override
						public void onError(Exception ex) {
							ex.printStackTrace();
							hideProgressDialog();
						}
					});
			return;
		}

		date = cal.getTime();
		String previous = df.format(date);
		System.out.println(">>> now: " + now);
		System.out.println(">>> prev: " + previous);

		if(!checkChartDaily){
			restService.getStockTransaction(marketName, previous, now, new IStockResponseListener<ArrayList<StockTrades>>() {

				@Override
				public void onResponse(ArrayList<StockTrades> data) {
					hideProgressDialog();
					if (data != null && data.size() > 0) {
						List<Trades> listTrades = data.get(0).Trades;
						if (listTrades != null && listTrades.size() > 0) {
							Collections.reverse(listTrades);
							double x = -1;
							double minY = Double.MAX_VALUE;
							int displayDateStep = listTrades.size() / 3;
							int countStep = 0;
							for (Trades trade : listTrades) {
								x++;
								double y = trade.last_price / 1000;
								if (minY > y) {
									minY = y;
								}
								// add a new data point to the current series
								mCurrentSeries.add(x, y);
								// repaint the chart such as the newly added
								// point to be visible
								mChartView.repaint();
							}
							if (displayDateStep != 0) {
								for (int i = 0; i < listTrades.size(); i++) {
									if (i % displayDateStep == 0) {
										countStep++;
										mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTrades.get(i).trading_time));
									} else {
										if (i == listTrades.size() - 1 && countStep < 4) {
											mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTrades.get(i).trading_time));
										} else {
											mRenderer.addXTextLabel(i, "");
										}
									}
								}
							} else {
								for (int i = 0; i < listTrades.size(); i++) {
									mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTrades.get(i).trading_time));
								}
							}
							mRenderer.setYAxisMin(minY - 0.1);
							mRenderer.setXLabels(0);
						}
					}
				}

				@Override
				public void onError(Exception ex) {
					ex.printStackTrace();
					hideProgressDialog();
				}
			});
		}else{
			loadChartLastDay(marketName);
		}
		
	}
	
	private void renderChart(ArrayList<StockLastDay> dataChart) {
		hideProgressDialog();
		if (dataChart != null && dataChart.size() > 0) {
			if (dataChart != null && dataChart.size() > 0) {
				//Collections.reverse(dataChart);
				double x = -1;
				double minY = Double.MAX_VALUE;
				for (StockLastDay trade : dataChart) {
					x++;
					double y = trade.last_price / 1000;
					if (minY > y) {
						minY = y;
					}
					// add a new data point to the current series
					mCurrentSeries.add(x, y);
					// repaint the chart such as the newly added
					// point to be visible
					mChartView.repaint();
				}
//				if (displayDateStep != 0) {
//					for (int i = 0; i < dataChart.size(); i++) {
//						if (i % displayDateStep == 0) {
//							countStep++;
//							mRenderer.addXTextLabel(i, Utils.convertStringtoDate(dataChart.get(i).trading_time));
//						} else {
//							if (i == dataChart.size() - 1 && countStep < 4) {
//								mRenderer.addXTextLabel(i, Utils.convertStringtoDate(dataChart.get(i).trading_time));
//							} else {
//								mRenderer.addXTextLabel(i, "");
//							}
//						}
//					}
//				} else {
//					for (int i = 0; i < dataChart.size(); i++) {
//						mRenderer.addXTextLabel(i, Utils.convertStringtoDate(dataChart.get(i).trading_time));
//					}
//				}
				mRenderer.setYAxisMin(minY - 0.1);
				mRenderer.setXLabels(0);
			}
		}
	}
	
	private void loadChartLastDay(String stockCode){
		restService.getStockLastDay(stockCode, new IStockResponseListener<ArrayList<StockLastDay>>() {
			
			@Override
			public void onResponse(ArrayList<StockLastDay> data) {
				if(data != null){
					mCurrentSeries.clear();
					mDataChartLastDay = data;
					mHandler.sendEmptyMessage(ONE_DAY);
				}else{
					hideProgressDialog();
				}
			}
			
			@Override
			public void onError(Exception ex) {
				hideProgressDialog();
				ex.printStackTrace();
			}
		});
	}

	private class OnStockChartFragmentClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {

			case R.id.market_tab_one_day:
				ctvADay.setChecked(true);
				ctvAMonth.setChecked(false);
				ctvSixMonths.setChecked(false);
				ctvAYear.setChecked(false);
				ctvFiveYears.setChecked(false);
				if (currentDate != ONE_DAY) {
					showProgressDialog();
					currentDate = ONE_DAY;
					loadChartLastDay(stockCode);
					//drawChartView(stockCode);
				}
				break;
			case R.id.market_tab_one_month:
				ctvADay.setChecked(false);
				ctvAMonth.setChecked(true);
				ctvSixMonths.setChecked(false);
				ctvAYear.setChecked(false);
				ctvFiveYears.setChecked(false);
				if (currentDate != ONE_MONTH) {
					showProgressDialog();
					currentDate = ONE_MONTH;
					drawChartView(stockCode);
				}
				break;
			case R.id.market_tab_six_months:
				ctvADay.setChecked(false);
				ctvAMonth.setChecked(false);
				ctvSixMonths.setChecked(true);
				ctvAYear.setChecked(false);
				ctvFiveYears.setChecked(false);
				if (currentDate != SIX_MONTHS) {
					showProgressDialog();
					currentDate = SIX_MONTHS;
					drawChartView(stockCode);
				}
				break;
			case R.id.market_tab_one_year:
				ctvADay.setChecked(false);
				ctvAMonth.setChecked(false);
				ctvSixMonths.setChecked(false);
				ctvAYear.setChecked(true);
				ctvFiveYears.setChecked(false);
				if (currentDate != ONE_YEAR) {
					showProgressDialog();
					currentDate = ONE_YEAR;
					drawChartView(stockCode);
				}
				break;
			case R.id.market_tab_five_years:
				ctvADay.setChecked(false);
				ctvAMonth.setChecked(false);
				ctvSixMonths.setChecked(false);
				ctvAYear.setChecked(false);
				ctvFiveYears.setChecked(true);
				if (currentDate != FIVE_YEARS) {
					showProgressDialog();
					currentDate = FIVE_YEARS;
					drawChartView(stockCode);
				}
				break;
			}
		}
	}
	
	private Handler mHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case ONE_DAY:
				renderChart(mDataChartLastDay);
				break;

			default:
				break;
			}
			return false;
		}
	});

}

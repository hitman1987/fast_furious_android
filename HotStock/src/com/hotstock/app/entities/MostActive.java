package com.hotstock.app.entities;

public class MostActive {
	public int id;
	public String code;
	public double volume;
    public double close_price;
    public String percentage_change;
    public String type;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public double getClose_price() {
		return close_price;
	}
	public void setClose_price(double close_price) {
		this.close_price = close_price;
	}
	public String getPercentage_change() {
		return percentage_change;
	}
	public void setPercentage_change(String percentage_change) {
		this.percentage_change = percentage_change;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}

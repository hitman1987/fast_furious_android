package com.hotstock.app.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hotstock.R;
import com.hotstock.app.AppController;
import com.hotstock.app.adapter.StockSnapshotListAdapter;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.Snapshots;
import com.hotstock.app.entities.StockSnapshots;
import com.hotstock.app.utils.Enums;

public class StocksSnapshotFragment extends Fragment {

	private IStockRestfulService restService = new IStockRestfulServiceImpl();

	private ListView lvSnapshot;
	private StockSnapshotListAdapter snapshotListAdapter;

	private String stockCode;

	private ProgressDialog pDialog;

	public StocksSnapshotFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_stocks_snapshot, container, false);

		stockCode = getArguments().getString(Enums.PREF_STOCK_CODE, null);
		if (stockCode == null) {
			stockCode = "VNM";
		}
		initView(rootView);
		loadData(stockCode);

		return rootView;
	}

	private void initView(View view) {
		lvSnapshot = (ListView) view.findViewById(R.id.fragment_stocks_snapshot_lv_main);
		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);
	}

	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}

	private void loadData(String stockCode) {
		showProgressDialog();
		restService.getStockSnapshots(stockCode, new IStockResponseListener<ArrayList<StockSnapshots>>() {

			@Override
			public void onResponse(ArrayList<StockSnapshots> data) {
				ArrayList<Snapshots> dtos = new ArrayList<Snapshots>();
				if (data.size() > 0) {
					dtos = data.get(0).Snapshots;
				}
				snapshotListAdapter = new StockSnapshotListAdapter(AppController.getAppContext(), dtos);
				lvSnapshot.setAdapter(snapshotListAdapter);
				hideProgressDialog();
			}

			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
				hideProgressDialog();
			}
		});
	}

}

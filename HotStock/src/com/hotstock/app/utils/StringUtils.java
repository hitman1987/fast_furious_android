package com.hotstock.app.utils;

public class StringUtils {
	public static final String EMPTY_STR = "";
	
	public static boolean isNullOrEmpty(String str) {
		return ((str == null) || (EMPTY_STR.equals(str)));
	}
	
    public static boolean isNotEmpty(String str) {
		return !isNullOrEmpty(str);
	}
}

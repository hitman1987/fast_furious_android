package com.hotstock.app.entities;

public class StockFinancial {
	 public double NSL;
     public double CURRENT_ASSET;
     public double GOP;
     public double NIAT;
     public double PBT;
     public double TOTAL_ASSET;
     
	public double getNSL() {
		return NSL;
	}
	public void setNSL(double nSL) {
		NSL = nSL;
	}
	public double getCURRENT_ASSET() {
		return CURRENT_ASSET;
	}
	public void setCURRENT_ASSET(double cURRENT_ASSET) {
		CURRENT_ASSET = cURRENT_ASSET;
	}
	public double getGOP() {
		return GOP;
	}
	public void setGOP(double gOP) {
		GOP = gOP;
	}
	public double getNIAT() {
		return NIAT;
	}
	public void setNIAT(double nIAT) {
		NIAT = nIAT;
	}
	public double getPBT() {
		return PBT;
	}
	public void setPBT(double pBT) {
		PBT = pBT;
	}
	public double getTOTAL_ASSET() {
		return TOTAL_ASSET;
	}
	public void setTOTAL_ASSET(double tOTAL_ASSET) {
		TOTAL_ASSET = tOTAL_ASSET;
	}
}

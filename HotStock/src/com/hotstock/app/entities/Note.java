package com.hotstock.app.entities;

public class Note {
	
	 public int id;
     public String note;
     
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}   
     
}

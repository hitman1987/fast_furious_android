package com.hotstock.app.entities;

public class ForeignTrades {
	 public int id;
	 public double total_purchases;
	 public double total_sales;
	 public double net_purchases;
	 public String created_time;
	 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getTotal_purchases() {
		return total_purchases;
	}
	public void setTotal_purchases(double total_purchases) {
		this.total_purchases = total_purchases;
	}
	public double getTotal_sales() {
		return total_sales;
	}
	public void setTotal_sales(double total_sales) {
		this.total_sales = total_sales;
	}
	public double getNet_purchases() {
		return net_purchases;
	}
	public void setNet_purchases(double net_purchases) {
		this.net_purchases = net_purchases;
	}
	public String getCreated_time() {
		return created_time;
	}
	public void setCreated_time(String created_time) {
		this.created_time = created_time;
	}
}

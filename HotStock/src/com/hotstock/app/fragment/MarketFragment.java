package com.hotstock.app.fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer.FillOutsideLine;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.android.volley.VolleyLog;
import com.hotstock.R;
import com.hotstock.app.AppController;
import com.hotstock.app.adapter.MarketEtfAdapter;
import com.hotstock.app.adapter.MarketTradingForeignTradingAdapter;
import com.hotstock.app.adapter.MarketTradingMostActiveAdapter;
import com.hotstock.app.adapter.TopNewsAdapter;
import com.hotstock.app.api.IStockResponseListener;
import com.hotstock.app.api.IStockRestfulService;
import com.hotstock.app.api.IStockRestfulServiceImpl;
import com.hotstock.app.entities.ETF;
import com.hotstock.app.entities.ForeignTrades;
import com.hotstock.app.entities.Indices;
import com.hotstock.app.entities.MarketChart;
import com.hotstock.app.entities.Markets;
import com.hotstock.app.entities.MostActive;
import com.hotstock.app.entities.News;
import com.hotstock.app.utils.IStockModel;
import com.hotstock.app.utils.Utils;
import com.hotstock.app.viewcontrol.FlipViewSupportList;
import com.hotstock.app.viewcontrol.VerticalListView;
import com.hotstock.app.views.NewDetailsActivity;

public class MarketFragment extends Fragment implements OnClickListener {

	private CheckedTextView ctvVnZone, ctvWorldZone, ctvEtfZone;
	private CheckedTextView ctvADay, ctvAMonth, ctvSixMonths, ctvAYear, ctvFiveYears;
	private CheckedTextView ctvTradingCodeLeft, ctvTradingCodeRight;
	private CheckedTextView ctvTradingIndexLeft, ctvTradingIndexRight;
	private LinearLayout lnDataLayout;
	private VerticalListView lnNewLayout;
	private FlipViewSupportList vpFlipChartNews;
	private ViewFlipper vpFlipMain;

	private ProgressDialog pDialog;

	private TopNewsAdapter mAdapterTopNews;

	private ListView lvListviewETF;
	private MarketEtfAdapter mMarketEtfAdapter;

	// Most trading list
	private ListView lvMostTradingForeignTrading, lvMostTradingMostActive, lvMostTradingNetBuySell;
	private MarketTradingForeignTradingAdapter mMarketTradingOverViewAdapter;
	private MarketTradingMostActiveAdapter mMarketTradingMostActiveAdapterSell;
	private MarketTradingMostActiveAdapter mMarketTradingMostActiveAdapterbuy;

	// market code layout
	private ViewGroup mCodeLayout, mIndexLayout;

	@SuppressWarnings("deprecation")
	private final GestureDetector detector = new GestureDetector(new SwipeGestureDetector());
	private IStockRestfulService restService = new IStockRestfulServiceImpl();

	private static final int GET_MARKET_SUCCESS = 0;
	private static final int GET_MARKET_FAIL = 1;
	private static final int GET_NEWS_SUCCESS = 2;
	private static final int GET_MARKET_ETF = 3;
	private static final int GET_MARKET_FOREIGN_TRADING = 4;
	private static final int GET_MARKET_MOST_ACTIVE_BUY = 5;
	private static final int GET_MARKET_MOST_ACTIVE_SELL = 6;
	private static final int GET_MARKET_CHART_DAILY = 7;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	private ArrayList<Markets> mDataMarkets = new ArrayList<Markets>();
	private ArrayList<News> mDataNews = new ArrayList<News>();
	private ArrayList<ETF> mDataETF = new ArrayList<ETF>();
	private ArrayList<ForeignTrades> mDataForeignTrading = new ArrayList<ForeignTrades>();
	private ArrayList<MostActive> mDataMostAciveBuy = new ArrayList<MostActive>();
	private ArrayList<MostActive> mDataMostAciveSell = new ArrayList<MostActive>();

	// //////////////////
	// For chart engine
	// /////////////////
	/** The main dataset that includes all the series that go into a chart. */
	private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();
	/** The main renderer that includes all the renderers customizing a chart. */
	private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
	/** The most recently added series. */
	private XYSeries mCurrentSeries;
	/** The most recently created renderer, customizing the current series. */
	private XYSeriesRenderer mCurrentRenderer;
	/** The chart view that displays the data. */
	private GraphicalView mChartView;

	public static final int ONE_DAY = 0xa1;
	public static final int ONE_MONTH = ONE_DAY + 1;
	public static final int SIX_MONTHS = ONE_MONTH + 1;
	public static final int ONE_YEAR = SIX_MONTHS + 1;
	public static final int FIVE_YEARS = ONE_YEAR + 1;

	private int currentDate = ONE_DAY;
	private String currentMarketName = "HNX";
	
	private final int LIMITLOAD = 50;
	private int currentNewOffset = 0;
	private boolean isLoadingNew = false;
	
	private final int LIMITLOAD_TRADING = 10;
	private int currentTradingOffset = 0;
	private boolean isLoadingTrading = false;
	
	private final int LIMITLOAD_MOSTACTIVE = 10;
	private int currentMostActiveOffset = 0;
	private boolean isLoadingMostActive = false;
	
	private final int LIMITLOAD_NETBUYSELL = 10;
	private int currentNetBuySellOffset = 0;
	private boolean isLoadingNetBuySell = false;
	
	private final int LIMITLOAD_DAILY = 1000;
	private int currentDailyChartOffset = 0;
	private ArrayList<MarketChart> dailyChartData = new ArrayList<MarketChart>();

	public MarketFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_market_main, container, false);
		ctvVnZone = (CheckedTextView) rootView.findViewById(R.id.market_trading_zone_left);
		ctvWorldZone = (CheckedTextView) rootView.findViewById(R.id.market_trading_zone_center);
		ctvEtfZone = (CheckedTextView) rootView.findViewById(R.id.market_trading_zone_right);
		ctvADay = (CheckedTextView) rootView.findViewById(R.id.market_tab_one_day);
		ctvAMonth = (CheckedTextView) rootView.findViewById(R.id.market_tab_one_month);
		ctvSixMonths = (CheckedTextView) rootView.findViewById(R.id.market_tab_six_months);
		ctvAYear = (CheckedTextView) rootView.findViewById(R.id.market_tab_one_year);
		ctvFiveYears = (CheckedTextView) rootView.findViewById(R.id.market_tab_five_years);
		ctvTradingCodeLeft = (CheckedTextView) rootView.findViewById(R.id.market_trading_market_left);
		ctvTradingCodeRight = (CheckedTextView) rootView.findViewById(R.id.market_trading_market_right);
		ctvTradingIndexLeft = (CheckedTextView) rootView.findViewById(R.id.market_trading_market_index);
		ctvTradingIndexRight = (CheckedTextView) rootView.findViewById(R.id.market_trading_market_summary);
		lnDataLayout = (LinearLayout) rootView.findViewById(R.id.market_data_layout);
		vpFlipChartNews = (FlipViewSupportList) rootView.findViewById(R.id.market_chart_news_flipper);
		vpFlipMain = (ViewFlipper) rootView.findViewById(R.id.market_main_swipe);
		lnNewLayout = (VerticalListView) rootView.findViewById(R.id.fragment_stocks_news_lv_main);
		lnNewLayout.setOnScrollListener(new OnScrollListener(){

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if (totalItemCount <= 0 || visibleItemCount <= 0) {
					return;
				}
				if (totalItemCount == firstVisibleItem + visibleItemCount && !isLoadingNew) {
					// 排他制御フラグをON
					isLoadingNew = true;
					loadDataMarketNews(true);
				}
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				
			}
			
		});

		pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);

		lvListviewETF = (ListView) rootView.findViewById(R.id.market_etf_list_view);

		mCodeLayout = (ViewGroup) rootView.findViewById(R.id.market_tab_trading_code);
		mIndexLayout = (ViewGroup) rootView.findViewById(R.id.market_trading_index_sum);

		lvMostTradingForeignTrading = (ListView) rootView.findViewById(R.id.fragment_market_trading_most_list_overview);
		lvMostTradingForeignTrading.setOnScrollListener(new OnScrollListener(){

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if (totalItemCount <= 0 || visibleItemCount <= 0) {
					return;
				}
				if (totalItemCount == firstVisibleItem + visibleItemCount && !isLoadingTrading) {
					// 排他制御フラグをON
					isLoadingTrading = true;
					loadDataSummaryTrading(true);
				}
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				
			}
			
		});
		lvMostTradingMostActive = (ListView) rootView.findViewById(R.id.fragment_market_trading_most_list_news);
		lvMostTradingMostActive.setOnScrollListener(new OnScrollListener(){

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if (totalItemCount <= 0 || visibleItemCount <= 0) {
					return;
				}
				if (totalItemCount == firstVisibleItem + visibleItemCount && !isLoadingMostActive) {
					// 排他制御フラグをON
					isLoadingMostActive = true;
					loadDataMostActive(true);
				}
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				
			}
			
		});
		lvMostTradingNetBuySell = (ListView) rootView.findViewById(R.id.fragment_market_trading_most_list_net_buy_sell);
		lvMostTradingNetBuySell.setOnScrollListener(new OnScrollListener(){

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if (totalItemCount <= 0 || visibleItemCount <= 0) {
					return;
				}
				if (totalItemCount == firstVisibleItem + visibleItemCount && !isLoadingNetBuySell) {
					// 排他制御フラグをON
					isLoadingNetBuySell = true;
					loadDataNetBuySell(true);
				}
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				
			}
			
		});

		setUpListener();
		if (savedInstanceState != null) {
			mDataset = (XYMultipleSeriesDataset) savedInstanceState.getSerializable("dataset");
			mRenderer = (XYMultipleSeriesRenderer) savedInstanceState.getSerializable("renderer");
			mCurrentSeries = (XYSeries) savedInstanceState.getSerializable("current_series");
			mCurrentRenderer = (XYSeriesRenderer) savedInstanceState.getSerializable("current_renderer");
		}

		initChart(rootView, "HOSE");
		return rootView;
	}

	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// save the current data, for instance when changing screen orientation
		outState.putSerializable("dataset", mDataset);
		outState.putSerializable("renderer", mRenderer);
		outState.putSerializable("current_series", mCurrentSeries);
		outState.putSerializable("current_renderer", mCurrentRenderer);
	}

	private void initChart(View v, String marketName) {
		// set some properties on the main renderer
		mRenderer.setApplyBackgroundColor(true);
		mRenderer.setBackgroundColor(Color.TRANSPARENT);
		mRenderer.setMarginsColor(AppController.getAppContext().getResources().getColor(android.R.color.transparent));
		mRenderer.setAxisTitleTextSize(16);
		mRenderer.setChartTitleTextSize(20);
		mRenderer.setLabelsTextSize(15);
		mRenderer.setLegendTextSize(15);
		mRenderer.setMargins(new int[] { 40, 40, 40, 40 });
		mRenderer.setZoomButtonsVisible(false);
		mRenderer.setPointSize(5);
		mRenderer.setShowGridX(true);
		mRenderer.setGridColor(Color.BLACK);
		mRenderer.setPanEnabled(false);
		mRenderer.setYLabelsAlign(Align.RIGHT);
		mRenderer.setPanEnabled(false, false);
		mRenderer.setZoomEnabled(false, false);

		if (mChartView == null) {
			LinearLayout layout = (LinearLayout) v.findViewById(R.id.market_chart);
			mChartView = ChartFactory.getLineChartView(AppController.getAppContext(), mDataset, mRenderer);
			mChartView.setBackgroundColor(Color.TRANSPARENT);
			// enable the chart click events
			// mRenderer.setClickEnabled(true);
			mRenderer.setSelectableBuffer(10);
			// mChartView.setOnClickListener(new View.OnClickListener() {
			// public void onClick(View v) {
			// // handle the click event on the chart
			// SeriesSelection seriesSelection =
			// mChartView.getCurrentSeriesAndPoint();
			// if (seriesSelection == null) {
			// Toast.makeText(AppController.getAppContext(), "No chart element",
			// Toast.LENGTH_SHORT).show();
			// } else {
			// // display information of the clicked point
			// Toast.makeText(
			// AppController.getAppContext(),
			// "Chart element in series index " +
			// seriesSelection.getSeriesIndex() + " data point index "
			// + seriesSelection.getPointIndex() + " was clicked" +
			// " closest point value X=" + seriesSelection.getXValue()
			// + ", Y=" + seriesSelection.getValue(),
			// Toast.LENGTH_SHORT).show();
			// }
			// }
			// });
			layout.addView(mChartView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		} else {
			mChartView.repaint();
		}

		String seriesTitle = "Series " + (mDataset.getSeriesCount() + 1);
		// create a new series of data
		XYSeries series = new XYSeries(seriesTitle);
		mDataset.addSeries(series);
		mCurrentSeries = series;
		// create a new renderer for the new series
		XYSeriesRenderer renderer = new XYSeriesRenderer();
		mRenderer.addSeriesRenderer(renderer);
		// set some renderer properties
		renderer.setPointStyle(PointStyle.POINT);
		renderer.setColor(AppController.getAppContext().getResources().getColor(R.color.chart_color));
		renderer.setFillPoints(false);
		renderer.setDisplayChartValues(false);
		renderer.setDisplayChartValuesDistance(10);
		renderer.setShowLegendItem(false);
		FillOutsideLine fill = new FillOutsideLine(FillOutsideLine.Type.BOUNDS_ABOVE);
		fill.setColor(AppController.getAppContext().getResources().getColor(R.color.chart_color));
		renderer.addFillOutsideLine(fill);
		fill = new FillOutsideLine(FillOutsideLine.Type.BOUNDS_BELOW);
		fill.setColor(AppController.getAppContext().getResources().getColor(R.color.chart_color));
		renderer.addFillOutsideLine(fill);
		mCurrentRenderer = renderer;
		mChartView.repaint();

		drawChartView(marketName);
	}

	private void drawChartView(final String marketName) {

		mCurrentSeries.clear();
		currentMarketName = marketName;
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		final String now = df.format(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		switch (currentDate) {
		case ONE_DAY:
			loadDataDailyMarket(false);
			return;
		case ONE_MONTH:
			cal.add(Calendar.MONTH, -1);
			break;
		case SIX_MONTHS:
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous3Months = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			String previous3Months1Day = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			String previous6Months = df.format(date);
			// System.out.println(">>> now: " + now);
			// System.out.println(">>> prev 3 months: " + previous3Months);
			// System.out.println(">>> prev 3 months and one day: " + previous3Months1Day);
			// System.out.println(">>> prev 6 months: " + previous6Months);
			final List<Indices> listTradesFinal = new ArrayList<Indices>();
			restService.getMarketChart(marketName, previous6Months, previous3Months1Day, new IStockResponseListener<ArrayList<Markets>>() {

				@Override
				public void onResponse(ArrayList<Markets> data) {
					if (data != null && data.size() > 0) {
						List<Indices> listTrades = data.get(0).Indices;
						if (listTrades != null && listTrades.size() > 0) {
							Collections.reverse(listTrades);
							for (Indices trade : listTrades) {
								listTradesFinal.add(trade);
							}
						}
					}

					restService.getMarketChart(marketName, previous3Months, now, new IStockResponseListener<ArrayList<Markets>>() {

						@Override
						public void onResponse(ArrayList<Markets> data) {
							hideProgressDialog();
							if (data != null && data.size() > 0) {
								List<Indices> listTrades = data.get(0).Indices;
								if (listTrades != null && listTrades.size() > 0) {
									Collections.reverse(listTrades);
									for (Indices trade : listTrades) {
										listTradesFinal.add(trade);
									}

									double x = -1;
									int displayDateStep = listTradesFinal.size() / 5;
									int countStep = 0;
									for (Indices trade : listTradesFinal) {
										x++;
										double y = trade.index;
										// add a new data point to the current series
										mCurrentSeries.add(x, y);
										// repaint the chart such as the newly added
										// point to be visible
										mChartView.repaint();
									}
									if (displayDateStep != 0) {
										for (int i = 0; i < listTradesFinal.size(); i++) {
											if (i % displayDateStep == 0) {
												countStep++;
												mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal.get(i).updated_time));
											} else {
												if (i == listTradesFinal.size() - 1 && countStep < 6) {
													mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal.get(i).updated_time));
												} else {
													mRenderer.addXTextLabel(i, "");
												}
											}
										}
									} else {
										for (int i = 0; i < listTradesFinal.size(); i++) {
											mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal.get(i).updated_time));
										}
									}
									mRenderer.setXLabels(0);
								}
							}
						}

						@Override
						public void onError(Exception ex) {
							ex.printStackTrace();
							hideProgressDialog();
						}
					});
				}

				@Override
				public void onError(Exception ex) {
					ex.printStackTrace();
					hideProgressDialog();
				}
			});
			return;
		case ONE_YEAR:
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous3Months1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous3Months1Day1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous6Months1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous6Months1Day1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous9Months1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous9Months1Day1 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous12Months1 = df.format(date);
			System.out.println(">>> now: " + now);
			System.out.println(">>> prev 3 months: " + previous3Months1);
			System.out.println(">>> prev 3 months and one day: " + previous3Months1Day1);
			System.out.println(">>> prev 6 months: " + previous6Months1);
			System.out.println(">>> prev 6 months and one day: " + previous6Months1Day1);
			System.out.println(">>> prev 9 months: " + previous9Months1);
			System.out.println(">>> prev 9 months and one day: " + previous9Months1Day1);
			System.out.println(">>> prev 12 months: " + previous12Months1);
			final List<Indices> listTradesFinal1 = new ArrayList<Indices>();
			restService.getMarketChart(marketName, previous12Months1, previous9Months1Day1,
					new IStockResponseListener<ArrayList<Markets>>() {

						@Override
						public void onResponse(ArrayList<Markets> data) {
							if (data != null && data.size() > 0) {
								List<Indices> listTrades = data.get(0).Indices;
								if (listTrades != null && listTrades.size() > 0) {
									Collections.reverse(listTrades);
									for (Indices trade : listTrades) {
										// System.out.println(">>>>>>> " + trade.trading_time);
										listTradesFinal1.add(trade);
									}
								}
							}

							restService.getMarketChart(marketName, previous9Months1, previous6Months1Day1,
									new IStockResponseListener<ArrayList<Markets>>() {

										@Override
										public void onResponse(ArrayList<Markets> data) {
											if (data != null && data.size() > 0) {
												List<Indices> listTrades = data.get(0).Indices;
												if (listTrades != null && listTrades.size() > 0) {
													Collections.reverse(listTrades);
													for (Indices trade : listTrades) {
														// System.out.println(">>>>>>>> " + trade.trading_time);
														listTradesFinal1.add(trade);
													}
												}
											}

											restService.getMarketChart(marketName, previous6Months1, previous3Months1Day1,
													new IStockResponseListener<ArrayList<Markets>>() {

														@Override
														public void onResponse(ArrayList<Markets> data) {
															if (data != null && data.size() > 0) {
																List<Indices> listTrades = data.get(0).Indices;
																if (listTrades != null && listTrades.size() > 0) {
																	Collections.reverse(listTrades);
																	for (Indices trade : listTrades) {
																		// System.out.println(">>>>>>>> " + trade.trading_time);
																		listTradesFinal1.add(trade);
																	}
																}
															}

															restService.getMarketChart(marketName, previous3Months1, now,
																	new IStockResponseListener<ArrayList<Markets>>() {

																		@Override
																		public void onResponse(ArrayList<Markets> data) {
																			hideProgressDialog();
																			if (data != null && data.size() > 0) {
																				List<Indices> listTrades = data.get(0).Indices;
																				if (listTrades != null && listTrades.size() > 0) {
																					Collections.reverse(listTrades);
																					for (Indices trade : listTrades) {
																						// System.out.println(">>>>>>>> " + trade.trading_time);
																						listTradesFinal1.add(trade);
																					}
																				}

																				double x = -1;
																				int displayDateStep = listTradesFinal1.size() / 5;
																				int countStep = 0;
																				for (Indices trade : listTradesFinal1) {
																					x++;
																					double y = trade.index;
																					// add a new data point to the current series
																					mCurrentSeries.add(x, y);
																					// repaint the chart such as the newly added
																					// point to be visible
																					mChartView.repaint();
																				}
																				if (displayDateStep != 0) {
																					for (int i = 0; i < listTradesFinal1.size(); i++) {
																						if (i % displayDateStep == 0) {
																							countStep++;
																							mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal1.get(i).updated_time));
																						} else {
																							if (i == listTradesFinal1.size() - 1 && countStep < 6) {
																								mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal1.get(i).updated_time));
																							} else {
																								mRenderer.addXTextLabel(i, "");
																							}
																						}
																					}
																				} else {
																					for (int i = 0; i < listTradesFinal1.size(); i++) {
																						mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal1.get(i).updated_time));
																					}
																				}
																				mRenderer.setXLabels(0);
																			}
																		}

																		@Override
																		public void onError(Exception ex) {
																			ex.printStackTrace();
																			hideProgressDialog();
																		}
																	});
														}

														@Override
														public void onError(Exception ex) {
															ex.printStackTrace();
															hideProgressDialog();
														}
													});
										}

										@Override
										public void onError(Exception ex) {
											ex.printStackTrace();
											hideProgressDialog();
										}
									});
						}

						@Override
						public void onError(Exception ex) {
							ex.printStackTrace();
							hideProgressDialog();
						}
					});
			return;
		case FIVE_YEARS:
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous3Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous3Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous6Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous6Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous9Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous9Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous12Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous12Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous15Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous15Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous18Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous18Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous21Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous21Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous24Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous24Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous27Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous27Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous30Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous30Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous33Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous33Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous36Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous36Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous39Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous39Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous42Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous42Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous45Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous45Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous48Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous48Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous51Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous51Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous54Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous54Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous57Months2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			final String previous57Months1Day2 = df.format(date);
			cal.setTime(date);
			cal.add(Calendar.MONTH, -3);
			date = cal.getTime();
			final String previous60Months2 = df.format(date);
			System.out.println(">>> now: " + now);
			System.out.println(">>> prev 3 months: " + previous3Months2);
			System.out.println(">>> prev 3 months and one day: " + previous3Months1Day2);
			System.out.println(">>> prev 6 months: " + previous6Months2);
			System.out.println(">>> prev 6 months and one day: " + previous6Months1Day2);
			System.out.println(">>> prev 9 months: " + previous9Months2);
			System.out.println(">>> prev 9 months and one day: " + previous9Months1Day2);
			System.out.println(">>> prev 12 months: " + previous12Months2);
			System.out.println(">>> prev 12 months and one day: " + previous12Months1Day2);
			System.out.println(">>> prev 15 months: " + previous15Months2);
			System.out.println(">>> prev 15 months and one day: " + previous15Months1Day2);
			System.out.println(">>> prev 18 months: " + previous18Months2);
			System.out.println(">>> prev 18 months and one day: " + previous18Months1Day2);
			System.out.println(">>> prev 21 months: " + previous21Months2);
			System.out.println(">>> prev 21 months and one day: " + previous21Months1Day2);
			System.out.println(">>> prev 24 months: " + previous24Months2);
			System.out.println(">>> prev 24 months and one day: " + previous24Months1Day2);
			System.out.println(">>> prev 27 months: " + previous27Months2);
			System.out.println(">>> prev 27 months and one day: " + previous27Months1Day2);
			System.out.println(">>> prev 30 months: " + previous30Months2);
			System.out.println(">>> prev 30 months and one day: " + previous30Months1Day2);
			System.out.println(">>> prev 33 months: " + previous33Months2);
			System.out.println(">>> prev 33 months and one day: " + previous33Months1Day2);
			System.out.println(">>> prev 36 months: " + previous36Months2);
			System.out.println(">>> prev 36 months and one day: " + previous36Months1Day2);
			System.out.println(">>> prev 39 months: " + previous39Months2);
			System.out.println(">>> prev 39 months and one day: " + previous39Months1Day2);
			System.out.println(">>> prev 42 months: " + previous42Months2);
			System.out.println(">>> prev 42 months and one day: " + previous42Months1Day2);
			System.out.println(">>> prev 45 months: " + previous45Months2);
			System.out.println(">>> prev 45 months and one day: " + previous45Months1Day2);
			System.out.println(">>> prev 48 months: " + previous48Months2);
			System.out.println(">>> prev 48 months and one day: " + previous48Months1Day2);
			System.out.println(">>> prev 51 months: " + previous51Months2);
			System.out.println(">>> prev 51 months and one day: " + previous51Months1Day2);
			System.out.println(">>> prev 54 months: " + previous54Months2);
			System.out.println(">>> prev 54 months and one day: " + previous54Months1Day2);
			System.out.println(">>> prev 57 months: " + previous57Months2);
			System.out.println(">>> prev 57 months and one day: " + previous57Months1Day2);
			System.out.println(">>> prev 60 months: " + previous60Months2);
			final List<Indices> listTradesFinal2 = new ArrayList<Indices>();
			restService.getMarketChart(marketName, previous60Months2, previous57Months1Day2,
					new IStockResponseListener<ArrayList<Markets>>() {

						@Override
						public void onResponse(ArrayList<Markets> data) {
							if (data != null && data.size() > 0) {
								List<Indices> listTrades = data.get(0).Indices;
								if (listTrades != null && listTrades.size() > 0) {
									Collections.reverse(listTrades);
									for (Indices trade : listTrades) {
										listTradesFinal2.add(trade);
									}
								}
							}

							restService.getMarketChart(marketName, previous57Months2, previous54Months1Day2,
									new IStockResponseListener<ArrayList<Markets>>() {

										@Override
										public void onResponse(ArrayList<Markets> data) {
											if (data != null && data.size() > 0) {
												List<Indices> listTrades = data.get(0).Indices;
												if (listTrades != null && listTrades.size() > 0) {
													Collections.reverse(listTrades);
													for (Indices trade : listTrades) {
														listTradesFinal2.add(trade);
													}
												}
											}

											restService.getMarketChart(marketName, previous54Months2, previous51Months1Day2,
													new IStockResponseListener<ArrayList<Markets>>() {

														@Override
														public void onResponse(ArrayList<Markets> data) {
															if (data != null && data.size() > 0) {
																List<Indices> listTrades = data.get(0).Indices;
																if (listTrades != null && listTrades.size() > 0) {
																	Collections.reverse(listTrades);
																	for (Indices trade : listTrades) {
																		listTradesFinal2.add(trade);
																	}
																}
															}

															restService.getMarketChart(marketName, previous51Months2, previous48Months1Day2,
																	new IStockResponseListener<ArrayList<Markets>>() {

																		@Override
																		public void onResponse(ArrayList<Markets> data) {
																			if (data != null && data.size() > 0) {
																				List<Indices> listTrades = data.get(0).Indices;
																				if (listTrades != null && listTrades.size() > 0) {
																					Collections.reverse(listTrades);
																					for (Indices trade : listTrades) {
																						listTradesFinal2.add(trade);
																					}
																				}
																			}

																			restService.getMarketChart(marketName, previous48Months2,
																					previous45Months1Day2,
																					new IStockResponseListener<ArrayList<Markets>>() {

																						@Override
																						public void onResponse(ArrayList<Markets> data) {
																							if (data != null && data.size() > 0) {
																								List<Indices> listTrades = data.get(0).Indices;
																								if (listTrades != null && listTrades.size() > 0) {
																									Collections.reverse(listTrades);
																									for (Indices trade : listTrades) {
																										listTradesFinal2.add(trade);
																									}
																								}
																							}

																							restService
																									.getMarketChart(
																											marketName,
																											previous45Months2,
																											previous42Months1Day2,
																											new IStockResponseListener<ArrayList<Markets>>() {

																												@Override
																												public void onResponse(
																														ArrayList<Markets> data) {
																													if (data != null
																															&& data.size() > 0) {
																														List<Indices> listTrades = data
																																.get(0).Indices;
																														if (listTrades != null
																																&& listTrades.size() > 0) {
																															Collections
																																	.reverse(listTrades);
																															for (Indices trade : listTrades) {
																																listTradesFinal2
																																		.add(trade);
																															}
																														}
																													}

																													restService
																															.getMarketChart(
																																	marketName,
																																	previous42Months2,
																																	previous39Months1Day2,
																																	new IStockResponseListener<ArrayList<Markets>>() {

																																		@Override
																																		public void onResponse(
																																				ArrayList<Markets> data) {
																																			if (data != null
																																					&& data.size() > 0) {
																																				List<Indices> listTrades = data
																																						.get(0).Indices;
																																				if (listTrades != null
																																						&& listTrades
																																								.size() > 0) {
																																					Collections
																																							.reverse(listTrades);
																																					for (Indices trade : listTrades) {
																																						listTradesFinal2
																																								.add(trade);
																																					}
																																				}
																																			}

																																			restService
																																					.getMarketChart(
																																							marketName,
																																							previous39Months2,
																																							previous36Months1Day2,
																																							new IStockResponseListener<ArrayList<Markets>>() {

																																								@Override
																																								public void onResponse(
																																										ArrayList<Markets> data) {
																																									if (data != null
																																											&& data.size() > 0) {
																																										List<Indices> listTrades = data
																																												.get(0).Indices;
																																										if (listTrades != null
																																												&& listTrades
																																														.size() > 0) {
																																											Collections
																																													.reverse(listTrades);
																																											for (Indices trade : listTrades) {
																																												listTradesFinal2
																																														.add(trade);
																																											}
																																										}
																																									}

																																									restService
																																											.getMarketChart(
																																													marketName,
																																													previous36Months2,
																																													previous33Months1Day2,
																																													new IStockResponseListener<ArrayList<Markets>>() {

																																														@Override
																																														public void onResponse(
																																																ArrayList<Markets> data) {
																																															if (data != null
																																																	&& data.size() > 0) {
																																																List<Indices> listTrades = data
																																																		.get(0).Indices;
																																																if (listTrades != null
																																																		&& listTrades
																																																				.size() > 0) {
																																																	Collections
																																																			.reverse(listTrades);
																																																	for (Indices trade : listTrades) {
																																																		listTradesFinal2
																																																				.add(trade);
																																																	}
																																																}
																																															}

																																															restService
																																																	.getMarketChart(
																																																			marketName,
																																																			previous33Months2,
																																																			previous30Months1Day2,
																																																			new IStockResponseListener<ArrayList<Markets>>() {

																																																				@Override
																																																				public void onResponse(
																																																						ArrayList<Markets> data) {
																																																					if (data != null
																																																							&& data.size() > 0) {
																																																						List<Indices> listTrades = data
																																																								.get(0).Indices;
																																																						if (listTrades != null
																																																								&& listTrades
																																																										.size() > 0) {
																																																							Collections
																																																									.reverse(listTrades);
																																																							for (Indices trade : listTrades) {
																																																								listTradesFinal2
																																																										.add(trade);
																																																							}
																																																						}
																																																					}

																																																					restService
																																																							.getMarketChart(
																																																									marketName,
																																																									previous30Months2,
																																																									previous27Months1Day2,
																																																									new IStockResponseListener<ArrayList<Markets>>() {

																																																										@Override
																																																										public void onResponse(
																																																												ArrayList<Markets> data) {
																																																											if (data != null
																																																													&& data.size() > 0) {
																																																												List<Indices> listTrades = data
																																																														.get(0).Indices;
																																																												if (listTrades != null
																																																														&& listTrades
																																																																.size() > 0) {
																																																													Collections
																																																															.reverse(listTrades);
																																																													for (Indices trade : listTrades) {
																																																														listTradesFinal2
																																																																.add(trade);
																																																													}
																																																												}
																																																											}

																																																											restService
																																																													.getMarketChart(
																																																															marketName,
																																																															previous27Months2,
																																																															previous24Months1Day2,
																																																															new IStockResponseListener<ArrayList<Markets>>() {

																																																																@Override
																																																																public void onResponse(
																																																																		ArrayList<Markets> data) {
																																																																	if (data != null
																																																																			&& data.size() > 0) {
																																																																		List<Indices> listTrades = data
																																																																				.get(0).Indices;
																																																																		if (listTrades != null
																																																																				&& listTrades
																																																																						.size() > 0) {
																																																																			Collections
																																																																					.reverse(listTrades);
																																																																			for (Indices trade : listTrades) {
																																																																				listTradesFinal2
																																																																						.add(trade);
																																																																			}
																																																																		}
																																																																	}

																																																																	restService
																																																																			.getMarketChart(
																																																																					marketName,
																																																																					previous24Months2,
																																																																					previous21Months1Day2,
																																																																					new IStockResponseListener<ArrayList<Markets>>() {

																																																																						@Override
																																																																						public void onResponse(
																																																																								ArrayList<Markets> data) {
																																																																							if (data != null
																																																																									&& data.size() > 0) {
																																																																								List<Indices> listTrades = data
																																																																										.get(0).Indices;
																																																																								if (listTrades != null
																																																																										&& listTrades
																																																																												.size() > 0) {
																																																																									Collections
																																																																											.reverse(listTrades);
																																																																									for (Indices trade : listTrades) {
																																																																										listTradesFinal2
																																																																												.add(trade);
																																																																									}
																																																																								}
																																																																							}

																																																																							restService
																																																																									.getMarketChart(
																																																																											marketName,
																																																																											previous21Months2,
																																																																											previous18Months1Day2,
																																																																											new IStockResponseListener<ArrayList<Markets>>() {

																																																																												@Override
																																																																												public void onResponse(
																																																																														ArrayList<Markets> data) {
																																																																													if (data != null
																																																																															&& data.size() > 0) {
																																																																														List<Indices> listTrades = data
																																																																																.get(0).Indices;
																																																																														if (listTrades != null
																																																																																&& listTrades
																																																																																		.size() > 0) {
																																																																															Collections
																																																																																	.reverse(listTrades);
																																																																															for (Indices trade : listTrades) {
																																																																																listTradesFinal2
																																																																																		.add(trade);
																																																																															}
																																																																														}
																																																																													}

																																																																													restService
																																																																															.getMarketChart(
																																																																																	marketName,
																																																																																	previous18Months2,
																																																																																	previous15Months1Day2,
																																																																																	new IStockResponseListener<ArrayList<Markets>>() {

																																																																																		@Override
																																																																																		public void onResponse(
																																																																																				ArrayList<Markets> data) {
																																																																																			if (data != null
																																																																																					&& data.size() > 0) {
																																																																																				List<Indices> listTrades = data
																																																																																						.get(0).Indices;
																																																																																				if (listTrades != null
																																																																																						&& listTrades
																																																																																								.size() > 0) {
																																																																																					Collections
																																																																																							.reverse(listTrades);
																																																																																					for (Indices trade : listTrades) {
																																																																																						listTradesFinal2
																																																																																								.add(trade);
																																																																																					}
																																																																																				}
																																																																																			}

																																																																																			restService
																																																																																					.getMarketChart(
																																																																																							marketName,
																																																																																							previous15Months2,
																																																																																							previous12Months1Day2,
																																																																																							new IStockResponseListener<ArrayList<Markets>>() {

																																																																																								@Override
																																																																																								public void onResponse(
																																																																																										ArrayList<Markets> data) {
																																																																																									if (data != null
																																																																																											&& data.size() > 0) {
																																																																																										List<Indices> listTrades = data
																																																																																												.get(0).Indices;
																																																																																										if (listTrades != null
																																																																																												&& listTrades
																																																																																														.size() > 0) {
																																																																																											Collections
																																																																																													.reverse(listTrades);
																																																																																											for (Indices trade : listTrades) {
																																																																																												listTradesFinal2
																																																																																														.add(trade);
																																																																																											}
																																																																																										}
																																																																																									}

																																																																																									restService
																																																																																											.getMarketChart(
																																																																																													marketName,
																																																																																													previous12Months2,
																																																																																													previous9Months1Day2,
																																																																																													new IStockResponseListener<ArrayList<Markets>>() {

																																																																																														@Override
																																																																																														public void onResponse(
																																																																																																ArrayList<Markets> data) {
																																																																																															if (data != null
																																																																																																	&& data.size() > 0) {
																																																																																																List<Indices> listTrades = data
																																																																																																		.get(0).Indices;
																																																																																																if (listTrades != null
																																																																																																		&& listTrades
																																																																																																				.size() > 0) {
																																																																																																	Collections
																																																																																																			.reverse(listTrades);
																																																																																																	for (Indices trade : listTrades) {
																																																																																																		listTradesFinal2
																																																																																																				.add(trade);
																																																																																																	}
																																																																																																}
																																																																																															}

																																																																																															restService
																																																																																																	.getMarketChart(
																																																																																																			marketName,
																																																																																																			previous9Months2,
																																																																																																			previous6Months1Day2,
																																																																																																			new IStockResponseListener<ArrayList<Markets>>() {

																																																																																																				@Override
																																																																																																				public void onResponse(
																																																																																																						ArrayList<Markets> data) {
																																																																																																					if (data != null
																																																																																																							&& data.size() > 0) {
																																																																																																						List<Indices> listTrades = data
																																																																																																								.get(0).Indices;
																																																																																																						if (listTrades != null
																																																																																																								&& listTrades
																																																																																																										.size() > 0) {
																																																																																																							Collections
																																																																																																									.reverse(listTrades);
																																																																																																							for (Indices trade : listTrades) {
																																																																																																								listTradesFinal2
																																																																																																										.add(trade);
																																																																																																							}
																																																																																																						}
																																																																																																					}

																																																																																																					restService
																																																																																																							.getMarketChart(
																																																																																																									marketName,
																																																																																																									previous6Months2,
																																																																																																									previous3Months1Day2,
																																																																																																									new IStockResponseListener<ArrayList<Markets>>() {

																																																																																																										@Override
																																																																																																										public void onResponse(
																																																																																																												ArrayList<Markets> data) {
																																																																																																											if (data != null
																																																																																																													&& data.size() > 0) {
																																																																																																												List<Indices> listTrades = data
																																																																																																														.get(0).Indices;
																																																																																																												if (listTrades != null
																																																																																																														&& listTrades
																																																																																																																.size() > 0) {
																																																																																																													Collections
																																																																																																															.reverse(listTrades);
																																																																																																													for (Indices trade : listTrades) {
																																																																																																														listTradesFinal2
																																																																																																																.add(trade);
																																																																																																													}
																																																																																																												}
																																																																																																											}

																																																																																																											restService
																																																																																																													.getMarketChart(
																																																																																																															marketName,
																																																																																																															previous3Months2,
																																																																																																															now,
																																																																																																															new IStockResponseListener<ArrayList<Markets>>() {

																																																																																																																@Override
																																																																																																																public void onResponse(
																																																																																																																		ArrayList<Markets> data) {
																																																																																																																	hideProgressDialog();
																																																																																																																	if (data != null
																																																																																																																			&& data.size() > 0) {
																																																																																																																		List<Indices> listTrades = data
																																																																																																																				.get(0).Indices;
																																																																																																																		if (listTrades != null
																																																																																																																				&& listTrades
																																																																																																																						.size() > 0) {
																																																																																																																			Collections
																																																																																																																					.reverse(listTrades);
																																																																																																																			for (Indices trade : listTrades) {
																																																																																																																				listTradesFinal2
																																																																																																																						.add(trade);
																																																																																																																			}
																																																																																																																		}

																																																																																																																		double x = -1;
																																																																																																																		int displayDateStep = listTradesFinal2.size() / 5;
																																																																																																																		int countStep = 0;
																																																																																																																		for (Indices trade : listTradesFinal2) {
																																																																																																																			x++;
																																																																																																																			double y = trade.index;
																																																																																																																			// add a new data point to the current series
																																																																																																																			mCurrentSeries.add(x, y);
																																																																																																																			// repaint the chart such as the newly added
																																																																																																																			// point to be visible
																																																																																																																			mChartView.repaint();
																																																																																																																		}
																																																																																																																		if (displayDateStep != 0) {
																																																																																																																			for (int i = 0; i < listTradesFinal2.size(); i++) {
																																																																																																																				if (i % displayDateStep == 0) {
																																																																																																																					countStep++;
																																																																																																																					mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal2.get(i).updated_time));
																																																																																																																				} else {
																																																																																																																					if (i == listTradesFinal2.size() - 1 && countStep < 6) {
																																																																																																																						mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal2.get(i).updated_time));
																																																																																																																					} else {
																																																																																																																						mRenderer.addXTextLabel(i, "");
																																																																																																																					}
																																																																																																																				}
																																																																																																																			}
																																																																																																																		} else {
																																																																																																																			for (int i = 0; i < listTradesFinal2.size(); i++) {
																																																																																																																				mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTradesFinal2.get(i).updated_time));
																																																																																																																			}
																																																																																																																		}
																																																																																																																		mRenderer
																																																																																																																				.setXLabels(0);
																																																																																																																	}
																																																																																																																}

																																																																																																																@Override
																																																																																																																public void onError(
																																																																																																																		Exception ex) {
																																																																																																																	ex.printStackTrace();
																																																																																																																	hideProgressDialog();
																																																																																																																}
																																																																																																															});
																																																																																																										}

																																																																																																										@Override
																																																																																																										public void onError(
																																																																																																												Exception ex) {
																																																																																																											ex.printStackTrace();
																																																																																																											hideProgressDialog();
																																																																																																										}
																																																																																																									});
																																																																																																				}

																																																																																																				@Override
																																																																																																				public void onError(
																																																																																																						Exception ex) {
																																																																																																					ex.printStackTrace();
																																																																																																					hideProgressDialog();
																																																																																																				}
																																																																																																			});
																																																																																														}

																																																																																														@Override
																																																																																														public void onError(
																																																																																																Exception ex) {
																																																																																															ex.printStackTrace();
																																																																																															hideProgressDialog();
																																																																																														}
																																																																																													});
																																																																																								}

																																																																																								@Override
																																																																																								public void onError(
																																																																																										Exception ex) {
																																																																																									ex.printStackTrace();
																																																																																									hideProgressDialog();
																																																																																								}
																																																																																							});
																																																																																		}

																																																																																		@Override
																																																																																		public void onError(
																																																																																				Exception ex) {
																																																																																			ex.printStackTrace();
																																																																																			hideProgressDialog();
																																																																																		}
																																																																																	});
																																																																												}

																																																																												@Override
																																																																												public void onError(
																																																																														Exception ex) {
																																																																													ex.printStackTrace();
																																																																													hideProgressDialog();
																																																																												}
																																																																											});
																																																																						}

																																																																						@Override
																																																																						public void onError(
																																																																								Exception ex) {
																																																																							ex.printStackTrace();
																																																																							hideProgressDialog();
																																																																						}
																																																																					});
																																																																}

																																																																@Override
																																																																public void onError(
																																																																		Exception ex) {
																																																																	ex.printStackTrace();
																																																																	hideProgressDialog();
																																																																}
																																																															});
																																																										}

																																																										@Override
																																																										public void onError(
																																																												Exception ex) {
																																																											ex.printStackTrace();
																																																											hideProgressDialog();
																																																										}
																																																									});
																																																				}

																																																				@Override
																																																				public void onError(
																																																						Exception ex) {
																																																					ex.printStackTrace();
																																																					hideProgressDialog();
																																																				}
																																																			});
																																														}

																																														@Override
																																														public void onError(
																																																Exception ex) {
																																															ex.printStackTrace();
																																															hideProgressDialog();
																																														}
																																													});
																																								}

																																								@Override
																																								public void onError(
																																										Exception ex) {
																																									ex.printStackTrace();
																																									hideProgressDialog();
																																								}
																																							});
																																		}

																																		@Override
																																		public void onError(
																																				Exception ex) {
																																			ex.printStackTrace();
																																			hideProgressDialog();
																																		}
																																	});
																												}

																												@Override
																												public void onError(Exception ex) {
																													ex.printStackTrace();
																													hideProgressDialog();
																												}
																											});
																						}

																						@Override
																						public void onError(Exception ex) {
																							ex.printStackTrace();
																							hideProgressDialog();
																						}
																					});
																		}

																		@Override
																		public void onError(Exception ex) {
																			ex.printStackTrace();
																			hideProgressDialog();
																		}
																	});
														}

														@Override
														public void onError(Exception ex) {
															ex.printStackTrace();
															hideProgressDialog();
														}
													});
										}

										@Override
										public void onError(Exception ex) {
											ex.printStackTrace();
											hideProgressDialog();
										}
									});
						}

						@Override
						public void onError(Exception ex) {
							ex.printStackTrace();
							hideProgressDialog();
						}
					});
			return;
		}

		date = cal.getTime();
		String previous = df.format(date);

		restService.getMarketChart(marketName, previous, now, new IStockResponseListener<ArrayList<Markets>>() {

			@Override
			public void onResponse(ArrayList<Markets> data) {
				hideProgressDialog();
				if (data != null && data.size() > 0) {
					List<Indices> listTrades = data.get(0).Indices;
					if (listTrades != null && listTrades.size() > 0) {
						Collections.reverse(listTrades);
						double x = -1;
						int displayDateStep = listTrades.size() / 3;
						int countStep = 0;
						for (Indices trade : listTrades) {
							x++;
							double y = trade.index;
							// add a new data point to the current series
							mCurrentSeries.add(x, y);
							// repaint the chart such as the newly added
							// point to be visible
							mChartView.repaint();
						}
						if (displayDateStep != 0) {
							for (int i = 0; i < listTrades.size(); i++) {
								if (i % displayDateStep == 0) {
									countStep++;
									mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTrades.get(i).updated_time));
								} else {
									if (i == listTrades.size() - 1 && countStep < 4) {
										mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTrades.get(i).updated_time));
									} else {
										mRenderer.addXTextLabel(i, "");
									}
								}
							}
						} else {
							for (int i = 0; i < listTrades.size(); i++) {
								mRenderer.addXTextLabel(i, Utils.convertStringtoDate(listTrades.get(i).updated_time));
							}
						}
						mRenderer.setXLabels(0);
					}
				}
			}

			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
				hideProgressDialog();
			}
		});
	}

	public void setUpListener() {
		ctvVnZone.setOnClickListener(this);
		ctvWorldZone.setOnClickListener(this);
		ctvEtfZone.setOnClickListener(this);
		ctvADay.setOnClickListener(this);
		ctvAMonth.setOnClickListener(this);
		ctvSixMonths.setOnClickListener(this);
		ctvAYear.setOnClickListener(this);
		ctvFiveYears.setOnClickListener(this);
		ctvTradingCodeLeft.setOnClickListener(this);
		ctvTradingCodeRight.setOnClickListener(this);
		ctvTradingIndexLeft.setOnClickListener(this);
		ctvTradingIndexRight.setOnClickListener(this);
		vpFlipChartNews.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(final View view, final MotionEvent event) {
				detector.onTouchEvent(event);
				return true;
			}
		});
		vpFlipChartNews.setUpGestureDetector(detector);
		lnNewLayout.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intentMainScreen = new Intent(AppController.getAppContext(), NewDetailsActivity.class);
				intentMainScreen.putExtra("idNews", mDataNews.get(position).getId());
				startActivity(intentMainScreen);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.market_trading_zone_left:
			ctvVnZone.setChecked(true);
			ctvWorldZone.setChecked(false);
			ctvEtfZone.setChecked(false);
			ctvTradingIndexLeft.setChecked(true);
			ctvTradingCodeLeft.setChecked(true);

			ctvTradingIndexLeft.setClickable(true);
			ctvTradingIndexRight.setClickable(true);
			ctvTradingCodeLeft.setClickable(true);
			ctvTradingCodeRight.setClickable(true);

			mCodeLayout.setVisibility(View.VISIBLE);
			mIndexLayout.setVisibility(View.VISIBLE);

			vpFlipMain.setDisplayedChild(vpFlipMain.indexOfChild(vpFlipMain.findViewById(R.id.fragment_market_main_vn)));
			break;
		case R.id.market_trading_zone_center:
			ctvVnZone.setChecked(false);
			ctvWorldZone.setChecked(true);
			ctvEtfZone.setChecked(false);
			break;
		case R.id.market_trading_zone_right:
			ctvVnZone.setChecked(false);
			ctvWorldZone.setChecked(false);
			ctvEtfZone.setChecked(true);

			ctvTradingCodeLeft.setChecked(false);
			ctvTradingCodeRight.setChecked(false);
			ctvTradingIndexLeft.setChecked(false);
			ctvTradingIndexRight.setChecked(false);

			ctvTradingIndexLeft.setClickable(false);
			ctvTradingIndexRight.setClickable(false);
			ctvTradingCodeLeft.setClickable(false);
			ctvTradingCodeRight.setClickable(false);

			mCodeLayout.setVisibility(View.GONE);
			mIndexLayout.setVisibility(View.GONE);

			vpFlipMain.setDisplayedChild(vpFlipMain.indexOfChild(vpFlipMain.findViewById(R.id.fragment_market_etf)));
			loadDataETF();
			break;
		case R.id.market_tab_one_day:
			ctvADay.setChecked(true);
			ctvAMonth.setChecked(false);
			ctvSixMonths.setChecked(false);
			ctvAYear.setChecked(false);
			ctvFiveYears.setChecked(false);
			if (currentDate != ONE_DAY) {
				showProgressDialog();
				currentDate = ONE_DAY;
				this.drawChartView(currentMarketName);
			}
			break;
		case R.id.market_tab_one_month:
			ctvADay.setChecked(false);
			ctvAMonth.setChecked(true);
			ctvSixMonths.setChecked(false);
			ctvAYear.setChecked(false);
			ctvFiveYears.setChecked(false);
			if (currentDate != ONE_MONTH) {
				showProgressDialog();
				currentDate = ONE_MONTH;
				this.drawChartView(currentMarketName);
			}
			break;
		case R.id.market_tab_six_months:
			ctvADay.setChecked(false);
			ctvAMonth.setChecked(false);
			ctvSixMonths.setChecked(true);
			ctvAYear.setChecked(false);
			ctvFiveYears.setChecked(false);
			if (currentDate != SIX_MONTHS) {
				showProgressDialog();
				currentDate = SIX_MONTHS;
				this.drawChartView(currentMarketName);
			}
			break;
		case R.id.market_tab_one_year:
			ctvADay.setChecked(false);
			ctvAMonth.setChecked(false);
			ctvSixMonths.setChecked(false);
			ctvAYear.setChecked(true);
			ctvFiveYears.setChecked(false);
			if (currentDate != ONE_YEAR) {
				showProgressDialog();
				currentDate = ONE_YEAR;
				this.drawChartView(currentMarketName);
			}
			break;
		case R.id.market_tab_five_years:
			ctvADay.setChecked(false);
			ctvAMonth.setChecked(false);
			ctvSixMonths.setChecked(false);
			ctvAYear.setChecked(false);
			ctvFiveYears.setChecked(true);
			if (currentDate != FIVE_YEARS) {
				showProgressDialog();
				currentDate = FIVE_YEARS;
				this.drawChartView(currentMarketName);
			}
			break;
		case R.id.market_trading_market_left:
			ctvTradingCodeLeft.setChecked(true);
			ctvTradingCodeRight.setChecked(false);
			if (vpFlipMain.getCurrentView().getId() == R.id.fragment_market_main_vn) {
				setUpData("HOSE-VN30)");
				drawChartView("HOSE");
			} else {
				currentMarketName = "HOSE";
				loadDataSummaryTrading(false);
				loadDataMostActive(false);
				loadDataNetBuySell(false);
			}
			loadDataMarketNews(false);
			break;
		case R.id.market_trading_market_right:
			ctvTradingCodeLeft.setChecked(false);
			ctvTradingCodeRight.setChecked(true);
			if (vpFlipMain.getCurrentView().getId() == R.id.fragment_market_main_vn) {
				setUpData("HNX30-HNX)");
				drawChartView("HNX");
			} else {
				currentMarketName = "HNX";
				loadDataSummaryTrading(false);
				loadDataMostActive(false);
				loadDataNetBuySell(false);
			}
			loadDataMarketNews(false);
			break;
		case R.id.market_trading_market_index:
			ctvTradingIndexLeft.setChecked(true);
			ctvTradingIndexRight.setChecked(false);
			vpFlipMain.setDisplayedChild(vpFlipMain.indexOfChild(vpFlipMain.findViewById(R.id.fragment_market_main_vn)));
			break;
		case R.id.market_trading_market_summary:
			ctvTradingIndexLeft.setChecked(false);
			ctvTradingIndexRight.setChecked(true);
			vpFlipMain.setDisplayedChild(vpFlipMain.indexOfChild(vpFlipMain.findViewById(R.id.fragment_market_trading_most)));
			loadDataSummaryTrading(false);
			loadDataMostActive(false);
			loadDataNetBuySell(false);
			break;
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		loadData();
		loadDataMarketNews(false);
	}

	private void setUpData(String code) {
		lnDataLayout.removeAllViews();
		if (AppController.getAppContext() == null)
			return;
		LayoutInflater inflate = LayoutInflater.from(AppController.getAppContext());

		for (final Markets item : mDataMarkets) {
			if (code.contains(item.getCode())) {
				int textColor = Utils.getStockColor(Utils.getPriceTrend(item.getIndices().get(0).getPercentage_change()));
				View itemView = inflate.inflate(R.layout.fragment_market_list_item, null);
				((TextView) itemView.findViewById(R.id.market_data_change_market)).setText(item.getName());
				((TextView) itemView.findViewById(R.id.market_data_change_index)).setText(String.valueOf(item.getIndices().get(0).getIndex()));
				((TextView) itemView.findViewById(R.id.market_data_change_index)).setTextColor(AppController.getAppContext().getResources().getColor(textColor));
				// if (item.getIndices().get(0).getPercentage_change() > 0) {
				// ((TextView) itemView
				// .findViewById(R.id.market_data_change_chg))
				// .setTextColor(Color.parseColor("#1aFF1a"));
				// }
				((TextView) itemView.findViewById(R.id.market_data_change_chg)).setText(String.valueOf(item.getIndices().get(0)
						.getPercentage_change())
						+ "%");
				((TextView) itemView.findViewById(R.id.market_data_change_chg)).setTextColor(AppController.getAppContext().getResources().getColor(textColor));
				((TextView) itemView.findViewById(R.id.market_data_change_volume)).setText(Utils.formatAsStockVolume(item.getIndices().get(0)
						.getVolume()));
				itemView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						drawChartView(item.getName());
						loadDataMarketNews(false);
					}

				});
				lnDataLayout.addView(itemView);
			}
		}
	}

	private void setUpDataNews() {
		if (mAdapterTopNews == null) {
			mAdapterTopNews = new TopNewsAdapter(AppController.getAppContext(), mDataNews);
			lnNewLayout.setAdapter(mAdapterTopNews);
		} else {
			mAdapterTopNews.notifyDataSetChanged();
		}
	}

	private void setUpDataETF() {
		if (mMarketEtfAdapter == null) {
			mMarketEtfAdapter = new MarketEtfAdapter(AppController.getAppContext(), mDataETF);
			lvListviewETF.setAdapter(mMarketEtfAdapter);
		} else {
			mMarketEtfAdapter.notifyDataSetChanged();
		}
		hideProgressDialog();
	}

	private void loadData() {
		showProgressDialog();
		// /////////// Get MarKet Main //////////////////////////
		restService.getMarketList(new IStockResponseListener<ArrayList<Markets>>() {

			@Override
			public void onResponse(ArrayList<Markets> data) {
				mDataMarkets = data;
				mHandler.sendEmptyMessage(GET_MARKET_SUCCESS);
			}

			@Override
			public void onError(Exception ex) {
				hideProgressDialog();
				VolleyLog.d("TAG", ex);
			}
		});
	}

	private void loadDataMarketNews(final boolean isLoadMore) {
		isLoadingNew = true;
		if (!isLoadMore) {
			currentNewOffset = 0;
		} else {
			currentNewOffset += LIMITLOAD;
		}
		restService.getNewsMarket(currentMarketName, String.valueOf(LIMITLOAD),
				String.valueOf(currentNewOffset),
				new IStockResponseListener<ArrayList<News>>() {

					@Override
					public void onResponse(ArrayList<News> data) {
						isLoadingNew = false;
						if (!isLoadMore) {
							mDataNews.clear();
						}
						for (News item : data) {
							mDataNews.add(item);
						}
						mHandler.sendEmptyMessage(GET_NEWS_SUCCESS);
					}

					@Override
					public void onError(Exception ex) {
						hideProgressDialog();
						ex.printStackTrace();
						isLoadingNew = false;
					}
				});
	}

	private void loadDataETF() {
		showProgressDialog();
		// ///////// Get MarKet ETF //////////////////////////
		restService.getMarketETF(new IStockResponseListener<ArrayList<ETF>>() {

			@Override
			public void onResponse(ArrayList<ETF> data) {
				mDataETF = data;
				mHandler.sendEmptyMessage(GET_MARKET_ETF);
			}

			@Override
			public void onError(Exception ex) {
				ex.printStackTrace();
				hideProgressDialog();
			}
		});
	}

	private void loadDataSummaryTrading(final boolean isLoadMore) {
		// showProgressDialog();
		// ///////// Get Trading OverView //////////////////////////
		if (!isLoadMore) {
			currentTradingOffset = 0;
		} else {
			currentTradingOffset += LIMITLOAD_TRADING;
		}
		restService.getMarketForeignTrades(currentMarketName, IStockModel
				.getInstance().getAccessToken(), String.valueOf(LIMITLOAD_TRADING), String.valueOf(currentTradingOffset),
				new IStockResponseListener<ArrayList<ForeignTrades>>() {

					@Override
					public void onResponse(ArrayList<ForeignTrades> data) {
						isLoadingTrading = false;
						if (!isLoadMore) {
							mDataForeignTrading.clear();
						}
						for (ForeignTrades newItem : data) {
							mDataForeignTrading.add(newItem);
						}
						mHandler.sendEmptyMessage(GET_MARKET_FOREIGN_TRADING);
					}

					@Override
					public void onError(Exception ex) {
						isLoadingTrading = false;
						ex.printStackTrace();
					}
				});
	}
	
	private void loadDataMostActive(final boolean isLoadMore) {
		// ///////// Get Trading Most Active Buy //////////////////////////
		if (!isLoadMore) {
			currentMostActiveOffset = 0;
		} else {
			currentMostActiveOffset += LIMITLOAD_MOSTACTIVE;
		}
		restService.getMarketTradingMostActiveBuy(currentMarketName,
				IStockModel.getInstance().getAccessToken(), String.valueOf(LIMITLOAD_MOSTACTIVE), String.valueOf(currentMostActiveOffset),
				new IStockResponseListener<ArrayList<MostActive>>() {

					@Override
					public void onResponse(ArrayList<MostActive> data) {
						isLoadingMostActive = false;
						if (!isLoadMore) {
							mDataMostAciveBuy.clear();
						}
						for (MostActive newItem : data) {
							mDataMostAciveBuy.add(newItem);
						}
						mHandler.sendEmptyMessage(GET_MARKET_MOST_ACTIVE_BUY);
					}

					@Override
					public void onError(Exception ex) {
						isLoadingMostActive = false;
						ex.printStackTrace();
					}
				});
	}

	private void loadDataNetBuySell(final boolean isLoadMore) {
		// ///////// Get Trading Most Active Sell //////////////////////////
		if (!isLoadMore) {
			currentNetBuySellOffset = 0;
		} else {
			currentNetBuySellOffset += LIMITLOAD_NETBUYSELL;
		}
		restService.getMarketTradingMostActiveSell(currentMarketName,
				IStockModel.getInstance().getAccessToken(), String.valueOf(LIMITLOAD_NETBUYSELL), String.valueOf(currentNetBuySellOffset),
				new IStockResponseListener<ArrayList<MostActive>>() {

					@Override
					public void onResponse(ArrayList<MostActive> data) {
						isLoadingNetBuySell = false;
						if (!isLoadMore) {
							mDataMostAciveSell.clear();
						}
						for (MostActive newItem : data) {
							mDataMostAciveSell.add(newItem);
						}
						mHandler.sendEmptyMessage(GET_MARKET_MOST_ACTIVE_SELL);
					}

					@Override
					public void onError(Exception ex) {
						isLoadingNetBuySell = false;
						ex.printStackTrace();
					}
				});
	}
	
	private void loadDataDailyMarket(final boolean isLoadMore) {
		// showProgressDialog();
		// ///////// Get Trading OverView //////////////////////////
		if (!isLoadMore) {
			currentDailyChartOffset = 0;
		} else {
			currentDailyChartOffset += LIMITLOAD_DAILY;
		}
		restService.getDailyMarket(String.valueOf(LIMITLOAD_TRADING), String.valueOf(currentDailyChartOffset), getMarketIDByName(currentMarketName),
				new IStockResponseListener<ArrayList<MarketChart>>() {

					@Override
					public void onResponse(ArrayList<MarketChart> data) {
						for (MarketChart item:data) {
							dailyChartData.add(item);
						}
						if (data.size() < LIMITLOAD_DAILY) {
							mHandler.sendEmptyMessage(GET_MARKET_CHART_DAILY);
						} else {
							loadDataDailyMarket(true);
						}
					}

					@Override
					public void onError(Exception ex) {
						currentDailyChartOffset = 0;
						hideProgressDialog();
						ex.printStackTrace();
					}
				});
	}
	
	private void renderChart(ArrayList<MarketChart> dataChart) {
		hideProgressDialog();
		if (dataChart != null && dataChart.size() > 0) {
			double x = -1;
			int displayDateStep = dataChart.size() / 3;
			int countStep = 0;
			for (MarketChart itemData : dataChart) {
				x++;
				double y = itemData.index;
				// add a new data point to the current series
				mCurrentSeries.add(x, y);
				// repaint the chart such as the newly added
				// point to be visible
				mChartView.repaint();
			}
			if (displayDateStep != 0) {
				for (int i = 0; i < dataChart.size(); i++) {
					if (i % displayDateStep == 0) {
						countStep++;
						mRenderer
								.addXTextLabel(i,
										Utils.convertStringtoDate(dataChart
												.get(i).updated_time));
					} else {
						if (i == dataChart.size() - 1 && countStep < 4) {
							mRenderer
									.addXTextLabel(i, Utils
											.convertStringtoDate(dataChart
													.get(i).updated_time));
						} else {
							mRenderer.addXTextLabel(i, "");
						}
					}
				}
			} else {
				for (int i = 0; i < dataChart.size(); i++) {
					mRenderer
							.addXTextLabel(
									i,
									Utils.convertStringtoDate(dataChart.get(i).updated_time));
				}
			}
			mRenderer.setXLabels(0);
		}
	}
	
	private String getMarketIDByName(String name){
		for (Markets item: mDataMarkets){
			if (item.getName().equalsIgnoreCase(currentMarketName))
				return String.valueOf(item.getId());
		}
		return String.valueOf(1);
	}

	private Handler mHandler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			if (AppController.getAppContext() == null || AppController.getAppContext() == null)
				return false;
			switch (msg.what) {
			case GET_MARKET_SUCCESS:
				hideProgressDialog();
				setUpData("HOSE-VN30)");
				drawChartView("HOSE");
				break;
			case GET_MARKET_FAIL:
				break;
			case GET_NEWS_SUCCESS:
				hideProgressDialog();
				setUpDataNews();
				break;
			case GET_MARKET_ETF:
				setUpDataETF();
				break;
			case GET_MARKET_FOREIGN_TRADING:
				if (mMarketTradingOverViewAdapter == null) {
					mMarketTradingOverViewAdapter = new MarketTradingForeignTradingAdapter(AppController.getAppContext(), mDataForeignTrading);
					lvMostTradingForeignTrading.setAdapter(mMarketTradingOverViewAdapter);
				} else {
					mMarketTradingOverViewAdapter.notifyDataSetChanged();
				}
				break;
			case GET_MARKET_MOST_ACTIVE_BUY:
				if (mMarketTradingMostActiveAdapterbuy == null) {
					mMarketTradingMostActiveAdapterbuy = new MarketTradingMostActiveAdapter(AppController.getAppContext(), mDataMostAciveBuy);
					lvMostTradingMostActive.setAdapter(mMarketTradingMostActiveAdapterbuy);
				} else {
					mMarketTradingMostActiveAdapterbuy.notifyDataSetChanged();
				}
				break;
			case GET_MARKET_MOST_ACTIVE_SELL:
				if (mMarketTradingMostActiveAdapterSell == null) {
					mMarketTradingMostActiveAdapterSell = new MarketTradingMostActiveAdapter(AppController.getAppContext(), mDataMostAciveSell);
					lvMostTradingNetBuySell.setAdapter(mMarketTradingMostActiveAdapterSell);
				} else {
					mMarketTradingMostActiveAdapterSell.notifyDataSetChanged();
				}
				break;
			case GET_MARKET_CHART_DAILY:
				// how to render chart with new format 
				renderChart(dailyChartData);
				break;
			default:
				break;
			}
			return false;
		}
	});

	public class SwipeGestureDetector extends SimpleOnGestureListener {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			try {
				// right to left swipe
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					vpFlipChartNews.setInAnimation(AnimationUtils.loadAnimation(AppController.getAppContext(), R.anim.left_in));
					vpFlipChartNews.setOutAnimation(AnimationUtils.loadAnimation(AppController.getAppContext(), R.anim.left_out));

					RadioButton dotNews = null;
					if (vpFlipChartNews.getDisplayedChild() == 0) {
						dotNews = (RadioButton) vpFlipMain.findViewById(R.id.dot_news);
					} else {
						dotNews = (RadioButton) vpFlipMain.findViewById(R.id.dot_chart);
					}
					if (dotNews != null) {
						dotNews.setChecked(true);
					}

					vpFlipChartNews.showNext();
					return true;
				} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					vpFlipChartNews.setInAnimation(AnimationUtils.loadAnimation(AppController.getAppContext(), R.anim.right_in));
					vpFlipChartNews.setOutAnimation(AnimationUtils.loadAnimation(AppController.getAppContext(), R.anim.right_out));
					RadioButton dotNews = null;
					if (vpFlipChartNews.getDisplayedChild() == 0) {
						dotNews = (RadioButton) vpFlipMain.findViewById(R.id.dot_news);
					} else {
						dotNews = (RadioButton) vpFlipMain.findViewById(R.id.dot_chart);
					}
					if (dotNews != null) {
						dotNews.setChecked(true);
					}

					vpFlipChartNews.showPrevious();
					return true;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return false;
		}
	}
}
